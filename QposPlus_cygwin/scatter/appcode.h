/*
********************************************************************************
*
*   File Name:
*       appcode.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _APPCODE_H
#define _APPCODE_H

#ifdef __cplusplus
extern "C"
{
#endif//__cplusplus

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#include "proj_cfg.h"
#include "lark_api.h"
#include "scatter.h"


/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#define SIZE_STR(x)			sizeof(#x)

#ifdef USE_CHECK_PARAM
  extern void assert_failed(pu8 file,uint  line );
  #if defined(__GNUC__) || defined(VM_WIN32)
    #define check_param(expr)           ((expr) ? ((void)0) : assert_failed((unsigned char *)__FILE__, __LINE__))
  #else
    #define check_param(expr)           ((expr) ? ((void)0) : assert_failed((unsigned char *)__MODULE__, __LINE__))
  #endif
#else
  #define check_param(expr)           ((void)0)
#endif  /* #ifdef USE_CHECK_PARAM */

#define TRACE_LEVEL_FIXED       (0x80)
#define TRACE_LEVEL_ALL         (0x0F)

/* level: 0~15 */
#define TRACE_LVL_ERROR         (1)
#define TRACE_LVL_NOTICE        (3)
#define TRACE_LVL_DEFAULT       (5)
#define TRACE_LVL_INFO          (9)
#define TRACE_LVL_DRIVER        (11)
#define TRACE_LVL_TEMP          (13)
#define TRACE_LVL_SYS           (15)


#ifdef DBG_TRACE
#define PRINT_TIME          print_time(void)

  extern u32 get_trace_level(void );
#if defined(CFG_DOCK_DEVICE) || defined(CFG_UNIVERSAL_SDK)
  #define TRACE(lvl, fmt, ...) \
    do { \
      if ((lvl <= DBG_TRACE_LVL) && (lvl <= get_trace_level())) \
      { Eprintf_(fmt, ##__VA_ARGS__); } \
    } while (0)
#else
  #define TRACE(lvl, fmt, ...) \
    do { \
      if ((lvl <= DBG_TRACE_LVL) && (lvl <= get_trace_level())) \
      { Printf_(fmt, ##__VA_ARGS__); } \
    } while (0)
#endif
  #define TRACE_VALUE(lvl, pbuf, cnt) \
    do { \
        if ((lvl <= DBG_TRACE_LVL) && (lvl <= get_trace_level())) \
        { trace_value(pbuf, cnt); } \
    } while (0)
  #define TRACE_ASCII(lvl, pbuf, cnt) \
    do { \
        if ((lvl <= DBG_TRACE_LVL) && (lvl <= get_trace_level())) \
        { trace_ascii(pbuf, cnt); } \
    } while (0)
#define TRACE_DATA(lvl, pbuf, cnt) \
    do { \
        if ((lvl <= DBG_TRACE_LVL) && (lvl <= get_trace_level())) \
        { trace_data(pbuf, cnt); } \
    } while (0)
#define TRACE_VAL(l, x, len)		\
	do {	\
		TRACE((l), #x": ");			\
		TRACE_VALUE((l), x, len);	\
	}while(0)
#define TRACE_ASC(l, x, len)		\
	do {	\
		TRACE((l), #x": ");			\
		TRACE_ASCII((l), x, len);	\
	}while(0)
#define TRACE_INFO_FULL(lvl, pbuf, cnt, fmt, preface, tail) \
    do { \
			u8 *pTrace = (u8 *)(pbuf);		\
			u32 dwTrace = (u32 )(cnt);		\
			if(SIZE_STR(preface) > 2) TRACE(lvl, "%s", (char *)preface);	\
			else if(preface) TRACE(lvl, #pbuf": ");	\
			while(pTrace && dwTrace--){ TRACE(lvl, fmt?fmt:"%02X ", *pTrace++); }	\
			if(SIZE_STR(tail) > 2) TRACE(lvl, "%s", (char *)tail);	\
			else if(tail) TRACE(lvl, "\r\n");	\
    } while(0)

#define TRACE_INFO_1_ARGS(lvl, message, size)              				TRACE_INFO_FULL(lvl, message, size, 0, 1, 1)
#define TRACE_INFO_2_ARGS(lvl, message, size, fmt)         				TRACE_INFO_FULL(lvl, message, size, fmt, 1, 1)
#define TRACE_INFO_3_ARGS(lvl, message, size, fmt, preface)   			TRACE_INFO_FULL(lvl, message, size, fmt, preface, 1)
#define TRACE_INFO_4_ARGS(lvl, message, size, fmt, preface, tail)   	TRACE_INFO_FULL(lvl, message, size, fmt, preface, tail)

#define GET_7TH_ARG(arg1, arg2, arg3, arg4, arg5, arg6, arg7, ...) arg7
#define TRACE_INFO_MACRO_CHOOSER(...) \
    GET_7TH_ARG(__VA_ARGS__, TRACE_INFO_4_ARGS, TRACE_INFO_3_ARGS, TRACE_INFO_2_ARGS, TRACE_INFO_1_ARGS, )
#define TRACE_INFO(...) TRACE_INFO_MACRO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)
#else   /* #ifdef DBG_TRACE */
  #define TRACE(lvl, fmt, ...)          ((void)0)	//(...)
  #define TRACE_VALUE(lvl, pbuf, cnt)   ((void)0)
  #define TRACE_ASCII(lvl, pbuf, cnt)   ((void)0)
  #define TRACE_DATA(lvl, pbuf, cnt)    ((void)0)
  #define TRACE_VAL(l, x, len)			((void)0)
  #define TRACE_ASC(l, x, len)			((void)0)
  #define TRACE_INFO(...)				((void)0)	//(...)
#endif  /* #ifdef DBG_TRACE */

#define  AMEX_CLESS_PROCESSING_RESTRICTION_CONTACTLESS_TRANS_NOT_ALLOWED_2      0x43108

/*-----------------------------------------------------------------------------
|   RKD feature start -- by Feng Jian, 2022-01-24
+----------------------------------------------------------------------------*/
#define SIZE_OF_KDH_ID					28
#define CA_CERT_LEN_MAX					2300
#define CA_RSA_BITS						4096
#define	RSA_E							65537
#define CA_RSA_KEY_DATA_LEN(b)     		(2 + b/8 + 2 + 4)
#define AES_BLK_SIZE 					16
#define KCV_MSG							((u8[]){0,0,0,0,0,0,0,0})
#define CHAR2LONG(c0, c1, c2, c3)		(u32)((c0)<<24|(c1)<<16|(c2)<<8|(c3))
#define CHAR2WORD(c0, c1)				(u16)((c0)<<8|(c1))
#define CHAR2BIN(c)						(((c)&0x7F) >= 'a' ? (((c)&0x7F) - 'a' + 10) \
										: ((((c)&0x7F) >= 'A') ? (((c)&0x7F) - 'A' + 10) : (((c)&0x7F) - '0')))
#define CH2_DEC(p)						(CHAR2BIN((p)[0])*10 + CHAR2BIN((p)[1]))
#define CH3_DEC(p)						(CHAR2BIN(*(p))*100 + CHAR2BIN((p)[1])*10 + CHAR2BIN((p)[2]))
#define BYTES_OF_NUMBER(x)				((x) < 0x100 ? 1 : ((x) < 0x10000 ? 2 : ((x) < 0x1000000 ? 3 : 4)))
#define U16_BYTES(w)					((u8[] ){(w)>>8, w})
#define U32_BYTES(d)					((u8[] ){LONG_HH(d), LONG_HL(d), LONG_LH(d), LONG_LL(d)})
#define U16_SZ(w)						((char[] ){(w)>>8, w, 0})
#define U16_BYTES_BUF(p, w)				do{*(p) = (w)>>8; *((p) + 1) = ((w)&0xFF);}while(0)
#define PTR2WORD(x)						(*(x)<<8|*((x) + 1))
#define PTR2DWORD(x)					(PTR2WORD(x)<<16|PTR2WORD(x + 2))
#define PTR2DWORD_3B					(PTR2WORD(x)<<16|*((x) + 2))
#define MAX_AES_KEYS					5
#define MAX_CUSTOM_KEY_GRP				10
#define MAX_IPEK_PER_GRP				3
#define MAX_MKSK_PER_GRP				4
#define CUSTOM_KEY_MKSK_LEN_MAX        	(24)
#define CUSTOM_KEY_DUKPT_KSN_LEN_MAX   	(10)
#define CUSTOM_KEY_DUKPT_KEY_LEN_MAX   	(24)
#define CUSTOM_KEY_RSA_LEN_MAX         	(264)
#define CUSTOM_KEY_TCK_LEN_MAX         	(24)
#define MAX_AES_BYTES					(32)
#define KCV_SHORT_LEN					3
#define KSI_DID_LEN						7
#define MAX_RSA_BITS 					2048
#define MAX_RSA_BYTES					(MAX_RSA_BITS/8)
#define RSA_E_BYTES 					3
#define NULL_UV							(T_U8_VIEW){NULL, 0};
#define MCMD_HEAD(wCmd, eId, tm)		(u8[] ){'M', 0, 6, eId, (wCmd)>>8, (wCmd)&0xFF, (tm), 0, 0}
#define IS_M_CMD(p)						((p)[0] == 'M' && PTR2WORD((p) + 1) == (PTR2WORD((p) + 7) + 6) && (p)[3] >= 0x20 && (p)[3] < 0x60)
#define STRINGIFY_HELPER(A) 			#A
#define STRINGIFY(...) 					STRINGIFY_HELPER(__VA_ARGS__)
#define KEY  							STRINGIFY(K)
#define HEX2NIBBLE(c) 					(*(c) >= 'A' ? (*(c) - 'A')+10 : (*(c)-'0'))
#define HEX2BYTE(c) 					(HEX2NIBBLE(c)*16 + HEX2NIBBLE(c+1))
#define MIN_LEN_TR34_MSG				(CA_RSA_BITS/8 + 96 + AES_BLK_SIZE + sizeof(T_RKD_MSG_INFO))

typedef enum
{
	RKD_BAD_TYPE	= 0,
	RKD_IPEK 		= CHAR2WORD('B', '1'),
	RKD_ZMK_TMK 	= CHAR2WORD('K', '0'),
	RKD_KBPK 		= CHAR2WORD('K', '1')
} T_RKD_TYPE;
#define RKD_TYPE_TR34HEAD(p) 	CHAR2WORD((p)[5], (p)[6])

typedef enum
{
	ANY_USAGE	= 0,
	KEY_PIN,
    KEY_DATA,
    KEY_MAC,
}T_KEY_USAGE;

#pragma pack(push)
#pragma pack(1)
typedef struct
{
	u8 arrData[SIZE_OF_KDH_ID];
	u32 dwCrc;
} T_KDH_ID;

typedef struct{
	u8 idx;
	u8 arrKcv[KCV_SHORT_LEN];
	u8 arrKsiDid[KSI_DID_LEN];
}T_RKD_MSG_INFO;

typedef struct {
	u8 byLen;
	u8 arrData[];
} T_BYTE_KEY;

typedef struct {
	u8 kcv[KCV_SHORT_LEN];
	T_BYTE_KEY key;
	u8 array[MAX_AES_BYTES];
} T_AES_KEY;

typedef struct {
	u8 arrLen[sizeof(u16)];
	u8 arrData[];
} T_WORD_KEY;

typedef struct {
		T_WORD_KEY n;
		u8 array1[MAX_RSA_BYTES];
		T_WORD_KEY e;
		u8 array2[RSA_E_BYTES];
		u8 pad;						//keep compatiable with pParam_Custom_Key_Rsa_t
		T_WORD_KEY d;
		u8 array3[MAX_RSA_BYTES];
} T_RSA_N_E_D;

typedef struct{
	u8 idx		: 4;
	u8 byUsage	: 4;
	u8 byVer;
	u8 ksn[CUSTOM_KEY_DUKPT_KSN_LEN_MAX];
	u8 kcv[KCV_SHORT_LEN];
	T_BYTE_KEY key;
	u8 array[CUSTOM_KEY_DUKPT_KEY_LEN_MAX];
} T_IPE_KEY;

typedef struct{
	u8 ksn[CUSTOM_KEY_DUKPT_KSN_LEN_MAX];
	u8 key[CUSTOM_KEY_DUKPT_KEY_LEN_MAX];
} T_PER_IPEK;

typedef struct{
	u8 len;
	T_PER_IPEK arrIpek[MAX_IPEK_PER_GRP];
} T_CUSTOM_KEY_DUKPT;	//same format with Param_Custom_Key_Dukpt_t

typedef struct{
	u8 len;
	u8 arrMksk[MAX_MKSK_PER_GRP][CUSTOM_KEY_MKSK_LEN_MAX];
} T_CUSTOM_KEY_MKSK;

typedef struct{
	u8 idx		: 4;
	u8 byUsage	: 4;
	u8 byVer;
	u8 kcv[KCV_SHORT_LEN];
	T_BYTE_KEY key;
	u8 array[CUSTOM_KEY_MKSK_LEN_MAX];
} T_MASTER_KEY;

typedef struct{
	u32 dwOffset;
	u32 dwSize;
} T_PARAM_INFO;

typedef struct{
	u16 wBytes;
	u8 	arrData[CA_RSA_BITS/8];
	u8 	arrE[BYTES_OF_NUMBER(RSA_E)];
	u32 dwCrc;
} T_RSA_KEY;

typedef struct{
	u8 byKbVer;
	u8 byAlgo;
	u8 byMode;
	u8 arrKeyVer[2];
	u8 kcv[KCV_SHORT_LEN];
} T_KEY_EXT_INFO;
#pragma pack(pop)

#define FADDR_RSA_PUB			FLASH_ADDR_CA_PRIVATE_KEY_PARAM
#define FADDR_RSA_PRI			(FADDR_RSA_PUB + sizeof(T_RSA_KEY))
#define FADDR_RSA_END			(FADDR_RSA_PRI + sizeof(T_RSA_KEY))
#define FADDR_RKD_AES_LST(i)	(FADDR_RSA_END + ((i)%MAX_AES_KEYS)*sizeof(T_AES_KEY))
#define FADDR_KDH_ID			(FADDR_RSA_END + MAX_AES_KEYS*sizeof(T_AES_KEY))
#define FADDR_TR34_KEY			FADDR_RKD_AES_LST(0)
#define FADDR_KBPK_KEY			FADDR_RKD_AES_LST(1)

typedef enum
{
    AES_CYPHER_128,
    AES_CYPHER_192,
    AES_CYPHER_256,
} AES_CYPHER_T;

extern int aes_encrypt_ecb(AES_CYPHER_T mode, unsigned char *dst,unsigned char *data, int len, unsigned char *key);
extern int aes_decrypt_ecb(AES_CYPHER_T mode, unsigned char *dst,unsigned char *data, int len, unsigned char *key);
extern int aes_encrypt_cbc(AES_CYPHER_T mode, unsigned char *dst,unsigned char *data, int len, unsigned char *key, unsigned char *iv);
extern int aes_decrypt_cbc(AES_CYPHER_T mode, unsigned char *dst,unsigned char *data, int len, unsigned char *key, unsigned char *iv);
extern T_RKD_TYPE decrypt_tr34_block(T_U8_VIEW uvTr34Data, T_RKD_MSG_INFO *pMsgInfo);
extern u32 readIpekCv(u8 arrKcvDest[MAX_IPEK_PER_GRP][KCV_SHORT_LEN], u8 byGrp);
extern u32 pack_all_kcv(T_U8_VIEW uvDest);
extern u16 read_rdt_rsa_key(T_RSA_KEY *pPubKey, T_RSA_KEY *pPriKey);
extern void gen_rsa_keypair(u16 wBits);
extern u32 make_m_cmd(u8 *buf, u16 wCmd, u8 byId, u8 bySec, T_U8_VIEW uvData);

/*-----------------------------------------------------------------------------
|   RKD feature end -- by Feng Jian, 2022-01-24
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Enumerations
+----------------------------------------------------------------------------*/
typedef enum
{
    #ifdef MQTT_ENABLE
    APP_EVENT_MQTT_CMD = BIT(9),
    #endif
    #ifdef CFG_GUI_SDK
    APP_EVENT_REGISTER_PAYMENT_LOCK = BIT(9),
    #endif
	APP_EVENT_PACKET_STATUS = BIT(10),
	APP_EVENT_CHARGING = BIT(11),
	APP_EVENT_USER_CANCEL = BIT(12),
	APP_EVENT_QUERY_CMD = BIT(13),
	APP_EVENT_RESET = BIT(14),
	APP_EVENT_TIMEOUT = BIT(15),
	APP_EVENT_USER_CONFIRM = BIT(16),
	APP_EVENT_APP2_IS_BUSY = BIT(17),
	APP_EVENT_KBD_OTHER = BIT(18),
	APP_EVENT_DESTRUCT_BUZZER = BIT(19),
	APP_EVENT_DATA_FRAME = BIT(20),
	APP_EVENT_SLEEP_MODE = BIT(21),
	APP_EVENT_NFC_PROMPT = BIT(22),
	APP_EVENT_BATT_BLINK = BIT(23),
	APP_EVENT_LOW_POWER_MODE = BIT(24),
	APP_EVENT_DESTRUCT_BUZZER_SWI = BIT(25),
	APP_EVENT_EXT_USB_STATUS = BIT(26),
	APP_EVENT_EXT_CHG_STATUS = BIT(27),
	APP_EVENT_DOCK_LOCK = BIT(28),
	APP_EVENT_DOCK_HANDSHAKE = BIT(29),
	APP_EVENT_BT_BLINK = BIT(30),
	APP_EVENT_GPOS_TEST = BIT(31),
}App_Event_t;

typedef enum
{
	APP_TIMER_ID_COM_SILENT = DEV_TIMER_ID_NUMS,
	APP_TIMER_ID_COM_STARTING,
	APP_TIMER_ID_LCD_BACKLIGHT,
	APP_TIMER_ID_QUERY_CMD,
	APP_TIMER_ID_POLL_TICK,
	APP_TIMER_ID_TIMER_TICK,
	APP_TIMER_ID_EVENTS_TIMEOUT,
	APP_TIMER_ID_DESTRUCT_BUZZER,
	APP_TIMER_ID_PROMPT,
	APP_TIMER_ID_PWR_TICK_SLEEP,
	APP_TIMER_ID_PWR_TICK_SHUTDOWN,
	APP_TIMER_ID_COM_SOC,
	APP_TIMER_ID_CHECK_TOKEN,
	APP_TIMER_ID_SIGNAL_QUERY,
	APP_TIMER_ID_LVGL_TICK,
	APP_TIMER_ID_KBD_BACKLIGHT,
	APP_TIMER_ID_FIXTIME,
	APP_TIMER_ID_DISP_SCROLL,
	APP_TIMER_ID_DISP_VIR_LED,
	APP_TIMER_ID_DISP_SOFTDESTRUCT_MSG,
	APP_TIMER_ID_AUTO_DISCONNECT_BT,
#ifdef CFG_TRADE_EFFECT_MP100
	APP_TIMER_ID_REMOVE_CARD,
#endif
}App_Timer_t;

typedef enum
{
	APP_MSG_ID_FRAME,
	APP_MSG_ID_KBD,
	APP_MSG_ID_POLL_SEC_TICK,
	APP_MSG_ID_TIMER,
    APP_MSG_ID_SDK_FRAME,
    APP_MSG_ID_LVGL_FRAME,
    #ifdef MQTT_ENABLE
    APP_MSG_ID_MQTT_EVENT,
    APP_MSG_ID_MQTT_FRAME,
    #endif
	APP_MSG_ID_NUMS,
}App_Msg_Id_t;

typedef enum
{
	ENCRYPT_MODE_START = 0x0000,

	ENCRYPT_MODE_DEFAULT = ENCRYPT_MODE_START,
	ENCRYPT_MODE_QF,
	ENCRYPT_MODE_ZZ,
	ENCRYPT_MODE_QDB,
	ENCRYPT_MODE_ZLYA,
	ENCRYPT_MODE_YX,
	ENCRYPT_MODE_HLC,
	ENCRYPT_MODE_LP,
	ENCRYPT_MODE_SXF,
	ENCRYPT_MODE_HY,
	ENCRYPT_MODE_0A,
	ENCRYPT_MODE_0B,
	ENCRYPT_MODE_0C,
	ENCRYPT_MODE_0D,
	ENCRYPT_MODE_0E,
	ENCRYPT_MODE_0F,

	ENCRYPT_MODE_JF,
	ENCRYPT_MODE_FRX,
	ENCRYPT_MODE_JCT,
	ENCRYPT_MODE_ZH,
	ENCRYPT_MODE_14,
	ENCRYPT_MODE_15,
	ENCRYPT_MODE_16,
	ENCRYPT_MODE_ZFSJ,
	ENCRYPT_MODE_MOSAMBEE,
	ENCRYPT_MODE_KLB,
	ENCRYPT_MODE_SOFTPAY =0x20,
	ENCRYPT_MODE_MYPINPAD = 0x21,
	ENCRYPT_MODE_PAYSKY = 0x22,
	ENCRYPT_MODE_PAGATODO = 0x25,

	ENCRYPT_MODE_NUMS,
}Encrypt_Mode_t;

typedef enum
{
	APP_RC_SUCCESS = RC_SUCCESS,
	APP_RC_FAIL = RC_FAIL,

	APP_RC_TIMEOUT = 0x10000001,
	APP_RC_RESET,
	APP_RC_CANCEL,
	APP_RC_COMPLETED,
	APP_RC_TERMINAL,
	APP_RC_NFC_TERMINAL,

	APP_RC_MAG_READY,
	APP_RC_ICC_READY,
	APP_RC_NFC_READY,
	APP_RC_FALL_BACK,
	APP_RC_PSAM_READY,

	APP_RC_OTHER,
	APP_RC_RESET2,
	APP_RC_EMVE_DENAIL,
	APP_RC_NFC_MULTI_CARD,
	APP_RC_EMVE_GAC2_DENAIL,
	APP_RC_EMVE_GAC2_NULL,//第二次gac失败与检查网络冲突情况，联网失败不显示cmd timeout
	APP_RC_NFC_NOT_ALLOW,//不允许交易
	APP_RC_EMV_APP_BLOCK,//不允许交易
	APP_RC_EMV_APP_SEE_PHONE,
	APP_RC_EMV_TRANS_TRY_ANOTHER_INTERFACE,
	APP_RC_EMV_TRANS_GPO_NOT_SUPPORT,
	APP_RC_EMV_REVERSAL,//冲正
	APP_RC_EMV_CARD_BLOCK,

	APP_RC_NFC_DOUBLETAP_DENAIL, // add by lyl 20211118
	APP_RC_CARD_NOT_SUPPORT,

	APP_RC_NUMS,
}App_Rc_Code_t;

typedef enum
{
	APP_PIN_TYPE_START = 0,

	APP_PIN_TYPE_ONLINE = APP_PIN_TYPE_START,
	APP_PIN_TYPE_OFFLINE,
	APP_PIN_TYPE_LAST_OFFLINE,
	APP_PIN_TYPE_MANAGE,
	APP_PIN_TYPE_REINPUT,
	APP_PIN_TYPE_INPUT_NEWPIN,
	APP_PIN_TYPE_REINPUT_NEWPIN,
	APP_PIN_TYPE_INPUT_OFFLINE_NEWPIN,
	APP_PIN_TYPE_REINPUT_OFFLINE_NEWPIN,

	APP_PIN_TYPE_NUMS,
}App_Pin_Type_t;

/*该tag类型包含了交易过程中用到的所有数据的必须的tag*/
typedef enum _pos_tag_t
{
    POS_TAG_START = 0,

    POS_TAG_HEAD = 0x7F00,
    APP_TAG_HEAD,
    POS_TAG_RES_EN_ONLINE_DATA,
    POS_TAG_RES_EN_ONLINE_RESULT,
    POS_TAG_RES_EN_BATCH_DATA,
    POS_TAG_RES_KN_ONLINE_DATA,
    POS_TAG_RES_KN_APP_DATA,

    /*custom tag*/
    POS_TAG_PSAM_ID = 0x9F8101,
    POS_TAG_PSAM_NO,
    POS_TAG_POS_ID,
    POS_TAG_MERCHANT_ID,
    POS_TAG_VENDOR_CODE,
    POS_TAG_DEVICE_NUMBER,
    POS_TAG_CSN,
    POS_TAG_KEY_GRP_SUPPORT,
    POS_TAG_KEYBOARD_SUPPORT,
    POS_TAG_BT_MAC,

    POS_TAG_BOOTLOADER_VERSION,
    POS_TAG_APP_VERSION,
    POS_TAG_HW_VERSION,
    POS_TAG_BATTERY_ADC,
    POS_TAG_BATTERY_LEVEL,
    POS_TAG_CHARGE_STATUS,
    POS_TAG_USB_STATUS,
    POS_TAG_MAG_SUPPORT,
    POS_TAG_ENCRYPTION_MODE,
    POS_WORK_KEY_STATUS,

    POS_TAG_INPUT_AMOUNT_ON_POS,
    POS_TAG_DISPLAY_AMOUNT_ON_POS,
    POS_TAG_TRADE_MODE,
    POS_TAG_CURRENCY_SYMBOL,
    POS_TAG_BUSINESS_MODE,
    POS_TAG_ADDITIONAL_DATA,
    POS_TAG_KEY_INDEX,
    POS_TAG_CUSTOM_DISPLAY,
    POS_TAG_CUSTOM_TRANS_RECORD_DATA,
    POS_TAG_1620_MAC,

    POS_TAG_FORCE_ONLINE,
    POS_TAG_RES_FORMAT_ID,
    POS_TAG_RES_CARD_STATUS,
    POS_TAG_RES_TRK1_LEN,
    POS_TAG_RES_EN_TRK1,
    POS_TAG_RES_TRK2_LEN,
    POS_TAG_RES_EN_TRK2,
    POS_TAG_RES_TRK3_LEN,
    POS_TAG_RES_EN_TRK3,
    POS_TAG_RES_PAN,

    POS_TAG_RES_ENCRYPTION_PAN,
    POS_TAG_RES_VALID_PERIOD,
    POS_TAG_RES_SERVICE_CODE,
    POS_TAG_RES_CARDHOLDER,
    POS_TAG_RES_PINBLOCK,
    POS_TAG_RES_NEW_PINBLOCK,
    POS_TAG_RES_TRK_KSN,
    POS_TAG_RES_PIN_KSN,
    POS_TAG_RES_TRK_RAND,
    POS_TAG_RES_PIN_RAND,

    POS_TAG_RES_RESPONSE,
    POS_TAG_RES_REQUEST_TYPE,
    POS_TAG_RES_SCRIPT,
    POS_TAG_RES_FORCE_APPROVE,
    POS_TAG_DIGITAL_ENVELOPE_PUK,
    POS_TAG_DIGITAL_ENVELOPE_PRK,
    POS_TAG_DIGITAL_ENVELOPE_TDK,
    POS_TAG_DIGITAL_ENVELOPE_DATA,
    POS_TAG_QF_TRADE_MODE,
    POS_TAG_1010_MAC,

    POS_TAG_HOST_TYPE,
    POS_TAG_ASC_PIN,
    POS_TAG_CLEAR_TRACK1,
    POS_TAG_CLEAR_TRACK2,
    POS_TAG_CLEAR_TRACK3,
    POS_TAG_RES_RESULT,
    POS_TAG_TRANS_TIMEOUT,
    POS_TAG_DISP_AMOUNT_POINT,
    POS_TAG_QF_RAND_ID,
    POS_TAG_MAG_CARD_TYPE,

    POS_TAG_MSG_HANDLER_TRANS_STEP_TYPE,
    POS_TAG_CUSTOM_DISPLAY_MODE,
    POS_TAG_CUSTOM_DISPLAY_STR,
    POS_TAG_GETTAG_OBLIGATE,
    POS_TAG_GETTAG_CARD_TYPE,
    POS_TAG_GETTAG_TRANS_RESULT,
    POS_TAG_GETTAG_TAG_NUMS,
    POS_TAG_GETTAG_TAG_LIST,
    POS_TAG_MOBILE,
    POS_TAG_INVOICE,

    POS_TAG_CUSTOM_DISPLAY_TYPE,//00:登录中 01:测试成功 02:GPRS 测试中 03:测试失败 04:请在背面签名 05:登录成功 06:登录失败 07:交易成功 08:交易失败 09:测试签名 0A:GO ONLINE
    POS_TAG_TRANS_RESULT,
    POS_TAG_TRANS_ACTION,//00:复位dock请求
    POS_TAG_CUSTOM_REQUEST_HOST,
    POS_TAG_CUSTOM_REQUEST_FRAME,
    POS_TAG_CUSTOM_RESPONSE_FRAME,
    POS_TAG_RRN,
    POS_TAG_DISIGN_TYPE,
    POS_TAG_REFNUM,
    POS_TAG_PAY_NAME,

    POS_TAG_CHQ_NAME,
    POS_TAG_CHQ_NUM,
    POS_TAG_HISTORY_ID,
    POS_TAG_SERVER_PAN,
    POS_TAG_TXN_ID,
    POS_TAG_IMEI,
    POS_TAG_CARD_RANGE,
    POS_TAG_SIGNATUREKEY,
    POS_TAG_NEWPIN,
    POS_TAG_CONFIRMPIN,
    POS_TAG_FALL_BACK,

    POS_TAG_RES_EN_ACCOUNT_TYPE,
    POS_TAG_DOCK_HARDWARE_VERSION,
    POS_TAG_DOCK_FIRMWARE_VERSION,
    POS_TAG_POS_RSA_D,
    POS_TAG_POS_RSA_N,
    POS_TAG_POS_RSA_E,
    POS_TAG_TRANSACTION_PASSWORD_MIN,
    POS_TAG_TRANSACTION_PASSWORD_MAX,
    POS_TAG_TRANSACTION_CONTACTLESS_MODE,
    POS_TAG_UPDATE_KEY,
    POS_TAG_ASC_NEW_PIN,
    POS_TAG_SALT_MD5,
    POS_TAG_RESPONSE_CODE,
    POS_TAG_FORCE_INPUTPIN,
    POS_TAG_CVV_CODE,
    POS_TAG_AMOUNT_TIPS_NUM,
    POS_TAG_TRADE_MODE_NO_SELECT,
    POS_TAG_EMV_OFFLINE_TIMES,
	POS_TAG_RET,
    APP_TAG_START = 0x9F8301,

    KERNEL_TAG_AID_LIST_TERMINAL                  = 0x9F8110,
    KERNEL_TAG_AID_LIST_CANDIDATE                 = 0x9F8111,
    KERNEL_TAG_AID_LIST_CANDIDATE_COUNTER         = 0x9F8112,
    KERNEL_TAG_AID_SELECT_RESULT_INDEX            = 0x9F8113,
    KERNEL_TAG_AID_LIST_DISPLAYNAME               = 0x9F8114,
    KERNEL_TAG_AID_SELECT_TRYAGAIN                = 0x9F8115,

	/*standard tag*/
	TAG_EMV_ISSUER_IDENTIFICATION_NB              =  0x42,			/*!< EMV - Issuer Identification Number (IIN).<br>The number that identifies the major industry and the card issuer and that forms the first part of the Primary Account Number (PAN).<br>	- Format : n 6.<br>	- Length : 3 bytes.<br>	- Source : Card. */
	TAG_EMV_AID_CARD                              =  0x4F,			/*!< EMV - Application Identifier (AID) - card.<br>Identifies the application as described in <i>ISO/IEC 7816-5</i>.<br>	- Format : b.<br>	- Length : 5-16 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLICATION_LABEL                     =  0x50,			/*!< EMV - Application label.<br>Mnemonic associated with the AID according to <i>ISO/IEC 7816-5</i>.<br>	- Format : ans with the special character limited to space.<br>	- Length : 1-16 bytes.<br>	- Source : Card. */
	TAG_EMV_TRACK_2_EQU_DATA                      =  0x57,			/*!< EMV - Track 2 Equivalent Data.<br>Contains the data elements of track 2 according to ISO/IEC 7813, excluding start sentinel, end sentinel, and Longitudinal Redundancy Check (LRC).<br>	- Format : b.<br>	- Length : var. up to 19 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_PAN                             =  0x5A,			/*!< EMV - Application Primary Account Number (PAN).<br>Valid cardholder account number.<br>	- Format : cn var. up to 19 digits..<br>	- Length : var. up to 10 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_TEMPLATE                        =  0x61,			/*!< EMV - Application Template.<br>Contains one or more data objects relevant to an application directory entry according to <i>ISO/IEC 7816-5</i>.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_FCI_TEMPLATE                          =  0x6F,			/*!< EMV - File Control Information (FCI) Template.<br>Identifies the FCI template according to <i>ISO/IEC 7816-4</i>.<br>	- Format : var.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_READ_RECORD_RESP_MESSAGE              =  0x70,			/*!< EMV - READ RECORD Response Message.<br>Contains the contents of the record read. (Mandatory for SFIs 1-10. Response messages for SFIs 11-30 are outside the scope of EMV, but may use template '70').<br>	- Format : var.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_SCRIPT_TEMPLATE_1              =  0x71,			/*!< EMV - Issuer Script Template 1.<br>Contains proprietary issuer data for transmission to the ICC before the second GENERATE AC command.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Issuer. */
	TAG_EMV_ISSUER_SCRIPT_TEMPLATE_2              =  0x72,			/*!< EMV - Issuer Script Template 2.<br>Contains proprietary issuer data for transmission to the ICC after the second GENERATE AC command.<br>	- Format : var.<br>	- Length : var.<br>	- Source : Issuer. */
	TAG_EMV_DICTIONARY_DISCR_TEMPLATE             =  0x73,			/*!< EMV - Directory Discretionary Template.<br>Issuer discretionary part of the directory according to <i>ISO/IEC 7816-5</i>.<br>	- Format : var.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_2    =  0x77,			/*!< EMV - Response Message Template Format 2.<br>Contains the data objects (with tags and lengths) returned by the ICC in response to a command.<br>	- Format : var.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_RESPONSE_MESSAGE_TEMPLATE_FORMAT_1    =  0x80,			/*!< EMV - Response Message Template Format 1.<br>Contains the data objects (without tags and lengths) returned by the ICC in response to a command.<br>	- Format : var.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_AMOUNT_AUTH_BIN                       =  0x81,			/*!< EMV - Amount, Authorised (Binary).<br>Authorised amount of the transaction (excluding adjustments).<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_AIP                                   =  0x82,			/*!< EMV - Application Interchange Profile.<br>Indicates the capabilities of the card to support specific functions in the application.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_COMMAND_TEMPLATE                      =  0x83,			/*!< EMV - Command Template.<br>Identifies the data field of a command message.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Terminal. */
	TAG_EMV_DF_NAME                               =  0x84,			/*!< EMV - Dedicated File (DF) Name.<br>Identifies the name of the DF as described in <i>ISO/IEC 7816-4</i>.<br>	- Format : b.<br>	- Length : 5-16 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_SCRIPT_COMMAND                 =  0x86,			/*!< EMV - Issuer Script Command.<br>Contains a command for transmission to the ICC.<br>	- Format : b.<br>	- Length : var. up to 261 bytes.<br>	- Source : Issuer. */
	TAG_EMV_APPLI_PRIORITY_INDICATOR              =  0x87,			/*!< EMV - Application Priority Indicator.<br>Indicates the priority of a given application or group of applications in a directory.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_SFI                                   =  0x88,			/*!< EMV - Short File Identifier (SFI).<br>Identifies the AEF referenced in commands related to a given ADF or DDF. It is a binary data object having a value in the range 1 to 30 and with the three high order bits set to zero.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_AUTHORISATION_CODE                    =  0x89,			/*!< EMV - Authorisation Code.<br>Value generated by the authorisation authority for an approved transaction.<br>	- Format : As defined by the Payment Systems.<br>	- Length : 6 bytes.<br>	- Source : Issuer. */
	TAG_EMV_AUTHORISATION_RESPONSE_CODE           =  0x8A,			/*!< EMV - Authorisation Response Code.<br>Code that defines the disposition of a message.<br>	- Format : an 2.<br>	- Length : 2 bytes.<br>	- Source : Issuer/Terminal. */
	TAG_EMV_CDOL_1                                =  0x8C,			/*!< EMV - Card Risk Management Data Object List 1 (CDOL1).<br>List of data objects (tag and length) to be passed to the ICC in the first GENERATE AC command.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_CDOL_2                                =  0x8D,			/*!< EMV - Card Risk Management Data Object List 2 (CDOL2).<br>List of data objects (tag and length) to be passed to the ICC in the second GENERATE AC command.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_CVM_LIST_CARD                         =  0x8E,			/*!< EMV - Cardholder Verification Method (CVM) List.<br>Identifies a method of verification of the cardholder supported by the application.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_CA_PUBLIC_KEY_INDEX_CARD              =  0x8F,			/*!< EMV - Certification Authority Public Key Index.<br>Identifies the certification authority憇 public key in conjunction with the RID.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_ISSUER_PUBLIC_KEY_CERTIFICATE         =  0x90,			/*!< EMV - Issuer Public Key Certificate.<br>Issuer public key certified by a certification authority.<br>	- Format : b.<br>	- Length : NCA.<br>	- Source : Card. */
	TAG_EMV_ISSUER_AUTHENTICATION_DATA            =  0x91,			/*!< EMV - Issuer Authentication Data.<br>Data sent to the ICC for online issuer authentication.<br>	- Format : b.<br>	- Length : 8-16 bytes.<br>	- Source : Issuer. */
	TAG_EMV_ISSUER_PUBLIC_KEY_REMAINDER           =  0x92,			/*!< EMV - Issuer Public Key Remainder.<br>Remaining digits of the Issuer Public Key Modulus.<br>	- Format : b.<br>	- Length : NI - NCA + 36.<br>	- Source : Card. */
	TAG_EMV_SIGNED_STATIC_APPLI_DATA              =  0x93,			/*!< EMV - Signed Static Application Data.<br>Digital signature on critical application parameters for SDA.<br>	- Format : b.<br>	- Length : NI.<br>	- Source : Card. */
	TAG_EMV_AFL                                   =  0x94,			/*!< EMV - Application File Locator (AFL).<br>Indicates the location (SFI, range of records) of the AEFs related to a given application.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_TVR                                   =  0x95,			/*!< EMV - Terminal Verification Results.<br>Status of the different functions as seen from the terminal.<br>	- Format : b.<br>	- Length : 5 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TDOL                                  =  0x97,			/*!< EMV - Transaction Certificate Data Object List (TDOL).<br>List of data objects (tag and length) to be used by the terminal in generating the TC Hash Value.<br>	- Format : b.<br>	- Length : var. up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_TC_HASH_VALUE                         =  0x98,			/*!< EMV - Transaction Certificate (TC) Hash Value.<br>Result of a hash function specified in <i>EMV 4.2 Book 2, Annex B3.1</i>.<br>	- Format : b.<br>	- Length : 20 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TRANSACTION_PIN_DATA                  =  0x99,			/*!< EMV - Transaction Personal Identification Number (PIN) Data.<br>Data entered by the cardholder for the purpose of the PIN verification.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Terminal. */
	TAG_EMV_TRANSACTION_DATE                      =  0x9A,			/*!< EMV - Transaction Date.<br>Local date that the transaction was authorised.<br>	- Format : n 6 (YYMMDD).<br>	- Length : 3 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TSI                                   =  0x9B,			/*!< EMV - Transaction Status Information.<br>Indicates the functions performed in a transaction.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TRANSACTION_TYPE                      =  0x9C,			/*!< EMV - Transaction Type.<br>Indicates the type of financial transaction, represented by the first two digits of the <i>ISO 8583:1987</i> Processing Code. The actual values to be used for the Transaction Type data element are defined by the relevant payment system.<br>	- Format : n 2.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_DDF_NAME                              =  0x9D,			/*!< EMV - Directory Definition File (DDF) Name.<br>Identifies the name of a DF associated with a directory.<br>	- Format : b.<br>	- Length : 5-16 bytes.<br>	- Source : Card. */
	TAG_EMV_FCI_PROPRIETARY_TEMPLATE              =  0xA5,			/*!< EMV - File Control Information (FCI) Proprietary Template.<br>Identifies the data object proprietary to this specification in the FCI template according to <i>ISO/IEC 7816-4</i>.<br>	- Format : var.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_CARDHOLDER_NAME                       =  0x5F20,			/*!< EMV - Cardholder Name.<br>Indicates cardholder name according to <i>ISO 7813</i>.<br>	- Format : ans 2-26.<br>	- Length : 2-26 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_EXPIRATION_DATE                 =  0x5F24,			/*!< EMV - Application Expiration Date.<br>Date after which application expires.<br>	- Format : n 6 (YYMMDD).<br>	- Length : 3 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_EFFECTIVE_DATE                  =  0x5F25,			/*!< EMV - Application Effective Date.<br>Date from which the application may be used.<br>	- Format : n 6 (YYMMDD).<br>	- Length : 3 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_COUNTRY_CODE                   =  0x5F28,			/*!< EMV - Issuer Country Code.<br>Indicates the country of the issuer according to <i>ISO 3166</i><br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_TRANSACTION_CURRENCY_CODE             =  0x5F2A,			/*!< EMV - Transaction Currency Code.<br>Indicates the currency code of the transaction according to <i>ISO 4217</i>.<br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_LANGUAGE_PREFERENCE                   =  0x5F2D,			/*!< EMV - Language Preference.<br>1-4 languages stored in order of preference, each represented by 2 alphabetical characters according to <i>ISO 639</i><br>Note: EMVCo strongly recommends that cards be personalised with data element '5F2D' coded in lowercase, but that terminals accept the data element whether it is coded in upper or lower case.<br>	- Format : an 2.<br>	- Length : 2-8 bytes.<br>	- Source : Card. */
	TAG_EMV_SERVICE_CODE                          =  0x5F30,			/*!< EMV - Service Code.<br>Service code as defined in <i>ISO/IEC 7813</i> for track 1 and track 2.<br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_PAN_SEQUENCE_NUMBER             =  0x5F34,			/*!< EMV - Application Primary Account Number (PAN) Sequence Counter.<br>Identifies and differentiates cards with the same PAN.<br>	- Format : n 2.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_TRANSACTION_CURRENCY_EXPONENT         =  0x5F36,			/*!< EMV - Tranasction Currency Exponent.<br>Indicates the implied position of the decimal point from the right of the transaction amount represented according to <i>ISO 4217</i>.<br>	- Format : n 1.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_ISSUER_URL                            =  0x5F50,			/*!< EMV - Issuer URL.<br>The URL provides the location of the Issuer憇 Library Server on the Internet.<br>	- Format : ans.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_IBAN                                  =  0x5F53,			/*!< EMV - International Bank Account Number (IBAN).<br>Uniquely identifies the account of a customer at a financial institution as defined in <i>ISO 13616</i>.<br>	- Format : var.<br>	- Length : var. up to 34 bytes.<br>	- Source : Card. */
	TAG_EMV_BANK_IDENTIFIER_CODE                  =  0x5F54,			/*!< EMV - Bank Identifier Code (BIC).<br>Uniquely identifies a bank as defined in <i>ISO 9362</i>.<br>	- Format : var.<br>	- Length : 8 or 11 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_COUNTRY_CODE_A2_FORMAT         =  0x5F55,			/*!< EMV - Issuer Country Code (alpha2 format).<br>Indicates the country of the issuer as defined in <i>ISO 3166</i> (using a 2 character alphabetic code).<br>	- Format : a 2.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_COUNTRY_CODE_A3_FORMAT         =  0x5F56,			/*!< EMV - Issuer Country Code (alpha3 format).<br>Indicates the country of the issuer as defined in <i>ISO 3166</i> (using a 3 character alphabetic code).<br>	- Format : a 3.<br>	- Length : 3 bytes.<br>	- Source : Card. */
	TAG_EMV_ACCOUNT_TYPE                          =  0x5F57,			/*!< EMV - Account Type.<br>Indicates the type of account selected on the terminal, coded as specified in <i>EMV 4.2 Book 3, Annex G</i>.<br>	- Format : n 2.<br>Length : 1 byte.<br>Source : Terminal. */
	TAG_EMV_ACQUIRER_IDENTIFIER                   =  0x9F01,			/*!< EMV - Acquirer Identifier.<br>Uniquely identifies the acquirer within each payment system.<br>	- Format : n 6-11.<br>Length : 6 bytes.<br>Source : Terminal. */
	TAG_EMV_AMOUNT_AUTH_NUM                       =  0x9F02,			/*!< EMV - Amount, Authorised (Numeric).<br>Authorised amount of the transaction (excluding adjustments).<br>	- Format : n 12.<br>	- Length : 6 bytes.<br>	- Source : Terminal. */
	TAG_EMV_AMOUNT_OTHER_NUM                      =  0x9F03,			/*!< EMV - Amount, Other (Numeric).<br>Secondary amount associated with the transaction representing a cashback amount.<br>	- Format : n 12.<br>	- Length : 6 bytes.<br>	- Source : Terminal. */
	TAG_EMV_AMOUNT_OTHER_BIN                      =  0x9F04,			/*!< EMV - Amount, Other (Binary).<br>Secondary amount associated with the transaction representing a cashback amount.<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_APPLI_DISCRET_DATA                    =  0x9F05,			/*!< EMV - Application Discretionary Data.<br>Issuer or payment system specified data relating to the application.<br>	- Format : b.<br>	- Length : 1-32 bytes.<br>	- Source : Card. */
	TAG_EMV_AID_TERMINAL                          =  0x9F06,			/*!< EMV - Application Identifier (AID) - terminal.<br>Identifies the application as described in <i>ISO/IEC 7816-5</i>.<br>	- Format : b.<br>	- Length : 5-16 bytes.<br>	- Source : Terminal. */
	TAG_EMV_APPLI_USAGE_CONTROL                   =  0x9F07,			/*!< EMV - Application Usage Control.<br>Indicates issuer憇 specified restrictions on the geographic usage and services allowed for the application.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_VERSION_NUMBER_CARD             =  0x9F08,			/*!< EMV - Application Version Number - card.<br>Version number assigned by the payment system for the application.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_VERSION_NUMBER_TERM             =  0x9F09,			/*!< EMV - Application Version Number - terminal.<br>Version number assigned by the payment system for the application.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_CARDHOLDER_NAME_EXTENDED              =  0x9F0B,			/*!< EMV - Cardholder Name Extended.<br>Indicates the whole cardholder name when greater than 26 characters using the same coding convention as in <i>ISO 7813</i>.<br>	- Format : ans 27-45.<br>	- Length : 27-45 bytes.<br>	- Source : Card. */
	TAG_EMV_IAC_DEFAULT                           =  0x9F0D,			/*!< EMV - Issuer Action Code - Default.<br>Specifies the issuer憇 conditions that cause a transaction to be rejected if it might have been approved online, but the terminal is unable to process the transaction online.<br>	- Format : b.<br>	- Length : 5 bytes.<br>	- Source : Card. */
	TAG_EMV_IAC_DENIAL                            =  0x9F0E,			/*!< EMV - Issuer Action Code - Denial.<br>Specifies the issuer憇 conditions that cause the denial of a transaction without attempt to go online.<br>	- Format : b.<br>	- Length : 5 bytes.<br>	- Source : Card. */
	TAG_EMV_IAC_ONLINE                            =  0x9F0F,			/*!< EMV - Issuer Action Code - Online.<br>Specifies the issuer憇 conditions that cause a transaction to be transmitted online.<br>	- Format : b.<br>	- Length : 5 bytes.<br>	- Source : Card */
	TAG_EMV_ISSUER_APPLI_DATA                     =  0x9F10,			/*!< EMV - Issuer Application Data.<br>Contains proprietary application data for transmission to the issuer in an online transaction. Note: For CCD-compliant applications, Annex C, section C7 defines the specific coding of the Issuer Application Data (IAD). To avoid potential conflicts with CCD-compliant applications, it is strongly recommended that the IAD data element in an application that is not CCD-compliant should not use the coding for a CCD-compliant application.<br>	- Format : b.<br>	- Length : var. up to 32 bytes.<br>	- Source : Card. */
	TAG_EMV_ISSUER_CODE_TABLE_INDEX               =  0x9F11,			/*!< EMV - Issuer Code Table Index.<br>Indicates the code table according to <i>ISO/IEC 8859</i> for displaying the Application Preferred Name.<br>	- Format : n 2.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_APPLI_PREFERED_NAME                   =  0x9F12,			/*!< EMV - Application Prefered Name.<br>Preferred mnemonic associated with the AID.<br>	- Format : ans.<br>	- Length : 1-16 bytes.<br>	- Source : Card. */
	TAG_EMV_LAST_ONLINE_ATC_REGISTER              =  0x9F13,			/*!< EMV - Last Online Application Transaction Counter (ATC) Register.<br>ATC value of the last transaction that went online.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_LOWER_CONSECUTIVE_OFFLINE_LIMIT       =  0x9F14,			/*!< EMV - Lower Consecutive Offline Limit.<br>Issuer-specified preference for the maximum number of consecutive offline transactions for this ICC application allowed in a terminal with online capability.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_MERCHANT_CATEGORY_CODE                =  0x9F15,			/*!< EMV - Merchant Category Code.<br>Classifies the type of business being done by the merchant, represented according to <i>ISO 8583:1993</i> for Card Acceptor Business Code.<br>	- Format : n 4.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_MERCHANT_IDENTIFIER                   =  0x9F16,			/*!< EMV - Merchant Identifier.<br>When concatenated with the Acquirer Identifier, uniquely identifies a given merchant.<br>	- Format : ans 15.<br>	- Length : 15 bytes.<br>	- Source : Terminal. */
	TAG_EMV_PIN_TRY_COUNTER                       =  0x9F17,			/*!< EMV - Personal Identification Number (PIN) Try Counter.<br>Number of PIN tries remaining.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_ISSUER_SCRIPT_IDENTIFIER              =  0x9F18,			/*!< EMV - Issuer Script Identifier.<br>Identification of the Issuer Script.<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Issuer. */
	TAG_EMV_TERMINAL_COUNTRY_CODE                 =  0x9F1A,			/*!< EMV - Terminal Country Code.<br>Indicates the country of the terminal, represented according to <i>ISO 3166</i>.<br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TERMINAL_FLOOR_LIMIT                  =  0x9F1B,			/*!< EMV - Terminal Floor Limit.<br>Indicates the floor limit in the terminal in conjunction with the AID.<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TERMINAL_IDENTIFICATION               =  0x9F1C,			/*!< EMV - Terminal Identification.<br>Designates the unique location of a terminal at a merchant.<br>	- Format : an 8.<br>	- Length : 8 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TERMINAL_RISK_MANAGEMENT_DATA         =  0x9F1D,			/*!< EMV - Terminal Risk Management Data.<br>Application-specific value used by the card for risk management purposes.<br>	- Format : b.<br>	- Length : 1-8 bytes.<br>	- Source : Terminal. */
	TAG_EMV_IFD_SERIAL_NUMBER                     =  0x9F1E,			/*!< EMV - Interface Device (IFD) Serial Number.<br>Unique and permanent serial number assigned to the IFD by the manufacturer.<br>	- Format : an 8.<br>	- Length : 8 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TRACK_1_DISCRET_DATA                  =  0x9F1F,			/*!< EMV - Track 1 Discretionary Data.<br>Discretionary part of track 1 according to <i>ISO/IEC 7813</i>.<br>	- Format : ans.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_TRACK_2_DISCRET_DATA                  =  0x9F20,			/*!< EMV - Track 2 Discretionary Data.<br>Discretionary part of track 2 according to <i>ISO/IEC 7813</i>.<br>	- Format : cn.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_TRANSACTION_TIME                      =  0x9F21,			/*!< EMV - Transaction Time.<br>Local time that the transaction was authorised.<br>	- Format : n 6 (HHMMSS).<br>	- Length : 3 bytes.<br>	- Source : Terminal. */
	TAG_EMV_CA_PUBLIC_KEY_INDEX_TERM              =  0x9F22,			/*!< EMV - Certification Authority Public Key Index.<br>Identifies the certification authority憇 public key in conjunction with the RID.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_UPPER_CONSECUTIVE_OFFLINE_LIMIT       =  0x9F23,			/*!< EMV - Upper Consecutive Offline Limit.<br>Issuer-specified preference for the maximum number of consecutive offline transactions for this ICC application allowed in a terminal without online capability.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_APPLICATION_CRYPTOGRAM                =  0x9F26,			/*!< EMV - Application Cryptogram.<br>Cryptogram returned by the ICC in response of the GENERATE AC command.<br>	- Format : b.<br>	- Length : 8 bytes.<br>	- Source : Card. */
	TAG_EMV_CRYPTOGRAM_INFO_DATA                  =  0x9F27,			/*!< EMV - Cryptogram Information Data.<br>Indicates the type of cryptogram and the actions to be performed by the terminal.<br>	- Format : b.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_ICC_PIN_ENCIPH_PK_CERTIFICATE         =  0x9F2D,			/*!< EMV - ICC PIN Encipherment Public Key Certificate.<br>ICC PIN Encipherment Public Key certified by the issuer.<br>	- Format : b.<br>	- Length : NI.<br>	- Source : Card. */
	TAG_EMV_ICC_PIN_ENCIPH_PK_EXPONENT            =  0x9F2E,			/*!< EMV - ICC PIN Encipherment Public Key Exponent.<br>ICC PIN Encipherment Public Key Exponent used for PIN encipherment.<br>	- Format : b.<br>	- Length : 1 or 3 bytes.<br>	- Source : Card. */
	TAG_EMV_ICC_PIN_ENCIPH_PK_REMAINDER           =  0x9F2F,			/*!< EMV - ICC PIN Encipherment Public Key Remainder.<br>Remaining digits of the ICC PIN Encipherment Public Key Modulus.<br>	- Format : b.<br>	- Length : NPE - NI + 42.<br>	- Source : Card. */
	TAG_EMV_ISSUER_PK_EXPONENT                    =  0x9F32,			/*!< EMV - Issuer Public Key Exponent.<br>Issuer public key exponent used for the verification of the Signed Static Application Data and the ICC Public Key Certificate.<br>	- Format : b.<br>	- Length : 1 to 3 bytes.<br>	- Source : Card. */
	TAG_EMV_TERMINAL_CAPABILITIES                 =  0x9F33,			/*!< EMV - Terminal Capabilities.<br>Indicates the card data input, CVM, and security capabilities of the terminal.<br>	- Format : b.<br>	- Length : 3 bytes.<br>	- Source : Terminal. */
	TAG_EMV_CVM_RESULTS                           =  0x9F34,			/*!< EMV - Cardholder Verification Method (CVM) Results.<br>Indicates the results of the last CVM performed.<br>	- Format : b.<br>	- Length : 3 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TERMINAL_TYPE                         =  0x9F35,			/*!< EMV - Terminal Type.<br>Indicates the environment of the terminal, its communications capability, and its operational control. Refer to \ref EmvLibTerminalTypes about possible values.<br>	- Format : n 2.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_ATC	                                  =  0x9F36,			/*!< EMV - Application Transaction Counter (ATC).<br>Counter maintained by the application in the ICC (incrementing the ATC is managed by the ICC).<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_UNPREDICTABLE_NUMBER                  =  0x9F37,			/*!< EMV - Unpredictable Number.<br>Value to provide variability and uniqueness to the generation of a cryptogram.<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_PDOL                                  =  0x9F38,			/*!< EMV - Processing Options Data Object List (PDOL).<br>Contains a list of terminal resident data objects (tags and lengths) needed by the ICC in processing the GET PROCESSING OPTIONS command.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_POS_ENTRY_MODE                        =  0x9F39,			/*!< EMV - Point Of Service (POS) Entry Mode.<br>Indicates the method by which the PAN was entered, according to the first two digits of the <i>ISO 8583:1987</i> POS Entry Mode.<br>	- Format : n 2.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_AMOUNT_REF_CURRENCY                   =  0x9F3A,			/*!< EMV - Amount, Reference Currency.<br>Authorised amount expressed in the reference currency.<br>	- Format : b.<br>	- Length : 4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_APPLI_REF_CURRENCY                    =  0x9F3B,			/*!< EMV - Application Reference Currency.<br>1-4 currency codes used between the terminal and the ICC when the Transaction Currency Code is different from the Application Currency Code; each code is 3 digits according to <i>ISO 4217</i>.<br>	- Format : n 3.<br>	- Length : 2-8 bytes.<br>	- Source : Card. */
	TAG_EMV_TRANSACTION_REF_CURRENCY_CODE         =  0x9F3C,			/*!< EMV - Transaction Reference Currency Code.<br>Code defining the common currency used by the terminal in case the Transaction Currency Code is different from the Application Currency Code.<br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TRANSACTION_REF_CURRENCY_EXPONENT     =  0x9F3D,			/*!< EMV - Transaction Reference Currency Exponent.<br>Indicates the implied position of the decimal point from the right of the transaction amount, with the Transaction Reference Currency Code represented according to <i>ISO 4217</i>.<br>	- Format : n 1.<br>	- Length : 1 byte.<br>	- Source : Terminal. */
	TAG_EMV_ADD_TERMINAL_CAPABILITIES             =  0x9F40,			/*!< EMV - Additional Terminal Capabilities.<br>Indicates the data input and output capabilities of the terminal.<br>	- Format : b.<br>	- Length : 5 bytes.<br>	- Source : Terminal. */
	TAG_EMV_TRANSACTION_SEQUENCE_COUNTER          =  0x9F41,			/*!< EMV - Transaction Sequence Counter.<br>Counter maintained by the terminal that is incremented by one for each transaction.<br>	- Format : n 4-8.<br>	- Length : 2-4 bytes.<br>	- Source : Terminal. */
	TAG_EMV_APPLI_CURRENCY_CODE                   =  0x9F42,			/*!< EMV - Application Currency Code.<br>Indicates the currency in which the account is managed according to <i>ISO 4217</i>.<br>	- Format : n 3.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_REF_CURRENCY_EXPONENT           =  0x9F43,			/*!< EMV - Application Reference Currency Code.<br>Indicates the implied position of the decimal point from the right of the amount, for each of the 1-4 reference currencies represented according to <i>ISO 4217</i>.<br>	- Format : n 1.<br>	- Length : 1-4 bytes.<br>	- Source : Card. */
	TAG_EMV_APPLI_CURRENCY_EXPONENT               =  0x9F44,			/*!< EMV - Application Currency Exponent.<br>Indicates the implied position of the decimal point from the right of the amount represented according to <i>ISO 4217</i>.<br>	- Format : n 1.<br>	- Length : 1 byte.<br>	- Source : Card. */
	TAG_EMV_DATA_AUTHENTICATION_CODE              =  0x9F45,			/*!< EMV - Data Authentication Code.<br>An issuer assigned value that is retained by the terminal during the verification process of the Signed Static Application Data.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_ICC_PK_CERTIFICATE                    =  0x9F46,			/*!< EMV - ICC Public Key Certificate.<br>ICC Public Key certified by the issuer.<br>	- Format : b.<br>	- Length : NI.<br>	- Source : Card. */
	TAG_EMV_ICC_PK_EXPONENT                       =  0x9F47,			/*!< EMV - ICC Public Key Exponent.<br>ICC Public Key Exponent used for the verification of the Signed Dynamic Application Data.<br>	- Format : b.<br>	- Length : 1 to 3 bytes.<br>	- Source : Card. */
	TAG_EMV_ICC_PK_REMAINDER                      =  0x9F48,			/*!< EMV - ICC Public Key Remainder.<br>Remaining digits of the ICC Public Key Modulus.<br>	- Format : b.<br>	- Length : NIC - NI + 42.<br>	- Source : Card. */
	TAG_EMV_DDOL                                  =  0x9F49,			/*!< EMV - Dynamic Data Authentication Data Object List (DDOL).<br>List of data objects (tag and length) to be passed to the ICC in the INTERNAL AUTHENTICATE command.<br>	- Format : b.<br>	- Length : up to 252 bytes.<br>	- Source : Card. */
	TAG_EMV_SDA_TAG_LIST                          =  0x9F4A,			/*!< EMV - Static Data Authentication Tag List.<br>List of tags of primitive data objects defined in this specification whose value fields are to be included in the Signed Static or Dynamic Application Data.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_SIGNED_DYNAMIC_APPLI_DATA             =  0x9F4B,			/*!< EMV - Signed Dynamic Application Data.<br>Digital signature on critical application parameters for DDA or CDA.<br>	- Format : b.<br>	- Length : NIC.<br>	- Source : Card. */
	TAG_EMV_ICC_DYNAMIC_NUMBER                    =  0x9F4C,			/*!< EMV - ICC Dynamic Number.<br>Time-variant number generated by the ICC, to be captured by the terminal.<br>	- Format : b.<br>	- Length : 2-8 bytes.<br>	- Source : Card. */
	TAG_EMV_LOG_ENTRY                             =  0x9F4D,			/*!< EMV - Log Entry.<br>Provides the SFI of the Transaction Log file and its number of records.<br>	- Format : b.<br>	- Length : 2 bytes.<br>	- Source : Card. */
	TAG_EMV_MERCHANT_NAME_AND_LOCATION            =  0x9F4E,			/*!< EMV - Merchant Name and Location.<br>Indicates the name and location of the merchant.<br>	- Format : ans.<br>	- Length : var.<br>	- Source : Terminal. */
	TAG_EMV_LOG_FORMAT                            =  0x9F4F,			/*!< EMV - Log Format.<br>List (in tag and length format) of data objects representing the logged data elements that are passed to the terminal when a transaction log record is read.<br>	- Format : b.<br>	- Length : var.<br>	- Source : Card. */
	TAG_EMV_FCI_ISSUER_DISCRET_DATA               =  0xBF0C,			/*!< EMV - File Control Information (FCI) Issuer Discretionary Data.<br>Issuer discretionary part of the FCI.<br>	- Format : var.<br>	- Length : var. up to 222 bytes.<br>	- Source : Card. */
	TAG_EMV_MASTERCARD_TRANSACTION_CATEGORY_CODE  =  0x9F53,            /*!<EMV Mastercard Merchant category code*/

	TAG_PBOC_CVM_IDCARD_VERIFY_IDNUMBER           =  0x9F61,            //ID Number
	TAG_PBOC_CVM_IDCARD_VERIFY_IDTYPE             =  0x9F62,            //ID Type
	TAG_PBOC_EC_TERMINAL_SUPPORT_INDICATOR        =  0x9F7A,            //电子现金终端支持指示器（EC Terminal Support Indicator）
	TAG_PBOC_EC_TERMINAL_TRANSACTION_lIMIT        =  0x9F7B,            //电子现金终端交易限额（EC Terminal Transaction Limit）
	TAG_CONTACTLESS_FLOOR_LIMIT                   =  0xDF19,            //非接floor limit
	TAG_PBOC_ARITHMETIC_SUPPORT_SM                =  0xDF69,            //国密算法支持
	TAG_EMV_TRANS_REVERSAL                        =  0x9F8104,

	POS_TAG_END,
}Pos_Tag_t;

/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
typedef struct
{
    Host_Type_t type;
    u32 start_addr;
}T_OTA;

typedef struct
{
    const char *pQrCode;
    u32 offset_x;
    u32 offset_y;
    u32 Multi_zoom;
    u32 Ms_timeout;
}QR_CORE_T,*pQR_CORE_T;

typedef enum
{
    EMV_PROCESS_MAG_OK=0x00,
    EMV_PROCESS_ONLINE_PIN,
    EMV_PROCESS_OFFLINE_PIN,
    EMV_PROCESS_NFC_ONLINE,
    EMV_PROCESS_NFC_APPROVED,
    EMV_PROCESS_NFC_DECLINE,
    EMV_PROCESS_OFFLINE_LASTE_PIN,

    EMV_PROCESS_REVERSAL=0x95,
    EMV_PROCESS_ICC_APP_SELECT,
    EMV_PROCESS_MAG_ACCOUNT_TYPE,
    EMV_PROCESS_ICC_GAC2,
    EMV_PROCESS_ICC_ONLINE,
    EMV_PROCESS_CUSTOM

}TRANS_MODE_T;

#if defined(CFG_DOCK_DEVICE) || defined(CFG_UNIVERSAL_SDK)
typedef enum
{
	GD_UNKOWN,
    GD001,
    GD002,
    GD003,
    GD004,
	GD_FUTURE = 0xFF
} T_GD_SERY;

typedef enum
{
	WIFI_UNKOWN,
	CHIP_WIFI,//GD001
	DOCK_WIFI,//GD004
	COMMON_WIFI,//A30
	NO_WIFI = 0xFF
} T_WIFI_DEV;

typedef enum{
	TCP,
	HTTP,
	HTTPS,
	UNKNOWN_PROTO
} T_WAN_PROTOCOL;

typedef struct {
		T_WAN_PROTOCOL eProtocol;
		T_U8_VIEW uvSite;
		u16 wPort;
		T_U8_VIEW uvPath;
} T_URL_INFO;

typedef enum
{
    TRANS_CARD_MAG=0x00,
    TRANS_CARD_ICC,
    TRANS_CARD_CTLS
}TRANS_CARD_T;

extern T_GD_SERY identify_gd_model(void);
extern T_WIFI_DEV identify_wifi_dev(void);
extern T_WIFI_DEV switch_wifi_dev(T_WIFI_DEV wfWanted);
extern void SetDevWifi(T_WIFI_DEV wifi);
extern u8* obtain_gdinfo(void);
extern u32 obtain_wifi_ap_list(char * pList, u32 dwLenMax);
extern Rc_t disonnect_current_wifi(void);
extern Rc_t disconnect_wifi_des();
extern Rc_t connect_wifi(const char * pSSID, const char * pPassword);
extern char* obtain_wifi_details(char *strApInfo, u8 bytLenMax);
extern int setup_custom_signature(char *strFile, u16 wWidth, u16 wHeight, u16 wSize);
extern int prn_print_str(u8 x, u8 y, char *strToPrint);
extern int prn_print_file(char *strFile);
extern u8* pack_gd_sock_req(T_U8_VIEW *puvContent, T_URL_INFO *url, u8 bytMinutes);
extern void obtain_Gprs_details(char*strmsg);
extern void obtain_Gps_details(pu8 pRecv,pu32 Recvlen,u32 timeout);
extern Rc_t HttpServerConnect(char * pURL,u32 port,char * pHost,SslAuthMode_t mode);
extern u32 lark_des_pkcs5(Des_Type_t desType,pu8 key,u32 keyLen,pu8 fill,pu8 ibuf,u32 ibufLen,pu8 obuf);
extern u32 format_pinblock(pu8 in_cardNoAscii,u32 cardNoAsciiLen,pu8 pinAsciiIn,u32 pinAsciiInLen,pu8 obuf );
extern void lark_OTA(T_OTA dev);
#endif

/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
extern void trace_value(pcu8 pbuf, uint cnt);
extern void trace_ascii(pcu8 pbuf, uint cnt);
extern void trace_data(pcu8 pbuf, uint cnt);
extern void lark_jump(u32 addr );
extern Rc_t lark_timer(Timer_Type_t type,u32 id,u32 time,void (*callback)(pvoid),pvoid param );
extern pvoid lark_alloc_mem(u32 size );
extern void lark_free_mem(pvoid paddr );
extern void lark_sleep(u32 ms );
extern Rc_t lark_send_msg(Os_Msg_Id_t id,pmessage_body_t pmsg );
extern Rc_t lark_wait_msg(Os_Msg_Id_t id,pmessage_body_t pmsg,u32 timeout );
extern Rc_t lark_query_msg(Os_Msg_Id_t id,pmessage_body_t pmsg );
extern Rc_t lark_flush_msg(Os_Msg_Id_t id );
extern Rc_t lark_flag_set(Os_Flag_Group_t group,u32 flags );
extern Rc_t lark_flag_clr(Os_Flag_Group_t group,u32 flags );
extern u32 lark_flag_wait_any(Os_Flag_Group_t group,u32 flags,u32 timeout );
extern u32 lark_flag_wait_all(Os_Flag_Group_t group,u32 flags,u32 timeout );
extern bool lark_flag_is_true(Os_Flag_Group_t group,u32 flags );

extern void lark_led(Color_t color,Switch_t swi );
extern void lark_led_direction(Color_t Color,Switch_t Swi,Dev_Led_Dir_t LedDir);
extern Rc_t lark_led_RGB565_Breath_Dir(Dev_Led_Dir_t Dir,u16 color,Switch_t LedSwi,u16 BreathTime,bool BreathEnable, bool BreathAsc);
extern void lark_led_RGB565_blink(u32 direction, u32 color,u32 period,u32 duty,u32 times);
extern void lark_led_charge(Switch_t Swi);
extern void lark_led_blink(Color_t color,u32 period,u32 duty,u32 times );
extern void lark_buzzer(Switch_t swi );
extern void lark_buzzer_config_swtich(Buzzer_Oper_t BuzzerOper);
extern void lark_buzzer_blink(u32 period,u32 duty,u32 times );
extern void lark_port_write(Port_t port,Switch_t swi );
extern void lark_port_read(Port_t port, pu8 pValue);
extern u32 lark_flash_read(u32 addr,pu8 pData,u32 size );
extern u32 lark_flash_write(u32 addr,pu8 pData,u32 size );
extern u32 lark_flash_erase(u32 addr,u32 size );
extern u32 lark_xflash_read(u32 addr,pu8 pData,u32 size );
extern u32 lark_xflash_write(u32 addr,pu8 pData,u32 size );
extern u32 lark_xflash_erase(u32 addr,u32 size );
extern u32 lark_nvram_read(u32 addr,pu8 pData,u32 size );
extern u32 lark_nvram_write(u32 addr,pu8 pData,u32 size );
extern Rc_t lark_com_open(Host_Type_t host );
extern Rc_t lark_com_write(Host_Type_t host,pu8 pData,u32 size,pu32 pWStatus);
extern u8 lark_com_config(Host_Type_t host,pvoid pParam );
extern void lark_com_close(Host_Type_t host );
extern u8 lark_gprs_config(pvoid pParam);
extern Card_Type_t lark_card_reader(Card_Type_t type,Switch_t swi );
extern Card_Type_t lark_card_check(Card_Type_t type );
extern Card_Type_t lark_card_wait(pCard_Wait_t pCardWait );
extern u32 lark_card_rw(Card_Type_t type,pu8 rwBuffer,u32 rwLen );
extern Rc_t lark_card_rw_ret(Card_Type_t type,pu8 rwBuffer,pu32 pRwLen,pu32 pDetailRetCode,u32 timeout);
extern Key_Num_t lark_kbd_read_current(void );
extern Key_Num_t lark_kbd_read_buffer(void );
extern void lark_kbd_clear_buffer(void );
extern void lark_kbd_backlight(Switch_t swi );
extern Rc_t lark_tp_remap(pTp_KeyMap_t pMap);
extern Rc_t lark_tp_switch(Switch_t swi);
extern void lark_lcd_sign_init(u32 x, u32 y, u32 w, u32 h, u32 color);
extern void lark_lcd_sign_write(u32 x1, u32 y1, u32 x2, u32 y2, u32 width, u32 color);
extern u32 lark_lcd_sign_read_point(u32 x, u32 y);
extern void lark_lcd_fill_area(u32 x, u32 y, u32 w, u32 h, u32 color);
extern void lark_lcd_write(Dev_Lcd_Mode_t mode,u32 x,u32 y,u32 w,u32 h,pu8 dispCode );
extern void lark_lcd_read(u32 x,u32 y,u32 w,u32 h,pu8 dispCode );
extern void lark_lcd_erase(u32 x,u32 y,u32 w,u32 h );
extern void lark_lcd_backlight(Switch_t swi );
extern u8 lark_camera_config(pvoid pParam);
extern void lark_update_generate(pu8 pvalue,u32 length );
extern void lark_update_verify(pu8 pvalue,u32 length );
extern void lark_update_enter(Host_Type_t type );
extern void lark_shutdown(void );
extern void lark_reboot(void );
extern void lark_sleep_mode(u32 ShutDownTime);
extern u32  lark_rtc_read(pu8 time );
extern void lark_rtc_write(pu8 time );
extern void lark_info_read(Info_Type_t type,pvoid pvalue );
extern Rc_t lark_info_write(Info_Type_t type,pvoid pvalue );

extern u32 lark_rand(pu8 pRand,u32 randLen );
extern Rc_t lark_rsa_decrypt(pu8 n,u32 nLen,pu8 d,u32 dLen,pu8 iBuf,u32 iLen,pu8 oBuf );
extern Rc_t lark_rsa_encrypt(pu8 n,u32 nLen,pu8 e,u32 eLen,pu8 iBuf,u32 iLen,pu8 oBuf );
extern Rc_t lark_rsa_encrypt_sign_pkcs_15(pu8 n, u32 nLen, pu8 e, u32 eLen,  pu8 d, u32 dLen, pu8 iBuf, u32 iLen, pu8 oBuf );

extern Rc_t lark_rsa_generate_key(pu8 n,pu8 d,pu8 e,u32 eLen,u32 keyBits ,Alg_Rsa_Generate_Key_Mode_t Mode);
extern u32 lark_des(Des_Type_t desType,pu8 key,u32 keyLen,pu8 fill,pu8 ibuf,u32 ibufLen,pu8 obuf );
extern u32 lark_aes(AES_Type_t AesType,pu8 pKey,u32 keyLen, pu8 fill, pu8 pIBuf,u32 IBufLen,pu8 pOBuf );
extern Rc_t lark_sha(Sha_Type_t type,pu8 ibuf,u32 ibufLen,pu8 obuf );
extern Rc_t lark_sha_complex(Sha_Type_t type,pu8 ibuf,u32 ibufLen,pu8 obuf ,Alg_Sha_IoCtl_t ShaIoCtl);
extern u32 lark_crc(u32 init,pu8 ibuf,u32 ibufLen );

extern void lark_fifo_buffer_read(pFifo_Buffer_t fifoType );
extern void lark_fifo_buffer_write(pFifo_Buffer_t fifoType );
extern void lark_fifo_buffer_clear(pFifo_Buffer_t fifoType );
extern void lark_filo_buffer_read(pFilo_Buffer_t filoType );
extern void lark_filo_buffer_write(pFilo_Buffer_t filoType );
extern void lark_filo_buffer_clear(pFilo_Buffer_t filoType );
extern u32 lark_keys_read(u32 addr,pu8 pData,u32 size );
extern u32 lark_keys_write(u32 addr,pu8 pData,u32 size );
extern void lark_recover_rom_boot(void);
extern Rc_t lark_gps_open(void);
extern u8 lark_gps_config(pvoid pParam);
extern void lark_gps_close(void);
extern Rc_t lark_wifi_config(pComWifiConfig_t pWifiConfig);
extern Rc_t lark_module_wakeup_pin(pDevComm_t pDevComm);
extern Rc_t lark_sdk_dspread_switch(u32 MainAppIndex);
extern Rc_t lark_md5(pu8 ibuf,u32 ibufLen,pu8 obuf, Alg_Md5_IoCtl_t IoCtl, pAlg_Md5_Ctx_t pMD5_CTX );
extern Rc_t lark_flash_dbglog_read(u32 Region, pu8 pBuf, u32 BufLen);
extern Rc_t lark_flash_dbglog_erase(void);
extern Rc_t lark_tp_read_map(pTp_Pos_t pPosition);
extern void lark_read_handware_id(pu8 pValue);
extern u32 lark_xflash_read_capacity(pu32 pStoreFlashCapa,pu32 pCodeFlashCapa );
extern u32 lark_xflash_read_info(pu32 pStoreFlashCapa,pu32 pCodeFlashCapa,pu8 pStoreFlashID,pu8 pCodeFlashID);
/*
*******************************************************************************
*   End of File
*******************************************************************************
*/

#ifdef __cplusplus
}
#endif//__cplusplus

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

#endif  /* #ifndef _APPCODE_H */

