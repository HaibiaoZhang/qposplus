/*
********************************************************************************
*
*   File Name:
*       lark_api.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _LARK_API_H
#define _LARK_API_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <stddef.h>
#include <time.h>
#include <float.h>
#include <math.h>
#include <errno.h>
#include <locale.h>
#include <setjmp.h>
#include <signal.h>
#include <stdarg.h>


/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#ifdef _LARK_API_C
#define GLOBAL
#else
#define GLOBAL  extern
#endif


/* Big-Endian Pointer to Long */
#define BE_PtrToLongLong(ptr)           \
	( \
		(((unsigned long long)(*((u8 *)(ptr) + 0))) << 56) | \
		(((unsigned long long)(*((u8 *)(ptr) + 1))) << 48) | \
		(((unsigned long long)(*((u8 *)(ptr) + 2))) << 40) | \
		(((unsigned long long)(*((u8 *)(ptr) + 3))) << 32)| \
		(((unsigned long long)(*((u8 *)(ptr) + 4))) << 24) | \
		(((unsigned long long)(*((u8 *)(ptr) + 5))) << 16) | \
		(((unsigned long long)(*((u8 *)(ptr) + 6))) << 8) | \
		(((unsigned long long)(*((u8 *)(ptr) + 7))) << 0) \
	)

/* Big-Endian Pointer to Long */
#define BE_PtrToLong(ptr)           \
	( \
		(((u32)(*((u8 *)(ptr) + 0))) << 24) | \
		(((u32)(*((u8 *)(ptr) + 1))) << 16) | \
		(((u32)(*((u8 *)(ptr) + 2))) << 8) | \
		(((u32)(*((u8 *)(ptr) + 3))) << 0) \
	)

/* Big-Endian Pointer to Short */
#define BE_PtrToShort(ptr)          \
	( \
		(((u16)(*((u8 *)(ptr) + 0))) << 8) | \
		(((u16)(*((u8 *)(ptr) + 1))) << 0) \
	)

/* Little-Endian Pointer to Long */
#define LE_PtrToLong(ptr)           \
	( \
		(((u32)(*((u8 *)(ptr) + 0))) << 0) | \
		(((u32)(*((u8 *)(ptr) + 1))) << 8) | \
		(((u32)(*((u8 *)(ptr) + 2))) << 16) | \
		(((u32)(*((u8 *)(ptr) + 3))) << 24) \
	)

/* Little-Endian Pointer to Short */
#define LE_PtrToShort(ptr)          \
	( \
		(((u16)(*((u8 *)(ptr) + 0))) << 0) | \
		(((u16)(*((u8 *)(ptr) + 1))) << 8) \
	)

/* Long Split */
#define LONG_HH(val)        ((((u32)(val)) >> 24) & 0xff)
#define LONG_HL(val)        ((((u32)(val)) >> 16) & 0xff)
#define LONG_LH(val)        ((((u32)(val)) >> 8) & 0xff)
#define LONG_LL(val)        (((u32)(val)) & 0xff)
#define LONG_H(val)         ((((u32)(val)) >> 16) & 0xffff)
#define LONG_L(val)         (((u32)(val)) & 0xffff)

/* Short Split */
#define SHORT_H(val)        ((((u16)(val)) >> 8) & 0xff)
#define SHORT_L(val)        (((u16)(val)) & 0xff)

#define BIT(idx)            ((u32)0x01 << (idx))
#define BIT_GET(var, idx)   ((((var) & BIT(idx)) == 0) ? 0 : 1)
#define BIT_SET(var, idx)   ((var) |= BIT(idx))
#define BIT_CLR(var, idx)   ((var) &= ~(BIT(idx)))

/* Seconds to Milliseconds */
#define SEC_TO_MS(secs)     ((u32)(secs) * 1000ul)
#define SEC_TO_US(secs)     ((u32)(secs) * 1000ul * 1000ul)
#define MIN_TO_SEC(minutes) ((u32)(minutes) * 60ul)
#define MIN_TO_MS(minutes)  ((u32)(minutes) * 60ul * 1000ul)

#define HIGH        (1)
#define LOW         (0)

#define U8_MAX      ((u8)(0xff))
#define U16_MAX     ((u16)(0xffff))
#define U32_MAX     ((u32)(0xffffffff))

#ifndef NULL
#define NULL        ((void *)0)
#endif

#define LARK_DEV_MAP_SET_LED1(map)          BIT_SET(map[0],0)
#define LARK_DEV_MAP_SET_LED2(map)          BIT_SET(map[0],1)
#define LARK_DEV_MAP_SET_LED3(map)          BIT_SET(map[0],2)
#define LARK_DEV_MAP_SET_LED4(map)          BIT_SET(map[0],3)
#define LARK_DEV_MAP_SET_LED_BREATH(map)    BIT_SET(map[0],4)
#define LARK_DEV_MAP_SET_LED_RGB(map)       BIT_SET(map[0],5)
#define LARK_DEV_MAP_SET_LED_MUTI_DIR(map)  BIT_SET(map[0],6) /* LED支持多方向控制 */
#define LARK_DEV_MAP_SET_BUZZER(map)        BIT_SET(map[1],0)
#define LARK_DEV_MAP_SET_FLASH(map)         BIT_SET(map[2],0)
#define LARK_DEV_MAP_SET_XFLASH(map)        BIT_SET(map[2],1)
#define LARK_DEV_MAP_SET_NVRAM(map)         BIT_SET(map[2],2)
#define LARK_DEV_MAP_SET_BATTERY(map)       BIT_SET(map[3],0)
#define LARK_DEV_MAP_SET_SHUTDOWN(map)      BIT_SET(map[3],1)
#define LARK_DEV_MAP_SET_REBOOT(map)        BIT_SET(map[3],2)
#define LARK_DEV_MAP_SET_SLEEP(map)         BIT_SET(map[3],3)
#define LARK_DEV_MAP_SET_RTC_CLOCK(map)     BIT_SET(map[4],0)
#define LARK_DEV_MAP_SET_RTC_PARAM(map)     BIT_SET(map[4],1)
#define LARK_DEV_MAP_SET_KEYBOARD(map)      BIT_SET(map[5],0)
#define LARK_DEV_MAP_SET_POWERKEY(map)      BIT_SET(map[5],1)
#define LARK_DEV_MAP_SET_USB(map)           BIT_SET(map[6],0)
#define LARK_DEV_MAP_SET_BLUETOOTH(map)     BIT_SET(map[6],1)
#define LARK_DEV_MAP_SET_SERIAL(map)        BIT_SET(map[6],2)
#define LARK_DEV_MAP_SET_WIFI(map)          BIT_SET(map[6],3)
#define LARK_DEV_MAP_SET_USB_HID(map)       BIT_SET(map[6],4)
#define LARK_DEV_MAP_SET_GPRS(map)          BIT_SET(map[6],5)
#define LARK_DEV_MAP_SET_SPI(map)           BIT_SET(map[6],6)
#define LARK_DEV_MAP_SET_MAG(map)           BIT_SET(map[7],0)
#define LARK_DEV_MAP_SET_ICC(map)           BIT_SET(map[7],1)
#define LARK_DEV_MAP_SET_NFC(map)           BIT_SET(map[7],2)
#define LARK_DEV_MAP_SET_MIFIRE(map)        BIT_SET(map[7],3)
#define LARK_DEV_MAP_SET_LCD(map)           BIT_SET(map[8],0)
#define LARK_DEV_MAP_SET_TP(map)            BIT_SET(map[8],1)
#define LARK_DEV_MAP_SET_RAND(map)          BIT_SET(map[9],0)
#define LARK_DEV_MAP_SET_RSA(map)           BIT_SET(map[10],0)
#define LARK_DEV_MAP_SET_DES(map)           BIT_SET(map[10],1)
#define LARK_DEV_MAP_SET_SHA(map)           BIT_SET(map[10],2)
#define LARK_DEV_MAP_SET_CRC(map)           BIT_SET(map[10],3)
#define LARK_DEV_MAP_SET_OS_SLEEP(map)      BIT_SET(map[11],0)
#define LARK_DEV_MAP_SET_OS_EVENT(map)      BIT_SET(map[11],1)
#define LARK_DEV_MAP_SET_OS_MESSAGE(map)    BIT_SET(map[11],2)
#define LARK_DEV_MAP_SET_OS_ALLOC_RAM(map)  BIT_SET(map[11],3)
#define LARK_DEV_MAP_SET_OS_RAW(map)        BIT_SET(map[11],4)
#define LARK_DEV_MAP_SET_FIFO(map)          BIT_SET(map[12],0)
#define LARK_DEV_MAP_SET_FILO(map)          BIT_SET(map[12],1)
#define LARK_DEV_MAP_SET_SP_TERMINAL(map)   BIT_SET(map[13],0)
#define LARK_DEV_MAP_SET_GPS(map)           BIT_SET(map[14],0)
#define LARK_DEV_MAP_SET_EXPANDER_IO(map)   BIT_SET(map[15],0)


#define LARK_DEV_MAP_GET_LED1(map)          BIT_GET(map[0],0)
#define LARK_DEV_MAP_GET_LED2(map)          BIT_GET(map[0],1)
#define LARK_DEV_MAP_GET_LED3(map)          BIT_GET(map[0],2)
#define LARK_DEV_MAP_GET_LED4(map)          BIT_GET(map[0],3)
#define LARK_DEV_MAP_GET_LED_BREATH(map)    BIT_GET(map[0],4)
#define LARK_DEV_MAP_GET_LED_RGB(map)       BIT_GET(map[0],5)
#define LARK_DEV_MAP_GET_LED_MUTI_DIR(map)  BIT_GET(map[0],6) /* LED支持多方向控制 */
#define LARK_DEV_MAP_GET_BUZZER(map)        BIT_GET(map[1],0)
#define LARK_DEV_MAP_GET_FLASH(map)         BIT_GET(map[2],0)
#define LARK_DEV_MAP_GET_XFLASH(map)        BIT_GET(map[2],1)
#define LARK_DEV_MAP_GET_NVRAM(map)         BIT_GET(map[2],2)
#define LARK_DEV_MAP_GET_BATTERY(map)       BIT_GET(map[3],0)
#define LARK_DEV_MAP_GET_SHUTDOWN(map)      BIT_GET(map[3],1)
#define LARK_DEV_MAP_GET_REBOOT(map)        BIT_GET(map[3],2)
#define LARK_DEV_MAP_GET_SLEEP(map)         BIT_GET(map[3],3)
#define LARK_DEV_MAP_GET_RTC_CLOCK(map)     BIT_GET(map[4],0)
#define LARK_DEV_MAP_GET_RTC_PARAM(map)     BIT_GET(map[4],1)
#define LARK_DEV_MAP_GET_KEYBOARD(map)      BIT_GET(map[5],0)
#define LARK_DEV_MAP_GET_POWERKEY(map)      BIT_GET(map[5],1)
#define LARK_DEV_MAP_GET_USB(map)           BIT_GET(map[6],0)
#define LARK_DEV_MAP_GET_BLUETOOTH(map)     BIT_GET(map[6],1)
#define LARK_DEV_MAP_GET_SERIAL(map)        BIT_GET(map[6],2)
#define LARK_DEV_MAP_GET_WIFI(map)          BIT_GET(map[6],3)
#define LARK_DEV_MAP_GET_USB_HID(map)       BIT_GET(map[6],4)
#define LARK_DEV_MAP_GET_GPRS(map)          BIT_GET(map[6],5)
#define LARK_DEV_MAP_GET_SPI(map)           BIT_GET(map[6],6)
#define LARK_DEV_MAP_GET_MAG(map)           BIT_GET(map[7],0)
#define LARK_DEV_MAP_GET_ICC(map)           BIT_GET(map[7],1)
#define LARK_DEV_MAP_GET_NFC(map)           BIT_GET(map[7],2)
#define LARK_DEV_MAP_GET_MIFIRE(map)        BIT_GET(map[7],3)
#define LARK_DEV_MAP_GET_LCD(map)           BIT_GET(map[8],0)
#define LARK_DEV_MAP_GET_TP(map)            BIT_GET(map[8],1)
#define LARK_DEV_MAP_GET_RAND(map)          BIT_GET(map[9],0)
#define LARK_DEV_MAP_GET_RSA(map)           BIT_GET(map[10],0)
#define LARK_DEV_MAP_GET_DES(map)           BIT_GET(map[10],1)
#define LARK_DEV_MAP_GET_SHA(map)           BIT_GET(map[10],2)
#define LARK_DEV_MAP_GET_CRC(map)           BIT_GET(map[10],3)
#define LARK_DEV_MAP_GET_OS_SLEEP(map)      BIT_GET(map[11],0)
#define LARK_DEV_MAP_GET_OS_EVENT(map)      BIT_GET(map[11],1)
#define LARK_DEV_MAP_GET_OS_MESSAGE(map)    BIT_GET(map[11],2)
#define LARK_DEV_MAP_GET_OS_ALLOC_RAM(map)  BIT_GET(map[11],3)
#define LARK_DEV_MAP_GET_OS_RAW(map)        BIT_GET(map[11],4)
#define LARK_DEV_MAP_GET_FIFO(map)          BIT_GET(map[12],0)
#define LARK_DEV_MAP_GET_FILO(map)          BIT_GET(map[12],1)
#define LARK_DEV_MAP_GET_SP_TERMINAL(map)   BIT_GET(map[13],0)
#define LARK_DEV_MAP_GET_GPS(map)           BIT_GET(map[14],0)
#define LARK_DEV_MAP_GET_EXPANDER_IO(map)   BIT_GET(map[15],0)


#define LARK_SYSTEM_24H_SELFCHECK_TIME        ((24 * 60 - 10) * 60) // 减去10min，保证设备24小时内自检
/*-----------------------------------------------------------------------------
|   Enumerations
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
typedef signed long long	s64,   *ps64;
typedef signed long 		s32,   *ps32;
typedef signed short		s16,   *ps16;
typedef signed char 		s8,    *ps8;
typedef signed int			sint,  *psint;

typedef unsigned long long	u64,   *pu64;
typedef unsigned long		u32,   *pu32;
typedef unsigned short		u16,   *pu16;
typedef unsigned char		u8,    *pu8;
typedef unsigned int		uint,  *puint;

typedef void			   *pvoid;
typedef const void		   *pcvoid;

typedef const s64		   *pcs64;
typedef const s32		   *pcs32;
typedef const s16		   *pcs16;
typedef const s8		   *pcs8;
typedef const sint		   *pcsint;
typedef const u64		   *pcu64;
typedef const u32		   *pcu32;
typedef const u16		   *pcu16;
typedef const u8		   *pcu8;
typedef const uint		   *pcuint;

typedef unsigned char       uchar;

typedef const char 		   *pc8;


#ifndef __cplusplus
typedef enum
{
	false = 0,
	true = !false
} bool;
#endif

typedef uint    msgid_t;

typedef struct _message_body_t
{
    msgid_t msg_id;  //消息ID
    u32     wparam;  // 其他类型： HostID 等
    u32     lparam;  //数据长度
    pvoid   pparam;  //数据指针
} message_body_t, *pmessage_body_t;

typedef enum _dev_id_t
{
	DEV_JUMP,

    DEV_LED,
    DEV_LED_DIRECTION,
    DEV_LED_RGB565_BREATH_DIR,
    DEV_LED_CHARGE,
    DEV_BUZZER,
    DEV_FLASH_READ,
    DEV_FLASH_WRITE,
    DEV_FLASH_ERASE,
    DEV_FLASH_READ_CAPACITY,
    DEV_XFLASH_READ,
    DEV_XFLASH_WRITE,
    DEV_XFLASH_ERASE,
    DEV_XFLASH_READ_CAPACITY,
    DEV_XFLASH_READ_INFO,
    DEV_NVRAM_READ,
    DEV_NVRAM_WRITE,
    DEV_PM_SHUTDOWN,
    DEV_PM_REBOOT,
    DEV_PM_SLEEP,
    DEV_RTC_READ,
    DEV_RTC_WRITE,
    DEV_UPDATE_GENERATE,
    DEV_UPDATE_VERIFY,
    DEV_UPDATE_ENTER,
    DEV_KBD_READ_CURRENT,
    DEV_KBD_READ_BUF,
    DEV_KBD_CLEAR_BUF,
    DEV_KBD_BACKLIGHT,
    DEV_TP_REMAP,
    DEV_TP_SWITCH,
    DEV_TP_READ_MAP,
    DEV_COM_OPEN,
    DEV_COM_WRITE,
    DEV_COM_CONFIG,
    DEV_COM_CLOSE,
    DEV_CARD_READER_OPEN,
    DEV_CARD_READER_CLOSE,
    DEV_CARD_WAIT,
    DEV_CARD_RW,
    DEV_CARD_CHECK,
    DEV_LCD_WRITE_AREA,
    DEV_LCD_READ_AREA,
    DEV_LCD_ERASE_AREA,
    DEV_LCD_BACKLIGHT,
    DEV_LCD_FILL_AREA,
    DEV_LCD_SIGN_DRAW,
    DEV_INFO_READ,
    DEV_INFO_WRITE,

    DEV_ALG_RAND,
    DEV_ALG_RSA_DECRYPT,
    DEV_ALG_RSA_ENCRYPT,
    DEV_ALG_RSA_GENERATE_KEY,
    DEV_ALG_RSA_OTHERS,
    DEV_ALG_SM_DECRYPT,
    DEV_ALG_SM_ENCRYPT,
    DEV_ALG_SM_GENERATE_KEY,
    DEV_ALG_DES,
    DEV_ALG_AES,
    DEV_ALG_SHA,
    DEV_ALG_CRC,
    DEV_ALG_ECC_SIGNATURE,
    DEV_ALG_ECC_VERIFY_SIGNATURE,

    DEV_DELAY_MS,
    DEV_DELAY_US,
    DEV_TIMER,
    DEV_MEM_FREE,
    DEV_MEM_ALLOC,
    DEV_OS_SEND_MSG,
    DEV_OS_WAIT_MSG,
    DEV_OS_QUERY_MSG,
    DEV_OS_FLUSH_MSG,
    DEV_OS_SET_FLAG,
    DEV_OS_CLEAR_FLAG,
    DEV_OS_WAIT_ANY_FLAG,
    DEV_OS_WAIT_ALL_FLAG,
    DEV_OS_FLAG_IS_TRUE,
    DEV_OS_SLEEP,
    DEV_OS_THREAD,

    DEV_FIFO_BUFFER_READ,
    DEV_FIFO_BUFFER_WRITE,
    DEV_FIFO_BUFFER_CLEAR,
    DEV_FILO_BUFFER_READ,
    DEV_FILO_BUFFER_WRITE,
    DEV_FILO_BUFFER_CLEAR,

	DEV_KEYS_READ,
	DEV_KEYS_WRITE,

    DEV_PRINTER,
    DEV_TWO_BIT_BAR_CODE,
    DEV_RECOVER_ROM_BOOT,
    DEV_PORT,
    DEV_GPS_OPEN,
    DEV_GPS_CONFIG,
    DEV_GPS_CLOSE,
    DEV_GPRS_CONFIG,
    DEV_WIFI_CONFIG,
    DEV_WAKEUP_MODULE_PIN,
    DEV_CAM_CONFIG,
    DEV_MULTI_APP_SWITCH,
    DEV_ALG_MD5,
    DEV_FLASH_DBG_LOG_READ,
    DEV_FLASH_DBG_LOG_ERASE,
    DEV_ALG_SHA_COMPLEX,

    DEV_HANDWARE_ID,

    DEV_BT_SPP_DISCONNECT,
    DEV_BT_BLE_DISCONNECT,
	DEV_ALG_AES_GCM,

    DEV_WDG_FEED,

}Dev_ID_t;

typedef enum _rc_t
{
	RC_SUCCESS,
	RC_FAIL,
	RC_QUIT,
#ifdef VM_WIN32
	RC_GO_UP,
	RC_GO_DOWN,
#else
	RC_UP,
	RC_DOWN,
#endif
    RC_NET_ERR
}Rc_t;

typedef enum _swithc_t
{
	OFF,
	ON,
}Switch_t;

typedef enum _rw_t
{
	RW_READ,
	RW_WRITE,
}Rw_t;

typedef enum _color_t
{
	BLUE,
	YELLOW,
	GREEN,
	RED,
	WHITE,
	CYAN,
	COLOR_END,
}Color_t;

typedef enum _port_t
{
	SP_WAKE_N,
    SP_ALARM,
}Port_t;

typedef enum
{
    UPD_DEV_USB,
    UPD_DEV_UART,
    UPD_DEV_BT_IVT_I40E_I482E,
    UPD_DEV_BT_IVT_I410E,
    UPD_DEV_BT_ISSC_BM77,
    UPD_DEV_BT_ITON_BB2710_30,
    UPD_DEV_BT_ITON_BB2710,
    UPD_DEV_BT_YK_661DM20,
    UPD_DEV_BT_TOSHIBA,                /* 东芝 双模(2.0 & 4.0) */
	UPD_DEV_BT_YICHIP,
	UPD_DEV_OTA,
	UPD_DEV_USB_ERASE,  /* 先擦除全部固件，然后启动USB升级 */
	UPD_DEV_SPI,

    UPD_DEV_NUMS,
}Upd_Dev_Type_t;

typedef enum
{
    BATT_CTRL_CHARGE_FULL = 0,
    BATT_CTRL_CHARGE_NOT_FULL,
    BATT_CTRL_CHARGE_40V_FULL,  //工程模式下，充电到4.0V认为充满

    BATT_CTRL_CHARGE_STATUS_END
}Charge_Ctrl_t;




#if defined(CFG_KEY_LONG_PRESS)
typedef enum _key_num_t
{
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_UP,
    KEY_DOWN,
    KEY_POWER,
    KEY_CANCEL,
    KEY_DELETE,
    KEY_CONFIRM,
    KEY_LONG_0,
	KEY_LONG_1,
	KEY_LONG_2,

    KEY_NONE = 0xFF,
}Key_Num_t;

#else
typedef enum _key_num_t
{
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_UP,
    KEY_DOWN,
    KEY_POWER,
    KEY_CANCEL,
    KEY_DELETE,
    KEY_CONFIRM,
    KEY_NONE = 0xFF,
}Key_Num_t;

#endif


typedef enum _host_type_t
{
	HOST_IDLE,

    HOST_UART,
    HOST_USB_CMD,
    HOST_USB_DEBUG,
    HOST_BT,
    HOST_WIFI,
    HOST_GPRS,
    HOST_PRINTER,
    HOST_DEV_OTA,  /* 用于OTA升级 */
    HOST_SPI,

	HOST_NUMS,
}Host_Type_t;

typedef enum _card_type_t
{
    CARD_NFC = BIT(0),
    CARD_IC = BIT(1),
    CARD_MAG = BIT(2),
    CARD_MIFARE = BIT(3),
    CARD_PSAM = BIT(4),
    CARD_SIM = BIT(5),
    CARD_FELICA = BIT(6),
    CARD_NFC_A = BIT(7),
    CARD_NFC_B = BIT(8),
}Card_Type_t;

typedef enum _PollType_t
{
    POLL_NFC_FUNCTION,
    POLL_NFC_ROBOT,
    POLL_NFC_FUNCTION_VISA,

}Poll_Type_t;

typedef enum
{
    RW_NFC_MIFARE_OK = 0,//操作正常
    RW_NFC_MIFARE_PARAM_ERROR,							//参数错误
    RW_NFC_MIFARE_TIMEOUT_ERROR,						//超时错误
    RW_NFC_MIFARE_CRC_ERROR,							//CRC错误
    RW_NFC_MIFARE_NACK_ERROR,							//卡片回复NACK
    RW_NFC_MIFARE_POLL_ERROR,							// 寻卡错误
    RW_NFC_MIFARE_OPERATION_ERROR,                     //操作流程错误
    RW_NFC_MIFARE_CMD_NO_AVAILABLE_ERROR,              // 命令不支持,即proj.h没有定义mifare卡
    RW_NFC_MIFARE_ERROR_END,
}NFC_MIFARE_RetCode_t;

typedef enum _info_type_t
{
    INFO_CPU_ID,
    INFO_DEV_ID,
    INFO_PCB_ID,
    INFO_HW_VERSION,
    INFO_OS_VERSION,
    INFO_DRV_VERSION,
    INFO_BOOT_VERSION,
    INFO_MAG_SUPPORT,
    INFO_USB_STATUS,
    INFO_BT_STATUS,
    INFO_BT_NAME,
    INFO_BT_TYPE,
    INFO_BT_MACADDR,
    INFO_BAT_LEVEL,
    INFO_BAT_ADC_VALUE,
    INFO_CHARGING_STATUS,
    INFO_KBD_BEEP,
    INFO_LCD_ATTRIBUTE,
    INFO_FLASH_SIZE,
    INFO_XFLASH_SIZE,
    INFO_NVRAM_SIZE,
    INFO_BAT_PARAM,
    INFO_SYS_STATUS,
    INFO_MAG_KEY,
    INFO_CHIP_DEV_MAP,
    INFO_BATT_CHARGE_CTRL,
    INFO_PART_UPGRADE_KEY,
    INFO_WIFI_SCAN_AP,
    INFO_WIFI_IP_ADDR,
    INFO_WIFI_LINK_AP_INF,
    INFO_WIFI_MAC_ADDR,
    INFO_WIFI_WORK_MODE,
    INFO_WIFI_DHCP_MODE,
    INFO_WIFI_AUTO_CONN_STATUS,
    INFO_WIFI_DNS,
    INFO_WIFI_HOST_NAME,
    INFO_BAT_STATUS,
    INFO_BAT_PARAMETERS,
    INFO_EMV_CONFIG_INFO,
    INFO_DEV_DESTRUCT_STATUS,  // 硬件自毁标志
    INFO_BATT_CHARGE_FORCE_CTRL,
    INFO_NFC_PARAM,
    INFO_BT_PAIRMODE,
    INFO_PCBA_TEST_RESULT,
    INFO_FULL_TEST_RESULT,
    INFO_WIFI_SSL_CERTIFICATION,
    INFO_WIFI_LOAD_SSL_CERTIFICATION,
    INFO_WIFI_QUEUE_BUF_SIZE,
    INFO_BT_FIX_PINCODE,
    INFO_WIFI_CONFIG,
	INFO_RGB_BREATH_LED_COLOR,
	INFO_OTA_APP_VERSION,

	/* 如果要增加INFO内容,需要按照顺序增加,且同步更新协议文档 */
	INFO_NUMS,
}Info_Type_t;

typedef enum _des_type_t
{
    DES_ECB_ENCRYPT,
    DES_ECB_DECRYPT,
    DES_CBC_ENCRYPT,
    DES_CBC_DECRYPT,
#if defined(CFG_AES_128 ) || defined (CFG_AES_256)
    DES_AES_ECB_ENCRYPT,
    DES_AES_ECB_DNCRYPT,

    DES_AES_CBC_ENCRYPT,
    DES_AES_CBC_DECRYPT,
#endif
}Des_Type_t;

typedef enum _sha_type_t
{
    SHA_1,    // 120 bits
    SHA_128,  //md5  128bits
	SHA_224,
    SHA_256,
    SHA_384,
    SHA_512,
}Sha_Type_t;

typedef enum
{
    RSA_PKCS1_ES_OAEP_SHA1_ENCRYPT,
}RSA_Others_Type_t;

/*
    AES计算，默认为ECB
    AES算法数据长度以 16bits对齐，算法不做补位，传入数据前完成补位
    AES密钥长度有三种：128bits(16bytes)、192bits(24bytes)、256bits(32bytes)
*/
typedef enum
{
    AES_ENCRYPT,
    AES_DECRYPT,

    AES_ECB_ENCRYPT,
    AES_ECB_DECRYPT,
    AES_CBC_ENCRYPT,
    AES_CBC_DECRYPT,

}AES_Type_t;

/*
    SM 是由中国国家安全局出的算法(算法支持，目前没用到，不实现)
    SM3：相当于SHA-256，输入数据最终得到256bits的输出。
    SM4: 对称加密，密钥长度为128bits
*/
typedef enum
{
    SM3,
    SM4_ECB_ENCRYPT,
    SM4_ECB_DECRYPT,
    SM4_CBC_ENCRYPT,
    SM4_CBC_DECRYPT,

}SM_Type_t;

/* ECC Signature ALG TYPE */
typedef enum
{
    ECC_ALG_224BIT,
    ECC_ALG_256BIT,
    ECC_ALG_384BIT,
    ECC_ALG_521BIT,

}ECC_Alg_Type_t;

typedef enum _timer_type_t
{
    TimerMsStart,
    TimerMsStop,
    TimerSecStart,
    TimerSecStop,
    TimerUsCountStart,
    TimerUsCountStop,
    TimerUsCountGet,
}Timer_Type_t;

typedef enum _inout_t
{
    IO_IN,
    IO_OUT,
}In_Out_t;

typedef enum _os_msg_id_t
{
	OS_MSG_ID_APP1 = 1,
	OS_MSG_ID_APP2,
	OS_MSG_ID_APP3
}Os_Msg_Id_t;

typedef enum _os_flag_group_t
{
	OS_FLAG_GROUP_SYS = 0,
	OS_FLAG_GROUP_APP,
}Os_Flag_Group_t;

typedef enum _sys_event_t
{
	SYS_EVENT_KBD_PRESSED = (int)BIT(31),  //按键事件
	SYS_EVENT_USB_CONNECT = BIT(30),  //usb状态改变事件
	SYS_EVENT_BT_CONNECT = BIT(29),   //蓝牙状态改变事件
	SYS_EVENT_ICC_INSERT = BIT(28),   //ic在为改变事件
	SYS_EVENT_RTC_PERSECOND = BIT(27),//rtc秒中断事件
	SYS_EVENT_MAG_INSERT = BIT(26),   //磁卡刷卡事件
	SYS_EVENT_NFC_INSERT = BIT(25),   //nfc在位事件
	SYS_EVENT_CHARGE_STATUS = BIT(24),//充电状态改变事件
	SYS_EVENT_WIFI_CONNECT = BIT(23), // wifi 连接状态改变事件(事件用于通知系统)
	SYS_EVENT_BT_LINK_KEY_CONFIRM = BIT(22), // BT 配对事件，配对密钥需要显示并等待确认
	SYS_EVENT_BT_LINK_KEY_GET = BIT(21), //  BT 配对事件，配对密钥需要由设备输入
	SYS_EVENT_BT_LINK_KEY_CANCEL = BIT(20), //  BT 配对取消事件

	SYS_EVENT_USB_STATUS = BIT(19),   //usb连接状态
	SYS_EVENT_BT_STATUS = BIT(18),    //蓝牙连接状态
	SYS_EVENT_CHARGING_STATUS = BIT(17),//充电状态
	SYS_EVENT_KBD_BEEP = BIT(16),     //按键蜂鸣状态
	SYS_EVENT_WIFI_AP_STATUS = BIT(15),    // wifi 连接 路由 状态
	SYS_EVENT_WIFI_DES_STATUS = BIT(14),    // wifi 连接 目标地址 状态
	SYS_EVENT_SYSTEM_TAMPERED_STATUS = BIT(13),    // 设置系统自毁 状态
	SYS_EVENT_TP_TOUCH = BIT(12),    // TP触摸事件

	SYS_EVENT_SYSTEM_ADC_USED = BIT(1),        // 用于标志ADC冲突时，例如：刷卡与采集电量ADC冲突
	SYS_EVENT_GROUP_APP = BIT(0),     //应用事件组有事件触发，由应用控制
}Sys_Event_t;

typedef enum
{
	DEV_TIMER_ID_KBD,
	DEV_TIMER_ID_WAIT_CARD,
	DEV_TIMER_ID_ICC_CARD,
	DEV_TIMER_ID_NFC_CARD,
	DEV_TIMER_ID_NFC_PROTECT,
	DEV_TIMER_ID_MIFIRE_POLL,
	DEV_TIMER_ID_USB_CHGI,
	DEV_TIMER_ID_BT_CONN,
	DEV_TIMER_ID_BT_CONF,
	DEV_TIMER_ID_PWR_MANAGE,
	DEV_TIMER_ID_GPRS_STATE,
    DEV_TIMER_ID_FELICA_POLL,
    DEV_TIMER_ID_WIFI_ANALYSIS_DATA,

	DEV_TIMER_ID_NUMS,
}Dev_Timer_Id_t;

typedef enum
{
	LCD_NORMAL,
	LCD_REVERSE,
	LCD_TRANS,
    LCD_MODE_COLOR,
}Dev_Lcd_Mode_t;

typedef enum
{
	BATT_LEVEL_ADC_SHUTDOWN,
	BATT_LEVEL_ADC_PERCENT0,
	BATT_LEVEL_ADC_PERCENT25,
	BATT_LEVEL_ADC_PERCENT50,
	BATT_LEVEL_ADC_PERCENT75,
	BATT_LEVEL_ADC_PERCENT100,

	BATT_LEVEL_ADC_NUMS
}Batt_Level_Adc_t;

typedef enum
{
	TP_EVENT_NONE,
	TP_EVENT_DOWN,
    TP_EVENT_UP,
}Tp_Event_t;

typedef  struct _drv_param_t
{
	Dev_ID_t DrvID;
	pvoid pValue;
}Drv_Param_t, *pDrv_Param_t;

typedef  struct _drv_jump_t
{
	u32 Addr;
}Drv_Jump_t, *pDrv_Jump_t;


typedef enum
{
    LED_DIR_ALL,
	LED_DIR_LEFT,
	LED_DIR_RIGHT,
	LED_DIR_UP,
    LED_DIR_DOWN,

}Dev_Led_Dir_t;

#define LARK_RGB565_COLOR(r, g, b) ((unsigned int)((r << 11) | (g << 5) | b))

typedef struct _led_oper_t
{
	Switch_t Status;
    u8 Direction;
	Color_t Color;
}Led_Oper_t,*pLed_Oper_t;

typedef struct _led_breath_oper_t
{
	Switch_t Status;
 	bool BreathEnable;
    bool BreathAsc;			/* Ascending/fade in (true), descending/fade out (false) */
    u8 Direction;
    u16 BreathPeriod;
	u16 Color;
}Led_Breath_Oper_t,*pLed_Breath_Oper_t;

typedef struct _port_oper_t
{
    Rw_t RwStatus;
	Port_t Port;
	Switch_t Status;
    pu8  pValue;
}Port_Oper_t,*pPort_Oper_t;

typedef struct _buzzer_poer_t
{
     Switch_t Status;
     bool ConfigEnable;
     u32 CfgFrequece_HZ; /* 单位hz*/
     u32 CfgDuty;        /* 占空比% */
}Buzzer_Oper_t,*pBuzzer_Oper_t;

typedef struct _flash_oper_t
{
	u32 Addr;
	u32 Len;
	pu8 Buf;
}Flash_Oper_t,*pFlash_Oper_t;

typedef struct _nvram_oper_t
{
	u32 Addr;
	u32 Len;
	pu8 Buf;
}Nvram_Oper_t,*pNvram_Oper_t;

typedef struct _rtc_oper_t
{
    u32 RtcCount; //RTC的真实计数值
	pu8 Buf;  // format: yyyymmddhhmmss
}Rtc_Oper_t,*pRtc_Oper_t;

typedef struct _upd_generate_t
{
    u32 Len;
    pu8 Buf;
}Upd_Generate_t,*pUpd_Generate_t;

typedef struct _upd_verify_t
{
    u32 Len;
    pu8 Buf;
}Upd_Verify_t,*pUpd_Verify_t;

typedef struct _upd_enter_t
{
   Host_Type_t Type;
   u32  FirmWareAddr;
   u32  FirmWareLen; /* 以K为单位，1k对其 */
}Upd_Enter_t,*pUpd_Enter_t;

typedef struct _kbd_read_t
{
    Key_Num_t  KeyNum;
}Kbd_Read_t,*pKbd_Read_t;

typedef struct _kbd_backlight_t
{
    Switch_t Status;
}Kbd_Backlight_t,*pKbd_Backlight_t;

typedef struct _PnsKeyMap_t
{
    /* Pin On Screen Key Map */
    u16 Key; /* Key键值定义 */
    u16 KeyLeftTopPointX;
    u16 KeyLeftTopPointY;
    u16 KeyRightBomPointX;
    u16 KeyRightBomPointY;
}PnsKeyMap_t,*pPnsKeyMap_t;

typedef struct _tp_KeyMap_t
{
    u16 cnt; /* 按键数量 */
    pPnsKeyMap_t pKeyMap;
}Tp_KeyMap_t,*pTp_KeyMap_t;

typedef struct _tp_oper_t
{
	Switch_t Status;
}Tp_Oper_t,*pTp_Oper_t;

typedef struct _tp_pos_t
{
    Tp_Event_t  event;
    u16 x;
    u16 y;
}Tp_Pos_t,*pTp_Pos_t;

typedef struct _com_open_t
{
    Host_Type_t Host;
}Com_Open_t,*pCom_Open_t;

typedef struct _com_write_t
{
    Host_Type_t Host;
    u32 Len;
    pu32 pWStatus;
    pu8 Buf;
}Com_Write_t,*pCom_Write_t;

typedef struct _ComConfig_t
{
    Host_Type_t Host;
    pvoid pValue;
}ComConfig_t,*pComConfig_t;

typedef struct _com_close_t
{
    Host_Type_t Host;
}Com_Close_t,*pCom_Close_t;

typedef struct _GPSConfig_t
{
    pvoid pValue;
}GPSConfig_t,*pGPSConfig_t;

typedef struct _card_reader_open_t
{
    Card_Type_t Type;
}Card_Reader_Open_t,*pCard_Reader_Open_t;

typedef struct _card_reader_close_t
{
    Card_Type_t Type;
}Card_Reader_Close_t,*pCard_Reader_Close_t;

typedef struct _card_wait_t
{
    Card_Type_t Type;
    u32 Atr_Len;
    pu8 Atr_Buf;
    u32 Uid_Len;
    pu8 Uid_Buf;
    u32 Atqa_len;
    pu8 Atqa_buf;
	u8 card_sak;
    bool (*CallBack_Cancel)(void );
	u32 IfuncRet;
	Poll_Type_t PollType;
}Card_Wait_t,*pCard_Wait_t;

typedef struct _card_rw_t
{
    Card_Type_t Type;
    u32 Rw_Len;
    pu8 Rw_Buf;
    bool (*pApduCallBack_Cancel)(void);
    u32 ResultCode;
    u32 TimeOut;
}Card_Rw_t,*pCard_Rw_t;

typedef struct _card_check_t
{
    Card_Type_t Type;
}Card_Check_t,*pCard_Check_t;

typedef struct _lcd_draw_line_t
{
    u32 x1;
    u32 y1;
    u32 x2;
    u32 y2;
    u32 width;
    u32 color;
}Lcd_Draw_Line_t,*pLcd_Draw_Line_t;

typedef enum
{
    LCD_SIGN_INIT,
    LCD_SIGN_WRITE,
    LCD_SIGN_CLEAR,
    LCD_SIGN_READ_POINT,
}LcdSignType_t;

typedef struct _lcd_sign_t
{
    LcdSignType_t SignType;
    pvoid pValue;
}Lcd_Sign_t,*pLcd_Sign_t;

typedef struct _lcd_write_t
{
	Dev_Lcd_Mode_t Mode;
    u32 x;
    u32 y;
    u32 w;
    u32 h;
    pu8 Buf;
    u32 color;
}Lcd_Write_t,*pLcd_Write_t;

typedef struct _lcd_read_t
{
    u32 x;
    u32 y;
    u32 w;
    u32 h;
    pu8 Buf;
}Lcd_Read_t,*pLcd_Read_t;

typedef struct _lcd_erase_t
{
    u32 x;
    u32 y;
    u32 w;
    u32 h;
}Lcd_Erase_t,*pLcd_Erase_t;

typedef struct _lcd_backlight_t
{
    Switch_t Status;
}Lcd_Backlight_t,*pLcd_Backlight_t;

/********************************************************************
                    Camera struct
**********************************************************************/
typedef enum
{
    CAM_CFG_FLASH_SET,
}CamConfigType_t;

typedef struct _ComCamConfig_t
{
    CamConfigType_t CfgType;
    pvoid pValue;
}ComCamConfig_t,*pComCamConfig_t;

typedef struct _info_cpu_id_t
{
    u8  length;
    u8  pvalue[24];
}Info_Cpu_Id_t,*pInfo_Cpu_Id_t;

typedef struct _info_dev_id_t
{
    u8  length;
    u8  pvalue[24];
}Info_Dev_Id_t,*pInfo_Dev_Id_t;

typedef struct _info_pcb_id_t
{
    u8  length;
    u8  pvalue[24];
}Info_Pcb_Id_t,*pInfo_Pcb_Id_t;

typedef struct _Info_Fw_Version_t
{
    u8  length;
    u8  pvalue[25];
}Info_Fw_Version_t,*pInfo_Fw_Version_t;


typedef struct _info_hw_version_t
{
    u8  length;
    u8  pvalue[24];
}Info_Hw_Version_t,*pInfo_Hw_Version_t;

typedef struct _info_os_version_t
{
    u8  length;
    u8  pvalue[24];
}Info_Os_Version_t,*pInfo_Os_Version_t;

typedef struct _info_dri_version_t
{
    u8  length;
    u8  pvalue[24];
}Info_Dri_Version_t,*pInfo_Dri_Version_t;

typedef struct _info_boot_version_t
{
    u8  length;
    u8  pvalue[24];
}Info_Boot_Version_t,*pInfo_Boot_Version_t;

typedef struct _info_Ota_App_version_t
{
    u8  length;
    u8  pvalue[24];
}Info_Ota_App_Version_t,*pInfo_Ota_App_Version_t;

typedef struct _info_lcd_display_size_t
{
    u32  XSize;
    u32  YSize;
}Info_Lcd_Display_Size_t,*pInfo_Lcd_Display_Size_t;

typedef struct _info_mag_support_t
{
    Switch_t trk1;
    Switch_t trk2;
	Switch_t trk3;
}Info_Mag_Support_t,*pInfo_Mag_Support_t;

typedef struct _info_usb_status_t
{
    Switch_t status;
}Info_Usb_Status_t,*pInfo_Usb_Status_t;

typedef struct _info_bt_status_t
{
    Switch_t status;
}Info_Bt_Status_t,*pInfo_Bt_Status_t;

typedef struct _info_bt_name_t
{
    u8  length;
    u8  pvalue[24];
}Info_Bt_Name_t,*pInfo_Bt_Name_t;

typedef struct _info_bt_type_t
{
    Upd_Dev_Type_t Type;
}Info_Bt_Type_t,*pInfo_Bt_Type_t;

typedef struct _info_bt_macaddr_t
{
    u8  length;
    u8  pvalue[24];
}Info_Bt_Macaddr_t,*pInfo_Bt_Macaddr_t;

typedef struct _info_bt_pincode_t
{
    u8  length;
    u8  pvalue[24];
}Info_Bt_Pincode_t,*pInfo_Bt_Pincode_t;

typedef struct _info_bt_pair_t
{
    u8  length;
    u8  pvalue[24];
}Info_Bt_Pairmode_t,*pInfo_Bt_Pairmode_t;

typedef struct _info_batlvl_t
{
    u8  lvl[4];
}Info_Batlvl_t,*pInfo_Batlvl_t;

typedef struct _info_batadclvl_t
{
    u8  lvl[4];
}Info_Batadclvl_t,*pInfo_Batadclvl_t;

typedef struct _info_charging_status_t
{
    Switch_t status;
}Info_Charging_Status_t,*pInfo_Charging_Status_t;

#if defined(CFG_DISP_VIRTUAL_LED)
typedef enum{
    VIR_LED1,
    VIR_LED2,
    VIR_LED3,
    VIR_LED4,
    VIR_LED5,
    VIR_LED_END,
}VIR_LED;
typedef struct _info_vir_led_t
{
    VIR_LED  vir_led;
    Switch_t led_sw;

}Info_Vir_t,*pInfo_VirLed_t;
#endif

typedef struct _info_kbd_beep_t
{
    Switch_t status;
}Info_Kbd_Beep_t,*pInfo_Kbd_Beep_t;

typedef struct _info_lcd_attribute_t
{
    u8  w[4];
	u8  h[4];
}Info_Lcd_Attribute_t,*pInfo_Lcd_Attribute_t;

typedef struct _info_flash_size_t
{
    u8  size[4];
}Info_Flash_Size_t,*pInfo_Flash_Size_t;

typedef struct _info_xflash_size_t
{
    u8  size[4];
}Info_Xflash_Size_t,*pInfo_Xflash_Size_t;

typedef struct _info_nvram_size_t
{
    u8  size[4];
}Info_Nvram_Size_t,*pInfo_Nvram_Size_t;

typedef struct _info_batt_param_t
{
	u8  param[BATT_LEVEL_ADC_NUMS*4];
}Info_Batt_Param_t,*pInfo_Batt_Param_t;

typedef struct _info_sys_status_t
{
	u8  task1_deep[4];
	u8  task1_size[4];
	u8  task2_deep[4];
	u8  task2_size[4];
	u8  tasksys_deep[4];
	u8  tasksys_size[4];
	u8  cpu_used[4];
	u8  mem1_free[4];
	u8  mem1_size[4];
	u8  mem2_free[4];
	u8  mem2_size[4];
	u8  mem3_free[4];
	u8  mem3_size[4];
	u8  mem4_free[4];
	u8  mem4_size[4];
	u8  mem5_free[4];
	u8  mem5_size[4];
	u8  mem6_free[4];
	u8  mem6_size[4];
	u8  mem7_free[4];
	u8  mem7_size[4];
	u8  mem8_free[4];
	u8  mem8_size[4];
}Info_Sys_Status_t,*pInfo_Sys_Status_t;

typedef struct _info_mag_key_t
{
    u8  len;
	u8  old_key[24];
	u8  new_key[24];
}Info_Mag_Key_t,*pInfo_Mag_Key_t;

typedef struct _info_chip_dev_map_t
{
    u8  map[24];
}Info_Chip_Dev_Map_t,*pInfo_Chip_Dev_Map_t;

typedef struct _info_part_upgrade_key_t
{
    u8  length;
    u8  pvalue[24];
}Info_Part_Upgrade_Key_t,*pInfo_Part_Upgrade_Key_t;

typedef struct _info_batt_charge_ctrl_t
{
    /*
       pStatus:
       0:BATT_CTRL_CHARGE_FULL 允许设备充满电
       1:BATT_CTRL_CHARGE_NOT_FULL 不允许设备充满点
       2:BATT_CTRL_CHARGE_40V_FULL 设备最高充到4.0V
    */
    u8 status;
    u8 Ret;
}Info_Batt_Charge_Ctrl_t,*pInfo_Batt_Charge_Ctrl_t;

typedef struct _info_dev_pcba_test_result_t
{
    /*
       Status:
       1: 测试成功
       0：测试失败
       others：没有测试
    */
    u8 status;
}Info_Dev_Pcba_Test_Result_t,*pInfo_Dev_Pcba_Test_Result_t;

typedef struct _info_dev_all_func_test_result_t
{
    /*
       Status:
       1: 测试成功
       0：测试失败
       others：没有测试
    */
    u8 status;
}Info_Dev_All_Func_Test_Result_t,*pInfo_Dev_All_Func_Test_Result_t;

typedef struct _info_nfc_param_t
{
    /*
        ParamRunNow:  1: 参数立即生效，写入非接芯片
                      others：参数只拷贝，不写入非接芯片
        NfcParamLen: read,nfc param buf max len
                     write,nfc param len
    */
    u8  ParamRunNow;
    u32 NfcParamLen;
    pu8 pNfcParamBuf;
}Info_Nfc_Param_t,*pInfo_Nfc_Param_t;

typedef struct _DevComm_t
{
    u32 RetCode;
    pu8 pIBuf;       /* 输入数据 */
    u32 ILen;
    pu32 pOLen;     /* 输出数据 */
    u32 OutMaxLen;
    pu8 pOBuf;
}DevComm_t,*pDevComm_t;

typedef enum
{
    CERT_SERVER,
    CERT_CLIENT,
    CERT_CLIENT_KEY,
}DevCertType_t;


typedef struct _info_part_rgb_breath_led_color_t
{
    u8  length;
    u8  pvalue[24];
}Info_Part_RGB_Breath_Led_Color_t,*pInfo_Part_RGB_Breath_Led_Color_t;


/********************************************************************
                    USB config struct
**********************************************************************/
//用于设备USB接口切换
typedef enum
{
    USB_CONNECT_MCU,
    USB_CONNECT_GPRS,
}UsbConnectType_t;

typedef enum
{
    USB_CFG_CONNECT_SWITCH,
}UsbConfigType_t;

typedef struct _ComUsbConfig_t
{
    UsbConfigType_t CfgType;
    pvoid pValue;
}ComUsbConfig_t,*pComUsbConfig_t;

/********************************************************************
                    WIFI staruct
**********************************************************************/

typedef enum
{
    WIFI_RET_SUCCESS = 0,                           /* execution succeed  */

    /* Error return code */
    WIFI_RET_CMD_PROCESS_FAIL,                      /* execution fail  */
    WIFI_RET_CMD_PROCESS_ERROR,                     /* execution error  */
    WIFI_RET_PARAM,                                 /* cmd param error, length or pbuf illegal  */
    WIFI_RET_ALLOC,                   /* alloc memory fail  */
    WIFI_RET_TIMEOUT_TOO_SHORT,       /* timeout too short  */
    WIFI_RET_CMD_MISS_ARGUMENT,       /* cmd miss argument  */
    WIFI_RET_CMD_PROCESS_TIMEOUT,     /* cmd process timeout  */
    WIFI_RET_CMD_SEND_BUF_OVERFLOW,   /* cmd buf overflow (drivers internal error)  */
    WIFI_RET_CMD_RECV_BUF_OVERFLOW,   /* cmd buf overflow (drivers internal error)  */
    WIFI_RET_CMD_NOT_SUPPORT,         /* cmd not support  10 */

    WIFI_RET_DNS_ANALYSIS_FAIL,       /* DNS analysis fail */
    WIFI_RET_AP_SSID_BUF_IS_NULL,     /* connect ap , ssid pbuf is null */
    WIFI_RET_AP_SSID_LEN_ILLEGAL,     /* connect ap , ssid length illegal,range: 1~32*/
    WIFI_RET_AP_PASSWD_BUF_IS_NULL,   /* connect ap , passwd pbuf is null */
    WIFI_RET_AP_PASSWD_LEN_ILLEGAL,   /* connect ap , passwd length illegal range: 8~32*/
    WIFI_RET_AP_CONN_FAIL,            /* connect ap fail */
    WIFI_RET_AP_DISCONNECT,           /* ap disconnect */
    WIFI_RET_AP_CONN_TIMEOUT,            /* connect ap/server timeout */
    WIFI_RET_AP_CONN_PASSWD,             /* connect ap passwd error */
    WIFI_RET_AP_CONN_AP_NOT_FIND,        /* connect ap, ap not find 20*/

    WIFI_RET_SERVER_PORT_BUF_IS_NULL, /* connect server , port pbuf is null */
    WIFI_RET_SERVER_PORT_LEN_ILLEGAL, /* connect server , port length illegal,range: >0 */
    WIFI_RET_SERVER_IP_BUF_IS_NULL,     /* connect server , ip pbuf is null */
    WIFI_RET_SERVER_IP_LEN_ILLEGAL,     /* connect server , ip length illegal, range: >0 */
    WIFI_RET_SERVER_NOT_LINK,         /* server not link, operate error, need connect server first */

    WIFI_RET_SERVER_LINK_INVALID,     /* server link invalid */
    WIFI_RET_SERVER_CLOSED,           /* server have closed */
    WIFI_RET_SERVER_CONN_FAIL,        /* server connect fail */
    WIFI_RET_SERVER_RECV_BUF_OVERFLOW,       /* Recv buf overflow */
    WIFI_RET_SERVER_SEND_BUF_IS_NULL,        /* send buf is null */

    WIFI_RET_SERVER_SEND_LEN_ILLEGAL,        /* send data length illegal,range: 1~4096 */
    WIFI_RET_SERVER_SEND_FAIL,               /* send data fail */
    WIFI_RET_SERVER_READ_FAIL,               /* RECV data fail */
    WIFI_RET_CERT_LEN_EXCEED_MAX_MIN, /* Cert len exceed max or min, range: 1~4096 */
    WIFI_RET_CERT_LOAD_FAIL,          /* Cert len exceed max or min, range: 1~4096 */

    WIFI_RET_CMD_RX_DATA_ANALYSIS,        /* rx data analysis error (drivers internal error) */
    WIFI_RET_CMD_RX_DATA_CMD_CLASS,       /* rx data analysis error (drivers internal error) */
    WIFI_RET_INITIALIZING,         /* init not finish , wiat few sec try again */
    WIFI_RET_BUSY,                    /* wifi busy, wait few sec try again */
    WIFI_RET_CMD_ANALYSIS_FAIL,       /* cmd response analysis fail */

    WIFI_RET_CMD_HEAD,
    WIFI_RET_FWHW_NOT_MATCH,

    WIFI_RET_GET_VER_BUF_IS_NULL,
    WIFI_RET_GET_VER_LEN_ILLEGAL,
    WIFI_RET_AP_LIST_BUF_IS_NULL,
    WIFI_RET_AP_LIST_LEN_ILLEGAL,
    WIFI_RET_CONNECTED,
    WIFI_RET_RECV_BUF_NO_DATA,
    WIFI_RET_TRANSMISSING_NOT_DEAL_CMD,  /* transmiss status , not deal any AT Cmd */
    WIFI_RET_UNKNOW    /* unknow error */
}WifiRet_t;

typedef enum
{
    WIFI_CONN_TCP = 0,
    WIFI_CONN_UDP,
    WIFI_CONN_SSL,
}WifiConnType_t;

typedef enum
{
    WIFI_MODE_STA = 1,   //wifi 基站模式（只能去连接别人）
    WIFI_MODE_SOFT_AP,   // wifi为热点模式，
    WIFI_MODE_AP_STA     // wifi即为热点，又为基站
}WifiWorkMode_t;

typedef enum
{
    WIFI_CFG_CONN_AP,
    WIFI_CFG_CONN_DES,
    WIFI_CFG_DISCONN_AP,
    WIFI_CFG_DISCONN_DES,

    WIFI_CFG_SET_SSL_CERTIFICATION_MODE,
    WIFI_CFG_GET_SSL_CERTIFICATION_MODE,
    WIFI_CFG_LOAD_SSL_CERTIFICATION,

    WIFI_CFG_GET_VERSION,
    WIFI_CFG_QUEUE_BUF_SIZE,
    WIFI_CFG_SET_SNTPTIME,
    WIFI_CFG_GET_SNTPTIME,
    WIFI_CFG_SEARCH_AP,
    WIFI_CFG_GET_LINK_AP_INF,
    WIFI_CFG_GET_IP_ADDR,
    WIFI_CFG_GET_MAC_ADDR,
    WIFI_CFG_SET_MAC_ADDR,
    WIFI_CFG_GET_WORK_MODE,
    WIFI_CFG_SET_WORK_MODE,
    WIFI_CFG_GET_DHCP_MODE,
    WIFI_CFG_SET_DHCP_MODE,
    WIFI_CFG_GET_HOST_NAME,
    WIFI_CFG_SET_HOST_NAME,
    WIFI_CFG_SET_AUTO_CONN_STATUS,
    WIFI_CFG_SET_DNS,

}WifiConfigType_t;

typedef struct _WifiComm_t
{
    u32 RetCode;
    u32 DataLen;
    u32 BufMaxLen;
    pu8 pBuf;
}WifiComm_t,*pWifiComm_t;

typedef struct _ComWifiConfig_t
{
    WifiConfigType_t CfgType;
    pvoid pValue;
}ComWifiConfig_t,*pComWifiConfig_t;
/*--------------------------------------------------------------------
                    WIFI Staruct End
--------------------------------------------------------------------*/

/********************************************************************
                    BT staruct
**********************************************************************/
typedef enum
{
    BT_CFG_GET_PIN_CODE,
    BT_CFG_SET_PAIR_MODE,
    BT_CFG_SET_PIN_CODE_CONFIRM_RESAULT,
    BT_CFG_PASS_KEY_ENTRY,

}BtConfigType_t;


typedef enum
{
    BT_CHIP_TYPE_NONE,
    BT_CHIP_TYPE_YC1021W,
    BT_CHIP_TYPE_YC1021J,

    BT_CHIP_TYPE_END
} BtChipType_t;

typedef struct _BtComm_t
{
    u32 RetCode;
    pu8 pIBuf;       /* BT输入数据 */
    u32 ILen;
    pu32 pOLen;     /* BT输出数据 */
    u32 OutMaxLen;
    pu8 pOBuf;
}BtComm_t,*pBtComm_t;

typedef struct _ComBtConfig_t
{
    BtConfigType_t CfgType;
    pvoid pValue;
}ComBtConfig_t,*pComBtConfig_t;
/********************************************************************
                    GPRS struct
**********************************************************************/
typedef enum
{
    GPRS_CONN_TCP = 0,
    GPRS_CONN_UDP,
}GprsConnType_t;

typedef enum _gprs_cmd_type_t
{
    GPRS_CMD_VERSION,
    GPRS_CMD_STATUS,
    GPRS_CMD_SIM_INFO,
    GPRS_CMD_USB_UPDATE,
    GPRS_CMD_OTA_UPDATE,

    GPRS_CMD_NUMS,
}Gprs_Cmd_Type_t;

typedef enum
{
    AUTH_NONE,
    AUTH_SERVER,
    AUTH_SERVER_CLIENT,
    AUTH_CLIENT,
}SslAuthMode_t;

typedef enum
{
    GPRS_CFG_VERSION_GET,
    GPRS_CFG_MODEL_ID_GET,
    GPRS_CFG_CARD_STATUS_GET,
    GPRS_CFG_NET_REGISTER_GET,
    GPRS_CFG_SIGNAL_LEVEL_GET,
    GPRS_CFG_IMEI_GET,
    GPRS_CFG_IMSI_GET,
    GPRS_CFG_ICCID_GET,
    GPRS_CFG_SET_APN,
    GPRS_CFG_HTTP_SERVER_CFG,
    GPRS_CFG_HTTP_GET,
    GPRS_CFG_CA_UPDATE,
    GPRS_CFG_CA_DELETE,
    GPRS_CFG_USB_UPDATE_BEGIN,
    GPRS_CFG_USB_UPDATE_END,
    GPRS_CFG_OTA_UPDATE_BEGIN,
    GPRS_CFG_OTA_UPDATE_END,
    GPRS_CFG_CONN_DES,
    GPRS_CFG_DISCONN_DES,
    GPRS_CFG_CONN_STATUS_GET,
}GprsConfigType_t;

typedef struct _ComGprsConfig_t
{
    GprsConfigType_t CfgType;
    pvoid pValue;
}ComGprsConfig_t,*pComGprsConfig_t;

typedef struct
{
    u32 ApnNameLen; //运营商名称长度
    u8 ApnName[32];
    u32 ApnLen;     // APN
    u8 Apn[32];
    u32 UserNameLen;  // 用户名
    u8 UserName[32];
    u32 UserPasswdLen;  // 用户密码
    u8 UserPasswd[32];
    u8 Mcc[5]; //3个数字
    u8 Mnc[5];  // 2~3个数字
}ApnParam_t,*pApnParam_t;

/********************************************************************
                    GPS struct
**********************************************************************/
typedef enum
{
    GPS_CFG_SET_RESTART = 1,
    GPS_CFG_CLEAR_LOCATION,
    GPS_CFG_SET_STANDBY,
    GPS_CFG_SET_MESSAGE_TIME,
    GPS_CFG_SET_PERIOD_LOWPOWER,
    GPS_CFG_SET_QZSS_NMEA,
    GPS_CFG_SET_QZSS,
    GPS_CFG_SET_FIND_STAR_MODE,
    GPS_CFG_SET_UART_BAUDRATE,
    GPS_CFG_SET_PPS,
    GPS_CFG_QUERY_MESSAGE_TIME,
    GPS_CFG_SET_SBAS,
    GPS_CFG_QUERY_SBAS,
    GPS_CFG_SET_NEMA_STATEMENT,
    GPS_CFG_QUERY_SOFTWARE_VERSION,
    GPS_CFG_SET_PROBABLE_LOCATION,
    GPS_CFG_QUERY_VALID_DATALEN,
    GPS_CFG_QUERY_VALID_DATA,
    GPS_CFG_SET_UART_SWITCH,
    GPS_CFG_SET_ANTENNA_TEST,
    GPS_CFG_UNINIT,

}GpsConfigType_t;

typedef struct _ComGpsConfig_t
{
    GpsConfigType_t CfgType;
    pvoid pValue;
}ComGpsConfig_t,*pComGpsConfig_t;

/*--------------------------------------------------------------------
                    SPI Staruct
--------------------------------------------------------------------*/
typedef enum
{
    SPI_RET_SUCCESS = 0,
    SPI_RET_TIMEOUT,
    SPI_FIFO_OVERFLOW,
}SpiRet_t;

typedef struct _emv_config_info_t
{
    u32 EMV_AppAddr;
    u32 EMV_AppSize;
    u32 EMV_CapkAddr;
    u32 EMV_CapkSize;
}EMV_ConfigInfo_t,*pEMV_ConfigInfo_t;

typedef struct _DevDestructStatus_t
{
    pu8 pStatusBuf;
    u32 BufMaxLen;
    pu32 pDataLen;
}DevDestructStatus_t,*pDevDestructStatus_t;

typedef struct _DevChargeForceCtrl_t
{
    Switch_t status;
}DevChargeForceCtrl_t,*pDevChargeForceCtrl_t;

typedef struct _sys_info_t
{
    Info_Type_t Type;
    pvoid pValue;
}Sys_Info_t,*pSys_Info_t;

typedef struct _alg_rand_t
{
    u32 Len;
    pu8 Buf;
}Alg_Rand_t,*pAlg_Rand_t;

typedef struct _alg_rsa_decrypt_t
{
    pu8 N;
    u32 NLen;
    pu8 D;
    u32 DLen;
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Rsa_Decrypt_t,*pAlg_Rsa_Decrypt_t;

typedef struct _alg_rsa_encrypt_t
{
    pu8 N;
    u32 NLen;
    pu8 E;
    u32 ELen;
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Rsa_Encrypt_t,*pAlg_Rsa_Encrypt_t;

typedef struct _alg_ecc_signature_t
{
    ECC_Alg_Type_t EccType;
    pu8 PrivateKey;
    u16 PrivateKeyLen;
    pu8 IBuf;
    u16 ILen;
    pu8 RSBuf;
    pu16 pRSLen;
}Alg_ECC_Signature_t,*pAlg_ECC_Signature_t;

typedef struct _alg_ecc_verify_signature_t
{
    ECC_Alg_Type_t EccType;
    pu8 PublicKey;
    u16 PublicKeyLen;
    pu8 IBuf;
    u16 ILen;
    pu8 RSBuf;
    u16 RSLen;
}Alg_ECC_Verify_Signature_t,*pAlg_ECC_Verify_Signature_t;

typedef struct _alg_rsa_others_t
{
    RSA_Others_Type_t Mode;
    pu8 N;
    u32 NLen;
    pu8 E;
    u32 ELen;
    pu8 D;
    u32 DLen;
    pu8 Label;
    u32 LabelLen;
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Rsa_Others_t,*pAlg_Rsa_Others_t;

typedef enum{
    N_HEAD_HIGH_BYTE_0B0000_AND_NO_CHECK_MODE = 0x0,
    N_HEAD_HIGH_BYTE_0B1000_AND_NO_CHECK_MODE = 0x1,
    N_HEAD_HIGH_BYTE_0B1100_AND_NO_CHECK_MODE = 0x3,
    N_HEAD_HIGH_BYTE_0B0000_AND_CHECK_MODE = 0x4,
    N_HEAD_HIGH_BYTE_0B1000_AND_CHECK_MODE = 0x5,
    N_HEAD_HIGH_BYTE_0B1100_AND_CHECK_MODE = 0x7,

}Alg_Rsa_Generate_Key_Mode_t;

typedef struct _alg_rsa_generate_key_t
{
    pu8 N;
    pu8 D;
    pu8 E;
    u32 ELen;
    u32 KeyBits;
    Alg_Rsa_Generate_Key_Mode_t Mode;
}Alg_Rsa_Generate_Key_t,*pAlg_Rsa_Generate_Key_t;

typedef struct _alg_sm_decrypt_t
{
	pvoid value;
}Alg_Sm_Decrypt_t,*pAlg_Sm_Decrypt_t;

typedef struct _alg_sm_encrypt_t
{
	pvoid value;
}Alg_Sm_Encrypt_t,*pAlg_Sm_Encrypt_t;

typedef struct _alg_sm_generate_key_t
{
	pvoid value;
}Alg_Sm_Generate_Key_t,*pAlg_Sm_Generate_Key_t;

typedef struct _alg_des_t
{
    Des_Type_t Type;
    pu8 Key;
    u32 KeyLen;
    pu8 Fill; /* 8 bytes */
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Des_t,*pAlg_Des_t;


typedef struct _alg_aes_t
{
    AES_Type_t Type;
    pu8 Key;
    u32 KeyLen;
	pu8 Fill; /* 16 bytes */
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Aes_t,*pAlg_Aes_t;


typedef struct _alg_sm_t
{
    SM_Type_t Type;
    pu8 Key;
    u32 KeyLen;
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
}Alg_Sm_t,*pAlg_Sm_t;

typedef enum{
    ALG_SHA_IOCTL_INIT,
    ALG_SHA_IOCTL_UPDATE,
    ALG_SHA_IOCTL_FINAL,
    ALG_SHA_IOCTL_COMPLETA,/* 完整计算 */
}Alg_Sha_IoCtl_t;

typedef struct _alg_sha_t
{
    Sha_Type_t Type;
    pu8 IBuf;
    u32 ILen;
    pu8 OBuf;
    Alg_Sha_IoCtl_t Alg_Sha_IoCtl;
}Alg_Sha_t,*pAlg_Sha_t;

typedef enum{
    ALG_MD5_IOCTL_INIT,
    ALG_MD5_IOCTL_UPDATE,
    ALG_MD5_IOCTL_FINAL,
    ALG_MD5_IOCTL_COMPLETA,/* 完整计算MD5 */
}Alg_Md5_IoCtl_t;

typedef struct
{
    unsigned int count[2];
    unsigned int state[4];
    unsigned char buffer[64];
}Alg_Md5_Ctx_t, *pAlg_Md5_Ctx_t;

typedef struct _alg_md5_t
{
    pu8 IBuf;
    pu8 OBuf;
    u32 ILen;
    pAlg_Md5_Ctx_t pMD5_CTX;
    Alg_Md5_IoCtl_t IoCtl;
}Alg_Md5_t,*pAlg_Md5_t;

typedef struct _alg_crc_t
{
	u32 init;
    pu8 IBuf;
    u32 ILen;
    u32 Vector;
}Alg_Crc_t,*pAlg_Crc_t;

typedef struct _delay_ms_t
{
    u32 Ms;
}Delay_Ms_t,*pDelay_Ms_t;

typedef struct _delay_us_t
{
    u32 Us;
}Delay_Us_t,*pDelay_Us_t;

typedef struct _timer_t
{
    Timer_Type_t Type;
    u32 TimerId;
    u32 Time;
    void (*CallBack)(pvoid);
    pvoid Param; /* 参数传给 callback */
}Timer_t,*pTimer_t;

typedef struct _mem_alloc_t
{
    pvoid pBuf;
    u32 BytesLen;
}Mem_Alloc_t,*pMem_Alloc_t;

typedef struct _mem_free_t
{
    pvoid pBuf;
}Mem_Free_t,*pMem_Free_t;

typedef struct _os_send_msg_t
{
    Os_Msg_Id_t MsgId;
    pmessage_body_t Msg;
}Os_Send_Msg_t,*pOs_Send_Msg_t;

typedef struct _os_wait_msg_t
{
    Os_Msg_Id_t MsgId;
    pmessage_body_t Msg;
    u32 TimeOutMs;
}Os_Wait_Msg_t,*pOs_Wait_Msg_t;

typedef struct _os_query_msg_t
{
    Os_Msg_Id_t MsgId;
    pmessage_body_t Msg;
    u32 Size;
}Os_Query_Msg_t,*pOs_Query_Msg_t;

typedef struct _os_flush_msg_t
{
    Os_Msg_Id_t MsgId;
}Os_Flush_Msg_t,*pOs_Flush_Msg_t;

typedef struct _os_set_flag_t
{
    Os_Flag_Group_t FlagGroup;
    u32 Flags;
}Os_Set_Flag_t,*pOs_Set_Flag_t;

typedef struct _os_clear_flag_t
{
    Os_Flag_Group_t FlagGroup;
    u32 Flags;
}Os_Clear_Flag_t,*pOs_Clear_Flag_t;

typedef struct _os_wait_any_flag_t
{
    Os_Flag_Group_t FlagGroup;
    u32 Flags;
    u32 TimeOutMs;
}Os_Wait_Any_Flag_t,*pOs_Wait_Any_Flag_t;

typedef struct _os_wait_all_flag_t
{
    Os_Flag_Group_t FlagGroup;
    u32 Flags;
    u32 TimeOutMs;
}Os_Wait_All_Flag_t,*pOs_Wait_All_Flag_t;

typedef struct _os_flag_is_true_t
{
    Os_Flag_Group_t FlagGroup;
    u32 Flags;
}Os_Flag_Is_True_t,*pOs_Flag_Is_True_t;

typedef struct _os_sleep_t
{
    u32 ms;
}Os_Sleep_t,*pOs_Sleep_t;

typedef struct _os_thread_t
{
    void (*handler)(pvoid);
	pvoid param;
}Os_Thread_t,*pOs_Thread_t;

typedef struct _fifo_buffer_t
{
	pvoid buffer; //队列缓存地址
	u32   width;  //队列元素宽度
	u32   length; //队列元素个数
	u32   incnt;  //队列输入元素标号
	u32   outcnt; //队列输出元素标号
	pvoid iobuffer;//队列输入输出缓存
	u32   ioctrl; //队列输入输出控制
}Fifo_Buffer_t,*pFifo_Buffer_t;

typedef struct _filo_buffer_t
{
	pvoid buffer; //栈缓存地址
	u32   width;  //栈元素宽度
	u32   length; //栈元素个数
	u32   iocnt;    //栈输入输出元素标号
	pvoid iobuffer; //栈输入输出缓存
}Filo_Buffer_t,*pFilo_Buffer_t;

typedef struct _keys_oper_t
{
	u32 Addr;
	u32 Len;
	pu8 Buf;
}Keys_Oper_t,*pKeys_Oper_t;

typedef enum
{
    PRINTER_OPER_START = 0,

    PRINTER_OPER_INIT = PRINTER_OPER_START,
    PRINTER_OPER_OPEN,
    PRINTER_OPER_CLOSE,
    PRINTER_OPER_ATTR_SET,
    PRINTER_OPER_ATTR_GET,
    PRINTER_OPER_PAPER_LINE,
    PRINTER_OPER_PRINT_STRING,
    PRINTER_OPER_PRINT_BITMAP,

    PRINTER_OPER_NUMS,
}PrinterOperCode_t;

typedef enum
{
    PRINTER_ERR_START = 0,

    PRINTER_ERR_SUCCESS = PRINTER_ERR_START,
    PRINTER_ERR_FAILED,

    PRINTER_ERR_END,
}PrinterErrCode_t;

typedef struct _printer_oper_t
{
    PrinterOperCode_t PrinterOperCode;
    PrinterErrCode_t PrinterErrCode;
	u32 Length;
	pu8 Buffer;
}PrinterOper_t,*pPrinterOper_t;

typedef enum
{
    TWO_DIME_CODE_START = 0,

    TWO_DIME_CODE_PDF417 = TWO_DIME_CODE_START,
    TWO_DIME_CODE_QRCODE,
    TWO_DIME_CODE_DATA_MATRIX,
    TWO_DIME_CODE_MAXI_CODE,
    TWO_DIME_CODE_CODE_49,
    TWO_DIME_CODE_CODE_16K,
    TWO_DIME_CODE_CODE_ONE,

    TWO_DIME_CODE_NUMS,
}TwoDimeCodeMode_t;

typedef struct _two_dimension_code_oper_t
{
    TwoDimeCodeMode_t Mode;
    const char * pCustomString;
    const char * pLinkData;
}TwoDimeCodeOper_t,*pTwoDimeCodeOper_t;


typedef struct _param_boot_t
{
    u32 RomBootValid;
    u8 ManufactureRsaN[256];
    u8 ManufactureRsaE[4];
    u8 Reserved[248];
}ParamBoot_t,*pParamBoot_t;

typedef struct{u8 *head; u32 len;} T_U8_VIEW;
#define UV_OK(uv)	((uv).head && (uv).len)
/*-----------------------------------------------------------------------------
|   NFC return code
+----------------------------------------------------------------------------*/
typedef enum
{
    NFC_EMV_ERROR_BEGIN = 0,

    NFC_EMV_OK = NFC_EMV_ERROR_BEGIN,
    NFC_EMV_INIT,
    NFC_EMV_TOGGLE_CARRIER,
    NFC_EMV_POLL_CARD,
    NFC_EMV_COLLISION,
    NFC_EMV_ACTIVATE,
    NFC_EMV_TRANSMISSION,
    NFC_EMV_PROTOCOL,
    NFC_EMV_REMOVE_CARD,
    NFC_EMV_CHECK_PRESENCE,
    NFC_EMV_TIMEOUT,
    NFC_EMV_INTERNAL,
    NFC_EMV_STOPPED,
    NFC_EMV_MULTI_CARD,

    NFC_EMV_ERROR_END
}NfcRet_t;


enum
{
    VERSION_STATUS_START = 0,
    VERSION_DEBUG,
    VERSION_RELEASE,
};

typedef enum
{
    DESTRUCT_TYPE = 0,
    DESTRUCT_HANDWARE,
    DESTRUCT_SOFTWARE,
}Destruct_type;
/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/
GLOBAL Info_Chip_Dev_Map_t g_InfoChipDevMap;


/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/
typedef struct  _gcm_param_t{
    u8 mode;
    pu8 key;
    u32 keyLen;
    pu8 input;
    u32 inputLen;
    pu8 iv;
    u32 ivLen;
    pu8 out;
    u32 outSize;
    int  code;
}gcm_param_t,*pgcm_param_t;
/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
GLOBAL Rc_t LARK_API(pDrv_Param_t Param);

/*
*******************************************************************************
*   End of File
*******************************************************************************
*/
#undef GLOBAL
/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

#endif  /* #ifndef _LARK_API_H */

