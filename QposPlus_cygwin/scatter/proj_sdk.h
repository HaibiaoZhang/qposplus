/*
********************************************************************************
*
*   File Name:
*       proj_sdk.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _PROJ_SDK_H
#define _PROJ_SDK_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#include "appcode.h"



/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/

#define LCD_LEFT_ALIGNMENT                 (0)
#define LCD_RIGHT_ALIGNMENT                (1)
#define LCD_CENTERED_ALIGNMENT             (2)

#define LCD_LINE_0                         (0)
#define LCD_LINE_1                         (1)
#define LCD_LINE_2                         (2)
#define LCD_LINE_3                         (3)
#define LCD_LINE_4                         (4)
#define LCD_LINE_5                         (5)
#define LCD_LINE_NUMS                      (6)
#define LCD_LINE_ICON                      (4)

#define LCD_DISP_DEVICE_INIT               (0)
#define LCD_DISP_WELCOME                   (1)
#define LCD_DISP_ENGINEERING_MODE          (2)
#define LCD_DISP_DEVICE_DESTRUCT           (3)
#define LCD_DISP_SLEEP_MODE                (4)
#define LCD_DISP_BATT_ICON                 (5)
#define LCD_DISP_USB_STATUS_ICON           (6)
#define LCD_DISP_BT_STATUS_ICON            (7)
#define LCD_DISP_TIME                      (8)
#define LCD_DISP_START_TRANSACTION         (9)
#define LCD_DISP_CHIP_CARD                 (10)
#define LCD_DISP_UPKEY_TO_START_NFC        (11)
#define LCD_DISP_SWIPE_ERROR               (12)
#define LCD_DISP_CHANGE_TO_CONTACT         (13)
#define LCD_DISP_TRANSACTION_TERMINATED    (14)
#define LCD_DISP_REMOVE_CARD               (15)
#define LCD_DISP_RETURN_TO_APP             (16)
#define LCD_DISP_TIMEOUT                   (17)
#define LCD_DISP_RESET                     (18)
#define LCD_DISP_CANCEL                    (19)
#define LCD_DISP_INPUT_ONLINE_PIN          (20)
#define LCD_DISP_PIN_MASK                  (21)
#define LCD_DISP_CONFIRM_IF_NO_PIN         (22)
#define LCD_DISP_CONFIRM_IF_NO_PIN_CLEAR   (23)
#define LCD_DISP_CUSTOMIZE                 (24)
#define LCD_DISP_OFFLINE_BALANCE           (25)
#define LCD_DISP_READING_CARD              (26)
#define LCD_DISP_GO_ONLINE                 (27)
#define LCD_DISP_INPUT_AMOUNT              (28)
#define LCD_DISP_TRANS_COUNTER             (29)
#define LCD_DISP_WIFI_STATUS_ICON          (30)
#define LCD_DISP_INPUT_PIN_AGAIN           (31)
#define LCD_DISP_INPUT_OFFLINE_PIN         (32)
#define LCD_DISP_INPUT_LAST_OFFLINE_PIN    (33)
#define LCD_DISP_TEST_WHITE                (34)
#define LCD_DISP_TEST_BLACK                (35)
#define LCD_DISP_TEST_BACKLIGHT            (36)
#define LCD_DISP_TEST_LED_BLUE             (37)
#define LCD_DISP_TEST_LED_RED              (38)
#define LCD_DISP_TEST_BUZZER               (39)
#define LCD_DISP_TEST_LED_YELLOW           (40)
#define LCD_DISP_TEST_LED_GREEN            (41)
#define LCD_DISP_TEST_KEYBOARD             (42)
#define LCD_DISP_TEST_KBD_VALUE            (43)
#define LCD_DISP_GPRS_SIGNAL_LVL           (44)
#define LCD_DISP_PRINT_LINE_BLINK          (45)
#define LCD_DISP_PRINT_AMOUNT_CODE         (46)
#define LCD_DISP_WAITING_FOR_HOST          (47)
#define LCD_DISP_SHUTDOWN                  (48)
#define LCD_DISP_NET_DESTINATION_IP        (49)
#define LCD_DISP_NET_ERROR                 (50)
#define LCD_DISP_NET_COMMUNICATE           (51)
#define LCD_DISP_NET_DATA_RECEPTION        (52)
#define LCD_DISP_NFC_RETAP                 (53)
#define LCD_DISP_SEE_PHONE_PLS             (54)
#define LCD_DISP_BATTERY_ERROR             (55)
#define LCD_DISP_CREDIT_DEBIT              (56)
#define LCD_DISP_CUSTOMIZE_TRANS_DISP      (57)
#define LCD_DISP_DECLINED_DISP             (100)
#define LCD_DISP_TRANSACTION_NOT_ALLOW     (101)
#define LCD_DISP_EMV_APP_BLOCK             (102)
#define LCD_DISP_AGING_TEST_MENU           (103)
#define LCD_DISP_AGING_TEST                (104)
#define LCD_DISP_VERIFY_OFFLINE_PIN        (105)
#define LCD_DISP_PIN_ERROR                 (106)
#define LCD_DISP_FREE_MSG                  (107)
#define LCD_DISP_CLERAD_ALL_LINE           (108)
#define LCD_DISP_CLERAD_LINE               (109)
#define LCD_DISP_USE_NFC                   (110)
#define LCD_DISP_PCI_VERSION               (111)
#define LCD_DISP_CHANGE_TO_MAG             (112)
#define LCD_DISP_FALL_BACK                 (113)
#define LCD_DISP_TRY_ANOTHER_INTERFACE     (114)
#define LCD_DISP_CARD_NOT_SUPPORT          (115)
#define LCD_DISP_NFC_DOUBLE_TAP            (116)


#define NET_ERROR_OK                       (0)
#define NET_ERROR_GPRS_REQ_DATA            (-1)
#define NET_ERROR_GPRS_HANDSHAKE           (-2)
#define NET_ERROR_GPRS_USER_CANCEL         (-3)
#define NET_ERROR_GPRS_FRAME_LENGTH        (-4)
#define NET_ERROR_WIFI_REQ_DATA            (-5)
#define NET_ERROR_WIFI_WRITE_DATA          (-6)
#define NET_ERROR_WIFI_DES_WRITE_DATA      (-7)
#define NET_ERROR_WIFI_DES                 (-8)
#define NET_ERROR_WIFI_USER_CANCEL         (-9)
#define NET_ERROR_WIFI_FRAME_LENGTH        (-10)
#define NET_ERROR_GPRS_CONNECT             (-11)
#define NET_ERROR_REQEST_PARAM             (-12)

#define DUKPT_TYPE_TRK                     (0)
#define	DUKPT_TYPE_EMV                     (1)
#define	DUKPT_TYPE_PIN                     (2)

#define	KEY_GROUP_0                        (0)
#define	KEY_GROUP_1                        (1)
#define	KEY_GROUP_2                        (2)
#define	KEY_GROUP_3                        (3)
#define	KEY_GROUP_4                        (4)
#define	KEY_GROUP_5                        (5)
#define	KEY_GROUP_6                        (6)
#define	KEY_GROUP_7                        (7)
#define	KEY_GROUP_8                        (8)
#define	KEY_GROUP_9                        (9)

#ifdef DBG_TRACE
#define TRACE_VIEW(l, x)	\
	do {	\
		T_U8_VIEW uv = (T_U8_VIEW ){(x).head, (x).len};		\
		TRACE((l), #x": ");									\
		TRACE_VALUE((l), uv.head, uv.len);					\
	}while(0)
#define TRACE_TLV(lvl, tbuf, tag) \
    do { \
		T_U8_VIEW uv = get_tlv_view(tbuf, tag);	\
        TRACE(lvl, #tag"(%08XH) : ", tag);	\
		TRACE_VALUE(lvl, uv.head, uv.len);	\
    } while (0)
#else
#define TRACE_VIEW(l, x)          	((void)0)
#define TRACE_TLV(lvl, tbuf, tag)   ((void)0)
#endif

#define BUFF_CLEAR(s) Memset_(s, 0x00, sizeof(s))
/*-----------------------------------------------------------------------------
|   Enumerations
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
typedef enum
{
    RES_SUCCESS = (0x00),
    RES_MASTKEY_LEN_ERROR,
    RES_MASTKEY_KCV_ERROR,
    RES_PIN_KCV_ERROR,
    RES_PIN_LEN_ERROR,
    RES_TRK_KCV_ERROR,
    RES_TRK_LEN_ERROR,
    RES_MCK_KCV_ERROR,
    RES_MCK_LEN_ERROR,
    RES_TRK_IPEK_LEN_ERROR,
    RES_EMV_IPEK_LEN_ERROR,
    RES_PIN_IPEK_LEN_ERROR,
    RES_TRK_IPEK_ERROR,
    RES_EMV_IPEK_ERROR,
    RES_PIN_IPEK_ERROR,
    RES_AES_KCV_ERROR,
    RES_AES_KEY_LEN_ERROR
}Error_Code_t;

typedef enum
{
    APP_CLEAR = (0x00),
    APP_ADD,
    APP_DELETE,
    APP_GET_LIST,
    APP_MODIFY,
    APP_READ,
    CAPK_CLEAR,
    CAPK_ADD,
    CAPK_DELETE,
    CAPK_GET_LIST,
    CAPK_READ

}Custom_Emv_Ctrl_t;

typedef enum
{
    MKSK_TMK = 0,
    MKSK_PIK,
    MKSK_TDK,
    MKSK_MCK,

    MKSK_NUMS,
}MkSk_Key_t;


typedef enum
{
    DUKPT_TRK = 0,
    DUKPT_EMV,
    DUKPT_PIN,

    DUKPT_NUMS,
}Dukpt_Key_t;

typedef enum
{
    CAP_TP,
    CAP_BT,
    CAP_WIFI,
    CAP_CELLULAR,
    CAP_LCD,
    CAP_USB,
    CAP_SERIAL,
    CAP_MAG,
    CAP_ICC,
    CAP_NFC,
    CAP_GPS,
    CAP_FOTA,
    CAP_LCD_BW
}Cap_t;

typedef enum
{
	FW_APP,
	FW_WIFI,
	FW_CELLULAR,
	FW_GPS,
	FW_BT,
	FW_TP,
	BAD_PATCH_TYPE = 0xFF
} FW_TYPE;

typedef struct
{
	FW_TYPE type;
	pu8 pvalue;
    T_OTA dev;
}T_UPDATE;

typedef enum
{
    GUI_TICK,
    GUI_TASK
}T_GUI;


/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
/*keyboard callback*/
extern void KeyboardPressedHandler(Key_Num_t Key_Num);
/*evernt query*/
extern u32 WaitEvents(u32 events,u32 timeout,pvoid pbuffer );
/*Input method interface*/
extern Rc_t TypewritingGetString(u32 BitMaps, Key_Num_t (*KbdCBK)(void ), pcs8 (*TypeStrCBK)(pcs8 String), pcs8 (*PromptCBK)(pcs8 pPrompt), pu8 pStringOut, u32 StringOutLengthMax);
/*-----------------------------------------------------------------------------
|   Display APIs
+----------------------------------------------------------------------------*/
extern void DisplayHandler(pmessage_body_t pmsg);
extern Rc_t LcdDrawPoint(Dev_Lcd_Mode_t mode,s32 x,s32 y );
extern Rc_t LcdDrawLine(Dev_Lcd_Mode_t mode,s32 x1,s32 y1,s32 x2,s32 y2 );
extern Rc_t LcdPrint(Dev_Lcd_Mode_t mode,u32 line,u32 alignment,u32 high,const char * dispCode );
extern Rc_t LcdPosition(Dev_Lcd_Mode_t mode,u32 x,u32 y,u32 high,const char * dispCode);
extern Rc_t LcdClearLine(Dev_Lcd_Mode_t mode,u32 line );
extern Rc_t LcdClearAll(Dev_Lcd_Mode_t mode);
extern u32 LcdGetStringCodeWidth(const char * inStr );
extern u32 GetLcdDispH();
/*-----------------------------------------------------------------------------
|   usb/BT Interrupt callback
|    type:USB/BT
|
|    c:recv char
+----------------------------------------------------------------------------*/
extern void EventHandle(u32 event_id);
extern void EventRegister(u32 event_id);
extern void TransCancle();
extern void TransKbdEvent(u32 key);
extern void FrameHandler(Host_Type_t type,u8 c);
/*-----------------------------------------------------------------------------
|   transaction APIs
+----------------------------------------------------------------------------*/
extern Rc_t QR_Generate(pQR_CORE_T Qrcode);
extern void SignatureTransmit(void);
extern u32 SignatureCallBack(pu8 pSign,u32 Maxlen);
extern u32 StartTrading(pu8 pTradingFile);
extern void StartTradingOnlineHandler(pu8 pTradingFile);
extern bool Set_dev_EncryptMode(u32 mode);
/*-----------------------------------------------------------------------------
|   connect server APIs
+----------------------------------------------------------------------------*/
extern Rc_t QposUpdateApn(pApnParam_t pApn);
extern Rc_t QposUpdateCert(const char *pCer,DevCertType_t type);
extern s32 SocketTransmit(pu8 pHost, u32 Port, pu8 pData, u32 DataLength, pu8 pReceivedData, u32 ReceivedDataLengthMax, u32 Timeout );
extern s32 SslTransmit(pu8 pHost, u32 Port, pu8 pData, u32 DataLength, pu8 pReceivedData, u32 ReceivedDataLengthMax, u32 Timeout );
/*-----------------------------------------------------------------------------
|   Key Injection APIs
+----------------------------------------------------------------------------*/
extern u32 UpdateDukpt(u32 keyGrp,pu8 Trk_ksn,u32 Trk_Ipek_len,pu8 Trk_Ipek,pu8 Trk_kcv,pu8 Emv_ksn,u32 Emv_Ipek_len,pu8 Emv_Ipek,pu8 Emv_kcv,pu8 Pin_ksn,u32 Pin_Ipek_len,pu8 Pin_Ipek,pu8 Pin_kcv);
extern u32 DukptGetKsn(u32 keyGrp,u32 keyType,pu8 pKsn );
extern u32 DukptSetKsn(u32 keyGrp,u32 keyType,pu8 pKsn );
extern bool DukptUpdateKsn(u32 ksnGrp,u32 ksnType );
extern u32 DukptEncrypt(u32 keyGrp,u32 keyType,pu8 pKsn,pu8 pInput,u32 inputLen,pu8 pOutput );
extern u32 DukptDecrypt(u32 keyGrp,u32 keyType,pu8 pKsn,pu8 pInput,u32 inputLen,pu8 pOutput );
extern u32 mksk_encrypt(u32 keyGrp,MkSk_Key_t keyType,pu8 pInput,u32 inputLen,pu8 pOutput);
extern u32 mksk_decrypt(u32 keyGrp,MkSk_Key_t keyType,pu8 pInput,u32 inputLen,pu8 pOutput);
extern u32 UpdateMasterKey(u32 keyGrp,pu8 enkey,u32 len,pu8 kcv );
extern u32 UpdateWorkKey(u32 keygrp,pu8 pinKey,pu8 pin_kcv,u32 pin_len,pu8 trkKey,pu8 trk_kcv,u32 trk_len,pu8 mckKey,pu8 mck_kcv,u32 mck_len );
/*-----------------------------------------------------------------------------
|   Firmware upgrader APIs
+----------------------------------------------------------------------------*/
extern Rc_t FirmwareOTA(T_UPDATE fw);
extern Rc_t FirmwareVerify(u32 fw_addr);
/*-----------------------------------------------------------------------------
|   Emv config APIs
+----------------------------------------------------------------------------*/
extern bool Custom_EmvConfig(Custom_Emv_Ctrl_t type,pu8 pTlvBuffer,pu32 pTlvBufferLen);
extern u32 Custom_GetEmvTag(u32 type,u32 tag,pu8 value);
/*-----------------------------------------------------------------------------
|   ICC & NFC card transmit  APIs
+----------------------------------------------------------------------------*/
extern u32 CardPowerOn(Card_Type_t card,u32 timeout,T_U8_VIEW *pAtr);
extern Rc_t CardPowerOff(Card_Type_t card);
extern u32 CardTransmit(Card_Type_t card,pu8 pApdu,u32 length);
extern Rc_t MifareCardProcess(u32 cmdId,pu8 pMifireData,pu32 mifireDataLen,u32 timeout);
/*-----------------------------------------------------------------------------
|   nvram  APIs(size:512 bytes only)
+----------------------------------------------------------------------------*/
extern Rc_t CustomParametersGet(pu8 pParameters, u32 Length);
extern Rc_t CustomParametersSet(pu8 pParameters, u32 Length);

/*-----------------------------------------------------------------------------
|   Flash  APIs
+----------------------------------------------------------------------------*/
extern u32 rw_param_user_data(Rw_t rw, pvoid param,u32 addr, u32 size);

/*-----------------------------------------------------------------------------
|   TLV  APIs
+----------------------------------------------------------------------------*/
extern T_U8_VIEW get_tlv_view(u8 *pTlvBuf, u32 dwTag);
extern void set_tlv_view(u8 *pTlvBuf, u32 dwTag, T_U8_VIEW uvVal);
extern u32 get_tlv_value(pu8 pool,u32 T,pu8 V );

/*-----------------------------------------------------------------------------
|   data Format covert  APIs
+----------------------------------------------------------------------------*/
extern u32 bcd_to_asc(pu8 bcd,u32 align,u32 asclen,pu8 asc );
extern u32 bcd_to_asc_sample(pu8 src,u32 len,pu8 dest );
extern u32 asc_to_bcd(pu8 src,u32 len,pu8 dest );
extern u32 asc_to_bcd_sample(pu8 src,u32 len,pu8 dest );

/*-----------------------------------------------------------------------------
|   amount Format covert  APIs
+----------------------------------------------------------------------------*/
extern u32 format_amt_ascn_to_bcd6(pu8 amt,u32 amtlen,pu8 amtOut );
extern u32 format_amt_bcd6_to_ascn(pu8 amt,u32 amtlen,pu8 amtOut );
extern u32 format_amt_asc_points_to_yuan(pu8 amt,u32 amtlen,pu8 amtOut );
extern u32 format_amt_int_to_str(u32 amt,pu8 amtIcon,bool amtPoint,pu8 pAmtDisp );

/*-----------------------------------------------------------------------------
|   Device config APIs
+----------------------------------------------------------------------------*/
/*sleep time config*/
extern void SetposSleepTimeout(u32 timeout);
extern u32 GetposSleepTimeout(void);
extern u32 GetposShutdownTimeout(void);
/*shutdown time config*/
extern void SetposShutdownTimeout(u32 timeout);
/*status*/
extern bool SetQPOS_ChargeStatus(bool Status);
extern bool Get_dev_ChargingStatus(void);
/*sim card*/
extern u32 Get_dev_gprs_signal_lvl(void);
extern u32 Get_dev_sim_status(void);
extern bool Get_dev_UsbConnect_Status(void);
extern Rc_t Get_dev_imei(pu8 pstr);
/*battery*/
extern u32 Get_dev_battery_lvl(void);
/*version*/
extern u32 GetWifiVersion(pu8 pVer);
extern u32 GetCellularVersion(pu8 pVer);
extern u32 GetGpsVersion(pu8 pVer);
extern u32 GetBootVer(pu8 pVer);
extern u32 GetAppVer(pu8 pVer);
/*information*/
extern u32 GetPosid(pu8 pValue);
extern u32 GetPosInfo(pu8 pTradingFile);
extern void DispDevInfo();
extern void GetDevInfor(pu8 pInfo);
extern void DevVer(pu8 pValue);
/*device capaility*/
extern Rc_t Capabilities(Cap_t type);
/*detect card status*/
extern bool Check_Card_Status(Card_Type_t type);
/*calculate xor8*/
extern u8 make_verifycode_xor8(pcu8 pdata, u32 datacnt, u32 init_verifycode );
extern void UpdateFotaProgress(u32 Progress);
extern u32 rw_tms_db(Rw_t rw, T_U8_VIEW uvDb);

/*
*******************************************************************************
*   End of File
*******************************************************************************
*/

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#endif  /* #ifndef _PROJ_SDK_H */

