/*
********************************************************************************
*
*   File Name:
*       proj_cfg.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/
#ifndef _PROJ_CFG_H
#define _PROJ_CFG_H


/*-----------------------------------------------------------------------------
|   ONLINE TXN
+----------------------------------------------------------------------------*/

#if 1
#define CFG_DOCK_DEVICE
#endif

/*-----------------------------------------------------------------------------
|   DEBUG
+----------------------------------------------------------------------------*/
#if 1
#define CFG_DBG
#define CFG_FRAME_DEBUG
#endif
#if defined(CFG_DBG)
#if 1
#define USE_CHECK_PARAM
#endif
#if 1
#define DBG_TRACE
#define DBG_TRACE_PORT           (HOST_USB_DEBUG)
#endif
#if 1
#define COMMAND_DBG
#endif
#if 0
#define EMV_PARAM_DBG
#endif
#if 0
#define FRAME_DEBUG
#endif


#if 0
#if !defined(TASK_DEBUG)
#define TASK_DEBUG
#endif
#define CFG_POWER_MANAGE_DEBUG
#endif

#if 0
#define TRANS_DATA_FORMAT_DBG
#endif

#define DBG_TRACE_LVL            (TRACE_LVL_INFO)
#endif

/*Certification update*/
#if 1
#define CFG_SERVER_CERT_UPDATE  //server CA cert
#endif

#if 0
#define CFG_SERVER_CERT_UPDATE //client cert
#endif

#if 0
#define CFG_SERVER_CERT_UPDATE //client key cert
#endif

/*signature test*/
#if 0
#define CFG_SIGNATURE
#endif
/*trans mode select*/
#if 1
#define CFG_TRANS_MODE
#endif

/*network request mode select*/
#if 1
#define CFG_REQUEST_MODE
#endif

/*card transaction */
#if 0
#define CFG_CARD_TRANS
#endif

/*8583 define*/
#if 0
#define CFG_8583
#endif

/*disp time*/
#if 0
#define CFG_DISP_TIME
#endif

/*Firmware OTA*/
#if 0
#define CFG_TMS_CLIENT
#define CFG_FIRMWARE_SAVE
#endif

/*OTA debug*/
#if 0
#define CFG_OTA_DEBUG
#endif

#if defined(CFG_OTA_DEBUG)
	#define TMS_TRACE_LVL DBG_TRACE_LVL
#else
	#define TMS_TRACE_LVL (DBG_TRACE_LVL + 1)
#endif

/*OTA RCD support*/
#if 0
#define CFG_RCD_DOWNLOAD
#endif

/*firmware revision verify*/
#if 0
#define CFG_REVISON_VERIFY_DISABLE
#endif
/*module OTA*/
#if 0
#define CFG_MODULE_OTA
#endif
#endif /* #ifndef _PROJ_CFG_H */
