#include "proj_sdk.h"
#include "lark_api.h"
#include "scatter.h"
#include "tms_ota.h"
#include "Req_Http.h"
#include "Req_Server.h"
#include "interface.h"

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
static void DisplayPercentage(u32 offset,u32 dwLenFile)
{
    char strDisp[20] = {0};
    u64 llRate = (u64)offset*10000/(u64)dwLenFile;
    u32 dwHiRate = offset*100/dwLenFile;
    u32 dwLoRate = llRate - dwHiRate*100;
    Sprintf_(strDisp, "%lu.%02d%%", dwHiRate, dwLoRate);
    LcdClearLine(LCD_NORMAL,LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, strDisp);
}
void SetProgress(u32 cnt,u32 offset,u32 conlen,u32 TotalLen)
{
    u32 i = 0;
    u32 Total_Cnt=0;
    u32 Process_size=0;
    u32 Percentage=0;
    u32 maxLen=0;

    if(TotalLen%conlen)
    {
        maxLen=TotalLen-(TotalLen%conlen);
    }
    Total_Cnt=maxLen/conlen;
    Total_Cnt/=10;
    Process_size=PROGRESS_SIZE_X/Total_Cnt;
    Percentage=cnt/10*Process_size;
    if(offset==TotalLen)
    {
        Percentage=PROGRESS_SIZE_X;
    }
    disp_backlight();
    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"<%s>offset=%d conlen=%d TotalLen=%d  maxLen=%d\r\n",__FUNCTION__,offset,conlen,TotalLen,maxLen);
    #endif
    DisplayPercentage(offset,TotalLen);
    for(i=0;i<Percentage;i++)
    {
        LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + i, PROGRESS_START_Y, PROGRESS_START_X + i, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    }

}
void Progress_Init(const char * pTitle)
{
    LcdClearAll(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, pTitle);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y - PROGRESS_SIZE_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X, PROGRESS_START_Y, PROGRESS_START_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y, PROGRESS_START_X + PROGRESS_SIZE_X, PROGRESS_START_Y - PROGRESS_SIZE_Y);
}
void UpdateModuleProcess(u32 Percentage)
{
    u32 i=0;
    u8 strDisp[8]={0};
    disp_backlight();
    Sprintf_(strDisp,"%d%",Percentage);
    LcdClearLine(LCD_NORMAL,LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, strDisp);
    for(i=0;i<Percentage*2;i++)
    {
        LcdDrawLine(LCD_NORMAL, PROGRESS_START_X + i, PROGRESS_START_Y, PROGRESS_START_X + i, PROGRESS_START_Y - PROGRESS_SIZE_Y);
    }
}
#if defined(CFG_TMS_OTA)
static u32 parseFwRevision(pu8 pvalue,pu8 pRevision)
{
    u32 idx=0;
    pu8 pCur=&pvalue[2];
    while(*pCur!='|')
    {
        pRevision[idx++]=*pCur;
        pCur++;
    }
    return idx;
}
static void FormatReqHead(pu8 pTlv)
{
    u8  tem[10]={0};
    u8  pos_id[20]={0};
    u32 len=0;
    u8  Revesion[8]={0};
    pu8 pUrl=NULL;

    //T_U8_VIEW uvID=get_tlv_view(pTlv,POS_TAG_POS_ID);
    T_U8_VIEW uvID={tem,0};
    uvID.len=GetPosid(uvID.head);
    TRACE_VIEW(DBG_TRACE_LVL,uvID);
    len=bcd_to_asc(uvID.head,0,uvID.len *2,pos_id);
    pUrl=lark_alloc_mem(256);
    Memset_(pUrl,0x00,256);

    Strcat_(pUrl,URL_TMS_HEART);
    Strcat_(pUrl,"sn=");
    Strcat_(pUrl,pos_id);
    Strcat_(pUrl,"&heart=");
    pu8 pCur=pUrl+Strlen_(pUrl);
    T_U8_VIEW uvHeart=get_tlv_view(pTlv,TMS_CONFIG_HEART);
    if(UV_OK(uvHeart))
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s--tlv heard\r\n",__FUNCTION__);
        TRACE_VIEW(DBG_TRACE_LVL,uvHeart);
        #endif
        Memcpy_(pCur,uvHeart.head,uvHeart.len);
        pCur+=uvHeart.len;
    }
    else
    {
        Strcat_(pCur,TMS_CFG_HEART);
        pCur+=Strlen_(TMS_CFG_HEART);
    }
    Strcat_(pUrl,"&update=");
    pCur+=Strlen_("&update=");
    T_U8_VIEW update=get_tlv_view(pTlv,TMS_CONFIG_UPDATE);
    if(UV_OK(update))
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s--tlv update\r\n",__FUNCTION__);
        TRACE_VIEW(DBG_TRACE_LVL,update);
        #endif
        Memcpy_(pCur,update.head,update.len);
        pCur+=update.len;
    }
    else
    {
        Strcat_(pCur,TMS_CFG_UPDATE);
        pCur+=Strlen_(TMS_CFG_UPDATE);
    }
    Strcat_(pUrl,"&revisions=");
    pCur+=Strlen_("&revisions=");
    T_U8_VIEW revision=get_tlv_view(pTlv,TMS_CONFIG_REVISION);
    if(UV_OK(revision))
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s--tlv revision\r\n",__FUNCTION__);
        TRACE_VIEW(DBG_TRACE_LVL,revision);
        #endif
        Memcpy_(pCur,revision.head,revision.len);
        pCur+=revision.len;
    }
    else
    {
        Strcat_(pCur,TMS_CFG_REVISIONS);
        pCur+=Strlen_(TMS_CFG_REVISIONS);
    }

    tlv_delete_(pTlv,TMS_CONFIG_HEART);
    tlv_delete_(pTlv,TMS_CONFIG_UPDATE);

    //store TMS_REQ_URL as a string
    T_U8_VIEW ReqURL={pUrl, Strlen_(pUrl) + 1};
    set_tlv_view(pTlv,TMS_REQ_URL,ReqURL);
    lark_free_mem(pUrl);
}
static Rc_t Sync_tms(pu8 pTlv,bool force)
{
    TMSConfigLoad(pTlv);
    FormatReqHead(pTlv);
    if(force)
    {
        T_U8_VIEW Rcd_revision=get_tlv_view(pTlv,TMS_CONFIG_REVISION);
        if(UV_OK(Rcd_revision))
        {
            tlv_delete_(pTlv,TMS_CONFIG_REVISION);
            LocalParameterDelete(TMS_CONFIG_REVISION);

        }
    }
    return GetTmsToken(pTlv);
}
static u32 FormatUrl(pu8 pTlv,pu8 pOut)
{
   pu8 pCur=NULL;
   u32 len=0;
   u32 idx=0;
   u32 index=0;
   bool Multi_app=false;
   u32 repo_idx=0;
   T_U8_VIEW fw[2]={NULL,0};
   u32 length=0;

   T_U8_VIEW revision_fw=get_tlv_view(pTlv,TMS_FW_MSG);
   if(UV_OK(revision_fw))
   {
        pCur=revision_fw.head;
        repo_idx=pCur[0]&0x0F;
   }

   T_U8_VIEW path=get_tlv_view(pTlv,TMS_FW_PATH);
   if(UV_OK(path))
   {
        pu8 pTail=path.head+path.len;
        while(*pTail!='/')
        {
            len++;
            pTail--;
        }

        u32 head_len=path.len-len+1;
        pCur=path.head;
        Memcpy_(&pOut[index],path.head,head_len);
        index+=head_len;

        pCur=path.head+head_len;
        while(idx<len)
        {
            if(*pCur=='|')
            {
                Multi_app=true;
            }
            pCur++;
            idx++;
        }
        pCur=path.head+head_len;
        len=path.len-head_len;

        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"repo_idx=%x %d\r\n",pCur[0],repo_idx);
        TRACE(DBG_TRACE_LVL,"repo=%d\r\n",len);
        TRACE_ASCII(DBG_TRACE_LVL,pCur,len);
        #endif

        idx=0;
        if(Multi_app)
        {
            fw[idx].head=pCur;
            while(len>0)
            {
                if(*pCur=='|')
                {
                    fw[idx].len=length;
                    idx++;
                    pCur++;
                    len--;
                    length=0;
                    fw[idx].head=pCur;
                }
                length++;
                pCur++;
                len--;
            }
            fw[idx].len=length;
            Memcpy_(&pOut[index],fw[repo_idx].head,fw[repo_idx].len);
            index+=fw[repo_idx].len;
        }
        else
        {
            Memcpy_(&pOut[index],pCur,len);
            index+=len;
        }
   }

   return index;

}
static void FormatReqFwHead(pu8 pTlv)
{
   pu8 pUrl=lark_alloc_mem(256);
   u32 idx=0;

   idx+=FormatUrl(pTlv,&pUrl[idx]);
   pUrl[idx++]='/';
   T_U8_VIEW revision=get_tlv_view(pTlv,TMS_CONFIG_REVISION);
   if(UV_OK(revision))
   {
        Memcpy_(&pUrl[idx],revision.head,revision.len);
        idx+=revision.len;
   }
   Memcpy_(&pUrl[idx],"&cut=",Strlen_("&cut="));
   idx+=Strlen_("&cut=");
   pUrl[idx++]='1';
   Memcpy_(&pUrl[idx],"&dev=",Strlen_("&dev="));
   idx+=Strlen_("&dev=");
   T_U8_VIEW Term_id=get_tlv_view(pTlv,TMS_DEV);
   if(UV_OK(Term_id))
   {
        Memcpy_(&pUrl[idx],Term_id.head,Term_id.len);
        idx+=Term_id.len;
   }
   //store TMS_REQ_URL as a string
   pUrl[idx] = 0;
   T_U8_VIEW Req_url={pUrl, idx + 1};
   set_tlv_view(pTlv,TMS_REQ_URL,Req_url);
   tlv_delete_(pTlv,TMS_FW_MSG);
   lark_free_mem(pUrl);
}

static void FirmwareDownlaod(pu8 pTlv)
{
	Rc_t result;
    FormatReqFwHead(pTlv);
	if(identify_wifi_dev() == COMMON_WIFI && lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS))
		result = req_patch_wifi(pTlv);
	else
		result = ServerReqFirmware(pTlv);
	
	switch(result)
	{
		case RC_FAIL:
			dispStrWait(LCD_CENTERED_ALIGNMENT,"Firmware Update\n FAIL", 30);
		break;
		case RC_QUIT:
			dispStrWait(LCD_CENTERED_ALIGNMENT, "Firmware Update\n Cancel", 3);
		break;
		default:
		break;
	}
}

static void updateReqMsg(pu8 pTlv)
{
    T_U8_VIEW msg=get_tlv_view(pTlv,TMS_FW_MSG);
    if(UV_OK(msg))
    {
        TRACE_VIEW(DBG_TRACE_LVL,msg);
        dispStrWait(LCD_CENTERED_ALIGNMENT,msg.head,3);
    }
}
/*--------------------------------------
|   Function Name:
|       TmsFirmwareUpdate
|   Description:tms app download interface
|               1.request TMS to get app downlaod detail(URL and app file Revevison)
|               2.request TMS to download app file
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t TmsFirmwareUpdate(bool Force)
{
    pu8 pTlvBuffer=lark_alloc_mem(CFG_TLV_POOL);
    tlv_new_(pTlvBuffer, PARAM_TLV_HEAD, 0, NULL);
    Rc_t Rc=Sync_tms(pTlvBuffer,Force);
    if(Rc==RC_FAIL)
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s-fail\r\n",__FUNCTION__);
        #endif
        updateReqMsg(pTlvBuffer);
    }
    else if(Rc==RC_QUIT)
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"firmware updated already\r\n",__FUNCTION__);
        #endif
    }
    else if(Rc==RC_DOWN)
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"no availble repo path\r\n",__FUNCTION__);
        #endif
        dispStrWait(LCD_CENTERED_ALIGNMENT,"No Availble Firmware",3);
    }
    else
    {
        FirmwareDownlaod(pTlvBuffer);
    }

    lark_free_mem(pTlvBuffer);
	return RC_SUCCESS;
}

#endif

/*--------------------------------------
|   Function Name:
|       UpdateFotaProgress
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
void UpdateFotaProgress(u32 progress)
{
     UpdateModuleProcess(progress);
}
/*--------------------------------------
|   Function Name:
|       QposUpdate
|   Description:module update interface
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t QposUpdate()
{
    Rc_t Rc=RC_FAIL;
    u32 idx=0;
    u32 len=Strlen_(CFG_OTA_URL_HEAD)+Strlen_(CFG_MODEL_FIRMWARE);

    if(Capabilities(CAP_FOTA)==RC_FAIL)
    {
        return Rc;
    }
    pu8 pTlvBuffer=lark_alloc_mem(CFG_TLV_POOL);
    tlv_new_(pTlvBuffer, PARAM_TLV_HEAD, 0, NULL);
    LoadVersion(pTlvBuffer);
    lark_free_mem(pTlvBuffer);
    Progress_Init(PROMPT_MODULE_OTA);
    T_UPDATE firmware={FW_CELLULAR,lark_alloc_mem(256)};
    pu8 pvalue=firmware.pvalue;

    pvalue[idx++]=0x01;
    pvalue[idx++]=LONG_LH(len);
    pvalue[idx++]=LONG_LL(len);
    Memcpy_(&pvalue[idx],CFG_OTA_URL_HEAD,Strlen_(CFG_OTA_URL_HEAD));
    idx+=Strlen_(CFG_OTA_URL_HEAD);
    Memcpy_(&pvalue[idx],CFG_MODEL_FIRMWARE,Strlen_(CFG_MODEL_FIRMWARE));
    idx+=Strlen_(CFG_MODEL_FIRMWARE);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"module url\r\n");
    TRACE_VALUE(DBG_TRACE_LVL,pvalue,idx);
    #endif

    Rc=FirmwareOTA(firmware);
    lark_free_mem(firmware.pvalue);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"<%s>-%s\r\n",__FUNCTION__,(Rc==RC_SUCCESS?"update success":"update fail"));
    #endif
    if(Rc==RC_SUCCESS)
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_2,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"update success\nVersion Verify...");
        if(CheckVersion()==RC_SUCCESS)
        {
            dispStrWait(CENTER, PROMPT_MODULE_UPDATE_SUCCESS,3);
        }
    }
    return Rc;
}
