#include "appcode.h"
#include "lark_api.h"
#include "proj_sdk.h"
#include "iso8583.h"
#include "pkt_adapt.h"
#include "interface.h"
//#if defined(CFG_8583)

extern s32 RequestHttpTransmit(char * pSend,u32 SendLen, char * pURL,u32 port, char * pReceived, u32 FrameLengthMax );
static const char *TPDU_HEAD = "6012340000602200000000";

const u32 arrTagDE55[] =
{
    0x5F2A, 0x5F34,0x82, 0x84, 0x91,
    0x95, 0x9A, 0x9C, 0x9F02,
    0x9F03, 0x9F09,0x9F10,0x9F1A, 0x9F1E,
    0x9F27, 0x9F33, 0x9F34, 0x9F35, 0x9F36, 0x9F37
};

char *trim(char *s, char cExt)
{
    char * p = s;
    int l = Strlen_(p);

    while(IS_SPACE(p[l - 1], cExt)) p[--l] = 0;
    while(* p && IS_SPACE(*p, cExt)) ++p, --l;
    Memcpy_(s, p, l + 1);
    return s;
}

u32 str2num(char *strAmt, u32 dwLen)
{
    u32 dwAmt = 0, dwFct = 1;
    int i;
    for(i = dwLen - 1; i >= 0; i--)
    {
        if(strAmt[i] >= '0' && strAmt[i] <= '9')
        {
            dwAmt += (strAmt[i] - '0')*dwFct;
            dwFct *= 10;
        }
    }
    return dwAmt;
}

char *uv2str(char *strRet, T_U8_VIEW *uv)
{
	if(strRet && uv)
	{
		Memcpy_(strRet, uv->head, uv->len);
		strRet[uv->len] = 0;
    }
	return strRet;
}

int uvcmpstr(T_U8_VIEW *uv, char *str)
{

	u32 i;
	if(uv->len != Strlen_(str)) return -1;
	for(i = 0; i < uv->len; i++)
		if(uv->head[i] != str[i]) return -1;
	return 0;
}

u32 su2num(T_U8_VIEW *uv)
{
    u32 dwAmt = 0, dwFct = 1;
    int i;
    for(i = uv->len - 1; i >= 0; i--)
    {
        if(uv->head[i] >= '0' && uv->head[i] <= '9')
        {
            dwAmt += (uv->head[i] - '0')*dwFct;
            dwFct *= 10;
        }
    }
    return dwAmt;
}

char *str_in_uv(T_U8_VIEW uvSrc, char *sub)
{
	char *bp, *sp;
	u8 *pTail = uvSrc.head + uvSrc.len;
    if(!UV_OK(uvSrc) || !sub) return (char *)uvSrc.head;
    
    while(uvSrc.head < pTail)
    {
        bp = (char *)uvSrc.head;
        sp = sub;
        do
        {
            if(!*sp) return (char *)uvSrc.head;
        }while(*sp++ == *bp++);
        uvSrc.head++;
    }
    return NULL;
}

u32 uv2bin(u8 *pBin, T_U8_VIEW uvAsc, T_PAD_DIRECTION pad)
{
	u8 *pCur = uvAsc.head, *pDest = pBin;
	if(!UV_OK(uvAsc)) return 0;
	if((uvAsc.len%2) && pad == PAD_0_LEFT)	//4 higher bits : 0
	{
		*pDest++ = CHAR2BIN(*pCur);
		pCur++;
	}

	while(pCur < (uvAsc.head + uvAsc.len))
	{
		*pDest = (CHAR2BIN(*pCur)<<4);
		pCur++;
		if(pCur >= uvAsc.head + uvAsc.len) break;
		*pDest |= CHAR2BIN(*pCur);
		pCur++; pDest++;
	}
	return (pDest - pBin);
}


T_URL_INFO* parse_url(T_URL_INFO* info, char* strUrl)
{
	T_U8_VIEW uvTmp = {(u8 *)strUrl, 0};
	char *pSuHead = strUrl;
    if (!info || !strUrl) return NULL;
	char *pCur = Strstr_(strUrl, "://");
	if(!pCur)
		info->eProtocol = TCP;
	else
	{
		uvTmp.len = pCur - strUrl;
		if(uvcmpstr(&uvTmp, "http") == 0)
			info->eProtocol = HTTP;
		else if(uvcmpstr(&uvTmp, "https") == 0)
			info->eProtocol = HTTPS;
		else
			info->eProtocol = UNKNOWN_PROTO;
		pSuHead = pCur + 3;
	}
	pCur = Strstr_(pSuHead, ":");
	if(pCur)
	{
		info->uvSite.head = (u8 *)pSuHead;
		info->uvSite.len = pCur - pSuHead;
		pSuHead = pCur + 1;
	}
	pCur = Strstr_(pSuHead, "/");
	if(pCur)
	{
		if(info->uvSite.head)
		{
			uvTmp.head = (u8 *)pSuHead;

			uvTmp.len = pCur - pSuHead;
			info->wPort = su2num(&uvTmp);
		}
		else
		{
			info->uvSite.head = (u8 *)pSuHead;
			info->uvSite.len = pCur - pSuHead;
			if(info->eProtocol == HTTP)
				info->wPort = 80;
			else if(info->eProtocol == HTTPS)
				info->wPort = 443;
		}
		pSuHead = pCur + 1;
		info->uvPath.head = (u8 *)pSuHead;
		info->uvPath.len = Strlen_(pSuHead);
	}
	else if(info->eProtocol == TCP)
	{
		uvTmp.head = (u8 *)pSuHead;
		uvTmp.len = Strlen_(pSuHead);
		info->wPort = su2num(&uvTmp);
	}
	return info;
}

#if 0
Rc_t request_txn_server(u8 *pTradingFile, char *strUrl)
{
	TRACE(TRACE_LVL_INFO, "Try to connect server via %s...\r\n",
		(identify_wifi_dev() == COMMON_WIFI && lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS))? "WiFi" : "GPRS");

	pu8 pSend = lark_alloc_mem(HTTP_FRAME_LENGTH_MAX);
    pu8 pRecv = lark_alloc_mem(HTTP_FRAME_LENGTH_MAX);
	Memset_(pSend, 0, HTTP_FRAME_LENGTH_MAX);

	T_URL_INFO urlInfo;
	parse_url(&urlInfo, strUrl);
	TRACE_ASC(TRACE_LVL_INFO, urlInfo.uvSite.head, urlInfo.uvSite.len);
	TRACE(TRACE_LVL_INFO, "port = %d\r\n", urlInfo.wPort);
	u8 *pCur = pSend + 2;		//skip length field of 8583 pkt

	T_U8_VIEW uvTmp = {(u8 *)TPDU_HEAD, Strlen_((char *)TPDU_HEAD)};
	//U8_VIEW(TPDU_HEAD, Strlen_((char *)TPDU_HEAD), &uvTmp);
	pCur += uv2bcd(&uvTmp, pCur, PAD_NONE);
	pCur += iso8583_pack(pCur, pTradingFile);
	u32 dwLen = pCur - pSend;
	pSend[0] = ((dwLen - 2)>>8);
	pSend[1] = ((dwLen - 2)&0xff);
	TRACE_VAL(TRACE_LVL_INFO, (u8 *)pSend, dwLen);

	s32 iLenResp =RequestHttpTransmit((char*)pSend,dwLen,strUrl,urlInfo.wPort,pRecv,HTTP_FRAME_LENGTH_MAX);

	tlv_delete_(pTradingFile, TAG_8583_USER_DEF);

	pCur = pRecv;
	Rc_t result = RC_FAIL;
	if(iLenResp >= 0)
	{
		dwLen = (iLenResp > 2 ? (pCur[0]<<8|pCur[1]) : 0);
		TRACE_VAL(TRACE_LVL_INFO, pCur, dwLen + 2);
		if(dwLen > Strlen_(TPDU_HEAD)/2 + 10)
		{
			TRACE(TRACE_LVL_INFO, "kick off unpacking 8583\r\n");
			pCur += 2 + Strlen_(TPDU_HEAD)/2;
			dwLen = iso8583_unpack(pCur, pTradingFile);
			TRACE(TRACE_LVL_INFO, "8583 unpacked %d fields\r\n", dwLen);
			uvTmp = tlv_uv_read(pTradingFile, TAG_8583_MSG_TYPE);
			if(!uvcmpstr(&uvTmp, "0210")) result = RC_SUCCESS;
			uvTmp = tlv_uv_read(pTradingFile, TAG_8583_RESP_CODE);
			if(result == RC_SUCCESS && !uvcmpstr(&uvTmp, "00"))
			{
			    tlv_replace_(pTradingFile,POS_TAG_RES_EN_ONLINE_RESULT, 4, (pu8)"\x8A\x02\x30\x30");
                result = RC_SUCCESS;
			}

		}
	}
	else
	{
        TRACE(TRACE_LVL_INFO, "uplink error = %d\r\n", iLenResp);
	}
	lark_free_mem(pSend);
    lark_free_mem(pRecv);
	return result;
}
static void construct_F55(pu8 pTlvBuf)
{
    pu8 pTlvDe55=lark_alloc_mem(256);
    tlv_new_(pTlvDe55, 0xFF00, 0, NULL);

	u8 i;
	for(i = 0; i < SIZE_ARRAY(arrTagDE55); i++)
	{
        T_U8_VIEW uvTag = get_tlv_view(pTlvBuf, arrTagDE55[i]);
        if(UV_OK(uvTag))
        {
            set_tlv_view(pTlvDe55, arrTagDE55[i], uvTag);
        }
        else
        {
            TRACE(DBG_TRACE_LVL, " TAG %X NOT found in %s\r\n", arrTagDE55[i], __FUNCTION__);
        }
	}

	T_U8_VIEW uvDe55 = {tlv_get_v_(pTlvDe55), tlv_get_l_(pTlvDe55)};
	set_tlv_view(pTlvBuf, TAG8583(TAG_8583_FIELD_55), uvDe55);
	lark_free_mem(pTlvDe55);
}
Rc_t online_pay(u8 *pTlvBuf)
{
	struct {u32 tag; char *strVal;} a8583tag[] = {
		{TAG_8583_MSG_TYPE, "0200"},
		{TAG_8583_VOUCHER, "000349"},
		{TAG_8583_ENTRY_MODE, "021"},
		{TAG_8583_COND_CODE, "00"},
		{TAG_8583_PING_IN_CODE, "12"},
		{TAG_8583_CARD_ACCEPT_ID, "235214526859236"},
		{TAG_8583_CURRENCY_CODE, "156"},
		{TAG_8583_SECURITY_CONTROL, "2000000000000000"},
		{TAG_8583_MAC, "67A2299A"}
	};

	u32 i;
	for(i = 0; i < SIZE_ARRAY(a8583tag); i++)
	{
        tlv_replace_(pTlvBuf, a8583tag[i].tag, Strlen_(a8583tag[i].strVal), (u8 *)a8583tag[i].strVal);
	}

    construct_F55(pTlvBuf);

	tlv_replace_(pTlvBuf, TAG_8583_TXN_TYPE, 6, (u8 *)"000000");
	char strAmt[13] = {0};
	tlv_bcdnum2str(pTlvBuf, TAG_EMV_AMOUNT_AUTH_NUM, strAmt, 0);
	TRACE(TRACE_LVL_INFO, "strAmt: %s, ", strAmt);
	tlv_replace_(pTlvBuf, TAG_8583_TXN_AMT, Strlen_(strAmt), (u8 *)strAmt);

	u8 *pNode = tlv_find_(pTlvBuf, POS_TAG_CLEAR_TRACK2);
	if(pNode)
	{
		u32 dwLength = tlv_get_l_(pNode);
		TRACE(TRACE_LVL_INFO, "Track2 Data Found: ");
		TRACE_ASCII(TRACE_LVL_INFO, tlv_get_v_(pNode), dwLength);
		tlv_replace_(pTlvBuf, TAG_8583_TRACK2_DATA, dwLength, tlv_get_v_(pNode));
	}

	pNode = tlv_find_(pTlvBuf, POS_TAG_RES_PINBLOCK);
	if(pNode)
	{
		u32 dwLength = tlv_get_l_(pNode);
		TRACE_VAL(TRACE_LVL_INFO, tlv_get_v_(pNode), dwLength);
		tlv_replace_(pTlvBuf, TAG_8583_PIN_DATA, dwLength, tlv_get_v_(pNode));
	}
	return request_txn_server(pTlvBuf, URL_POSP_DEMO);
}
#endif

static u32 detectLenWiFiSock(T_U8_VIEW uvBlk)
{
	const char *strFixed = "Content-Length:";
	if(PTR2DWORD(uvBlk.head) != CHAR2LONG('H','T','T','P'))
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, 4, "%c", "HTTP head NOT found: ", 0);
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, 4, "%02X", " -- ");
		return 0;
	}
	u8 *pCur = (u8 *)str_in_uv(uvBlk, "\r\n\r\n");
	if(!pCur)
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, uvBlk.len, "%c", "HTTP head_end NOT found: ");
		return 0;
	}
	T_U8_VIEW uvHead = {uvBlk.head, pCur + 4 - uvBlk.head};	
	pCur = (u8 *)str_in_uv(uvBlk, (char *)strFixed);
	if(!pCur)
	{
		TRACE_INFO(TMS_TRACE_LVL, uvBlk.head, uvBlk.len, "%c", "Content-Length NOT found: ");
		return 0;
	}
	pCur += Strlen_(strFixed);
	while(*pCur == ' ') pCur++;
	T_U8_VIEW uvNum = {pCur, 0};
	while(*pCur != '\r' && *pCur != '\n') pCur++;
	uvNum.len = pCur - uvNum.head;
	u32 dwLen = str2num((char *)uvNum.head, uvNum.len); 
	TRACE(TMS_TRACE_LVL, "Content-Length = %d, head = %d, ", dwLen, uvHead.len);
	return (dwLen + uvHead.len);
}

s32 WiFiSocket(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv)
{
    if(!UV_OK(uvRecv)) return NET_ERROR_WIFI_REQ_DATA;
    if(uvSend.len)	//send
    {
        if(!lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_DES_STATUS))
		{
			//disp_net_destination_ip();
			TRACE(TMS_TRACE_LVL, "Try to connect host via WiFi...\r\n");
			u8 arrCmd[80] = {0}, *pCur = arrCmd;
			WifiComm_t wifi = {0, 0, 1024, arrCmd}, *pWifi = &wifi;
			*pCur++ = (pUrlInfo->eProtocol == HTTPS ? WIFI_CONN_SSL : WIFI_CONN_TCP);
			*pCur++ = (u8 )pUrlInfo->uvSite.len;
			Memcpy_(pCur, pUrlInfo->uvSite.head, pUrlInfo->uvSite.len);
			pCur += pUrlInfo->uvSite.len;
			*pCur = (u8)Sprintf_((char *)pCur + 1, "%d", pUrlInfo->wPort);
			pCur += *pCur + 1;
			pWifi->DataLen = pCur - arrCmd;
			//TRACE_INFO(TMS_TRACE_LVL, arrCmd, pWifi->DataLen, "%02X ", "WiFi arrCmd: ");
			ComWifiConfig_t wificfg = {WIFI_CFG_CONN_DES, pWifi};
			if(lark_com_config(HOST_WIFI, &wificfg) != RC_SUCCESS)
			{
				TRACE(TMS_TRACE_LVL, "FAILED in connecting to host, ret %d\r\n", pWifi->RetCode);
				return NetErrorTransmit(pWifi->RetCode);
			}
		}
		//disp_net_communicate();
		u32 dwLenSend = WIFI_FRAME_LEN_MAX;
		while(uvSend.len)
		{
			if(uvSend.len < dwLenSend) dwLenSend = uvSend.len;
			u32 dwStsWrite = 0;
			if(lark_com_write(HOST_WIFI, uvSend.head, dwLenSend, &dwStsWrite))
				return NetErrorTransmit(dwStsWrite);
			uvSend.head += dwLenSend;
			uvSend.len -= dwLenSend;
			if(uvSend.len) lark_sleep(400);
		}
    }
	
    u32 dwTimeOut = DEMO_TIMEOUT_SEC*1000/EVENT_TIMEOUT_MS;
	T_U8_VIEW uvRecvPerWait = {uvRecv.head, 0};
	u32 dwLenProtocol = HTTP_DOWN_LENGTH_MAX, dwLenGot = 0;
	TRACE(DBG_TRACE_LVL, "<%s>Req was put into module, start fun_wait_events_net...\r\n", __FUNCTION__);
    while(dwTimeOut--)
    {
		u32 dwEvt = fun_wait_events_net(APP_EVENT_TIMEOUT|APP_EVENT_DATA_FRAME, EVENT_TIMEOUT_MS, uvRecvPerWait.head, &uvRecvPerWait.len);
		switch(dwEvt)
		{
			case APP_EVENT_TIMEOUT:
				if(!(dwTimeOut%10)) TRACE(DBG_TRACE_LVL, "<%s>wait_events timeout Got...%d\r\n", __FUNCTION__, dwTimeOut);
				if(!(--dwTimeOut))
				{
					TRACE(DBG_TRACE_LVL, "<%s>Timeout waiting for data...\r\n", __FUNCTION__);
					fun_wait_events_net(APP_EVENT_TIMEOUT|APP_EVENT_DATA_FRAME, 5000, uvRecvPerWait.head, &uvRecvPerWait.len);
					return NET_ERROR_WIFI_DES_WRITE_DATA;
				}
				continue;
			break;
			case APP_EVENT_DATA_FRAME:
			break;
			default:
				TRACE(DBG_TRACE_LVL, "<%s>fun_wait_events_net %XH...\r\n", __FUNCTION__, dwEvt);
				continue;
			break;
		}
		
        if(!dwLenGot)
		{
			TRACE(DBG_TRACE_LVL, "<%s>1st pac recv(%d), dwTimeOut %d changed to %d\r\n", 
				__FUNCTION__, uvRecvPerWait.len, dwTimeOut, PACKET_TIMEOUT_MS/EVENT_TIMEOUT_MS);
			dwTimeOut = PACKET_TIMEOUT_MS/EVENT_TIMEOUT_MS;
		}
		
		if(uvRecvPerWait.len == WIFI_FRAME_LEN_MAX) uvRecvPerWait.len -= 8;
		if(uvRecvPerWait.head == uvRecv.head)
		{
			switch(pUrlInfo->eProtocol)
			{
				case HTTP:
				case HTTPS:
					dwLenProtocol = detectLenWiFiSock(uvRecvPerWait);
				break;
				case TCP:
					dwLenProtocol = (uvRecvPerWait.head[0]<<8|uvRecvPerWait.head[1]);
				break;
				default:
				break;
			}
			TRACE(TMS_TRACE_LVL, "perwait len = %d,  total pac length detected to %d\r\n", uvRecvPerWait.len, dwLenProtocol);
			if(!dwLenProtocol)
			{
				TRACE(DBG_TRACE_LVL, "FAILED to detect HTTP frame, WiFi module failure? wait then quit...\r\n");
				fun_wait_events_net(APP_EVENT_TIMEOUT|APP_EVENT_DATA_FRAME, 5000, uvRecvPerWait.head, &uvRecvPerWait.len);
				return NET_ERROR_WIFI_DES_WRITE_DATA;
			}
		}
		dwLenGot +=  uvRecvPerWait.len;
		if(uvRecvPerWait.len < WIFI_FRAME_REAL_LEN)
			TRACE(TMS_TRACE_LVL, "Got NON-FULL frame(%d), ", uvRecvPerWait.len);
		if(dwLenGot >= dwLenProtocol)
		{
			TRACE(TMS_TRACE_LVL, "Got total len %d >= detected: %d, END HTTP resp!\r\n", dwLenGot, dwLenProtocol);
			break;
		}
		
		if(dwLenGot + WIFI_FRAME_LEN_MAX > uvRecv.len)
		{
			TRACE(TMS_TRACE_LVL, "Too large data recv %d VS %d\r\n", dwLenGot + WIFI_FRAME_LEN_MAX, uvRecv.len);
			u8 *pHeadEnd = (u8 *)str_in_uv(uvRecv, "\r\n\r\n");
			TRACE_INFO(TMS_TRACE_LVL, uvRecv.head, pHeadEnd ? (pHeadEnd - uvRecv.head) : 200, "%c", "HTTP head ----\r\n");
			return NET_ERROR_WIFI_FRAME_LENGTH;
		}
		uvRecvPerWait.head += uvRecvPerWait.len;
    }
	
	return dwLenGot;
}

