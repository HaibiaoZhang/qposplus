#ifndef _ISO8583_H
#define _ISO8583_H

typedef enum BCD_PAD
{
    PAD_NONE,
    PAD_LEFT,
    PAD_RIGHT,
	PAD_0_LEFT,
    PAD_0_RIGHT
} T_PAD_DIRECTION;

#define TLV_VAL(tlv, tag)   					tlv_get_v_(tlv_find_(tlv, tag))
#define TLV_LEN(tlv, tag)   					tlv_get_l_(tlv_find_(tlv, tag))
#define U8_VIEW(p, l, pUv)						do{ (pUv)->head = p; (pUv)->len = l; }while(0)
#define TLV_UV_WRITE(tbuf, tag, pUv)			tlv_replace_(tbuf, tag, (pUv)->len, (u8 *)(pUv)->head)
#define TLV_UV_READ(tbuf, tag, pUv)				\
	do{ 										\
	(pUv)->head = (char *)TLV_VAL(tbuf, tag); 	\
	(pUv)->len = TLV_LEN(tbuf, tag); 			\
	}while(0)

#pragma pack(push)
#pragma pack(1)
typedef struct{u8 bytId; u16 wDest; u16 wSource;} T_TPDU;
typedef struct{u8 bytApp; u8 bytVer; u8 bytPosSts; u8 bytReq; u16 dwReserved;} T_HEAD_PKT;
#pragma pack(pop)

#define TPDU_ID_TXN 	0x60
#define TPDU_ID_MNT 	0xF0
typedef enum
{
	MAG_PAY	= 0x60,
	IC_PAY,
	MAG_VAS,
	IC_VAS,
	MNT_SERV = 0xF0
} T_APP_TYPE;

typedef enum
{
	POS_NORMAL,
	POS_TESTING,
	POS_MAINTAIN
} T_POS_STS;

typedef enum
{
	REQ_NONE,
	REQ_MAG_PARA,
	REQ_NAG_STS,
	REQ_RE_LOGIN,
	REQ_UPT_KEY,
	REQ_IC_PARA,
	REQ_TMS_PARA
} T_REQ_TYPE_TXN;

typedef enum
{
	GET_BIN_FILE_BLOCK,
	PUT_BIN_FILE_BLOCK,
	GET_TXT_FILE_BLOCK,
	PUT_TXT_FILE_BLOCK
} T_REQ_TYPE_MNT;

#define TAG_8583_START       	APP_TAG_START
#define TAG8583(i)				(TAG_8583_START + i)
typedef enum
{
	TAG_8583_MSG_TYPE			=	TAG8583(0),
	TAG_8583_TXN_TYPE			= 	TAG8583(3),
	TAG_8583_TXN_AMT			= 	TAG8583(4),
	TAG_8583_VOUCHER			= 	TAG8583(11),
	TAG_8583_ENTRY_MODE			=	TAG8583(22),
	TAG_8583_COND_CODE			=	TAG8583(25),
	TAG_8583_PING_IN_CODE		=	TAG8583(26),
	TAG_8583_TRACK2_DATA		=	TAG8583(35),
	TAG_8583_RESP_CODE			=	TAG8583(39),
	TAG_8583_TERM_ID			=	TAG8583(41),
	TAG_8583_CARD_ACCEPT_ID		=	TAG8583(42),
	TAG_8583_CURRENCY_CODE		=	TAG8583(49),
	TAG_8583_PIN_DATA			=	TAG8583(52),
	TAG_8583_SECURITY_CONTROL	=	TAG8583(53),
	TAG_8583_FIELD_55			=	TAG8583(55),
	TAG_8583_USER_DEF			=	TAG8583(60),
	TAG_8583_MAC				=	TAG8583(64)
} T_TAG_8583;

#define BIN2CHAR(bytHalf)    ((bytHalf < 0x0a) ? ('0' + bytHalf) : (bytHalf - 0x0a + 'A'))
#define MY_BITGET(p,n)       ((p)[(n-1) / 8] & (0x80 >> ((n - 1) % 8)))
#define MY_BITSET(p,n)       ((p)[(n-1) / 8] |= (0x80 >> ((n - 1) % 8)))
#define MY_BITCLR(p,n)       ((p)[(n-1) / 8] &= (~(0x80 >> ((n - 1) % 8))))
#define ISO_FLDS 64
#define TYP_BIT  0
#define TYP_BCD  1
#define TYP_ASC  2
#define TYP_BIN  3
#define MAX_TLV_LEN         2048

typedef struct TAG_ISO_DEF {
	short typ;
	short fmt;
	int len;
	const char *dsc;
} ISO_DEF;

int iso8583_get(int idx, unsigned char *data, u8 *pTlvBuf);
int iso8583_pack(u8 *buf, u8 *pTlvBuf);
int iso8583_unpack(u8 *buf, u8 *pTlvBuf);
int iso8583_update(int idx, char *str, u8 *pTlvBuf);
u32 uv2bcd(T_U8_VIEW *src, u8 *dest, T_PAD_DIRECTION pad);
u32 bcd2uv(u8 *src, T_U8_VIEW *dest);
void trace_tlv_fld(u8 *tlv, u32 i, u8 type);
T_U8_VIEW tlv_uv_read(u8 *tlv, u32 tag);
#endif