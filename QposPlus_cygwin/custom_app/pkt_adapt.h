#ifndef _PKT_ADAPT_H
#define _PKT_ADAPT_H

#define HTTP_DOWN_LENGTH_MAX        (512*4)	//n*512 relied on WiFiSocket
#define UPG_BLOCK_DLT 				(220)
#define UPG_BLOCK_SIZE(CUT) 		(HTTP_DOWN_LENGTH_MAX - 600 + (CUT ? UPG_BLOCK_DLT : 0))
#define MEMBER_SIZE(type, member) 	sizeof(((type *)0)->member)
#define TO_UPPER(c) 				((c)-32*((c)>='a' && (c)<='z'))
#define CHAR2BIN(c)					(((c)&0x7F) >= 'a' ? (((c)&0x7F) - 'a' + 10) \
										: ((((c)&0x7F) >= 'A') ? (((c)&0x7F) - 'A' + 10) : (((c)&0x7F) - '0')))
#define SIZE_ARRAY(x)  				(sizeof(x)/sizeof(*x))
//#define CH_HEIGHT					11
#define CENTER						LCD_CENTERED_ALIGNMENT
#define RIGHT						LCD_RIGHT_ALIGNMENT
#define LEFT						LCD_LEFT_ALIGNMENT
#define KEY_MENU					KEY_POWER
#define PROMPT_PROCESSING           "Processing..."
#define EVENT_TIMEOUT_MS 			(50)
#define ONLINE_TIMEOUT_MINUTES      (2)
#define DEMO_TIMEOUT_SEC      		(10)
#define MAX_RETRY_ON_NULL_RESP      1
#define CMDID_PROCESS               0x21
#define CMD_TCP_SOCKET              0x41E0
#define CMD_GD_INFO					0x1131
#define MODEM_ACK                   (u8 *)"\x4D\0\1\x23\x6F"
#define CANCEL_REQ                  (u8 *)"\x4D\0\1\x28\x64"
#define LEN_PKT_NO_PARA             5
#define MIN_LEN_GPRS_RESP           21
#define PACKET_TIMEOUT_MS       	(1000)
#define ONLINE_TIMEOUT_MINUTES      (2)
#define B2WORD(buf)					(u16)((*(buf)<<8)|*((buf)+1))
#define LEN_OF_PKT(buf)				(B2WORD(buf + 1) + 4)
#define PTR_OF_DATA(buf)			(buf + 7)
#define RET_CODE(buf)				(buf[6])
#define PTR2WORD(x)					(*(x)<<8|*((x) + 1))
#define PTR2DWORD(x)				(PTR2WORD(x)<<16|PTR2WORD(x + 2))
#define U16_BYTES(w)				((u8[] ){(w)>>8, w})
#define U32_BYTES(d)				((u8[] ){LONG_HH(d), LONG_HL(d), LONG_LH(d), LONG_LL(d)})
#define CHAR2LONG(c0, c1, c2, c3)	(u32)((c0)<<24|(c1)<<16|(c2)<<8|(c3))
#define SIZE_STR(x)					sizeof(#x)
#define WIFI_FRAME_LEN_MAX			512
#define WIFI_FRAME_REAL_LEN			(WIFI_FRAME_LEN_MAX - 8)
#define NetErrorTransmit(x) 		(-(x + 100))
#define IS_SPACE(c, x)  \
    ((c) == ' ' || (c) == '\t' || (c) == '\r' || (c) == '\n' || (c) == '\v' || (c) == '\f' \
        || (x != '\0' && (c) == x))
		
#define TRACE_INFO_FULL(lvl, pbuf, cnt, fmt, preface, tail) \
    do { \
			u8 *pTrace = (u8 *)(pbuf);		\
			u32 dwTrace = (u32 )(cnt);		\
			if(SIZE_STR(preface) > 2) TRACE(lvl, "%s", (char *)preface);	\
			else if(preface) TRACE(lvl, #pbuf": ");	\
			while(pTrace && dwTrace--){ TRACE(lvl, fmt?fmt:"%02X ", *pTrace++); }	\
			if(SIZE_STR(tail) > 2) TRACE(lvl, "%s", (char *)tail);	\
			else if(tail) TRACE(lvl, "\r\n");	\
    } while(0)

#define TRACE_INFO_1_ARGS(lvl, message, size)              				TRACE_INFO_FULL(lvl, message, size, 0, 1, 1)
#define TRACE_INFO_2_ARGS(lvl, message, size, fmt)         				TRACE_INFO_FULL(lvl, message, size, fmt, 1, 1)
#define TRACE_INFO_3_ARGS(lvl, message, size, fmt, preface)   			TRACE_INFO_FULL(lvl, message, size, fmt, preface, 1)
#define TRACE_INFO_4_ARGS(lvl, message, size, fmt, preface, tail)   	TRACE_INFO_FULL(lvl, message, size, fmt, preface, tail)

#define GET_7TH_ARG(arg1, arg2, arg3, arg4, arg5, arg6, arg7, ...) arg7
#define TRACE_INFO_MACRO_CHOOSER(...) \
    GET_7TH_ARG(__VA_ARGS__, TRACE_INFO_4_ARGS, TRACE_INFO_3_ARGS, TRACE_INFO_2_ARGS, TRACE_INFO_1_ARGS, )
#define TRACE_INFO(...) TRACE_INFO_MACRO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define TRACE_TLV_ASC(lvl, tbuf, tag) \
    do { \
		T_U8_VIEW uv = get_tlv_view(tbuf, tag);	\
        TRACE(lvl, #tag"(%08XH) : ", tag);	\
		TRACE_ASCII(lvl, uv.head, uv.len);	\
    } while (0)
		
typedef enum
{
    TCP_ROUND = 0,			    //Whole round of TCP transceiver process
	SSL_ROUND = 1,			    //Whole round of SSL transceiver process
	CREATE_TCP_SOCK = 0x80,	    //Create TCP socket
	CLOSE_TCP_SOCK 	= 0x81,	    //Close TCP socket
	TCP_SOCK_SEND 	= 0x82,	    //Send data via TCP socket
	TCP_SOCK_RECV 	= 0x83,	    //Receive data via TCP socket
} t_SockAct;

typedef enum 
{
    SendData, 
    WaitAck, 
    WaitResp, 
    WaitMore,     
    EndComm,
    ToResume,
} T_GPRS_STS;

#pragma pack(push)
#pragma pack(1)
typedef struct{
	u32 dwOffset;
	u32 dwLenBlk;
	u32 dwLenFile;
	u32 dwETag;
	u16 wStatus;	
}T_BLOCK_HEAD;
#pragma pack(pop)

extern u32 uv2bcd(T_U8_VIEW *src, u8 *dest, T_PAD_DIRECTION pad);
extern u32 uv2bin(u8 *pBin, T_U8_VIEW uvAsc, T_PAD_DIRECTION pad);
extern Rc_t online_pay(u8 *pTlvBuf);
extern char *tlv_bcdnum2str(u8* pTradingFile, u32 dwTag, char *strGet, char cSplit);
extern T_URL_INFO* parse_url(T_URL_INFO* info, char* strUrl);
s32 chip_wifi_transmit(T_URL_INFO *pUrlInfo, u8 *pSendBuf, u32 dwSendLen, u8 *pRecvBuf);
extern Rc_t req_pos_upfile(void);
extern char *uv2str(char *strRet, T_U8_VIEW *uv);
extern s32 WiFiSocket(T_URL_INFO *pUrlInfo, T_U8_VIEW uvSend, T_U8_VIEW uvRecv);
extern char *str_in_uv(T_U8_VIEW uvSrc, char *sub);
extern char *trim(char *s, char cExt);
extern u32 strcmp_case(char *src, char *sub, bool sensitive);
extern char *splitstr(char *line, const char *search);
extern char *str_in_uv(T_U8_VIEW uvSrc, char *sub);
extern u32 str2num(char *strAmt, u32 dwLen);
extern u32 uv2asc(u8 *pAsc, T_U8_VIEW uvBin);
extern char *replacestr(char *line, const char *search, const char *replace);
extern u32 lark_xflash_write(u32 addr, pu8 pData, u32 size);
#endif
