#include "appcode.h"
#include "proj_sdk.h"
#include "lark_api.h"
#include "scatter.h"
#include "interface.h"
#if defined(CFG_TMS_OTA)
#include "tms_ota.h"
#endif

#ifdef CFG_DBG
static const char *strCallback[] =
{
    "SWIPE_CARD",
    "CHIP_CARD_ONLINE",
    "TAP_CARD_ONLINE",
    "TAP_CARD_APPROVED",
    "TAP_CARD_DECLINE",
    "TRANS_GAC2",
    "TRANS_APP_SELECT",
    "TRANS_REVERSAL"
};
#endif

static u32 g_EntryLine = LCD_LINE_1;
static u32 g_EntryAlignment = LEFT;

#define KEY_EVT_ALL		\
	(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER | APP_EVENT_TIMEOUT)

void disp_backlight(void);
Key_Num_t waitKeyWanted(u32 dwEvt, u32 dwMsec);
extern void SetRequestMode(u32 index);

typedef Rc_t (*FUNC_MENU_SEL)();

typedef union {
		u8 bytData[32];
		u32 dwKey;
} T_MSG_KEY;
typedef struct
{
    Lcd_Disp_Menu_t Menu_opt;
    char*pContent;
}T_MENU_t;

T_MENU_t MenuContent[]=
{
    {DISP_MENU_BASE,            PROMPT_MENU_BASE},
    {DISP_MENU_PAYMENT,         PROMPT_MENU_TXN},
    {DISP_MENU_QRCODE,          PROMPT_MENU_QR},
    {DISP_MENU_VERSION,         PROMPT_MENU_VERSION},
    {DISP_MENU_CONFIGURATION,   PROMPT_MENU_CONFIGURATION},
    {DISP_MENU_DEV_VER,         PROMPT_MENU_DEV_VERSION},
    {DISP_MENU_PCI_VER,         PROMPT_MENU_PCI_VERSION},
    {DISP_MENU_WIFI,            PROMPT_MENU_WIFI_CONFIGURATION},
    {DISP_MENU_CELLULAR,        PROMPT_MENU_CELLULAR_CONFIGURATION},
    {DISP_MENU_GPS,             PROMPT_MENU_GPS_CONFIGURATION},
    {DISP_MENU_FLIGHT,          PROMPT_MENU_FLIGHT_CONFIGURATION},
    {DISP_MENU_WIFI_OPEN,       PROMPT_MENU_WIFI_OPEN},
    {DISP_MENU_WIFI_CLOSE,      PROMPT_MENU_WIFI_CLOSE},
    {DISP_MENU_WIFI_CONNECT,    PROMPT_MENU_WIFI_SEARCH},
    {DISP_MENU_WIFI_DISCONNECT, PROMPT_MENU_WIFI_DISCONNECT},
    {DISP_MENU_WIFI_DETAIL,     PROMPT_MENU_WIFI_DETAILS},
    {DISP_MENU_WIFI_SSID_INPUT, PROMPT_MENU_WIFI_SSID_INPUT},
    {DISP_MENU_CELLULAR_OPEN,   PROMPT_MENU_CELLULAR_OPEN},
    {DISP_MENU_CELLULAR_CLOSE,  PROMPT_MENU_GPRS_CLOSE},
    {DISP_MENU_CELLULAR_DETAIL, PROMPT_MENU_CELLULAR_DETAILS},
    {DISP_MENU_CELLULAR_UPDATE, PROMPT_MENU_CELLULAR_UPDATE},
    {DISP_MENU_GPS_OPEN,        PROMPT_MENU_GPS_OPEN},
    {DISP_MENU_GPS_CLOSE,       PROMPT_MENU_GPS_CLOSE},
    {DISP_MENU_FLIGHT_OPEN,     PROMPT_MENU_FLIGHT_ON},
    {DISP_MENU_FLIGHT_CLOSE,    PROMPT_MENU_FLIGHT_OFF},
    {DISP_MENU_APN,             PROMPT_MENU_UPDATE_APN},
    {DISP_MENU_CERT,            PROMPT_MENU_UPDATE_CERT},
    {DISP_MENU_ABOUT,           PROMPT_MENU_ABOUT},
    {DISP_MENU_MUILT_APP,       PROMPT_MENU_MUILT_APP},
    {DISP_MENU_OTA,             PROMPT_MENU_OTA},
    {DISP_MENU_MAIN_APP2,       PROMPT_MENU_MAIN_APP2}
};
static const char* PCI_VER[]=
{
    "QPOS Plus Version 2.2.0",
    "QPOS Plus-HW V2.2",
    "QPOS Plus-SW V2.2",
    "QPOS Plus-IFM V2.2",
    "Approval Number: 17098 0820 430 43c 43c BCTCTC"
};
/**************************************************************************
Menu Difinition End
**************************************************************************/
static pu8 g_pTransBuffer = NULL;

static const char* TransMenu[]=
{
    "1.SWIPE",
    "2.ICC",
    "3.NFC",
    "4.SWIPE/ICC/NFC"
};

static const char * ReqestMenu[]=
{
    "1.WIFI Request",
    "2.Cellular Request"
};

static u8 T_MODE[4]={0x02,0x01,0x07,0x04};
char* bcdnum2amt(char *bcdnum, u32 dwLen, char *strAmt)
{
    u32 dwAmt = 0, dwFct = 1;
    char *pchTmp = bcdnum + dwLen - 1;

    while(pchTmp >= bcdnum)
    {
        dwAmt += (*pchTmp&0x0F)*dwFct;
            dwFct *= 10;
        dwAmt += (*pchTmp>>4)*dwFct;
            dwFct *= 10;
        pchTmp--;
    }
    Sprintf_(strAmt, "%lu.%02lu", dwAmt/100, dwAmt%100);
    return strAmt;
}

char *tlv_bcdnum2str(u8* pTradingFile, u32 dwTag, char *strGet, char cSplit)
{
    u8 *pNode = tlv_find_(pTradingFile, dwTag);
    if(!pNode) return NULL;
    u32 dwLen = tlv_get_l_(pNode), i;
    u8* pCur = tlv_get_v_(pNode);
    for(i = 0; i < dwLen; i++)
	{
		if(cSplit)
		{
			sprintf(strGet + i*3, "%02x", *pCur++);
			*(strGet + i*3 + 2) = (i == (dwLen - 1) ? 0 : cSplit);
		}
		else
			sprintf(strGet + i*2, "%02x", *pCur++);
	}
    return strGet;
}

static void autoexec_bat_fun(void)
{
    #ifdef CFG_DBG
    TRACE(TRACE_LVL_INFO, "Hi %s\r\n", __FUNCTION__);
    #endif
    #if defined(CFG_TMS_OTA)
    dispStrWait(CENTER, PROMPT_TMS_SYNC,0);
    lark_sleep(8000);
    TmsFirmwareUpdate(false);
    #endif
}

static void LcdClearAllLine(Dev_Lcd_Mode_t mode)
{
    u32 i;
    for(i = 0; i < LCD_LINE_NUMS; i++)
        LcdClearLine(mode,i);
}
#if defined(CFG_DISP_TIME)
static void DispTime(char* dispTime)
{
    LcdClearLine(LCD_NORMAL,LCD_LINE_4);
    LcdPrint(LCD_NORMAL,LCD_LINE_4,LCD_CENTERED_ALIGNMENT,CH_HEIGHT,dispTime);
}
#endif
void dispStrWait(u8 bytAlign, char *strDisp, u32 dwSec)
{
	LcdClearAllLine(LCD_NORMAL);
	LcdPrint(LCD_NORMAL, LCD_LINE_0, bytAlign, CH_HEIGHT, strDisp);
	if(dwSec) waitKeyWanted(APP_EVENT_USER_CONFIRM, dwSec == DW_MAX ? DW_MAX : dwSec*1000);
}
void dispLineWait(u32 line, u8 bytAlign, char *strDisp, u32 dwSec)
{
	LcdClearLine(LCD_NORMAL, line);
	LcdPrint(LCD_NORMAL, line, bytAlign, CH_HEIGHT, strDisp);
	if(dwSec) waitKeyWanted(APP_EVENT_USER_CONFIRM, dwSec == DW_MAX ? DW_MAX : dwSec*1000);
}
void DispHttpRequst(void)
{
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT,PROMPT_PROCESSING);
}
/*--------------------------------------
|   Function Name:
|       waitKeyWanted
|   Description: wait any key for wanted time
|   Parameters:
|	dwEvt	: events wanted to wait for, 0 for any
|	dwMsec	: ms count to wait
|   Returns	: key value for valid key, KEY_NONE for timeout
+-------------------------------------*/
Key_Num_t waitKeyWanted(u32 dwEvt, u32 dwMsec)
{
	T_MSG_KEY msgKey = {.dwKey = KEY_NONE};
	u32 events;
	if(!dwEvt) dwEvt = 0xFFFFFFFF;
	dwEvt |= APP_EVENT_TIMEOUT;
	do{
		events = WaitEvents(KEY_EVT_ALL, dwMsec, (void *)&msgKey);
		switch(events)
		{
			case APP_EVENT_USER_CANCEL:
				msgKey.dwKey = KEY_CANCEL;
			break;

			case APP_EVENT_USER_CONFIRM:
				msgKey.dwKey = KEY_CONFIRM;
			break;

			case APP_EVENT_TIMEOUT:
				msgKey.dwKey = KEY_NONE;
			break;

			case APP_EVENT_KBD_OTHER:
			default:
			break;
		}
	}while(!(events&dwEvt));
	return (Key_Num_t)msgKey.dwKey;
}
static void LcdInputLineDisplay(u32 line, char * pDsiplay)
{
    LcdClearLine(LCD_NORMAL,LCD_LINE_2);
    LcdPrint(LCD_NORMAL, line, RIGHT, CH_HEIGHT, pDsiplay);
}
static Key_Num_t EntryKbdValueCallBack(void )
{
	return waitKeyWanted(0, 5000);
}
static pcs8 EntryTypeStringCallBack(pcs8 pPrompt)
{
    /*keyboard input mode,
	  for example, "A��a��N" indicates capital letter, lowercase, and number */
    lark_lcd_write(LCD_NORMAL, 60, 0, 12, 5, (pu8)pPrompt);
    return pPrompt;
}

static pcs8 EntryStringCallBack(pcs8 String)
{
    #ifdef CFG_DBG
    TRACE(TRACE_LVL_INFO,"String: %s\r\n", String);
    #endif
    LcdClearLine(LCD_NORMAL, g_EntryLine);
    LcdPrint(LCD_NORMAL, g_EntryLine, g_EntryAlignment, CH_HEIGHT, (const char * )String);
    return String;
}

static Rc_t LineInput(const char * pTitle, u32 Mode, char * pString, u32 dwStrLenMax )
{
    if(pTitle && Strlen_(pTitle) && g_EntryLine)
    {
        LcdClearAllLine(LCD_NORMAL);
        LcdClearLine(LCD_REVERSE, LCD_LINE_0);
        LcdPrint(LCD_REVERSE, LCD_LINE_0, CENTER, CH_HEIGHT, pTitle);
    }
    return TypewritingGetString(Mode, EntryKbdValueCallBack, EntryStringCallBack, EntryTypeStringCallBack, (pu8 )pString, dwStrLenMax);
}

static pcs8 EntryStringMaskCallBack(pcs8 String)
{
    char * pStr = NULL;

    pStr = lark_alloc_mem(Strlen_((char * )String) + 1);

    Memset_(pStr, 0x00, Strlen_((char * )String) + 1);
    Memset_(pStr, '*', Strlen_((char * )String));
    LcdClearLine(LCD_NORMAL, g_EntryLine);
    LcdPrint(LCD_NORMAL, g_EntryLine, g_EntryAlignment, CH_HEIGHT, (const char * )String);

    lark_free_mem(pStr);

    return String;
}

static pcs8 EntryStringCustomCallBack(pcs8 String)
{
    LcdInputLineDisplay(g_EntryLine, (char *)String);
    return String;
}
static void Format_amt(pu8 amt,pu8 Out)
{
    char * pAmount = NULL;
    pAmount = lark_alloc_mem(128);

    Memset_(pAmount, 0x00, 128);
    if(Strlen_((char * )amt) == 0)
    {
        Strcat_(pAmount, "0.00");
    }
    else if(Strlen_((char * )amt) == 1)
    {
        Strcat_(pAmount, "0.0");
        Strcat_(pAmount,(char*)amt);
    }
    else if(Strlen_((char * )amt) ==2)
    {
        Strcat_(pAmount, "0.");
        Strcat_(pAmount,(char*)amt);
    }
    else
    {
        Memcpy_(pAmount, amt, Strlen_((char * )amt)-2);
        Strcat_(pAmount, ".");
        Memcpy_(&pAmount[Strlen_(pAmount)],&amt[Strlen_((char*)amt)-2],2);
    }
    Memcpy_(Out,pAmount,Strlen_(pAmount));
    lark_free_mem(pAmount);

}
static pcs8 EntryStringAmountCallBack(pcs8 String)
{
    char * pAmount = NULL;
    char * pAmountDisplay = NULL;
    u32 Cnt = 0;

    pAmount = lark_alloc_mem(128);
    Memset_(pAmount, 0x00, 128);

    if(String[Strlen_((char * )String) - 1] == '|')
    {
        Cnt = 1;
    }
    else
    {
        Cnt = 0;
    }

    if(Strlen_((char * )String) == Cnt + 0)
    {
        Strcat_(pAmount, "0.00");
    }
    else if(Strlen_((char * )String) == Cnt + 1)
    {
        Strcat_(pAmount, "0.0");
        Memcpy_(&pAmount[Strlen_(pAmount)], &String[0], 2);
    }
    else if(Strlen_((char * )String) == Cnt + 2)
    {
        Strcat_(pAmount, "0.");
        Memcpy_(&pAmount[Strlen_(pAmount)], &String[0], 3);
    }
    else
    {
        Memcpy_(pAmount, String, Strlen_((char * )String) - (Cnt + 2));
        Strcat_(pAmount, ".");
        Memcpy_(&pAmount[Strlen_(pAmount)], &String[Strlen_((char * )String) - (Cnt + 2)], 3);
    }
    Strcat_(pAmount, " ");
    Strcat_(pAmount, AMOUNT_CURRENCY_SYMBOL);
    #ifdef CFG_DBG
    //TRACE(DBG_TRACE_LVL, "dst %s\r\n", pAmount);
    #endif

    pAmountDisplay = pAmount;
    LcdClearLine(LCD_NORMAL, g_EntryLine);
    LcdPrint(LCD_NORMAL, g_EntryLine, LCD_RIGHT_ALIGNMENT, CH_HEIGHT, pAmountDisplay);
    lark_free_mem(pAmount);
    return String;
}
/*--------------------------------------
|   Function Name:
|       EntryStringPinMaskCallBack
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static pcs8 EntryStringPinMaskCallBack(pcs8 String)
{
    char * pStr = NULL;
    u32 i=0;

    pStr = lark_alloc_mem(16);
    while(i<Strlen_((char*)String))
    {
        pStr[i++]='*';
    }
    LcdClearLine(LCD_NORMAL,g_EntryLine);
    LcdPrint(LCD_NORMAL, g_EntryLine, g_EntryAlignment,CH_HEIGHT, (const char * )pStr);
    lark_free_mem(pStr);
    return String;
}
/*--------------------------------------
|   Function Name:
|       EntryStringInput
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t EntryStringInput(const char * pTitle, u32 Line, u32 Alignment, u32 Mode, const char Mask, char * pString, u32 StringLengthMax )
{
    /*������ʾ����*/
    if(pTitle != NULL &&Strlen_(pTitle) && Line != LCD_LINE_0)
    {
        LcdClearAllLine(LCD_NORMAL);
        LcdClearLine(LCD_REVERSE, LCD_LINE_0);
        LcdPrint(LCD_REVERSE, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, pTitle);
    }
    Rc_t ret=RC_FAIL;

    g_EntryLine = Line;
    g_EntryAlignment = Alignment;

    /*�ж��ַ���ʾģʽ*/
    if(Mask == 0x01)
    {
        ret=TypewritingGetString(Mode,EntryKbdValueCallBack,EntryStringMaskCallBack,EntryTypeStringCallBack,(pu8 )pString,StringLengthMax);
    }
    else if(Mask == 0x02)
    {
        ret=TypewritingGetString(Mode,EntryKbdValueCallBack,EntryStringCustomCallBack,EntryTypeStringCallBack,(pu8 )pString,StringLengthMax);
    }
    else if(Mask == 0x03)
    {
        ret=TypewritingGetString(Mode,EntryKbdValueCallBack,EntryStringAmountCallBack,EntryTypeStringCallBack,(pu8 )pString,StringLengthMax);
    }
    else if(Mask == 0x04)
    {
        ret=TypewritingGetString(Mode,EntryKbdValueCallBack,EntryStringPinMaskCallBack,EntryTypeStringCallBack,(pu8 )pString,StringLengthMax);
    }
    else
    {
        ret=TypewritingGetString(Mode,EntryKbdValueCallBack,EntryStringCallBack,EntryTypeStringCallBack,(pu8 )pString,StringLengthMax);
    }
    return ret;
}
static const char * GetMenuConsts(Lcd_Disp_Menu_t option )
{
   u32 i=0;
   for(i=0;i<sizeof(MenuContent)/sizeof(*MenuContent);i++)
   {
        if(option==MenuContent[i].Menu_opt)
        {
            break;
        }
   }
   return MenuContent[i].pContent;
}
/*--------------------------------------
|   Function Name:
|       DispStartTransaction
|   Description:
|   Returns:
+-------------------------------------*/
static void DispStartTransaction(pLcdStartTransaction_t pParam )
{
    LcdClearAllLine(LCD_NORMAL);
    if(pParam->amt)
    {
        char strAmt[32] = {0};
		Sprintf_(strAmt, "%s ", pParam->amt);
		Strcat_(strAmt,AMOUNT_CURRENCY_SYMBOL);
        LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT, strAmt);
    }
	char *strDisp[] =
	{
		PROMPT_PLS_USE_NFC, PROMPT_PLS_USE_ICC, PROMPT_PLS_USE_ICC_NFC, PROMPT_PLS_USE_MAG,
		PROMPT_PLS_USE_MAG_NFC, PROMPT_PLS_USE_MAG_ICC, PROMPT_PLS_USE_MAG_ICC_NFC
	};
	Card_Type_t type = pParam->type & (CARD_MAG|CARD_IC|CARD_NFC);
	if(type)
		LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT, strDisp[type - 1]);
}
/*--------------------------------------
|   Function Name:
|       MenuDisplayInputAmountMobileRefN
|   Description:
|   Returns:
+-------------------------------------*/
void MenuDisplayInputAmountMobileRefN(void)
{
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LEFT, CH_HEIGHT,"AMOUNT:");
    LcdPrint(LCD_NORMAL, LCD_LINE_0, RIGHT, CH_HEIGHT,AMOUNT_CURRENCY_SYMBOL);
}
/*--------------------------------------
|   Function Name:
|       MenuTransactionAmountQR
|   Description:
|   Returns:
+-------------------------------------*/
static Rc_t MenuTransactionAmountQR(Key_Num_t Key_Num, pu8 pTransBuffer)
{
    Rc_t ret=RC_FAIL;
    char Tem[32]={0};
    Sprintf_(Tem,"%d",Key_Num);
    ret=EntryStringInput(NULL, LCD_LINE_2, LCD_RIGHT_ALIGNMENT, BIT(3), 0x03, (char * )Tem, 12);
    if(ret==RC_SUCCESS)
    {
        Format_amt(Tem,pTransBuffer);
    }
    return ret;

}
/*--------------------------------------
|   Function Name:
|       MenuTransactionAmountMobileRefN
|   Description:
|   Returns:
+-------------------------------------*/
static Rc_t MenuTransactionAmountMobileRefN(Key_Num_t Key_Num, pu8 pTransBuffer)
{
    Rc_t ret=RC_FAIL;
    pu8 pBuffer = NULL;
    char Tem[32]={0};
    Sprintf_(Tem,"%d",Key_Num);
    ret=EntryStringInput(NULL, LCD_LINE_2, LCD_RIGHT_ALIGNMENT, BIT(3), 0x03, (char * )Tem, 12);
    if(ret==RC_SUCCESS)
    {
        pBuffer = lark_alloc_mem(64);
        Memset_(pBuffer, 0x00, 64);
        format_amt_ascn_to_bcd6(Tem, Strlen_((char * )Tem), pBuffer);
        tlv_replace_(pTransBuffer, TAG_EMV_AMOUNT_AUTH_NUM, 6, pBuffer);
        lark_free_mem(pBuffer);
    }
    return ret;

}
/*--------------------------------------
|   Function Name:
|       TransaInit
|   Description:
|   Returns:
+-------------------------------------*/
static void TransaInit(pu8 pTransBuffer)
{
    tlv_add_child_(pTransBuffer, TAG_EMV_TRANSACTION_TYPE, 1, (pu8)"\x01");
    tlv_add_child_(pTransBuffer, POS_TAG_TRADE_MODE, 1, (pu8)"\x08");


    //Transaction date
    //tlv_add_child_(pTransBuffer, TAG_EMV_TRANSACTION_DATE, 0, NULL);

    //Transaction time
    //tlv_add_child_(pTransBuffer, TAG_EMV_TRANSACTION_TIME, 0, NULL);

    //Random digit
    //tlv_add_child_(pTransBuffer, TAG_EMV_UNPREDICTABLE_NUMBER, 0, NULL);

    //custom defined data
    //tlv_add_child_(pTransBuffer, POS_TAG_ADDITIONAL_DATA, 0, NULL);

    //Key group index
    //tlv_add_child_(pTransBuffer, POS_TAG_KEY_INDEX, 0, NULL);

    //is to input amount on pos
    tlv_add_child_(pTransBuffer, POS_TAG_INPUT_AMOUNT_ON_POS, 1, (pu8)"\x00");

    //is to display amount on pos
    tlv_add_child_(pTransBuffer, POS_TAG_DISPLAY_AMOUNT_ON_POS, 1, (pu8)"\x01");

    //transaction currency code
    tlv_add_child_(pTransBuffer, TAG_EMV_TRANSACTION_CURRENCY_CODE, 2, (pu8)"\x03\x56");

    //accquire mode
    tlv_add_child_(pTransBuffer, POS_TAG_GETTAG_OBLIGATE, 1, (pu8)"\xFF");

    //card type
    tlv_add_child_(pTransBuffer, POS_TAG_GETTAG_CARD_TYPE, 1, (pu8)"\x00");
    //amount point
    tlv_add_child_(pTransBuffer, POS_TAG_DISP_AMOUNT_POINT, 1, (pu8)"\x01");

    //transaction result
    tlv_add_child_(pTransBuffer, POS_TAG_GETTAG_TRANS_RESULT, 1, (pu8)"\x01");

    //tag accquired in total
    tlv_add_child_(pTransBuffer, POS_TAG_GETTAG_TAG_NUMS, 1, (pu8)"\x00");
}
/*--------------------------------------
|   Function Name:
|       DisplayReqMenu
|   Description:
|   Returns:
+-------------------------------------*/
static void DisplayReqMenu(u32 index)
{
    u32 i=0;
    u32 mode=LCD_NORMAL;
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL,LCD_LINE_0, LCD_CENTERED_ALIGNMENT,CH_HEIGHT,PROMPT_MENU_TITLE);
    for(i=0;i<SIZE_ARRAY(ReqestMenu);i++)
    {
        mode=(index==i?LCD_REVERSE:LCD_NORMAL);
        LcdPrint(mode,LCD_LINE_1+i, LCD_LEFT_ALIGNMENT,CH_HEIGHT,ReqestMenu[i]);
    }
}
/*--------------------------------------
|   Function Name:
|       DisplayTransMenu
|   Description:
|   Returns:
+-------------------------------------*/
static void DisplayTransMenu(u32 index)
{
    u32 i=0;
    u32 mode=LCD_NORMAL;
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL,LCD_LINE_0, LCD_CENTERED_ALIGNMENT,CH_HEIGHT,PROMPT_MENU_TITLE);
    for(i=0;i<SIZE_ARRAY(TransMenu);i++)
    {
        mode=(index==i?LCD_REVERSE:LCD_NORMAL);
        LcdPrint(mode,LCD_LINE_1+i, LCD_LEFT_ALIGNMENT,CH_HEIGHT,TransMenu[i]);
    }
}
/*--------------------------------------
|   Function Name:
|       SelectTransMode
|   Description:poll card mode(MAG,ICC,NFC),if timeout,MAG|ICC|NFC
|   Returns:
+-------------------------------------*/
static Rc_t SelectTransMode(pu8 pTransBuffer)
{
    bool loop=true;
    u32 events=0;
    u32 index=0;
    Rc_t Rc=RC_FAIL;
    pu8 pbuffer = lark_alloc_mem(32);
    while(loop)
    {
        DisplayTransMenu(index);
        events=WaitEvents(APP_EVENT_USER_CANCEL | APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER, 50000,pbuffer);
        switch(events)
        {
           case APP_EVENT_USER_CANCEL:
                loop=false;
                Rc=RC_QUIT;
                break;
           case APP_EVENT_TIMEOUT:
                loop=false;
                Rc=RC_SUCCESS;
                break;
           case APP_EVENT_USER_CONFIRM:
                Rc=RC_SUCCESS;
                loop=false;
                tlv_modify_(pTransBuffer, POS_TAG_TRADE_MODE, 1, &T_MODE[index]);
                break;
           case APP_EVENT_KBD_OTHER:
                switch(((pu32 )pbuffer)[0])
                {
                    case KEY_UP:
                        if(index>0)
                        {
                            index-=1;
                        }
                        else
                        {
                            index=SIZE_ARRAY(TransMenu)-1;
                        }
                        break;
                    case KEY_DOWN:
                        if(index<SIZE_ARRAY(TransMenu)-1)
                        {
                            index+=1;
                        }
                        else
                        {
                            index=0;
                        }
                        break;
                }
                break;
        }
    }
    lark_free_mem(pbuffer);
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       RequestSelect
|   Description:server request method selection(WIFI or Cellular)
|   Returns:
+-------------------------------------*/
Rc_t RequestSelect(void)
{
    bool loop=true;
    u32 events=0;
    u32 index=0;
    Rc_t Rc=RC_FAIL;
    pu8 pbuffer = lark_alloc_mem(32);
    while(loop)
    {
        DisplayReqMenu(index);
        events=WaitEvents(APP_EVENT_USER_CANCEL | APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER, 50000,pbuffer);
        switch(events)
        {
           case APP_EVENT_USER_CANCEL:
                loop=false;
                Rc=RC_QUIT;
                break;
           case APP_EVENT_TIMEOUT:
                loop=false;
                Rc=RC_SUCCESS;
                SetRequestMode(0);
                break;
           case APP_EVENT_USER_CONFIRM:
                Rc=RC_SUCCESS;
                loop=false;
                SetRequestMode(index);
                break;
           case APP_EVENT_KBD_OTHER:
                switch(((pu32 )pbuffer)[0])
                {
                    case KEY_UP:
                        if(index>0)
                        {
                            index-=1;
                        }
                        break;
                    case KEY_DOWN:
                        if(index<SIZE_ARRAY(ReqestMenu)-1)
                        {
                            index+=1;
                        }
                        else
                        {
                            index=0;
                        }
                        break;
                }
                break;
        }
    }
    lark_free_mem(pbuffer);
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       MenuPaymentQR
|   Description:
|   Returns:
+-------------------------------------*/
static Rc_t MenuPaymentQR()
{
    char strQrcode[100] = "$:";
    pu8 pNode=NULL;
    u32 idx=0;
    QR_CORE_T Qr_code;

    Qr_code.pQrCode=strQrcode;
    Qr_code.offset_x=80;
    Qr_code.offset_y=25;//offset_y: min size is 25
    Qr_code.Multi_zoom=7;//Multi_zoom:min size is 4(4-9)
    Qr_code.Ms_timeout=10000;//Ms_timeout:set in Milliseconds(min timeout is 1000)
    dispStrWait(LEFT, PROMPT_PLS_INPUT_AMOUNT, 0);
    idx+=Strlen_("$:");
    if(MenuTransactionAmountQR(KEY_0,&strQrcode[idx])==RC_SUCCESS)
    {
        dispStrWait(CENTER, PROMPT_QRCODE_GENERATE, 0);
        lark_buzzer_blink(1000, 1000, 1);
        LcdClearAll(LCD_NORMAL);
        LcdPrint(LCD_NORMAL, LCD_LINE_5,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,strQrcode);
        if(QR_Generate(&Qr_code)==RC_FAIL)
        {
            dispStrWait(CENTER, PROMPT_QRCODE_GENERATE_FAIL, 3);
        }
    }

    return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       PaymentStart
|   Description:g_pTransBuffer:tranaction pool buffer(format in tlv)
|   Returns:
+-------------------------------------*/
static Rc_t PaymentStart(Key_Num_t Key_Num)
{
    Key_Num_t key=KEY_0;
    Rc_t Rc=RC_SUCCESS;
    if(g_pTransBuffer!=NULL)
    {
        lark_free_mem(g_pTransBuffer);
        g_pTransBuffer=NULL;
    }
    g_pTransBuffer = lark_alloc_mem(TRANSACTION_POOL_LENGTH);
    tlv_new_(g_pTransBuffer, POS_TAG_HEAD, 0, NULL);
    TransaInit(g_pTransBuffer);

    LcdClearAllLine(LCD_NORMAL);
    MenuDisplayInputAmountMobileRefN();
    if(Key_Num<KEY_9)
    {
        key=Key_Num;
    }
    if(MenuTransactionAmountMobileRefN(key, g_pTransBuffer)==RC_SUCCESS)
    {
        #ifdef CFG_TRANS_MODE
        Rc=SelectTransMode(g_pTransBuffer);
        #endif
        if(Rc==RC_SUCCESS)
        {
            StartTrading(g_pTransBuffer);
        }
        else if(Rc==RC_QUIT)
        {
            LcdClearAllLine(LCD_NORMAL);
            LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Transaction Cancel");
            waitKeyWanted(APP_EVENT_USER_CONFIRM,3000);
        }
    }
    if(g_pTransBuffer!=NULL)
    {
        lark_free_mem(g_pTransBuffer);
        g_pTransBuffer=NULL;
    }
	return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       dispVersion
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t dispVersion()
{
    pu8 pVer=lark_alloc_mem(128);
    u32 idx=0;
    Memset_(pVer,0x00,128);
    LcdClearAll(LCD_NORMAL);
    LcdPrint(LCD_REVERSE, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Version");
    //boot version
    Memcpy_(&pVer[idx],"Boot: ",Strlen_("Boot: "));
    idx+=Strlen_("Boot: ");
    idx+=GetBootVer(&pVer[idx]);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, LCD_LEFT_ALIGNMENT, CH_HEIGHT,(char*)pVer);
    Memset_(pVer,0x00,128);
    idx=0;
    Memcpy_(&pVer[idx],"App: ",Strlen_("App: "));
    idx+=Strlen_("App: ");
    idx+=GetAppVer(&pVer[idx]);
    LcdPrint(LCD_NORMAL, LCD_LINE_2, LCD_LEFT_ALIGNMENT, CH_HEIGHT,(char*)pVer);
    Memset_(pVer,0x00,128);
    idx=0;
    Memcpy_(&pVer[idx],"Wifi: ",Strlen_("Wifi: "));
    idx+=Strlen_("Wifi: ");
    idx+=GetWifiVersion(&pVer[idx]);
    LcdPrint(LCD_NORMAL, LCD_LINE_3, LCD_LEFT_ALIGNMENT, CH_HEIGHT,(char*)pVer);
    Memset_(pVer,0x00,128);
    idx=0;
    Memcpy_(&pVer[idx],"Cellular: ",Strlen_("Cellular: "));
    idx+=Strlen_("Cellular: ");
    idx+=GetCellularVersion(&pVer[idx]);
    LcdPrint(LCD_NORMAL, LCD_LINE_4, LCD_LEFT_ALIGNMENT, CH_HEIGHT,(char*)pVer);
    Memset_(pVer,0x00,128);
    idx=0;
    Memcpy_(&pVer[idx],"Gps: ",Strlen_("Gps: "));
    idx+=Strlen_("Gps: ");
    idx+=GetGpsVersion(&pVer[idx]);
    LcdPrint(LCD_NORMAL, LCD_LINE_5, LCD_LEFT_ALIGNMENT, CH_HEIGHT,(char*)pVer);
    waitKeyWanted(APP_EVENT_USER_CONFIRM,5000);
    lark_free_mem(pVer);
	return RC_SUCCESS;
}
static u32 FormatDeviceInfo(pu8 pInfo,T_U8_VIEW List[])
{
    pu8 pCur=pInfo;
    u32 cnt=0;
    u32 idx=0;
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"%s\r\n",__FUNCTION__);
    TRACE_ASCII(DBG_TRACE_LVL,pInfo,Strlen_(pInfo));
    #endif
    List[idx].head=pCur;
    while(cnt<Strlen_(pInfo))
    {
        if(*pCur=='\n')
        {
            List[idx].len=cnt;
            idx++;
            pCur++;
            List[idx].head=pCur;
            cnt=0;
        }
        else
        {
            cnt++;
            pCur++;
        }
    }
    return idx;

}
static void DispAbout(T_U8_VIEW List[],u32 idx,u32 Total)
{
    u32 i=0;
    LcdClearAll(LCD_NORMAL);
    pu8 pDisp=lark_alloc_mem(128);
    for(i=0;i<5;i++)
    {
        Memset_(pDisp,0x00,128);
        if(idx+i<=Total)
        {
            Memcpy_(pDisp,List[idx+i].head,List[idx+i].len);
            LcdPrint(LCD_NORMAL,LCD_LINE_0+i,LEFT,CH_HEIGHT,pDisp);
        }

    }
    lark_free_mem(pDisp);
}
/*--------------------------------------
|   Function Name:
|       MenuAboutDevice
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t MenuAboutDevice()
{
    u32  num=0;
    bool loop=true;
    u32  index=0;
    u32  event=0;
    u32  cnt=1;
    pu8  pInfo=lark_alloc_mem(512);
    Memset_(pInfo,0x00,512);
    GetDevInfor(pInfo);
    T_U8_VIEW Info[10]={NULL,0};
    num=FormatDeviceInfo(pInfo,Info);
    while(loop)
    {
        DispAbout(Info,index,num);
        event=WaitEvents(APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM|APP_EVENT_USER_CANCEL, 3000,NULL);
        if(event==APP_EVENT_USER_CANCEL)
        {
            loop=false;
            break;
        }
        else
        {
            index=(index+5>num?0:5*cnt);
            cnt=(cnt*5>num?1:(cnt+1));
        }
    }
    lark_free_mem(pInfo);
	return RC_SUCCESS;
}
static void dispPciVer(u32 idx)
{
    u32 i=0;
    LcdClearAll(LCD_NORMAL);
    for(i=0;i<4;i++)
    {
        LcdPrint(LCD_NORMAL,LCD_LINE_0+i,LEFT,CH_HEIGHT,PCI_VER[idx+i]);
    }
}
/*--------------------------------------
|   Function Name:
|       dispPCIVersion
|   Description: display EMV PCI version
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t dispPCIVersion()
{
   u32 event=0;
   bool loop=true;
   u32 index=0;

   while(loop)
   {
        dispPciVer(index);
        event=WaitEvents(APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM|APP_EVENT_USER_CANCEL, 3000,NULL);
        if(event==APP_EVENT_USER_CANCEL)
        {
            loop=false;
            break;
        }
        else
        {
            index=(index+5>SIZE_ARRAY(PCI_VER)?0:1);
        }
   }
   return RC_SUCCESS;
}

void lcd_sign_menu(void)
{
    LcdClearAll(LCD_NORMAL);
    lark_lcd_fill_area(0, 200, 320, 40, 0x00FF00);
    LcdPosition(LCD_TRANS,10,203,CH_HEIGHT,"CLR");
    LcdPosition(LCD_TRANS,270,203,CH_HEIGHT,"OK");
    lark_lcd_sign_init(0, 0, 320, 200, 0xFFFFFF);
}
static Callback_Type_t Get_callback_type(pu8 pTradingFile)
{
    Callback_Type_t result_type=CALLBACK_TRANS_MAG;
    T_U8_VIEW uvResult=get_tlv_view(pTradingFile,POS_TAG_RES_RESULT);
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"RES_RESULT=[%02x]\r\n",uvResult.head[0]);
    #endif
    if(UV_OK(uvResult))
    {
        switch(uvResult.head[0])
        {
            case 0x00:
            case 0x97:
                result_type=CALLBACK_TRANS_MAG;
                break;
            case 0x03:
                result_type=CALLBACK_TRANS_NFC_ONLINE;
                break;
            case 0x04:
                result_type=CALLBACK_TRANS_NFC_APPROVED;
                break;
            case 0x05:
                result_type=CALLBACK_TRANS_NFC_DECLINE;
                break;
            case 0x99:
                result_type=CALLBACK_TRANS_ICC_ONLINE;
                break;
            case 0x98:
                result_type=CALLBACK_TRANS_GAC2;
                break;
            case 0x96:
                result_type=CALLBACK_TRANS_APP_SELECT;
                break;
            case 0x95:
                result_type=CALLBACK_TRANS_REVERSAL;
                break;
            default:
                break;
        }
    }
    return result_type;

}
/*--------------------------------------
|   Function Name:
|       CustomMenuDisplay
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static void CustomMenuDisplay(pLcd_Custom_Menu_t dispMenu )
{
    u32 i = 0;
    u32 option_nums = 0;
    u32 cnt = 0;
    Dev_Lcd_Mode_t mode;
    char * pDisplayBuffer = NULL;
    const char * pConsts = NULL;
    dispMenu->update=true;
    if(!dispMenu->update)
    {
        dispMenu->update = true;
        return;
    }
    LcdClearLine(LCD_NORMAL, LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_CENTERED_ALIGNMENT, CH_HEIGHT, dispMenu->title);

    if(dispMenu->nums < LCD_LINE_NUMS - 1)
    {
        option_nums = dispMenu->nums + LCD_LINE_1;
    }
    else
    {
        option_nums = LCD_LINE_NUMS;
    }

    pDisplayBuffer = lark_alloc_mem(256);

    for(i=LCD_LINE_1; i<option_nums; i++)
    {

        Memset_(pDisplayBuffer, 0x00, 256);

        pConsts = (dispMenu->options)[dispMenu->start + i - LCD_LINE_1];

        cnt = 0;
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        ((pu8 )pDisplayBuffer)[cnt++] = dispMenu->start + i + '1' - 1;
        ((pu8 )pDisplayBuffer)[cnt++] = '.';
        Memcpy_(&((pu8 )pDisplayBuffer)[cnt], pConsts,Strlen_(pConsts));
        cnt += Strlen_(pConsts);
        ((pu8 )pDisplayBuffer)[cnt++] = '\0';

        if(dispMenu->start + i - LCD_LINE_1 == dispMenu->selected)
        {
            mode = LCD_REVERSE;
        }
        else
        {
            mode = LCD_NORMAL;
        }
        LcdClearLine(mode,i);
        LcdPrint(mode, i, LCD_LEFT_ALIGNMENT, CH_HEIGHT, pDisplayBuffer);
    }
    lark_free_mem(pDisplayBuffer);
}
/*--------------------------------------
|   Function Name:
|       AppSelectionProcess
|   Description:mutilple app selection
|   Parameters:
|               KERNEL_TAG_AID_LIST_CANDIDATE_COUNTER:tag for aids number
|               KERNEL_TAG_AID_LIST_DISPLAYNAME:tag for aid name
|               POS_TAG_RES_KN_APP_DATA:tlv data for aid list
|               KERNEL_TAG_AID_SELECT_RESULT_INDEX:Selected aid index(0-9)
|   Returns:
+-------------------------------------*/
Rc_t AppSelectionProcess(pu8 pTradingFile)
{
    pu8 pFile = NULL;
    pu8 pNode = NULL;
    pu8 pValue = NULL;
    u32 Length = 0;
    u32 AID_Numbers = 0;
    Lcd_Custom_Menu_t Lcd_Custom_Menu;
    bool loop = true;
    pvoid pbuffer = NULL;
    u32 events = 0;
    Rc_t Rc=RC_FAIL;

    Memset_(&Lcd_Custom_Menu, 0x00, sizeof(Lcd_Custom_Menu_t));

    pNode = tlv_find_(pTradingFile, POS_TAG_RES_KN_APP_DATA);
    if(!pNode)
    {
        goto AppSelectionProcess_end;
    }
    else{}

    pFile = tlv_get_v_(pNode);

    //TRACE_VALUE(DBG_TRACE_LVL, pFile, tlv_get_node_l_(pFile));

    //add app lelect process in there.

    pNode = tlv_find_(pFile, KERNEL_TAG_AID_LIST_CANDIDATE_COUNTER);
    if(!pNode)
    {
        goto AppSelectionProcess_end;
    }
    else{}

    pValue = tlv_get_v_(pNode);
    AID_Numbers = pValue[0];
    if(AID_Numbers == 0)
    {
        goto AppSelectionProcess_end;
    }
    else{}

    //TRACE(DBG_TRACE_LVL, "AID_Numbers : %d\r\n", AID_Numbers);

    pNode = tlv_find_(pFile, KERNEL_TAG_AID_LIST_DISPLAYNAME);

    while(Lcd_Custom_Menu.nums < AID_Numbers)
    {
        if(tlv_get_t_(pNode) != KERNEL_TAG_AID_LIST_DISPLAYNAME)
        {
            goto AppSelectionProcess_end;
        }
        else{}
        pValue = tlv_get_v_(pNode);
        Length = tlv_get_l_(pNode);
        Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums] = lark_alloc_mem(Length + 1);
        Memset_(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums], 0x00, Length + 1);
        Memcpy_(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums], pValue, Length);
        //TRACE(DBG_TRACE_LVL, "AID%d: %s\r\n", Lcd_Custom_Menu.nums, Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums]);
        Lcd_Custom_Menu.nums++;
        pNode += tlv_get_node_l_(pNode);
    }

    Lcd_Custom_Menu.title = lark_alloc_mem(32);
    Memset_(Lcd_Custom_Menu.title, 0x00, 32);
    Memcpy_(Lcd_Custom_Menu.title, "Select APP Please", Strlen_("Select APP Please"));//Select APP Please

    pbuffer = lark_alloc_mem(64);
    Memset_(pbuffer,0x00,64);
    //Lcd_Custom_Menu.update = true;
    while(loop)
    {
        CustomMenuDisplay(&Lcd_Custom_Menu);
        events = WaitEvents(APP_EVENT_USER_CANCEL | APP_EVENT_RESET | APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER, 10000,pbuffer);
        switch(events)
        {
        case APP_EVENT_USER_CANCEL:
            loop = false;
            break;
        case APP_EVENT_RESET:
            loop = false;
            break;
        case APP_EVENT_TIMEOUT:
            loop = false;
            Rc=RC_SUCCESS;
            ((pu8)pbuffer)[0] =0;
            break;
        case APP_EVENT_USER_CONFIRM:
            ((pu8)pbuffer)[0] = Lcd_Custom_Menu.selected;
            Rc=RC_SUCCESS;
            loop = false;
            break;
        case APP_EVENT_KBD_OTHER:
            switch(((pu32 )pbuffer)[0])
            {
            case KEY_UP:
                if(Lcd_Custom_Menu.selected > 0)
                {
                    if(Lcd_Custom_Menu.start > 0 && Lcd_Custom_Menu.selected == Lcd_Custom_Menu.start)
                    {
                        (Lcd_Custom_Menu.start)--;
                    }
                    else{}
                    (Lcd_Custom_Menu.selected)--;
                }
                else
                {
                    Lcd_Custom_Menu.update = false;
                }
                break;
            case KEY_DOWN:
                if(Lcd_Custom_Menu.selected < Lcd_Custom_Menu.nums - 1)
                {
                    if(Lcd_Custom_Menu.selected - Lcd_Custom_Menu.start == LCD_LINE_NUMS - 2)
                    {
                        (Lcd_Custom_Menu.start)++;
                    }
                    else{}
                    (Lcd_Custom_Menu.selected)++;
                }
                else
                {
                    Lcd_Custom_Menu.update = false;
                }
                break;
            default:
                Lcd_Custom_Menu.update = false;
                break;
            }
            break;
        default:
            Lcd_Custom_Menu.update = false;
            break;
        }
    }

    lark_free_mem(pbuffer);

    Lcd_Custom_Menu.nums = 0;
    while(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums])
    {
        lark_free_mem(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums]);
        Lcd_Custom_Menu.nums++;
    }
    tlv_add_child_(pTradingFile, KERNEL_TAG_AID_SELECT_RESULT_INDEX, 1, pbuffer);
    return Rc;

AppSelectionProcess_end:

    Lcd_Custom_Menu.nums = 0;
    while(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums])
    {
        lark_free_mem(Lcd_Custom_Menu.options[Lcd_Custom_Menu.nums]);
        Lcd_Custom_Menu.nums++;
    }

    tlv_replace_(pTradingFile, POS_TAG_RES_KN_APP_DATA, 0, NULL);
    return Rc;
}

#if defined(CFG_CARD_TRANS) || defined(CFG_8583)
const u32 g_TransTlv[]=
{
    POS_TAG_RES_RESULT,
    POS_TAG_RES_PAN,
    POS_TAG_RES_CARDHOLDER,
    TAG_EMV_AID_TERMINAL,
    POS_TAG_RES_SERVICE_CODE,
    TAG_EMV_AMOUNT_AUTH_NUM,
    TAG_EMV_TRANSACTION_TYPE,
    TAG_EMV_CVM_RESULTS,
    POS_TAG_CLEAR_TRACK2,
    POS_TAG_ASC_PIN,
    POS_TAG_RES_VALID_PERIOD,
    POS_TAG_RES_KN_ONLINE_DATA,
    TAG_EMV_CRYPTOGRAM_INFO_DATA,
    TAG_EMV_TERMINAL_CAPABILITIES
};
/*--------------------------------------
|   Function Name:
|       CardTransactionTlvObtain
|   Description:obtain tlv data needed to Transaction pool(g_pTransBuffer)
|   Returns:
+-------------------------------------*/
static void CardTransactionTlvObtain(pu8 pTradingFile)
{
    u32 i=0;
    for(i=0;i<SIZE_ARRAY(g_TransTlv);i++)
    {
        tlv_move_(pTradingFile,g_pTransBuffer,g_TransTlv[i]);
    }
}
#endif
static Rc_t FunServerGac2(pu8 pTlvbuffer)
{
    Rc_t Rc=RC_SUCCESS;
    /*1st Gen AC success*/
    T_U8_VIEW tag_9F27=get_tlv_view(g_pTransBuffer,TAG_EMV_CRYPTOGRAM_INFO_DATA);
    if(UV_OK(tag_9F27))
    {
        TRACE_VIEW(DBG_TRACE_LVL,tag_9F27);
        if(Memcmp_(tag_9F27.head,"\x40",tag_9F27.len)==0)
        {
            /*2nd Gen AC success*/
            TRACE(DBG_TRACE_LVL,"2nd Gen AC success\r\n");
            T_U8_VIEW cvm_result=get_tlv_view(g_pTransBuffer,TAG_EMV_CVM_RESULTS);
            T_U8_VIEW Terminal_cap=get_tlv_view(g_pTransBuffer,TAG_EMV_TERMINAL_CAPABILITIES);
            if(UV_OK(cvm_result) && UV_OK(Terminal_cap))
            {
                TRACE_VIEW(DBG_TRACE_LVL,cvm_result);
                TRACE_VIEW(DBG_TRACE_LVL,Terminal_cap);
                if((cvm_result.head[0] & 0x1E) && (Terminal_cap.head[1] & 0x20))
                {
                    #ifdef CFG_SIGNATURE_GAC
                    //Signature
                    Signature_Test();
                    #endif
                }
            }
        }
        else
        {
            /*2nd Gen AC fail*/
            TRACE(DBG_TRACE_LVL,"2nd Gen AC fail\r\n");
        }

    }
    return Rc;
}
static void PromptOnlineResult(Rc_t Rc,Callback_Type_t res)
{
   LcdClearAllLine(LCD_NORMAL);
   const char * msg=PROMPT_ONLINE_SUCCESS;
   switch(res)
   {
        case CALLBACK_TRANS_NFC_DECLINE:
            msg=PROMPT_NFC_DECLINE;
            break;
        case CALLBACK_TRANS_NFC_APPROVED:
            msg=PROMPT_NFC_APPROVED;
            break;
        case CALLBACK_TRANS_APP_SELECT:
            msg=PROMPT_PROCESSING;
            break;
        default:
            break;
   }

   msg=(Rc==RC_SUCCESS?msg:PROMPT_ONLINE_FAIL);
   if(Rc==RC_QUIT)
   {
        msg=PROMPT_ONLINE_CANCEL;
   }

   LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,msg);
}
void StartTradingOnlineHandler(pu8 pTradingFile)
{
    Rc_t Rc = RC_FAIL;
    u32 events=0;
    Callback_Type_t  result_type=Get_callback_type(pTradingFile);

    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"transaction callback type=%s\r\n",strCallback[result_type]);
    #endif

    #if defined(CFG_CARD_TRANS) || defined(CFG_8583)
    CardTransactionTlvObtain(pTradingFile);
    #endif

    switch(result_type)
    {
        case CALLBACK_TRANS_MAG:
            Rc=ServerReques_test();
            break;
        case CALLBACK_TRANS_ICC_ONLINE:
            #if defined(CFG_CARD_TRANS)
            Rc=CardTransaction_test(g_pTransBuffer)//card transaction test sample code
            if(Rc==RC_SUCCESS)
            {
                //callback online transaction result to card
                //2nd Gen AC,write response code / issue script back to IC card
                tlv_move_(g_pTransBuffer,pTradingFile,POS_TAG_RES_EN_ONLINE_RESULT);
            }
            #elif defined(CFG_8583)
            if(online_pay(g_pTransBuffer)==RC_SUCCESS)
            {
                //callback online transaction result to card
                //2nd Gen AC,write response code / issue script back to IC card
                tlv_move_(g_pTransBuffer,pTradingFile,POS_TAG_RES_EN_ONLINE_RESULT);
            }
            #else
            Rc=ServerReques_test();//server request test sample code
            #endif
            break;
        case CALLBACK_TRANS_NFC_ONLINE:
            Rc=ServerReques_test();
            break;
        case CALLBACK_TRANS_NFC_APPROVED:
        case CALLBACK_TRANS_NFC_DECLINE:
            Rc=RC_SUCCESS;
            break;
        case CALLBACK_TRANS_GAC2:
            Rc=FunServerGac2(g_pTransBuffer);
            break;
        case CALLBACK_TRANS_APP_SELECT:
            Rc=AppSelectionProcess(pTradingFile);
            break;
        case CALLBACK_TRANS_REVERSAL:
            break;
        default:
            break;
    }
    PromptOnlineResult(Rc,result_type);
}

/*--------------------------------------
|   Function Name:
|       WifiDisplayNotAvailable
|   Description: ��ʾWifi�����ú���
|   Parameters:
|   Returns:
+-------------------------------------*/
static void WifiDisplayNotAvailable(void)
{
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Wifi Not Available");
    LcdPrint(LCD_NORMAL, LCD_LINE_2, CENTER, CH_HEIGHT,"Please Replace POS");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 5000);
}

/*--------------------------------------
|   Function Name:
|       AnalysisWifiList
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 AnalysisWifiList(pu8 pValue,T_U8_VIEW list[])
{
    u32 i=0;
    u32 cnt=0;
    u32 len=0;
    //TRACE(DBG_TRACE_LVL,"%S\r\n",__FUNCTION__);
    //TRACE_ASCII(DBG_TRACE_LVL,pValue,Strlen_(pValue));
    while(pValue[cnt]!='{')
    {
        cnt++;
    }
    cnt++;
    list[i].head=&pValue[cnt];
    while(cnt<=Strlen_(pValue))
    {
        len++;
        cnt++;
        if(pValue[cnt]=='|')
        {
            list[i].len=len;
            len=0;
            i+=1;
            cnt++;
            list[i].head=&pValue[cnt];
        }
        else if(pValue[cnt]=='}')
        {
            list[i].len=len;
            break;
        }

    }
    return i+1;
}

/*--------------------------------------
|   Function Name:
|       DisplayWifiList
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static void DisplayWifiList(T_U8_VIEW list[],u32 select,u32 page,u32 list_num)
{
    u32 i=0;
    u32 idx=0;
    u32 cur_page=page*5;
    u32 mode=LCD_NORMAL;
    u8 DisplayLen = 0;
    LcdClearAll(LCD_NORMAL);
    pu8 pbuffer=lark_alloc_mem(128);
    LcdPrint(LCD_NORMAL, LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,PROMPT_WIFI_AP_LIST);
    for(i=0;i<5;i++)
    {
        mode=(i==select?LCD_REVERSE:LCD_NORMAL);
        idx=0;
        Memset_(pbuffer,0x00,128);
        if(cur_page+i<list_num)
        {
            idx+=Sprintf_(&pbuffer[idx],"%d.",i+1);
            Memcpy_(&pbuffer[idx],list[cur_page+i].head,list[cur_page+i].len);
            idx+=list[cur_page+i].len;
            if(mode==LCD_REVERSE)
            {
                //TRACE_ASCII(DBG_TRACE_LVL, pbuffer,idx);
            }

            /* һ�������ʾ17���ַ� */
            DisplayLen = Strlen_(pbuffer);
            if(DisplayLen > 16)
            {
                pbuffer[16] = '.';
                pbuffer[17] = '.';
                pbuffer[18] = '.';
                pbuffer[19] = '\0';
            }
            LcdPrint(mode,LCD_LINE_1+i,LCD_LEFT_ALIGNMENT, CH_HEIGHT,pbuffer);
        }
    }
    lark_free_mem(pbuffer);
}
#ifdef CFG_WIFI_DEBUG
static void TRACEWifiList(T_U8_VIEW list[],u32 num)
{
    u32 i=0;
    for(i=0;i<num;i++)
    {
        TRACE(DBG_TRACE_LVL,"%d.\t",i+1);
        TRACE_ASCII(DBG_TRACE_LVL,list[i].head,list[i].len);
    }
}
#endif
static Rc_t MenuWifi_Select(pu8 pWifiList,pu8 pWifi)
{
    u32 list_num=0;
    bool loop=true;
    u32 events=0;
    u32 cur=0;
    u32 index=0;
    u32 page=0;
    u32 key_select=0;
    u8 pbuffer[16]={0};
    Rc_t ret = RC_FAIL;

    T_U8_VIEW   uvlist[40]={NULL,0};
    list_num=AnalysisWifiList(pWifiList,uvlist);
    #ifdef CFG_WIFI_DEBUG
    TRACEWifiList(uvlist,list_num);
    #endif
    while(loop)
    {
        DisplayWifiList(uvlist,index,page,list_num);
        events=WaitEvents(APP_EVENT_USER_CANCEL | APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER, 50000,pbuffer);
        switch(events)
        {
            case APP_EVENT_USER_CANCEL:
            case APP_EVENT_TIMEOUT:
                loop=false;
                ret=RC_QUIT;
                break;
            case APP_EVENT_USER_CONFIRM:
                Memcpy_(pWifi,uvlist[cur].head,uvlist[cur].len);
                loop=false;
                ret=RC_SUCCESS;
                break;
            case APP_EVENT_KBD_OTHER:
                switch(((pu32 )pbuffer)[0])
                {
                    case KEY_UP:
                        if(cur>0)
                        {
                            if(page>0)
                            {
                                if(index>0)
                                {
                                    index-=1;
                                }
                                else
                                {
                                    page-=1;
                                    index=4;
                                }
                            }
                            else
                            {
                                if(index>0)
                                {
                                    index-=1;
                                }
                            }
                            cur-=1;
                        }
                        break;
                    case KEY_DOWN:
                        if(cur<list_num-1)
                        {
                            if(index<4)
                            {
                                index+=1;
                            }
                            else
                            {
                                index=0;
                                page+=1;
                            }
                            cur+=1;
                        }
                        break;
                    case KEY_1:
                    case KEY_2:
                    case KEY_3:
                    case KEY_4:
                    case KEY_5:
                        key_select=(pbuffer[0]-1)+page*5;
                        Memcpy_(pWifi,uvlist[key_select].head,uvlist[key_select].len);
                        loop=false;
                        ret=RC_SUCCESS;
                        break;
                    default:
                        break;
                }
                break;
        }
    }
    return ret;
}
/*--------------------------------------
|   Function Name:
|       MenuWIFI_Search
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t MenuWIFI_Search()
{
    char strApName[64] = {0}, strPassword[32] = {0};
	Rc_t ret = RC_FAIL;
	u8 res=0;
	pu8 pWifi=NULL;
	pu8 strApList = lark_alloc_mem(1024);
    Memset_(strApList, 0x00, 1024);
    LcdClearAll(LCD_NORMAL);
    dispStrWait(LCD_LEFT_ALIGNMENT, PROMPT_PROCESSING, 0);
    res=obtain_wifi_ap_list((char * )strApList, 1024);
    /* wifi ��Ӳ���汾��ƥ�� */
    if(res == 0x05)
    {
        WifiDisplayNotAvailable();
    }
    else if(res==0x00)
    {
        pWifi=lark_alloc_mem(64);
        Memset_(pWifi,0x00,64);
        ret=MenuWifi_Select(strApList,pWifi);
        if(ret==RC_SUCCESS)
        {
            char *strApSel=pWifi;
            ret=RC_FAIL;
            if(strApSel)
            {
                ret=EntryStringInput("WIFI Password", LCD_LINE_1, LCD_LEFT_ALIGNMENT, BIT(2), 0x00, strPassword, sizeof(strPassword));
        		if(ret==RC_SUCCESS && Strlen_(strPassword))
        		{
        			TRACE(TRACE_LVL_INFO,"%s,%s : %s\r\n",__FUNCTION__, strApSel, strPassword);
        			Sprintf_(strApName, "\"%s\"\n\"%s\"\n...", strApSel, strPassword);
        			ret=RC_FAIL;
        			LcdClearLine(LCD_NORMAL, LCD_LINE_0);
        			LcdPrint(LCD_NORMAL, LCD_LINE_0, LCD_LEFT_ALIGNMENT, CH_HEIGHT, strApSel);
        			LcdClearLine(LCD_NORMAL, LCD_LINE_1);
        			LcdPrint(LCD_NORMAL, LCD_LINE_1, LCD_LEFT_ALIGNMENT, CH_HEIGHT, PROMPT_PROCESSING);
        			ret = connect_wifi(strApSel, strPassword);
        		}
        		else
        		{
                    ret=RC_FAIL;
        		}
            }
            dispStrWait(LCD_LEFT_ALIGNMENT, ret == RC_SUCCESS ? PROMPT_CONNECT_SUCCESS : PROMPT_INPUT_ERROR, 5);
        }
        lark_free_mem(pWifi);
    }
    else
    {
        dispStrWait(LCD_LEFT_ALIGNMENT,PROMPT_WIFI_NO,5);
    }

    lark_free_mem(strApList);
	return ret;
}

static Rc_t MenuWIFI_Configuration()
{
	char strApName[64] = {0}, strPassword[32] = {0};
	Rc_t ret = RC_FAIL;

    LcdClearAllLine(LCD_NORMAL);
    LineInput("WIFI SSID", BIT(2), strApName, sizeof(strApName));
    if(Strlen_(strApName))
	{
		LineInput("WIFI Password", BIT(2), strPassword, sizeof(strPassword));
		if(Strlen_(strPassword))
		{
		    #ifdef CFG_DBG
			TRACE(TRACE_LVL_INFO,"Password: %s\r\n", strPassword);
			#endif
			dispStrWait(LEFT, PROMPT_PROCESSING, 0);
			ret = connect_wifi(strApName, strPassword);

            /* wifi ��Ӳ���汾��ƥ�� */
            if(ret == RC_QUIT)
            {
                WifiDisplayNotAvailable();
                ret = RC_FAIL;
                return ret;
            }
		}
	}

	dispStrWait(LEFT, ret == RC_SUCCESS ? PROMPT_CONNECT_SUCCESS : PROMPT_INPUT_ERROR, 5);
	#ifdef CFG_DBG
    TRACE(TRACE_LVL_INFO,"%s end\r\n",__FUNCTION__);
    #endif
	return ret;
}

static Rc_t MenuWIFI_Details()
{
	char strDetail[100] = {0}, *strDisp = obtain_wifi_details(strDetail, sizeof(strDetail));
	LcdClearAllLine(LCD_NORMAL);
	LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT, strDisp ? strDisp : "Not Connect Ap");

	waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
	return RC_SUCCESS;
}

static Rc_t MenuCellular_Details()
{
    char* strmsg=NULL;
    strmsg=lark_alloc_mem(256);
    Memset_(strmsg,0x00,256);
    obtain_Gprs_details(strmsg);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, LEFT, CH_HEIGHT,strmsg);
    lark_free_mem(strmsg);
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return RC_SUCCESS;
}

static Rc_t MenuCellular_Open()
{
    Rc_t Rc=RC_SUCCESS;
    Rc=lark_com_open(HOST_GPRS);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Cellular open Success");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return Rc;
}
static Rc_t MenuCellular_Close()
{
    lark_com_close(HOST_GPRS);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Cellular Closed");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return RC_SUCCESS;
}

static Rc_t MenuCellular_OTA()
{
    return QposUpdate();
}

static Rc_t MenuWifi_Open()
{
    Rc_t Rc=RC_SUCCESS;
    Rc=lark_com_open(HOST_WIFI);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"WIFI open Success");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return Rc;
}
static Rc_t MenuWifi_Close()
{
    lark_com_close(HOST_WIFI);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"WIFI Closed");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return RC_SUCCESS;
}
static Rc_t MenuGPS_Open()
{
    Rc_t Rc=RC_SUCCESS;
    Rc=lark_gps_open();
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"GPS open Success");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return Rc;
}
static Rc_t MenuGPS_Close()
{
    lark_gps_close();
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"GPS Closed");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return RC_SUCCESS;
}
static Rc_t MenuWIFI_Disconnect()
{
    Rc_t ret = RC_SUCCESS;

    LcdClearAllLine(LCD_NORMAL);
	ret = disonnect_current_wifi();

    if(ret == RC_QUIT)
    {
        WifiDisplayNotAvailable();
    }
    else if(ret == RC_SUCCESS)
    {
        dispStrWait(LEFT, PROMPT_DISCONNECTED, 5);
    }
    else
    {

    }

	return RC_SUCCESS;
}
static Rc_t MenuFlightMode_Open()
{
    Rc_t Rc=RC_SUCCESS;
    lark_com_close(HOST_WIFI);//wifi
    lark_com_close(HOST_GPRS);//gprs
    lark_gps_close();//GPS
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Flight Mode ON");
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return Rc;
}
static Rc_t MenuFlightMode_Close()
{
    Rc_t Rc=RC_SUCCESS;
    LcdClearAllLine(LCD_NORMAL);
    Rc=lark_com_open(HOST_WIFI);//wifi
    if(Rc==RC_SUCCESS)
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT,"WIFI ON");
    }
    else
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT,"WIFI OFF");
    }
    Rc=lark_com_open(HOST_GPRS);//Cellular
    if(Rc==RC_SUCCESS)
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Cellular ON");
    }
    else
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Cellular OFF");
    }
    Rc=lark_gps_open();//GPS
    if(Rc==RC_SUCCESS)
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_2, CENTER, CH_HEIGHT,"GPS ON");
    }
    else
    {
        LcdPrint(LCD_NORMAL, LCD_LINE_2, CENTER, CH_HEIGHT,"GPS OFF");
    }
    waitKeyWanted(APP_EVENT_USER_CANCEL | APP_EVENT_USER_CONFIRM, 30000);
    return Rc;
}
static Rc_t MenuApnUpdate()
{
    Rc_t Rc=RC_SUCCESS;
    u32 ret=0;
    pApnParam_t pApn=NULL;
    u8 apn_mcc[3]={0x04,0x06,0x00};
    u8 apn_mnc[2]={0x00,0x00};
    pApn=lark_alloc_mem(sizeof(ApnParam_t));
    Memset_(pApn,0x00,sizeof(ApnParam_t));
    Memcpy_(pApn->ApnName,OPERTOR_NAME,Strlen_(OPERTOR_NAME));
    pApn->ApnNameLen=Strlen_(OPERTOR_NAME);
    Memcpy_(pApn->Apn,APN_NAME,Strlen_(APN_NAME));
    pApn->ApnLen=Strlen_(APN_NAME);
    Memcpy_(pApn->Mcc,apn_mcc,sizeof(apn_mcc));
    Memcpy_(pApn->Mnc,apn_mnc,sizeof(apn_mnc));
    ret=QposUpdateApn(pApn);
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"ret=%d\r\n",ret);
    #endif
    lark_free_mem(pApn);
    dispStrWait(CENTER, PROMPT_APN_UPDATE_SUCCESS, 3);
    return Rc;
}
static Rc_t MenuUpdateCert()
{
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,PROMPT_PROCESSING);
    #if defined(CFG_SERVER_CERT_UPDATE)
    if(QposUpdateCert(SERVER_CERT,CERT_SERVER)==RC_FAIL)
    {
        dispStrWait(CENTER, "Server Cert update Fail", 0);
        return RC_FAIL;
    }
    #endif
    #if defined(CFG_CLIENT_CERT_UPDATE)
    if(QposUpdateCert(CLIENT_CERT,CERT_CLIENT)==RC_FAIL)
    {
        dispStrWait(CENTER, "Client Cert update Fail", 0);
        return RC_FAIL;
    }
    #endif
    #if defined(CFG_CLIENT_KEY_CERT_UPDATE)
    if(QposUpdateCert(CLIENT_KEY_CERT,CERT_CLIENT_KEY)==RC_FAIL)
    {
        dispStrWait(CENTER, "Clinet key Cert update Fail", 0);
        return RC_FAIL;
    }
    #endif

    dispStrWait(CENTER, PROMPT_CERT_UPDATE_SUCCESS, 3);
}

static Rc_t MenuMuiltApp2Select(void)
{
    bool loop = true;
    lark_flag_clr(OS_FLAG_GROUP_APP,APP_EVENT_TIMEOUT);
    timer_register(APP_TIMER_ID_EVENTS_TIMEOUT, 2000);
    LcdClearAllLine(LCD_NORMAL);
    LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT,"Shut Down......");
    LcdPrint(LCD_NORMAL, LCD_LINE_2, CENTER, CH_HEIGHT,"Please Power On Manually");
    while(loop)
    {
        if(lark_flag_is_true(OS_FLAG_GROUP_APP,APP_EVENT_TIMEOUT))
        {
            loop = false;
        }
    }
    lark_mulit_app_switch(2);
    return 0;
}

/*--------------------------------------
|   Function Name:
|       MenuDisplay
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static void MenuDisplay(pLcd_Menu_t dispMenu )
{
    u32 i = 0;
    u32 option_nums = 0;
    u32 cnt = 0;
    Dev_Lcd_Mode_t mode;
    char * pDisplayBuffer = NULL;
    const char * pConsts = NULL;

    if(!dispMenu->update)
    {
        dispMenu->update = true;
        return;
    }
    else{}

    LcdClearLine(LCD_NORMAL, LCD_LINE_0);
    LcdPrint(LCD_NORMAL, LCD_LINE_0, CENTER, CH_HEIGHT, GetMenuConsts(dispMenu->title));

    if(dispMenu->nums < LCD_LINE_NUMS - 1)
    {
        option_nums = dispMenu->nums + LCD_LINE_1;
    }
    else
    {
        option_nums = LCD_LINE_NUMS;
    }

    pDisplayBuffer = lark_alloc_mem(256);

    for(i=LCD_LINE_1; i<option_nums; i++)
    {

        Memset_(pDisplayBuffer, 0x00, 256);

        pConsts = GetMenuConsts((dispMenu->options)[dispMenu->start + i - LCD_LINE_1]);

        cnt = 0;
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        Memcpy_(&((pu8 )pDisplayBuffer)[cnt], pConsts,Strlen_(pConsts));
        cnt += Strlen_(pConsts);
        ((pu8 )pDisplayBuffer)[cnt++] = '\0';

        if(dispMenu->start + i - LCD_LINE_1 == dispMenu->selected)
        {
            mode = LCD_REVERSE;
        }
        else
        {
            mode = LCD_NORMAL;
        }

        LcdClearLine(mode,i);

        LcdPrint(mode, i, LEFT, CH_HEIGHT, pDisplayBuffer);

        Memset_(pDisplayBuffer, 0x00, 256);
        cnt = 0;
        ((pu8 )pDisplayBuffer)[cnt++] = ' ';
        if(dispMenu->start + i + 1 - 1>9)
        {
            Sprintf_(&((pu8 )pDisplayBuffer)[cnt++],"%d",dispMenu->start + i + 1 - 1);
            cnt++;
        }
        else
        {
            ((pu8 )pDisplayBuffer)[cnt++] = dispMenu->start + i + '1' - 1;

        }

        ((pu8 )pDisplayBuffer)[cnt++] = '.';
        ((pu8 )pDisplayBuffer)[cnt++] = '\0';

        LcdPrint(mode, i, LEFT, CH_HEIGHT, pDisplayBuffer);
    }

    for(i=option_nums; i<LCD_LINE_NUMS; i++)
    {
        LcdClearLine(LCD_NORMAL,i);
    }

    lark_free_mem(pDisplayBuffer);
}
static Rc_t MenuMainTxn(u32 opt)
{
    switch(opt)
    {
        case DISP_MENU_PAYMENT:
            return PaymentStart(KEY_0);
        case DISP_MENU_DEV_VER:
            return dispVersion();
        case DISP_MENU_PCI_VER:
            return dispPCIVersion();
        case DISP_MENU_WIFI_OPEN:
            return MenuWifi_Open();
        case DISP_MENU_WIFI_CLOSE:
            return MenuWifi_Close();
        case DISP_MENU_WIFI_CONNECT:
            return MenuWIFI_Search();
        case DISP_MENU_WIFI_DISCONNECT:
            return MenuWIFI_Disconnect();
        case DISP_MENU_WIFI_DETAIL:
            return MenuWIFI_Details();
        case DISP_MENU_WIFI_SSID_INPUT:
            return MenuWIFI_Configuration();
        case DISP_MENU_CELLULAR_OPEN:
            return MenuCellular_Open();
        case DISP_MENU_CELLULAR_CLOSE:
            return MenuCellular_Close();
        case DISP_MENU_CELLULAR_DETAIL:
            return MenuCellular_Details();
        case DISP_MENU_GPS_OPEN:
            return MenuGPS_Open();
        case DISP_MENU_GPS_CLOSE:
            return MenuGPS_Close();
        case DISP_MENU_FLIGHT_OPEN:
            return MenuFlightMode_Open();
        case DISP_MENU_FLIGHT_CLOSE:
            return MenuFlightMode_Close();
        case DISP_MENU_QRCODE:
            return MenuPaymentQR();
        case DISP_MENU_APN:
            return MenuApnUpdate();
        case DISP_MENU_CERT:
            return MenuUpdateCert();
        case DISP_MENU_ABOUT:
            return MenuAboutDevice();
        case DISP_MENU_MAIN_APP2:
            return MenuMuiltApp2Select();
        #if defined(CFG_TMS_OTA)
        case DISP_MENU_OTA:
            return TmsFirmwareUpdate(true);
       #endif
        case DISP_MENU_CELLULAR_UPDATE:
            return MenuCellular_OTA();
        default:
            break;
    }
    return RC_SUCCESS;
}
static u32 Menu_Main[]=
{
   DISP_MENU_PAYMENT,
   DISP_MENU_QRCODE,
   DISP_MENU_VERSION,
   DISP_MENU_CONFIGURATION,
   DISP_MENU_ABOUT,
   #if defined(CFG_TMS_OTA)
   DISP_MENU_OTA,
   #endif
   DISP_MENU_MUILT_APP
};

static u32 Menu_Version[]=
{
    DISP_MENU_DEV_VER,
    DISP_MENU_PCI_VER,
};

static u32 Menu_config[]=
{
    DISP_MENU_WIFI,
    DISP_MENU_CELLULAR,
    DISP_MENU_APN,
    DISP_MENU_CERT,
    DISP_MENU_GPS
};

static u32 Menu_wifi[]=
{
    DISP_MENU_WIFI_OPEN,
    DISP_MENU_WIFI_CLOSE,
    DISP_MENU_WIFI_CONNECT,
    DISP_MENU_WIFI_DISCONNECT,
    DISP_MENU_WIFI_DETAIL,
    DISP_MENU_WIFI_SSID_INPUT
};

static u32 Menu_Cellular[]=
{
    DISP_MENU_CELLULAR_OPEN,
    DISP_MENU_CELLULAR_CLOSE,
    DISP_MENU_CELLULAR_DETAIL,
    DISP_MENU_CELLULAR_UPDATE
};

static u32 Menu_Gps[]=
{
    DISP_MENU_GPS_OPEN,
    DISP_MENU_GPS_CLOSE
};

static u32 Menu_Flight[]=
{
    DISP_MENU_FLIGHT_OPEN,
    DISP_MENU_FLIGHT_CLOSE
};

static u32 Menu_Switch[]=
{
    DISP_MENU_MAIN_APP2
};
static MENU_ITEM_T Menu_init[]=
{
    {DISP_MENU_BASE,            SIZE_ARRAY(Menu_Main),      Menu_Main},
    {DISP_MENU_VERSION,         SIZE_ARRAY(Menu_Version),   Menu_Version},
    {DISP_MENU_CONFIGURATION,   SIZE_ARRAY(Menu_config),    Menu_config},
    {DISP_MENU_WIFI,            SIZE_ARRAY(Menu_wifi),      Menu_wifi},
    {DISP_MENU_CELLULAR,        SIZE_ARRAY(Menu_Cellular),  Menu_Cellular},
    {DISP_MENU_GPS,             SIZE_ARRAY(Menu_Gps),       Menu_Gps},
    {DISP_MENU_FLIGHT,          SIZE_ARRAY(Menu_Flight),    Menu_Flight},
    {DISP_MENU_MUILT_APP,       SIZE_ARRAY(Menu_Switch),    Menu_Switch}
};

static bool OptionMenuTitle(Lcd_Disp_Menu_t title)
{
    bool ret=false;
    switch(title)
    {
        case DISP_MENU_BASE:
            if(Capabilities(CAP_BT)==RC_FAIL)
            {
                ret=true;
            }
            break;
        case DISP_MENU_CONFIGURATION:
            if(Capabilities(CAP_GPS)==RC_FAIL)
            {
                ret=true;
            }
            break;
        case DISP_MENU_CELLULAR:
            if(Capabilities(CAP_FOTA)==RC_FAIL)
            {
                ret=true;
            }
            break;
        default:
            break;
    }
    return ret;
}
static void MenuBuild(pLcd_Menu_t pLcdMenu )
{
    u32 i=0;
    u32 j=0;

    for(i=0;i<SIZE_ARRAY(Menu_init);i++)
    {
        if(Menu_init[i].title==pLcdMenu->title)
        {
            pLcdMenu->start = 0;
            pLcdMenu->selected = 0;
            pLcdMenu->nums = Menu_init[i].item_cnt;
            if(OptionMenuTitle(pLcdMenu->title))
            {
                pLcdMenu->nums-=1;
            }

            for(j=0;j<pLcdMenu->nums;j++)
            {
                (pLcdMenu->options)[j]=Menu_init[i].items[j];
            }
        }
    }
}

void MenuMainProcess()
{
    bool loop = true;
    pLcd_Menu_t p_lcd_menu = NULL;
    pvoid pbuffer = NULL;
    u32 events = 0;
    pbuffer = lark_alloc_mem(32);
    p_lcd_menu = lark_alloc_mem(sizeof(Lcd_Menu_t));
    p_lcd_menu->title = DISP_MENU_BASE;
    MenuBuild(p_lcd_menu);
    while(loop)
    {
        MenuDisplay(p_lcd_menu);
        events=WaitEvents(APP_EVENT_USER_CANCEL | APP_EVENT_TIMEOUT | APP_EVENT_USER_CONFIRM | APP_EVENT_KBD_OTHER, 10000,pbuffer);
        switch(events)
        {
            case APP_EVENT_USER_CANCEL:
                 switch(p_lcd_menu->title)
                 {
                    case DISP_MENU_BASE:
                        loop = false;
                        break;
                    case DISP_MENU_PAYMENT:
                    case DISP_MENU_QRCODE:
                    case DISP_MENU_VERSION:
                    case DISP_MENU_CONFIGURATION:
                    case DISP_MENU_ABOUT:
                    case DISP_MENU_OTA:
                    case DISP_MENU_MUILT_APP:
                        p_lcd_menu->title = DISP_MENU_BASE;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_DEV_VER:
                    case DISP_MENU_PCI_VER:
                        p_lcd_menu->title = DISP_MENU_VERSION;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_WIFI:
                    case DISP_MENU_CELLULAR:
                    case DISP_MENU_GPS:
                    case DISP_MENU_FLIGHT:
                    case DISP_MENU_APN:
                    case DISP_MENU_CERT:
                        p_lcd_menu->title = DISP_MENU_CONFIGURATION;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_WIFI_OPEN:
                    case DISP_MENU_WIFI_CLOSE:
                    case DISP_MENU_WIFI_CONNECT:
                    case DISP_MENU_WIFI_DISCONNECT:
                    case DISP_MENU_WIFI_DETAIL:
                    case DISP_MENU_WIFI_SSID_INPUT:
                        p_lcd_menu->title = DISP_MENU_WIFI;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_CELLULAR_OPEN:
                    case DISP_MENU_CELLULAR_CLOSE:
                    case DISP_MENU_CELLULAR_DETAIL:
                    case DISP_MENU_CELLULAR_UPDATE:
                         p_lcd_menu->title = DISP_MENU_CELLULAR;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_GPS_OPEN:
                    case DISP_MENU_GPS_CLOSE:
                         p_lcd_menu->title = DISP_MENU_GPS;
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_FLIGHT_OPEN:
                    case DISP_MENU_FLIGHT_CLOSE:
                         p_lcd_menu->title = DISP_MENU_FLIGHT;
                        MenuBuild(p_lcd_menu);
                        break;
                    default:
                        loop=false;
                        break;
                 }
                 break;
           case APP_EVENT_TIMEOUT:
                loop = false;
                break;
           case APP_EVENT_USER_CONFIRM:
                switch((p_lcd_menu->options)[p_lcd_menu->selected])
                {
                    case DISP_MENU_VERSION:
                    case DISP_MENU_CONFIGURATION:
                    case DISP_MENU_WIFI:
                    case DISP_MENU_CELLULAR:
                    case DISP_MENU_GPS:
                    case DISP_MENU_FLIGHT:
                    case DISP_MENU_MUILT_APP:
                        p_lcd_menu->title = (p_lcd_menu->options)[p_lcd_menu->selected];
                        MenuBuild(p_lcd_menu);
                        break;
                    case DISP_MENU_PAYMENT:
                    case DISP_MENU_QRCODE:
                    case DISP_MENU_ABOUT:
                    case DISP_MENU_OTA:
                        if(p_lcd_menu->title == DISP_MENU_BASE)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_DEV_VER:
                    case DISP_MENU_PCI_VER:
                        if(p_lcd_menu->title == DISP_MENU_VERSION)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_WIFI_OPEN:
                    case DISP_MENU_WIFI_CLOSE:
                    case DISP_MENU_WIFI_CONNECT:
                    case DISP_MENU_WIFI_DISCONNECT:
                    case DISP_MENU_WIFI_DETAIL:
                    case DISP_MENU_WIFI_SSID_INPUT:
                        if(p_lcd_menu->title == DISP_MENU_WIFI)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_CELLULAR_OPEN:
                    case DISP_MENU_CELLULAR_CLOSE:
                    case DISP_MENU_CELLULAR_DETAIL:
                    case DISP_MENU_CELLULAR_UPDATE:
                        if(p_lcd_menu->title == DISP_MENU_CELLULAR)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_GPS_OPEN:
                    case DISP_MENU_GPS_CLOSE:
                        if(p_lcd_menu->title == DISP_MENU_GPS)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_FLIGHT_OPEN:
                    case DISP_MENU_FLIGHT_CLOSE:
                        if(p_lcd_menu->title == DISP_MENU_FLIGHT)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_APN:
                    case DISP_MENU_CERT:
                        if(p_lcd_menu->title == DISP_MENU_CONFIGURATION)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    case DISP_MENU_MAIN_APP2:
                        if(p_lcd_menu->title == DISP_MENU_MUILT_APP)
                        {
                            MenuMainTxn((p_lcd_menu->options)[p_lcd_menu->selected]);
                        }
                        break;
                    default:
                        loop = false;
                        break;
                }

                break;
          case APP_EVENT_KBD_OTHER:
                switch(((pu32 )pbuffer)[0])
                {
                    case KEY_UP:
                        if(p_lcd_menu->selected > 0)
                        {
                            if(p_lcd_menu->start > 0 && p_lcd_menu->selected == p_lcd_menu->start)
                            {
                                (p_lcd_menu->start)--;
                            }
                            else
                            {

                            }

                            (p_lcd_menu->selected)--;
                        }
                        else
                        {
                            p_lcd_menu->update = false;
                        }
                        break;
                    case KEY_DOWN:
                        if(p_lcd_menu->selected < p_lcd_menu->nums - 1)
                        {
                            if(p_lcd_menu->selected - p_lcd_menu->start == LCD_LINE_NUMS - 2)
                            {
                                (p_lcd_menu->start)++;
                            }
                            else
                            {

                            }

                            (p_lcd_menu->selected)++;
                        }
                        else
                        {
                            p_lcd_menu->update = false;
                        }
                        break;
                    default:
                        p_lcd_menu->update = false;
                        break;
                }
                break;
           default:
                p_lcd_menu->update = false;
                break;
        }
    }
    lark_free_mem(pbuffer);
    lark_free_mem(p_lcd_menu);
}

/*----------------------------------------------------------------------------
|   Function Name:
|       DisplayHandler
|   Description: SDK call back function to handle dsiplay
|   Parameters:
|   Returns:
+---------------------------------------------------------------------------*/
void DisplayHandler(pmessage_body_t pmsg)
{
	const char **ppMask = (const char **)(pmsg->pparam);

    switch(pmsg->wparam)
    {
    case LCD_DISP_DEVICE_INIT:
        dispStrWait(CENTER, PROMPT_DEVICE_INIT, 0);
        break;
    case LCD_DISP_WELCOME:
        MenuDisplayInputAmountMobileRefN();
        break;
    case LCD_DISP_START_TRANSACTION:
        DispStartTransaction((pLcdStartTransaction_t )(pmsg->pparam));
        break;
    case LCD_DISP_UPKEY_TO_START_NFC:
        LcdClearLine(LCD_NORMAL,LCD_LINE_0);
        LcdPrint(LCD_NORMAL, LCD_LINE_2, CENTER, CH_HEIGHT, PROMPT_UPKEY_TO_START_NFC);
        break;
    case LCD_DISP_RESET:
        dispStrWait(CENTER, PROMPT_RESET, 0);
        break;
    case LCD_DISP_INPUT_ONLINE_PIN:
		dispStrWait(LEFT, PROMPT_INPUT_ONLINE_PIN, 0);
        break;
    case LCD_DISP_INPUT_OFFLINE_PIN:
        dispStrWait(LEFT, PROMPT_INPUT_OFFLINE_PIN, 0);
        break;
    case LCD_DISP_PIN_MASK:
		LcdClearLine(LCD_NORMAL, LCD_LINE_1);
		LcdPrint(LCD_NORMAL, LCD_LINE_1, LEFT, CH_HEIGHT, *ppMask);
        break;
    case LCD_DISP_CONFIRM_IF_NO_PIN:
        LcdPrint(LCD_NORMAL, LCD_LINE_2, RIGHT, CH_HEIGHT, PROMPT_CONFIRM_IF_NO_PIN);
        break;
    case LCD_DISP_READING_CARD:
        dispStrWait(CENTER, PROMPT_READING_CARD, 0);
        break;
    case LCD_DISP_SHUTDOWN:
        dispStrWait(CENTER, PROMPT_SHUTDOWN, 0);
        break;
	case LCD_DISP_NET_DATA_RECEPTION:
		//LcdClearLine(LCD_NORMAL, LCD_LINE_1);
		//LcdPrint(LCD_NORMAL, LCD_LINE_1, LEFT, CH_HEIGHT, PROMPT_RECEIVING_DATA);
		break;
    case LCD_DISP_TRANSACTION_TERMINATED:
        LcdClearLine(LCD_NORMAL, LCD_LINE_0);
        LcdClearLine(LCD_NORMAL, LCD_LINE_1);
		LcdPrint(LCD_NORMAL, LCD_LINE_1, CENTER, CH_HEIGHT, PROMPT_TRANSACTION_TERMINAL);
		break;
    case LCD_DISP_FALL_BACK:
        dispStrWait(CENTER, PROMPT_FALLBACK, 5);
        break;
    case LCD_DISP_TRY_ANOTHER_INTERFACE:
        dispStrWait(CENTER, PROMPT_TRY_ANOTHER_INTERFACE, 3);
        break;
	case LCD_DISP_NET_COMMUNICATE:
	case LCD_DISP_NET_DESTINATION_IP:
	case DISP_TRANSACTION_COUNTER:
		break;
	case LCD_DISP_CHIP_CARD:
	    dispStrWait(CENTER, PROMPT_PLS_INSERT, 0);
	    break;
	case LCD_DISP_CARD_NOT_SUPPORT:
	    dispStrWait(CENTER, PROMPT_CARD_NOT_SUPPORT, 3);
	    break;
    case LCD_DISP_TIME:
        #if defined(CFG_DISP_TIME)
        if(!lark_flag_is_true(OS_FLAG_GROUP_APP,APP_EVENT_APP2_IS_BUSY))
        {
            DispTime(char*)(pmsg->pparam));
        }
        #endif
        break;
    default:
        #ifdef CFG_DBG
        TRACE(TRACE_LVL_INFO, "unknown display id %d\r\n", pmsg->wparam);
        #endif
        break;
    }
}

/*--------------------------------------
|   Function Name:
|       FrameHandler
|   Description:usb/BT Interrupt callback
|   Parameters:
|   Returns:
+-------------------------------------*/
void FrameHandler(Host_Type_t type,u8 c)
{
    TRACE(DBG_TRACE_LVL,"       %s-type=%d c=%02x\r\n",__FUNCTION__,type,c);
}
/*--------------------------------------
|   Function Name:
|       KeyboardPressedHandler
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
void KeyboardPressedHandler(Key_Num_t Key_Num)
{
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL, "%s, Key_Num = %d\r\n", __FUNCTION__, Key_Num);
    #endif

	if(Key_Num == KEY_NONE)
	{
		/* Equivalent to the mechanism of autoexec_bat */
		autoexec_bat_fun();
	}
	else if(Key_Num == KEY_MENU)
    {
		MenuMainProcess();
    }
    else if(Key_Num <= KEY_9 || Key_Num == KEY_CONFIRM)
	{
		PaymentStart(Key_Num);
	}
	message_body_t msg = {0, LCD_DISP_WELCOME, 0, NULL};
	DisplayHandler(&msg);
}


