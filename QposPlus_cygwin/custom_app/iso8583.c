/*********************************************************************************
*
*   File Name:
*       iso8583.c
*   Version:
*       V1.0
*   Description:
*       Implement ISO8583 functionality, such as init, pack, unpack
*
********************************************************************************
*/

#include "lark_api.h"
#include "proj_sdk.h"
#include "iso8583.h"

static ISO_DEF s_iso_def[] =
{
     /*000*/ {TYP_BCD, 0, 4, "Message Type Indicator"},
     /*001*/ {TYP_BIT, 0, 8, "Bitmap"},
     /*002*/ {TYP_BCD, 2, 19, "Primary Account number"},
     /*003*/ {TYP_BCD, 0, 6, "Processing Code"},
     /*004*/ {TYP_BCD, 0, 12, "Amount, Transaction"},
     /*005*/ {TYP_BCD, 0, 12, "Amount, Reconciliation"},
     /*006*/ {TYP_BCD, 0, 12, "Amount, Cardholder billing"},
     /*007*/ {TYP_BCD, 0, 10, "Date and time, transmission"},
     /*008*/ {TYP_BCD, 0, 8, "Amount, Cardholder billing fee"},
     /*009*/ {TYP_BCD, 0, 8, "Conversion rate, Reconciliation"},
     /*010*/ {TYP_BCD, 0, 8, "Conversion rate, Cardholder billing"},
     /*011*/ {TYP_BCD, 0, 6, "Systems trace audit number"},
     /*012*/ {TYP_BCD, 0, 12, "Date and time, Local transaction"},
     /*013*/ {TYP_BCD, 0, 4, "Date, Effective"},
     /*014*/ {TYP_BCD, 0, 4, "Date, Expiration"},
     /*015*/ {TYP_BCD, 0, 6, "Date, Settlement"},
     /*016*/ {TYP_BCD, 0, 4, "Date, Conversion"},
     /*017*/ {TYP_BCD, 0, 4, "Date, Capture"},
     /*018*/ {TYP_BCD, 0, 4, "Merchant type"},
     /*019*/ {TYP_BCD, 0, 3, "Country code, Acquiring institution"},
     /*020*/ {TYP_BCD, 0, 3, "Country code, Primary account number"},
     /*021*/ {TYP_BCD, 0, 3, "Country code, Forwarding institution"},
     /*022*/ {TYP_BCD, 0, 3, "Point of service data code"},
     /*023*/ {TYP_BCD, 0, 3, "Card sequence number"},
     /*024*/ {TYP_BCD, 0, 3, "Function code"},
     /*025*/ {TYP_BCD, 0, 2, "Message reason code"},
     /*026*/ {TYP_BCD, 0, 2, "Card acceptor business code"},
     /*027*/ {TYP_BCD, 0, 1, "Approval code length"},
     /*028*/ {TYP_BCD, 0, 6, "Date, Reconciliation"},
     /*029*/ {TYP_BCD, 0, 3, "Reconciliation indicator"},
     /*030*/ {TYP_BCD, 0, 24, "Amounts, original"},
     /*031*/ {TYP_ASC, 2, 99, "Acquirer reference data"},
     /*032*/ {TYP_BCD, 2, 11, "Acquirer institution identification code"},
     /*033*/ {TYP_BCD, 2, 11, "Forwarding institution identification code"},
     /*034*/ {TYP_ASC, 2, 28, "Primary account number, extended"},
     /*035*/ {TYP_BCD, 2, 37, "Track 2 data"},
     /*036*/ {TYP_ASC, 3, 104, "Track 3 data"},
     /*037*/ {TYP_ASC, 0, 12, "Retrieval reference number"},
     /*038*/ {TYP_ASC, 0, 6, "Approval code"},
     /*039*/ {TYP_ASC, 0, 2, "Response code"},
     /*040*/ {TYP_BCD, 0, 3, "Service code"},
     /*041*/ {TYP_ASC, 0, 8, "Card acceptor terminal identification"},
     /*042*/ {TYP_ASC, 0, 15, "Card acceptor identification code"},
     /*043*/ {TYP_ASC, 2, 99, "Card acceptor name/location"},
     /*044*/ {TYP_ASC, 2, 99, "Additional response data"},
     /*045*/ {TYP_ASC, 2, 76, "Track 1 data"},
     /*046*/ {TYP_ASC, 3, 204, "Amounts, Fees"},
     /*047*/ {TYP_ASC, 3, 999, "Additional data - national"},
     /*048*/ {TYP_ASC, 3, 999, "Additional data - private"},
     /*049*/ {TYP_ASC, 0, 3, "Currency code, Transaction"},
     /*050*/ {TYP_ASC, 0, 3, "Currency code, Reconciliation"},
     /*051*/ {TYP_ASC, 0, 3, "Currency code, Cardholder billing"},
     /*052*/ {TYP_BIN, 0, 8, "Personal identification number, PIN) data"},
     /*053*/ {TYP_BCD, 0, 16, "Security related control information"},
     /*054*/ {TYP_ASC, 3, 120, "Amounts, additional"},
     /*055*/ {TYP_BIN, 3, 255, "IC card system related data"},
     /*056*/ {TYP_BIN, 3, 255, "Original data elements"},
     /*057*/ {TYP_BCD, 0, 3, "Authorization life cycle code"},
     /*058*/ {TYP_BCD, 2, 11, "Authorizing agent institution Id Code"},
     /*059*/ {TYP_ASC, 3, 999, "Transport data"},
     /*060*/ {TYP_BCD, 3, 999, "Reserved for national use"},
     /*061*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*062*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*063*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*064*/ {TYP_BIN, 0, 8, "Message authentication code field"},
     /*065*/ {TYP_BIN, 0, 8, "Reserved for ISO use"},
     /*066*/ {TYP_ASC, 3, 204, "Amounts, original fees"},
     /*067*/ {TYP_BCD, 0, 2, "Extended payment data"},
     /*068*/ {TYP_BCD, 0, 3, "Country code, receiving institution"},
     /*069*/ {TYP_BCD, 0, 3, "Country code, settlement institution"},
     /*070*/ {TYP_BCD, 0, 3, "Country code, authorizing agent Inst."},
     /*071*/ {TYP_BCD, 0, 8, "Message number"},
     /*072*/ {TYP_ASC, 3, 999, "Data record"},
     /*073*/ {TYP_BCD, 0, 6, "Date, action"},
     /*074*/ {TYP_BCD, 0, 10, "Credits, number"},
     /*075*/ {TYP_BCD, 0, 10, "Credits, reversal number"},
     /*076*/ {TYP_BCD, 0, 10, "Debits, number"},
     /*077*/ {TYP_BCD, 0, 10, "Debits, reversal number"},
     /*078*/ {TYP_BCD, 0, 10, "Transfer, number"},
     /*079*/ {TYP_BCD, 0, 10, "Transfer, reversal number"},
     /*080*/ {TYP_BCD, 0, 10, "Inquiries, number"},
     /*081*/ {TYP_BCD, 0, 10, "Authorizations, number"},
     /*082*/ {TYP_BCD, 0, 10, "Inquiries, reversal number"},
     /*083*/ {TYP_BCD, 0, 10, "Payments, number"},
     /*084*/ {TYP_BCD, 0, 10, "Payments, reversal number"},
     /*085*/ {TYP_BCD, 0, 10, "Fee collections, number"},
     /*086*/ {TYP_BCD, 0, 16, "Credits, amount"},
     /*087*/ {TYP_BCD, 0, 16, "Credits, reversal amount"},
     /*088*/ {TYP_BCD, 0, 16, "Debits, amount"},
     /*089*/ {TYP_BCD, 0, 16, "Debits, reversal amount"},
     /*090*/ {TYP_BCD, 0, 10, "Authorizations, reversal number"},
     /*091*/ {TYP_BCD, 0, 3, "Country code, transaction Dest. Inst."},
     /*092*/ {TYP_BCD, 0, 3, "Country code, transaction Orig. Inst."},
     /*093*/ {TYP_BCD, 2, 11, "Transaction Dest. Inst. Id code"},
     /*094*/ {TYP_BCD, 2, 11, "Transaction Orig. Inst. Id code"},
     /*095*/ {TYP_ASC, 2, 99, "Card issuer reference data"},
     /*096*/ {TYP_BIN, 3, 999, "Key management data"},
     /*097*/ {TYP_BCD, 0, 1 + 16, "Amount, Net reconciliation"},
     /*098*/ {TYP_ASC, 0, 25, "Payee"},
     /*099*/ {TYP_ASC, 2, 11, "Settlement institution Id code"},
     /*100*/ {TYP_BCD, 2, 11, "Receiving institution Id code"},
     /*101*/ {TYP_ASC, 2, 17, "File name"},
     /*102*/ {TYP_ASC, 2, 28, "Account identification 1"},
     /*103*/ {TYP_ASC, 2, 28, "Account identification 2"},
     /*104*/ {TYP_ASC, 3, 100, "Transaction description"},
     /*105*/ {TYP_BCD, 0, 16, "Credits, Chargeback amount"},
     /*106*/ {TYP_BCD, 0, 16, "Debits, Chargeback amount"},
     /*107*/ {TYP_BCD, 0, 10, "Credits, Chargeback number"},
     /*108*/ {TYP_BCD, 0, 10, "Debits, Chargeback number"},
     /*109*/ {TYP_ASC, 2, 84, "Credits, Fee amounts"},
     /*110*/ {TYP_ASC, 2, 84, "Debits, Fee amounts"},
     /*111*/ {TYP_ASC, 3, 999, "Reserved for ISO use"},
     /*112*/ {TYP_ASC, 3, 999, "Reserved for ISO use"},
     /*113*/ {TYP_ASC, 3, 999, "Reserved for ISO use"},
     /*114*/ {TYP_ASC, 3, 999, "Reserved for ISO use"},
     /*115*/ {TYP_ASC, 3, 999, "Reserved for ISO use"},
     /*116*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*117*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*118*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*119*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*120*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*121*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*122*/ {TYP_ASC, 3, 999, "Reserved for national use"},
     /*123*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*124*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*125*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*126*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*127*/ {TYP_ASC, 3, 999, "Reserved for private use"},
     /*128*/ {TYP_BIN, 0, 8, "Message authentication code field"}
};

T_U8_VIEW tlv_uv_read(u8 *tlv, u32 tag)
{
	T_U8_VIEW uv = {TLV_VAL(tlv, tag), TLV_LEN(tlv, tag)};
	return uv;
}

void trace_tlv_fld(u8 *tlv, u32 i, u8 type)
{
	T_U8_VIEW uv = tlv_uv_read(tlv, TAG8583(i));
	TRACE(TRACE_LVL_INFO, "FLd %02d: ", i);
	if(type == TYP_BIN)
	{
		u8 i;
		for(i = 0; i < uv.len; i++)
			TRACE(TRACE_LVL_INFO, "%02X", uv.head[i]);
		TRACE(TRACE_LVL_INFO, "\r\n");
	}
	TRACE_ASCII(TRACE_LVL_INFO, uv.head, uv.len);
}

u32 binlen_convert(u8* pbyBcd, u8 bytLen, u8 bytType)
{
    u8 i;
    u32 dwRet = 0, dwRate = 1, dwFactor = 10;	//((bytType == TYP_BCD) ? 10 : 0x10);

    for(i = 0; i < bytLen; i++)
    {
        dwRet += ((pbyBcd[bytLen - i - 1]&0x0F)*dwRate + (pbyBcd[bytLen - i - 1]>>4)*dwRate*dwFactor);
        dwRate *= (dwFactor*dwFactor);
    }
    return dwRet;
}

/* return len of bcd, NOT str */
u32 bcd2uv(u8 *src, T_U8_VIEW *dest)
{
    u32 i = 0, j = 0;
    do
    {
		if(i || !(dest->len%2))
			dest->head[j++] = BIN2CHAR((src[i]>>4));
        dest->head[j++] = BIN2CHAR((src[i]&0x0F));
        i++;
    }while(j < dest->len);
    return i;
}

static unsigned char aschex2bin(char thechar)
{
	thechar &= 0x7f; // Ignore top bit
	if (thechar >= 'a')
		return (thechar - 'a' + 10);
    else if(thechar >= 'A')
        return (thechar - 'A' + 10);
    else
        return (thechar - '0');
}

u32 uv2bcd(T_U8_VIEW *src, u8 *dest, T_PAD_DIRECTION pad)
{
	u32 i = 0, j = 0;

    if((src->len%2) && pad == PAD_NONE) return 0;
    if((src->len%2) == 0) pad = PAD_NONE;
    do
    {
        if(i == 0 && pad == PAD_LEFT)
            dest[j++] = aschex2bin(src->head[i++]);
        else if(i == (u32)(src->len - 1) && pad == PAD_RIGHT)
            dest[j++] = (aschex2bin(src->head[i++])<<4);
        else
        {
            dest[j++] = ((aschex2bin(src->head[i])<<4)|aschex2bin(src->head[i + 1]));
            i += 2;
        }
    }while(i<src->len);
	return j;
}

int iso8583_update(int idx, char *str, u8 *pTlvBuf)
{
	tlv_replace_(pTlvBuf, TAG8583(idx), Strlen_(str), (u8 *)str);
	return Strlen_(str);
}

int iso8583_get(int idx, unsigned char *data, u8 *pTlvBuf)
{
    u8 *pNode = tlv_find_(pTlvBuf, TAG8583(idx));
    u32 dwLen = 0;

    if(pNode)
    {
        dwLen = tlv_get_l_(pNode);
        if(dwLen && data)
            Memcpy_(data, tlv_get_v_(pNode), dwLen);
    }
    return (int)dwLen;
}

int iso8583_pack(u8 *buf, u8 *pTlvBuf)
{
	u8 *pCursor = buf;

	T_U8_VIEW uvTmp = tlv_uv_read(pTlvBuf, TAG8583(0));
	if (!uvTmp.head || uvTmp.len != (u32)s_iso_def[0].len)
	{
		TRACE(TRACE_LVL_DEFAULT,"message type error, len = %d vs %d\r\n", uvTmp.len, s_iso_def[0].len);
		return -1;
	}

	pCursor += uv2bcd(&uvTmp, pCursor, PAD_NONE);
	u32 dwLenMsgType = pCursor - buf;
    pCursor += ISO_FLDS/8;

	u32 i;
	for (i = 2; i <= ISO_FLDS; i++)
    {
		T_U8_VIEW uvFld;

        MY_BITCLR(buf + dwLenMsgType, i);
		uvFld = tlv_uv_read(pTlvBuf, TAG8583(i));
		if(!uvFld.head || !uvFld.len || uvFld.len > (u32)s_iso_def[i].len)
			continue;
        MY_BITSET(buf + dwLenMsgType, i);

        //length field = BCD with 0 paaded left
        if(s_iso_def[i].fmt)
        {
            u8 bytLenAsc = (s_iso_def[i].fmt + 1)/2*2;		//LLL set to 3 BCD digits(2 BCD bytes)
			char strFmt[8];
			Sprintf_(strFmt, "%%0%dd", bytLenAsc);
            Sprintf_((char *)pCursor, strFmt, uvFld.len);
			U8_VIEW(pCursor, bytLenAsc, &uvTmp);
            pCursor += uv2bcd(&uvTmp, pCursor, PAD_NONE);
        }
        trace_tlv_fld(pTlvBuf, i, s_iso_def[i].typ);
        switch (s_iso_def[i].typ)
        {
        case TYP_BCD:
            pCursor += uv2bcd(&uvFld, (u8 *)pCursor, PAD_LEFT);
            break;
        case TYP_ASC:
        case TYP_BIN:
            Memcpy_(pCursor, uvFld.head, uvFld.len);
            pCursor += uvFld.len;
            break;
        default:
            TRACE(TRACE_LVL_DEFAULT, "exception flds = %d\r\n", i);
            return -1;
        }
    }
    TRACE(TRACE_LVL_DEFAULT, "\r\n");
	return (pCursor - buf);
}

int iso8583_unpack(u8 *buf, u8 *pTlvBuf)
{
    u8 *pbytBitmap, *pCursor = buf;
	u32 i = 0, dwCount = 0;
	T_U8_VIEW uvTmp = {NULL, 0};

	U8_VIEW(lark_alloc_mem(s_iso_def[0].len), s_iso_def[0].len, &uvTmp);
	pCursor += bcd2uv(buf, &uvTmp);
	TLV_UV_WRITE(pTlvBuf, TAG8583(0), &uvTmp);
	lark_free_mem(uvTmp.head);

    pbytBitmap = pCursor;
	tlv_replace_(pTlvBuf, TAG8583(1), ISO_FLDS/8, pbytBitmap);
    pCursor += ISO_FLDS/8;
    for (i = 2; i <= ISO_FLDS; i++)
    {
		T_U8_VIEW uvFld;

        if(!MY_BITGET(pbytBitmap, i)) continue;
        dwCount++;

        if(s_iso_def[i].fmt)
        {
            u8 bytLenBin = (s_iso_def[i].fmt + 1)/2;
            uvFld.len = binlen_convert(pCursor, bytLenBin, s_iso_def[i].typ);
            pCursor += bytLenBin;
        }
        else
            uvFld.len = s_iso_def[i].len;
		uvFld.head = pCursor;

        switch(s_iso_def[i].typ)
        {
        case TYP_BCD:
			U8_VIEW(lark_alloc_mem(uvFld.len), uvFld.len, &uvTmp);
			pCursor += bcd2uv((u8 *)uvFld.head, &uvTmp);
			TLV_UV_WRITE(pTlvBuf, TAG8583(i), &uvTmp);
			lark_free_mem(uvTmp.head);
            break;
        case TYP_ASC:
        case TYP_BIN:
			TLV_UV_WRITE(pTlvBuf, TAG8583(i), &uvFld);
			pCursor += uvFld.len;
            break;
        default:
            TRACE(TRACE_LVL_DEFAULT, "exception flds = %d\r\n", i);
            return -1;
        }
		trace_tlv_fld(pTlvBuf, i, s_iso_def[i].typ);
    }
    return dwCount;
}
