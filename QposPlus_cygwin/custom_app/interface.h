/*
********************************************************************************
*
*   File Name:
*       interface.h
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

#ifndef _INTERFACE_H
#define _INTERFACE_H

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Macros
+----------------------------------------------------------------------------*/
#define URL_POSP_DEMO	                    "www.dspread.net:20020"
#define SIZE_ARRAY(x)  				        (sizeof(x)/sizeof(*x))
#define DW_MAX								(0xFFFFFFFF)
#define SIZE_ARRAY(x)  				        (sizeof(x)/sizeof(*x))
#define CH_HEIGHT					        (GetLcdDispH())
#define CENTER						        LCD_CENTERED_ALIGNMENT
#define RIGHT						        LCD_RIGHT_ALIGNMENT
#define LEFT						        LCD_LEFT_ALIGNMENT
#define KEY_MENU					        KEY_POWER
#define TRANSACTION_POOL_LENGTH             (1024)
#define HTTP_FRAME_LENGTH_MAX               (1024)
#define DISP_TRANSACTION_COUNTER			(59)

#define AMOUNT_CURRENCY_SYMBOL              "$"
#define PROMPT_DEVICE_INIT                  "DEVICE INIT ..."
#define PROMPT_TMS_SYNC                     "Sync Tms\npls wait..."
#define PROMPT_WELCOME                      "WELCOME"
#define PROMPT_PLS_USE_MAG_ICC_NFC          "USE MAG/ICC/NFC"
#define PROMPT_PLS_USE_MAG_ICC              "PLS USE MAG/ICC"
#define PROMPT_PLS_USE_MAG_NFC              "PLS USE MAG/NFC"
#define PROMPT_PLS_USE_ICC_NFC              "PLS USE ICC/NFC"
#define PROMPT_PLS_USE_MAG                  "PLS USE MAG"
#define PROMPT_PLS_USE_ICC                  "PLS USE ICC"
#define PROMPT_PLS_USE_NFC                  "PLS USE NFC"
#define PROMPT_UPKEY_TO_START_NFC           "PRESS \"��\" START NFC"
#define PROMPT_RESET                        "TRANS TERMINATE"
#define PROMPT_INPUT_ONLINE_PIN             "INPUT ONLINE PIN:"
#define PROMPT_INPUT_OFFLINE_PIN            "INPUT OFFLINE PIN:"
#define PROMPT_CONFIRM_IF_NO_PIN            "PRESS \"��\" IF NO PIN"
#define PROMPT_READING_CARD                 "READING CARD..."
#define PROMPT_RECEIVING_DATA               "Receiving..."
#define PROMPT_SHUTDOWN                     "SHUTDOWN..."
#define PROMPT_BATTERY_ERROR                "BATTERY ERROR"
#define PROMPT_TRANSACTION_SUCCESS          "TRANS SUCCESS"
#define PROMPT_TRANSACTION_TERMINAL         "TRANS TERMINATED"
#define PROMPT_ONLINE_TXN_FAILED          	"online trans fail"
#define PROMPT_MENU_TITLE                   "SELECT"
#define PROMPT_QRCODE_GENERATE              "QRCODE ..."
#define PROMPT_QRCODE_GENERATE_FAIL         "Qrcode Generate Fail\npls check Param"
#define PROMPT_SIGNATURE_ON_BACK_PANEL      "PLEASE SIGNATURE \nON THE BACK PANEL"
#define PROMPT_SIGNATURE_FAILED             "SIGNATURE FAILED"
#define PROMPT_GO_ONLINE                    "GO ONLINE..."
#define PROMPT_REQ_POSP                   	"Req to posp..."
#define PROMPT_PROCESSING                   "Processing..."
#define PROMPT_WIFI_AP_LIST                 "WIFI AP List"
#define PROMPT_INPUT_ERROR                  "Input Error"
#define PROMPT_WIFI_NO                      "Wifi Not Find"
#define PROMPT_CONNECT_SUCCESS              "Connect Success"
#define PROMPT_CONNECT_ERROR                "Connect Error"
#define PROMPT_DISCONNECTED                 "Disconnected"
/*menu item*/
#define PROMPT_MENU_BASE                    "Menu"
#define PROMPT_MENU_TXN                         "Card Payment Demo"
#define PROMPT_MENU_QR                          "QR Payment Demo"
#define PROMPT_MENU_VERSION                     "Version"
#define PROMPT_MENU_DEV_VERSION                     "Show Version"
#define PROMPT_MENU_PCI_VERSION                     "PCI Info"
#define PROMPT_MENU_CONFIGURATION               "Configuration"
#define PROMPT_MENU_WIFI_CONFIGURATION	            "Wifi Config"
#define PROMPT_MENU_WIFI_OPEN                           "WiFi Open"
#define PROMPT_MENU_WIFI_CLOSE                          "WiFi Close"
#define PROMPT_MENU_WIFI_SEARCH                         "Wifi search"
#define PROMPT_MENU_WIFI_DISCONNECT                     "Wifi Disconnected"
#define PROMPT_MENU_WIFI_DETAILS                        "WIFI Details"
#define PROMPT_MENU_WIFI_SSID_INPUT                     "WIFI SSID Input"
#define PROMPT_MENU_CELLULAR_CONFIGURATION		    "Cellular Config"
#define PROMPT_MENU_CELLULAR_OPEN                       "Cellular Open"
#define PROMPT_MENU_GPRS_CLOSE                          "Cellular Close"
#define PROMPT_MENU_CELLULAR_DETAILS                    "Cellular Details"
#define PROMPT_MENU_CELLULAR_UPDATE                     "Cellular OTA"
#define PROMPT_MENU_GPS_CONFIGURATION		        "Gps Config"
#define PROMPT_MENU_GPS_OPEN                            "Gps Open"
#define PROMPT_MENU_GPS_CLOSE                           "Gps Close"
#define PROMPT_MENU_GPS_DETAILS                         "Gps Details"
#define PROMPT_MENU_FLIGHT_CONFIGURATION	        "Flight Mode"
#define PROMPT_MENU_FLIGHT_ON                           "Flight On"
#define PROMPT_MENU_FLIGHT_OFF                          "Flight Off"
#define PROMPT_MENU_UPDATE_APN                      "Apn Setup"
#define PROMPT_MENU_UPDATE_CERT                     " Certification"
#define PROMPT_MENU_ABOUT                       "About"
#define PROMPT_MENU_DL_OBJ					    "Remote upgrade fw"

#define PROMPT_WAIT_PRN      				"Please Wait...\nPrinting..."
#define PROMPT_GPS_INVALID_POSITION         "No valid Position Information"
#define PROMPT_APN_UPDATE_SUCCESS           "APN Update Success"
#define PROMPT_CERT_UPDATE_SUCCESS          "Cert Update Success"
#define PROMPT_PLS_INPUT_AMOUNT             "Amount:"
#define OPERTOR_NAME                        "China Mobile Internet"
#define APN_NAME                            "cmnet"
#define PROMPT_MENU_MUILT_APP               " Multi App Select"
#define PROMPT_MENU_MAIN_APP2               "Switch BT App"
#define PROMPT_FALLBACK                     "TRANS FALLBACK"
#define PROMPT_TRY_ANOTHER_INTERFACE        "Try Another Interface"
#define PROMPT_PLS_INSERT                   "Please Insert card"
#define PROMPT_ONLINE_SUCCESS               "Online Request Success"
#define PROMPT_ONLINE_CANCEL                "Online Request Cancel"
#define PROMPT_ONLINE_FAIL                  "Online Request Fail"
#define PROMPT_NFC_DECLINE                  "Trans Decline"
#define PROMPT_NFC_APPROVED                 "Trans Approved"
#define PROMPT_CARD_NOT_SUPPORT             "Card Not Support"
#define PROMPT_MENU_OTA                     "Force firmware OTA"
#define PROMPT_MODULE_OTA                   "Cellular OTA"
#define PROMPT_MODULE_UPDATE_SUCCESS         "Update Success"

#if defined(CFG_OTA_DEBUG)
	#define TMS_TRACE_LVL DBG_TRACE_LVL
#else
	#define TMS_TRACE_LVL (DBG_TRACE_LVL + 1)
#endif
/************************certification define*************************************/
/*--------------------------------------
|  Certification update:(format in pem)
|  1.server ca certification
|  2.client certification
|  3.client key certification
|  if need to verify cert when request,need to update certification according to your need.
|   modify your certification follow bellow cert format
+-------------------------------------*/
#if defined(CFG_SERVER_CERT_UPDATE)
#define SERVER_CERT                                                     \
"-----BEGIN CERTIFICATE-----\r\n"                                       \
"MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBh\r\n"  \
"MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\r\n"  \
"d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD\r\n"  \
"QTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVT\r\n"  \
"MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j\r\n"  \
"b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG\r\n"  \
"9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsB\r\n"  \
"CSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97\r\n"  \
"nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt\r\n"  \
"43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7P\r\n"  \
"T19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4\r\n"  \
"gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAO\r\n"  \
"BgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbR\r\n"  \
"TLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUw\r\n"  \
"DQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/Esr\r\n"  \
"hMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg\r\n"  \
"06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJF\r\n"  \
"PnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0ls\r\n"  \
"YSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQk\r\n"  \
"CAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=\r\n"                  \
"-----END CERTIFICATE-----\r\n"
#endif

#if defined(CFG_CLIENT_CERT_UPDATE)
#define CLIENT_CERT                                                     \
"-----BEGIN CERTIFICATE-----\r\n"                                       \
"MIIDlzCCAn8CAwD6zzANBgkqhkiG9w0BAQUFADCBjzELMAkGA1UEBhMCQ04xEDAO\r\n"  \
"BgNVBAgMB0JlaWppbmcxEDAOBgNVBAcMB2JlaWppbmcxEDAOBgNVBAoMB2RzcHJl\r\n"  \
"YWQxEDAOBgNVBAsMB2RzcHJlYWQxDjAMBgNVBAMMBWtzaGVyMSgwJgYJKoZIhvcN\r\n"  \
"AQkBFhlqYWNrLmJhaWppYW5ndW9Aa3NoZXIuY29tMB4XDTIwMDgxODA4NDY1NVoX\r\n"  \
"DTMwMDgxNjA4NDY1NVowgZAxCzAJBgNVBAYTAkNOMRAwDgYDVQQIDAdCZWlqaW5n\r\n"  \
"MREwDwYDVQQHDAhDaGFveWFuZzEQMA4GA1UECgwHZHNwcmVhZDERMA8GA1UECwwI\r\n"  \
"a3NoZXJkZXYxDTALBgNVBAMMBGphY2sxKDAmBgkqhkiG9w0BCQEWGWphY2suYmFp\r\n"  \
"amlhbmd1b0Brc2hlci5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\r\n"  \
"AQCyt6nxo9wB1Ul5F6ixLrUiQDzTPo0I9uuS+foKZ3cnkp4p+S2g6ZdZaCSas8AR\r\n"  \
"a7aG7S6q6KjIRqaDyd4R7/07GO24oWWo354YZ5DLcgr+SNH9ErRkoSHJKf1YxhXi\r\n"  \
"HuBJIFfGqjph/a/3xe3wWgnvT4DaAXNcTnL8TWJYAmOxYkkU0iDG4aYqVgkdZWm4\r\n"  \
"o8Mv/DEzlPUkSdjJcSX/RaNJQSabH0C3+WbGrnM1bleHbMdPz6yjbCx+UnZ6cboj\r\n"  \
"WuBhMXSG6eW1jl5jutI87YIIjq8uIxN9vqB9qY+LIThtJzg419yiYW/TrajQSLwM\r\n"  \
"IUM8gDP0dQ2sWA1FSsB2sZ0HAgMBAAEwDQYJKoZIhvcNAQEFBQADggEBAJ8ZqNhE\r\n"  \
"wcOrEuPtH4TnU3mBcuIvFlQqrNrpe6Qt7fqebvf2p8USoaf6oUvqG8dBEtlXXVeP\r\n"  \
"qJUypQyf96uJxYR5Fdwjiep1Y6gUkTsjVcogWQvxCxFrnC+XsGw4S0whGYo3SVaW\r\n"  \
"8m8tPDZokrOcTq2GMzAWlAN+ObUUAvpUnYR05CBVFy2Dr7lkMaeuv6IaUk4UjPtC\r\n"  \
"2X0NMuY1zKpPWC+3ezMukYKJ3eHCv4UMfb8B3KZg59NNs/4/0b2VGPo6bErFimk1\r\n"  \
"f83CISZebdIMQ5cigQ7TS6Ed15ByTH+zYnbnaKheTtGHs6DinW3yK08c6+xdoXa8\r\n"  \
"6Znpc3wWfl63j9s=\r\n"                                                  \
"-----END CERTIFICATE-----\r\n"
#endif

#if defined(CFG_CLIENT_KEY_CERT_UPDATE)
#define CLIENT_KEY_CERT                                                 \
"-----BEGIN RSA PRIVATE KEY-----\r\n"                                   \
"MIIEowIBAAKCAQEAsrep8aPcAdVJeReosS61IkA80z6NCPbrkvn6Cmd3J5KeKfkt\r\n"  \
"oOmXWWgkmrPAEWu2hu0uquioyEamg8neEe/9OxjtuKFlqN+eGGeQy3IK/kjR/RK0\r\n"  \
"ZKEhySn9WMYV4h7gSSBXxqo6Yf2v98Xt8FoJ70+A2gFzXE5y/E1iWAJjsWJJFNIg\r\n"  \
"xuGmKlYJHWVpuKPDL/wxM5T1JEnYyXEl/0WjSUEmmx9At/lmxq5zNW5Xh2zHT8+s\r\n"  \
"o2wsflJ2enG6I1rgYTF0hunltY5eY7rSPO2CCI6vLiMTfb6gfamPiyE4bSc4ONfc\r\n"  \
"omFv062o0Ei8DCFDPIAz9HUNrFgNRUrAdrGdBwIDAQABAoIBABUlGdxXMM6et6S2\r\n"  \
"IeVky1WUGB4+Rl2LKXAoV2cnurdZ55C7xKRHPvr7EWmkXtMw1VW9dkoOYPnhMg5h\r\n"  \
"sf6ZUHLPjkDMB8UlQAVPK5iGk7pmT0IMFn/ZeK/eYfVA6pHoaUz9oKUGuwWhCFLe\r\n"  \
"fjpzzL0dwPcKOOXw+jXTyhDzp2vzLD+S3CagjOR4JOWgbEQHyzS9AkVBKF5l5ss/\r\n"  \
"OYdkDing+ekIaHCH+pcbXZ77cwFCJ7+q5kypzjDmNosSD0ZTyOVxKNWL56zMo87d\r\n"  \
"2X921sggUS2AMIlVwYa5IC/+H6hyuHRaipOjXSh26PNrJs0FMAyRSMjh1FMdN64S\r\n"  \
"QBDGnkECgYEA56/OEmqaWLYCVIswLObiEZkNbOtLvF+MwMrU6IGk4ZwhseLH0/Hm\r\n"  \
"4QW4F6H3ycR96TovYQMTcF5om+E4Gxmsna+kY3z0ef/JK0f23yux2jHRBgQIFdFL\r\n"  \
"DTI5kJJAikZF0M7WDF5LOiGA7ucItVSJFwuZnljXaxQeSeYiEMp5hHcCgYEAxXja\r\n"  \
"uuo4hxC4BsldptlqB87JjuvL16cEGmZX8GmzU2hY+bSAUK4GAXV9sjJMB9QQDC34\r\n"  \
"UKi6oTodPnXQF9xZThOJ0msydWSsrqruQ1Ejra8FMdp+XZDGakYiLpv2anOpUHdP\r\n"  \
"9n3dh4NsREzSFAy1QbmbESUz8lQIBBriwWaqn/ECgYB7t9Fe3DYXcPnFk53iHFhT\r\n"  \
"9VrJ8su6h3tt5+HRVolpQCpuBx+V+fLD7n3jgMYEpDxoMn3iW/YhZwiNVLLVJdyb\r\n"  \
"R5OM3Mjf7MrLpd4aRgbu8PMhNz3qCJz/Kva6UJ7ON8BIRGrgp+mb3RAwgoaP7dhW\r\n"  \
"ygb6G47Myy1xYMaIGNbS+QKBgQCfhMJEzeI+EV5ndREnMGg7yGr//jDbkzp4TQZq\r\n"  \
"2igXj6qYhJxHEF0fnpacdY7/n+oavKPkHHkutrBa/XbNX53wuU3TYe8P7/Si4Me6\r\n"  \
"L3h/3Gt420TLNqFhxifkuO51DdvPeaJpv0FOL/cssVXfBmkBn/rI/eGZtxIGpuVY\r\n"  \
"UCQzMQKBgA427jCOcFXaWoCqjc+sgULKDEObXXGmqrnwp+HpuHvE1Y8/EHPdc5nA\r\n"  \
"dq4IyeybCXX8hCWFD627SEbVhdlWQwVRglx2ReNTTjg7EW/MJ/WUreemv7UdLzMf\r\n"  \
"w/EngRAYukIa4atlXnVpsIPWdMNm6pMbI14u3x9TiL1Sbf/UwjdH\r\n"              \
"-----END RSA PRIVATE KEY-----\r\n"
#endif
/*-----------------------------------------------------------------------------
|   Typedefs
+----------------------------------------------------------------------------*/
typedef enum
{
    DISP_MENU_BASE=0,
    DISP_MENU_PAYMENT,
    DISP_MENU_QRCODE,
    DISP_MENU_VERSION,
    DISP_MENU_CONFIGURATION,
    DISP_MENU_DEV_VER,
    DISP_MENU_PCI_VER,
    DISP_MENU_WIFI,
    DISP_MENU_CELLULAR,
    DISP_MENU_GPS,
    DISP_MENU_FLIGHT,

    DISP_MENU_WIFI_OPEN,
    DISP_MENU_WIFI_CLOSE,
    DISP_MENU_WIFI_CONNECT,
    DISP_MENU_WIFI_DISCONNECT,
    DISP_MENU_WIFI_DETAIL,
    DISP_MENU_WIFI_SSID_INPUT,

    DISP_MENU_CELLULAR_OPEN,
    DISP_MENU_CELLULAR_CLOSE,
    DISP_MENU_CELLULAR_DETAIL,
    DISP_MENU_CELLULAR_UPDATE,

    DISP_MENU_GPS_OPEN,
    DISP_MENU_GPS_CLOSE,

    DISP_MENU_FLIGHT_OPEN,
    DISP_MENU_FLIGHT_CLOSE,

    DISP_MENU_APN,
    DISP_MENU_CERT,
    DISP_MENU_ABOUT,
    DISP_MENU_OTA,
    DISP_MENU_MUILT_APP,
    DISP_MENU_MAIN_APP2,

    DISP_MENU_NUMS
}Lcd_Disp_Menu_t;
typedef struct
{
	Lcd_Disp_Menu_t title;
	Lcd_Disp_Menu_t options[16];
	u32 			nums;
	u32 			start;
	u32 			selected;
    bool            update;
}Lcd_Menu_t,*pLcd_Menu_t;
typedef struct
{
	char *          title;
	char *          options[16];
	u32 			nums;
	u32 			start;
	u32 			selected;
    bool            update;
}Lcd_Custom_Menu_t,*pLcd_Custom_Menu_t;

typedef struct
{
    Card_Type_t  type;
    const char * amt;
    u32          timeout;
}LcdStartTransaction_t,*pLcdStartTransaction_t;

typedef enum
{
    CALLBACK_TRANS_MAG=0x00,
    CALLBACK_TRANS_ICC_ONLINE,
    CALLBACK_TRANS_NFC_ONLINE,
    CALLBACK_TRANS_NFC_APPROVED,
    CALLBACK_TRANS_NFC_DECLINE,
    CALLBACK_TRANS_GAC2,
    CALLBACK_TRANS_APP_SELECT,
    CALLBACK_TRANS_REVERSAL

}Callback_Type_t;

typedef struct
{
	const char * time;
}Lcd_Time_t,*pLcd_Time_t;

typedef struct
{
    u32 title;
    u32 item_cnt;
    pu32 items;
}MENU_ITEM_T;
/*-----------------------------------------------------------------------------
|   prototypes
+----------------------------------------------------------------------------*/
extern Rc_t online_pay(u8 *pTlvBuf);
extern void lcd_sign_menu(void);
#endif
