/*
********************************************************************************
*
*   File Name:
*       Req_Server.c
*   Author:
*       SW R&D Department
*   Version:
*       V1.0
*   Description:
*
*
********************************************************************************
*/

/*-----------------------------------------------------------------------------
|   Includes
+----------------------------------------------------------------------------*/
#include "Req_Http.h"
#include "Req_Server.h"
#include "appcode.h"
#include "proj_sdk.h"
#include "lark_api.h"
#include "interface.h"
#include "iso8583.h"
#include "pkt_adapt.h"

#define URL_TEST    "http://spoc.dspread.com:8084/port/test"
#define URL_TRANS_TEST ""
#define SERVER_PORT  (8084)
#define TMS_PORT            9090
extern Rc_t RequestSelect(void);
extern void DispHttpRequst(void);
/*-----------------------------------------------------------------------------
|   Variables
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Constants
+----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
|   Functions
+----------------------------------------------------------------------------*/
u32 crc32bzip2(u32 crc, void const *mem, size_t len)
{
    unsigned char const *data = mem;
	unsigned k;
    if (data == NULL) return 0;
    crc = ~crc;
    while (len--)
    {
        crc ^= (unsigned)(*data++) << 24;
        for (k = 0; k < 8; k++)
            crc = crc & 0x80000000 ? (crc << 1) ^ 0x4c11db7 : crc << 1;
    }
    crc = ~crc;
    return crc;
}
/*--------------------------------------
|   Function Name:
|       RequestPackHttpFrame
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t RequestPackHttpFrame(char * strMsg, char * pURL, char * pMethod,char * pFrame,char * pRange, u32 FrameLengthMax )
{
    HttpPack_t HttpPack;

    Memset_((pu8)(&HttpPack), 0x00, sizeof(HttpPack));
    TRACE(DBG_TRACE_LVL, "\r\nRequest_URL: %s\r\n\r\n",pURL);
    HttpPack.pRequestMessage = strMsg;
    HttpPack.pRequestBody = pFrame;
    HttpPack.Line.pMethod = lark_alloc_mem(32);
    HttpPack.Line.pUrl = lark_alloc_mem(256);
    HttpPack.Line.pVersion = lark_alloc_mem(32);
    HttpPack.Head.pConnection = lark_alloc_mem(32);
    HttpPack.Head.pHost = lark_alloc_mem(128);
    HttpPack.Head.pContentType = lark_alloc_mem(64);
    if(pRange!=NULL)
    {
        HttpPack.Head.pRange = lark_alloc_mem(64);
        Memset_(HttpPack.Head.pRange, 0x00, 64);
    }

    Memset_(HttpPack.pRequestBody, 0x00, FrameLengthMax);
    Memset_(HttpPack.Line.pMethod, 0x00, 32);
    Memset_(HttpPack.Line.pUrl, 0x00, 256);
    Memset_(HttpPack.Line.pVersion, 0x00, 32);
    Memset_(HttpPack.Head.pConnection, 0x00, 32);
    Memset_(HttpPack.Head.pHost, 0x00, 128);
    Memset_(HttpPack.Head.pContentType, 0x00, 64);


    Strcat_(HttpPack.Line.pMethod, pMethod);

    HttpUrlGetPath(pURL, HttpPack.Line.pUrl, 256);

    Strcat_(HttpPack.Line.pVersion, "HTTP/1.1");//HTTP/1.1

    Strcat_(HttpPack.Head.pConnection, "Keep-Alive");

    HttpUrlGetHost(pURL, HttpPack.Head.pHost, 128);

    if(pRange!=NULL)
    {
        Strcat_(HttpPack.Head.pRange, pRange);
    }

    Strcat_(HttpPack.Head.pContentType, "application/x-www-form-urlencoded");//application/json

    HttpFramePack(&HttpPack);

    lark_free_mem(HttpPack.Line.pMethod);
    lark_free_mem(HttpPack.Line.pUrl);
    lark_free_mem(HttpPack.Line.pVersion);
    lark_free_mem(HttpPack.Head.pConnection);
    lark_free_mem(HttpPack.Head.pHost);
    lark_free_mem(HttpPack.Head.pContentType);
    if(HttpPack.Head.pRange!=NULL)
    {
        lark_free_mem(HttpPack.Head.pRange);
    }

    return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       GetHttpHeadFramLen
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 GetHttpHeadFramLen(const char *strMsg)
{
    int Cnt = 0;
    char* pStr=NULL;
    if(strMsg==NULL)
    {
        return Cnt;
    }
    pStr = Strstr_(strMsg, "\r\n\r\n");
    if(pStr)
    {
        pStr += 4;
/*        while(*pStr=='\r' || *pStr=='\n')*/
/*        {*/
/*            pStr++;*/
/*        }*/
        Cnt=pStr-strMsg;
    }
    else
    {
        Cnt = 0;
    }
    pStr=NULL;
    return Cnt;
}
/*--------------------------------------
|   Function Name:
|       GetHttpContentFram
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 GetHttpContentFram(const char *strMsg,s32 FramLen)
{
    s32 Cnt = 0;
    char* pStr=NULL;

    if(strMsg==NULL || FramLen==0)
    {
        return Cnt;
    }
    pStr = Strstr_(strMsg, "\r\n\r\n");
    if(pStr)
    {
        pStr += 4;

        Cnt=pStr-strMsg;
    }
    else
    {
        Cnt = 0;
    }
    return FramLen-Cnt;
}
/*--------------------------------------
|   Function Name:
|       RequestHttpMessageCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static bool RequestHttpMessageCheck(char * pHttpBody,u32 ReceiveLen)
{
    HttpUnpack_t HttpUnpack;
    u32 ContentLen = 0;
    u32 HeadLen=0;
    bool res=false;
    if(pHttpBody==NULL || ReceiveLen==0)
    {
        return true;
    }
    Memset_((pu8)(&HttpUnpack), 0x00, sizeof(HttpUnpack));
    HttpUnpack.pResponseBody = pHttpBody;

    HttpUnpack.Head.pContentLength = lark_alloc_mem(32);
    HttpUnpack.Head.pConnection = lark_alloc_mem(32);
    Memset_(HttpUnpack.Head.pContentLength, 0x00,32);
    Memset_(HttpUnpack.Head.pConnection, 0x00,32);
    HttpFrameUnpack(&HttpUnpack);

    ContentLen = Atoi_(HttpUnpack.Head.pContentLength);
    if(ContentLen!=0)
    {
        HeadLen=GetHttpHeadFramLen(HttpUnpack.pResponseBody);
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"\r\n%s--ReceiveLen=%d contentlen=%d  HeadLen=%d\r\n",__FUNCTION__,ReceiveLen,ContentLen,HeadLen);
        #endif
        if(ContentLen+HeadLen==ReceiveLen)
        {
            res= true;
        }
    }
    else if(Strcmp_(HttpUnpack.Head.pConnection,"close"))
    {
        res=true;
    }
    lark_free_mem(HttpUnpack.Head.pContentLength);
    lark_free_mem(HttpUnpack.Head.pConnection);
    return res;
}
/*--------------------------------------
|   Function Name:
|       ParamFormatCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static void ParamFormatCheck(pu8 pTLV )
{
    if(tlv_get_t_(pTLV) != PARAM_TLV_HEAD)
    {
        Memset_(pTLV, 0x00, PARAM_LENGTH);
        tlv_new_(pTLV, PARAM_TLV_HEAD, 0, NULL);
        CustomParametersSet(pTLV, PARAM_LENGTH);
    }
}

/*--------------------------------------
|   Function Name:
|       MosParamTLV_GetData
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 ParamTLV_GetData(u32 Tag, pu8 pData, u32 LengthMax )
{
    pu8 pTLV_Buffer = NULL;
    pu8 pNode = NULL;
    pu8 pValue = NULL;
    u32 Length = 0;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    pNode = tlv_find_(pTLV_Buffer, Tag);
    if(pNode)
    {
        pValue = tlv_get_v_(pNode);
        Length = tlv_get_l_(pNode);
    }
    if(Length != 0 && Length <= LengthMax)
    {
        Memcpy_(pData, pValue, Length);
    }
    else
    {
        Length = 0;
    }
    lark_free_mem(pTLV_Buffer);
    return Length;
}
/*--------------------------------------
|   Function Name:
|       ParamTLV_SetData
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static u32 ParamTLV_SetData(u32 Tag, pu8 pData, u32 Length )
{
    pu8 pTLV_Buffer = NULL;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    tlv_replace_(pTLV_Buffer, Tag, Length, pData);
    CustomParametersSet(pTLV_Buffer, 512);
    lark_free_mem(pTLV_Buffer);

    return RC_SUCCESS;
}

/*--------------------------------------
|   Function Name:
|       TradeLocalParametersLoad
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t LocalParametersLoad(pu8 pTransBuffer, u32 Tag)
{
    pu8 pBuffer = NULL;
    u32 BufferLength = 0;
    Rc_t Rc=RC_FAIL;

    pBuffer = lark_alloc_mem(256);
    Memset_(pBuffer, 0x00, 256);
    BufferLength = ParamTLV_GetData(Tag, pBuffer, 256);
    if(BufferLength)
    {
        tlv_replace_(pTransBuffer, Tag, BufferLength, pBuffer);
        Rc=RC_SUCCESS;
    }
    lark_free_mem(pBuffer);
    return Rc;
}
Rc_t LocalParameterDelete(u32 Tag)
{
    pu8 pTLV_Buffer = NULL;
    pu8 pNode = NULL;
    pu8 pValue = NULL;
    u32 Length = 0;

    pTLV_Buffer = lark_alloc_mem(512);
    CustomParametersGet(pTLV_Buffer, 512);
    ParamFormatCheck(pTLV_Buffer);
    pNode = tlv_find_(pTLV_Buffer, PARAM_TLV_HEAD);
    if(pNode)
    {
        pValue=lark_alloc_mem(512);
        tlv_new_(pValue,0x7F00,tlv_get_l_(pNode),tlv_get_v_(pNode));
        if(tlv_find_(pValue,Tag))
        {
            tlv_delete_(pValue,Tag);
        }
        Memset_(pTLV_Buffer,0x00,512);
        tlv_new_(pTLV_Buffer,PARAM_TLV_HEAD,0,NULL);
        tlv_batch_move_(pValue,pTLV_Buffer);
        lark_free_mem(pValue);
        CustomParametersSet(pTLV_Buffer,512);
    }
    lark_free_mem(pTLV_Buffer);
}
/*--------------------------------------
|   Function Name:
|       TradeLocalParametersSave
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t LocalParametersSave(pu8 pTransBuffer, u32 Tag)
{
    pu8 pNode = NULL;
    Rc_t Rc=RC_SUCCESS;

    pNode = tlv_find_(pTransBuffer, Tag);
    if(!pNode || tlv_get_l_(pNode) == 0)
    {
        Rc=RC_FAIL;
    }
    else
    {
        Rc=ParamTLV_SetData(Tag, tlv_get_v_(pNode), tlv_get_l_(pNode));
    }
    return Rc;
}

/*--------------------------------------
|   Function Name:
|       UpdateKeyDataToTLV
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t UpdateKeyDataToTLV(char * strMsg, char * strKey, u32 Tag, pu8 pTradingFile )
{
    char * pTempBuffer = NULL;

    pTempBuffer = lark_alloc_mem(256);

    Memset_(pTempBuffer, 0x00, 256);

    HttpGetStrKey(strMsg, strKey, pTempBuffer, 256);

    if(Strlen_(pTempBuffer) == 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }
    else
    {
        T_U8_VIEW uvValue={pTempBuffer,Strlen_(pTempBuffer)};
        //tlv_replace_(pTradingFile, Tag, Strlen_(pTempBuffer), (pu8)pTempBuffer);
        if(pTradingFile!=NULL)
        {
            set_tlv_view(pTradingFile,Tag, uvValue);
        }

        lark_free_mem(pTempBuffer);
        return RC_SUCCESS;
    }
}

/*--------------------------------------
|   Function Name:
|       KeyDataCompare
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t KeyDataCompare(char * strMsg, char * strKey,const char * strCompareValue)
{
    char * pTempBuffer = NULL;

    pTempBuffer = lark_alloc_mem(256);

    Memset_(pTempBuffer, 0x00, 256);

    HttpGetStrKey(strMsg, strKey, pTempBuffer, 256);

    if(Strlen_(pTempBuffer) == 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }

    if(Strcmp_(strCompareValue, pTempBuffer) != 0)
    {
        lark_free_mem(pTempBuffer);
        return RC_FAIL;
    }
    else
    {
        lark_free_mem(pTempBuffer);
        return RC_SUCCESS;
    }
}

/*--------------------------------------
|   Function Name:
|       HttpLineStatusCheck
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t HttpLineStatusCheck(pu8 pTradingFile, const char * pHttpBody)
{
    Rc_t Rc = RC_FAIL;
    HttpUnpack_t HttpUnpack;
    Memset_((pu8)(&HttpUnpack), 0x00, sizeof(HttpUnpack));
    HttpUnpack.pResponseBody = (char * )pHttpBody;

    HttpUnpack.Line.pStatus = lark_alloc_mem(64);
    Memset_(HttpUnpack.Line.pStatus, 0x00, 64);

    HttpFrameUnpack(&HttpUnpack);
    Rc=((Strcmp_(HttpUnpack.Line.pStatus,"200")|| Strcmp_(HttpUnpack.Line.pStatus,"206"))?RC_SUCCESS:RC_FAIL);
    lark_free_mem(HttpUnpack.Line.pStatus);
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       HttpFrameTransmit
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static s32 HttpFrameTransmit(pu8 pScheme, pu8 pHost, u32 Port, pu8 pData, u32 DataLength, pu8 pReceivedData, u32 ReceivedDataLengthMax, u32 Timeout )
{
    s32 ReceivedLength = 0;
    s32 Length = 0;

    if(pScheme==NULL || pHost==NULL || pData==NULL)
    {
        return 0;
    }
    if(Strcmp_((char *)pScheme, "https") == 0)
    {
        ReceivedLength = SslTransmit((pu8)pHost, Port, pData, DataLength, pReceivedData, ReceivedDataLengthMax, Timeout);
        if(ReceivedLength > 0)
        {
            if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
            {
                ReceivedLength += Length;
                goto HttpFrameTransmit_end;
            }
            else
            {

            }
        }
        else if(ReceivedLength<0)
        {
            pReceivedData[0] = '\0';
            goto HttpFrameTransmit_end;
        }
        else
        {
            ReceivedLength = Length;
            pReceivedData[0] = '\0';
            goto HttpFrameTransmit_end;
        }

       while(1)
       {
           Length = SslTransmit(NULL, 0, NULL, 0, &pReceivedData[ReceivedLength], ReceivedDataLengthMax - ReceivedLength, ONLINE_PACKET_TIMEOUT);
           if(Length > 0)
           {
               ReceivedLength += Length;
               if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
               {
                   goto HttpFrameTransmit_end;
               }
               else
               {
                   //continue to wait
               }
           }
           else
           {
               ReceivedLength = Length;
               pReceivedData[0] = '\0';
               goto HttpFrameTransmit_end;
           }
       }
   }
   else
   {
       ReceivedLength = SocketTransmit((pu8)pHost, Port, pData, DataLength, pReceivedData, ReceivedDataLengthMax, Timeout);
       if(ReceivedLength > 0)
       {
           if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
           {
               ReceivedLength += Length;
               goto HttpFrameTransmit_end;
           }
           else
           {
               //continue to wait
           }
       }
       else if(ReceivedLength<0)
       {
             pReceivedData[0] = '\0';
             goto HttpFrameTransmit_end;
       }
       else
       {
           goto HttpFrameTransmit_end;
       }

       while(1)
       {
           Length = SocketTransmit(NULL, 0, NULL, 0, &pReceivedData[ReceivedLength], ReceivedDataLengthMax - ReceivedLength, ONLINE_PACKET_TIMEOUT);
           if(Length > 0)
           {
               ReceivedLength += Length;
               if(RequestHttpMessageCheck((char * )pReceivedData,ReceivedLength))
               {
                   goto HttpFrameTransmit_end;
               }
               else
               {
                   //continue to wait
               }
           }
           else
           {
               ReceivedLength = Length;
               pReceivedData[0] = '\0';
               goto HttpFrameTransmit_end;
           }
       }
   }

HttpFrameTransmit_end:
    if(ReceivedLength < 0)
    {
        TRACE(DBG_TRACE_LVL, "ErrCode =%d:\r\n",ReceivedLength);
    }
    return ReceivedLength;
}

/*--------------------------------------
|   Function Name:
|       RequestHttpTransmit
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
s32 RequestHttpTransmit(char * pSend,u32 SendLen, char * pURL,u32 port, char * pReceived, u32 FrameLengthMax )
{
    char * pScheme = NULL;
    char * pHost = NULL;
    s32    Length = 0;
    if(pSend == NULL || pURL == NULL || FrameLengthMax == 0)
    {
        return 0;
    }

    TRACE(DBG_TRACE_LVL, "\r\npSend(%d) : \r\n%s\r\n\r\n",SendLen,pSend);
    //TRACE_VALUE(DBG_TRACE_LVL,pSend,SendLen);

    pScheme = lark_alloc_mem(128);
    pHost = lark_alloc_mem(64);
    Memset_(pScheme, 0x00, 128);
    Memset_(pHost, 0x00,64);

    HttpUrlGetScheme(pURL, pScheme, 128);
    check_param(Strlen_(pScheme) <= 128);

    HttpUrlGetHost(pURL, pHost, 64);
    check_param(Strlen_(pHost) <= 64);
    //request config
    //AUTH_NONE:does not verify Cert
    //AUTH_SERVER_CLIENT:verify client and server cert
    //AUTH_SERVER:verify server cert only
    //AUTH_CLIENT:verify client cert only
    if(HttpServerConnect(pURL,port,pHost,AUTH_NONE)==RC_FAIL)//Test url does not verify cert by default
    {
        goto RequestHttpTransmit_end;
    }
    Length = HttpFrameTransmit((pu8 )pScheme, (pu8 )pHost, port, (pu8)(pSend),SendLen, (pu8)pReceived, FrameLengthMax, ONLINE_REQUEST_TIMEOUT);
    check_param(Strlen_(pReceived) <= FrameLengthMax);
RequestHttpTransmit_end:
    if(pScheme!=NULL)
    {
        Memset_(pScheme, 0x00, 128);
        lark_free_mem(pScheme);
        pScheme=NULL;
    }
    if(pHost!=NULL)
    {
        Memset_(pHost, 0x00,64);
        lark_free_mem(pHost);
        pHost=NULL;
    }
    TRACE(DBG_TRACE_LVL, "\r\nRECV(%d) : \r\n",Length);
    return Length;
}
void SetRequestMode(u32 index)
{
    if(index==0)
    {
        SetDevWifi(COMMON_WIFI);
    }
    else if(index==1)
    {
         SetDevWifi(NO_WIFI);
    }
}
/*--------------------------------------
|   Function Name:
|       lcd_sign
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
static Rc_t lcd_sign(void)
{
    Tp_Pos_t position;
    u16 lastpos[2] = {0xFFFF,0xFFFF};
    u32 size = 0;
    bool loop=true;
    Rc_t Rc=RC_FAIL;
    Key_Num_t keyValue;

    lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH);
    disp_backlight();
    while(loop)
    {
        if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH))
        {
            lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_TP_TOUCH);
            lark_tp_read_map(&position);
            if((position.x>10)&&(position.x<70)&&(position.y>205)&&(position.y<235))//clr
            {
                if(position.event == TP_EVENT_UP)
                {
                    lark_lcd_sign_init(0, 0, 320, 200, 0xFFFFFF);
                }
                lastpos[0] = 0xFFFF;
                continue;
            }
            if((position.x>270)&&(position.x<310)&&(position.y>205)&&(position.y<235))//ok
            {
                if(position.event == TP_EVENT_UP)
                {
                    Rc=RC_SUCCESS;
            		break;
                }
                lastpos[0] = 0xFFFF;
                continue;
            }

            if(position.y>197)//��ʵ����ֵСһ�㣬���������ӿ�����ʱ����ǩ����
            {
                lastpos[0] = 0xFFFF;
                continue;
            }

            if(position.event != TP_EVENT_DOWN)
    		{
    			lastpos[0] = 0xFFFF;
    			continue;
    		}
            if(lastpos[0] == 0xFFFF)
    		{
    			lastpos[0] = position.x;
    			lastpos[1] = position.y;
    		}

            lark_lcd_sign_write(lastpos[0], lastpos[1], position.x, position.y, 2, 0x000000);
    		lastpos[0] = position.x;
    		lastpos[1] = position.y;
    		position.x = position.y = 0;
        }

        if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_KBD_PRESSED))
        {
            lark_flag_clr(OS_FLAG_GROUP_SYS, SYS_EVENT_KBD_PRESSED);

            keyValue = lark_kbd_read_current();
            if(keyValue == KEY_CANCEL)
            {
                lark_lcd_sign_init(0, 0, 320, 200, 0xFFFFFF);
                lastpos[0] = 0xFFFF;
                continue;
            }
            else if(keyValue == KEY_CONFIRM)
            {
                Rc=RC_SUCCESS;
        		break;
            }
            else
            {}
        }
    }
    return Rc;
}

/*--------------------------------------
|   Function Name:
|       Signature_Test
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
void Signature_Test()
{
    Rc_t Rc=RC_FAIL;

    SignatureTransmit();
    lcd_sign_menu();
    Rc=lcd_sign();
    if(Rc==RC_SUCCESS)
    {
        pu8 pSign=lark_alloc_mem(2048);
        Memset_(pSign,0x00,2048);
        u32 len=SignatureCallBack(pSign,2048);
        #ifdef CFG_DBG
        TRACE(DBG_TRACE_LVL,"sign length(%d)\r\n",len);
        TRACE_VALUE(DBG_TRACE_LVL,pSign,len);
        #endif
        lark_lcd_sign_init(0, 200, 320,40, 0xFFFFFF);
        lark_free_mem(pSign);
    }

}
/*--------------------------------------
|   Function Name:
|       ServerReques_test
|   Description:
|   Parameters:
|   Returns:
+-------------------------------------*/
Rc_t ServerReques_test(void)
{
    Rc_t Rc=RC_SUCCESS;
    char * strmsg=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;

    if(lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS) && Get_dev_sim_status())
    {
        #ifdef CFG_REQUEST_MODE
        Rc=RequestSelect();
        #endif
        if(Rc!=RC_SUCCESS)
        {
            return Rc;
        }
    }
    strmsg=lark_alloc_mem(512);
    pRecv=lark_alloc_mem(1024);
    DispHttpRequst();
    RequestPackHttpFrame(NULL,URL_TEST,"POST",strmsg,NULL,512);
    RecvLen=RequestHttpTransmit(strmsg,Strlen_(strmsg),URL_TEST,SERVER_PORT,pRecv,1024);
    lark_free_mem(strmsg);
    #ifdef CFG_DBG
    TRACE(DBG_TRACE_LVL,"recv(%d)\r\n",RecvLen);
    #endif
    if(RecvLen>0)
    {
        TRACE_VALUE(DBG_TRACE_LVL,pRecv,RecvLen);
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        #ifdef CFG_DBG
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        #endif

        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS || Memcmp_(Http_Response.head,"hello",5)==0)
        {
            Rc=RC_SUCCESS;
            #if defined(CFG_SIGNATURE)
            if(Capabilities(CAP_TP)==RC_SUCCESS)
            {
                Signature_Test();//signature interface sample code
            }
            #endif
            #ifdef CFG_DBG
            TRACE(DBG_TRACE_LVL,"request success\r\n");
            #endif
        }
    }
    else
    {
        Rc=RC_FAIL;
    }
    lark_free_mem(pRecv);
    return Rc;
}
/*--------------------------------------
|   Function Name:
|       ParsingTransResponse
|   Description:you need to impletment,
|           POS_TAG_RES_EN_ONLINE_RESULT:tag for transaction result,will transmit to card
|   Returns:
+-------------------------------------*/
static Rc_t ParsingTransResponse(pu8 pResponse,pu8 pTransBuffer)
{
    /*if get approve from server*/
    tlv_replace_(pTransBuffer,POS_TAG_RES_EN_ONLINE_RESULT, 4, (pu8)"\x8A\x02\x30\x30");
    return RC_SUCCESS;
}
/*--------------------------------------
|   Function Name:
|       CardTransaction_test
|   Description:it's sample code for card transaction online request,you need to modify
|                URL_TRANS_TEST:your transaction server URL
|   Returns:
+-------------------------------------*/
Rc_t CardTransaction_test(pu8 pTransBuffer)
{
    char * strmsg=NULL;
    char * pSend=NULL;
    char * pRecv=NULL;
    s32    RecvLen=0;
    u32    HeadLen=0;
    Rc_t Rc=RC_FAIL;
    /*step1:packet your request body*****/
    strmsg=lark_alloc_mem(SERVER_REQUEST_BODY_LENGTH);//SERVER_REQUEST_BODY_LENGTH:request body message length(can be modify according to needed)
    pSend=lark_alloc_mem(SERVER_REQUEST_LENGTH);//SERVER_REQUEST_LENGTH:request message length(request body lenth+request head length)
    //strmsg:request body in specific format(json or other format)
    //pSend:request message(include request body and request head)
    //RequestPackHttpFrame():you can modify request head according to needed

    //URL_TRANS_TEST:your server URL
    //SERVER_PORT:your server port
    RequestPackHttpFrame(strmsg,URL_TRANS_TEST,"POST",pSend,NULL,SERVER_REQUEST_LENGTH);
    lark_free_mem(strmsg);
    /*step2:transmit request to server**/
    pRecv=lark_alloc_mem(SERVER_RECV_LENGTH);//SERVER_RECV_LENGTH:response length(can be modify according to needed)
    RecvLen=RequestHttpTransmit(pSend,Strlen_(pSend),URL_TRANS_TEST,SERVER_PORT,pRecv,SERVER_RECV_LENGTH);
    lark_free_mem(pSend);
    /*step3:recv response and parsing**/
    if(RecvLen>0)
    {
        TRACE_VALUE(DBG_TRACE_LVL,pRecv,RecvLen);
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS)
        {
            Rc=RC_SUCCESS;
            TRACE(DBG_TRACE_LVL,"request success\r\n");
            Rc=ParsingTransResponse(Http_Response.head,pTransBuffer);
        }
    }
    else
    {
        //pls find net error code in proj_sdk.h for detail
        TRACE(DBG_TRACE_LVL," Error:%d\r\n",RecvLen);
    }
    lark_free_mem(pRecv);
    return Rc;
}
#if defined(CFG_TMS_OTA)
void TMSConfigLoad(pu8 pTlv)
{
    LocalParametersLoad(pTlv,TMS_CONFIG_REVISION);
    LocalParametersLoad(pTlv,TMS_CONFIG_HEART);
    LocalParametersLoad(pTlv,TMS_CONFIG_UPDATE);
    LocalParametersLoad(pTlv,TMS_UPDATE_OFFSET);
    LocalParametersLoad(pTlv,TMS_FW_LENGTH);
	LocalParametersLoad(pTlv,TMS_FW_BKPT_DATA);
}

Rc_t TMSConfigUpdate(pu8 pValue,pu8 pTlv)
{
    u32 cnt=0;
    Rc_t Rc=RC_SUCCESS;
    pu8 pTempbuf=lark_alloc_mem(256);
    Memset_(pTempbuf,0x00,256);
    char *pStr=NULL;
    pStr=Strstr_(pValue,"update");
    if(pStr)
    {
        pStr+=Strlen_("update");
        pStr+=2;
        while(*pStr!=',')
        {
            pTempbuf[cnt++]=*pStr;
            pStr++;
        }
        T_U8_VIEW update={pTempbuf,cnt};
        set_tlv_view(pTlv,TMS_CONFIG_UPDATE,update);
    }
    Memset_(pTempbuf,0x00,256);
    cnt=0;

    pStr=Strstr_(pValue,"heart");
    if(pStr)
    {
        pStr+=Strlen_("heart");
        pStr+=2;
        while(*pStr!='}')
        {
            pTempbuf[cnt++]=*pStr;
            pStr++;
        }
        T_U8_VIEW heart={pTempbuf,cnt};
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s-cnt=%d\r\n",__FUNCTION__,cnt);
        TRACE_VALUE(DBG_TRACE_LVL,heart.head,heart.len);
        #endif
        set_tlv_view(pTlv,TMS_CONFIG_HEART,heart);
    }
    Memset_(pTempbuf,0x00,256);
    cnt=0;

    T_U8_VIEW firmware=get_tlv_view(pTlv,TMS_FW_MSG);
    if(UV_OK(firmware))
    {
        pStr=firmware.head;
        while(*pStr!='|')
        {
            pStr++;
        }
        pStr++;
        while(*pStr!='|')
        {
            pTempbuf[cnt++]=*pStr;
            pStr++;
        }
        T_U8_VIEW revision={pTempbuf,cnt};
        T_U8_VIEW Rcd_revision=get_tlv_view(pTlv,TMS_CONFIG_REVISION);
        if(UV_OK(Rcd_revision) && !Memcmp_(Rcd_revision.head,revision.head,revision.len))
        {
            Rc=RC_QUIT;
            #if defined(CFG_REVISON_VERIFY_DISABLE)
            LocalParameterDelete(TMS_CONFIG_REVISION);
            #endif
            #if defined(CFG_OTA_DEBUG)
            TRACE(DBG_TRACE_LVL,"revision same,no need update\r\n");
            #endif
        }
        else
        {
            set_tlv_view(pTlv,TMS_CONFIG_REVISION,revision);
        }

    }

    lark_free_mem(pTempbuf);
    return Rc;

}

static void TmsConfigSave(pu8 pTlv)
{
    LocalParametersSave(pTlv,TMS_CONFIG_UPDATE);
    LocalParametersSave(pTlv,TMS_CONFIG_HEART);
    #if !defined(CFG_REVISON_VERIFY_DISABLE)
    LocalParametersSave(pTlv,TMS_CONFIG_REVISION);
    #endif
}
#if defined(CFG_RCD_DOWNLOAD)
static void BankupDownRcd(pu8 pTlv,u32 offset,u32 FwLen)
{
    if(offset)
    {
        u8 fw_offset[5]={0};
        fw_offset[0]=LONG_HH(offset);
        fw_offset[1]=LONG_HL(offset);
        fw_offset[2]=LONG_LH(offset);
        fw_offset[3]=LONG_LL(offset);

        T_U8_VIEW uvoffset={fw_offset,4};
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s-%d\r\n",__FUNCTION__,__LINE__);
        TRACE_VIEW(DBG_TRACE_LVL,uvoffset);
        #endif
        set_tlv_view(pTlv,TMS_UPDATE_OFFSET,uvoffset);
        LocalParametersSave(pTlv,TMS_UPDATE_OFFSET);
    }

    if(FwLen && (!tlv_find_(pTlv,TMS_FW_LENGTH)))
    {
        u8 fw_length[5]={0};
        fw_length[0]=LONG_HH(FwLen);
        fw_length[1]=LONG_HL(FwLen);
        fw_length[2]=LONG_LH(FwLen);
        fw_length[3]=LONG_LL(FwLen);
        T_U8_VIEW uvlength={fw_length,4};
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"%s-%d\r\n",__FUNCTION__,__LINE__);
        TRACE_VIEW(DBG_TRACE_LVL,uvlength);
        #endif
        set_tlv_view(pTlv,TMS_FW_LENGTH,uvlength);
        LocalParametersSave(pTlv,TMS_FW_LENGTH);
    }

}

static void BankupDownRcdload(pu8 pTlv,pu32 offset,pu32 fwleng)
{
    T_U8_VIEW fw_offset=get_tlv_view(pTlv,TMS_UPDATE_OFFSET);
    if(UV_OK(fw_offset))
    {
        TRACE_VIEW(DBG_TRACE_LVL,fw_offset);
         *offset=BE_PtrToLong(fw_offset.head);
    }

    T_U8_VIEW fw_lenth=get_tlv_view(pTlv,TMS_FW_LENGTH);
    if(UV_OK(fw_lenth))
    {
        TRACE_VIEW(DBG_TRACE_LVL,fw_lenth);
         *fwleng=BE_PtrToLong(fw_lenth.head);
    }
    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"%s  offset=%d  fwlen=%d\r\n",__FUNCTION__,*offset,*fwleng);
    #endif
}
#endif

#if defined(CFG_FIRMWARE_SAVE)
static Rc_t LocalFwSave(pu8 value,u32 len,u32 offset)
{
    u32     RmsgLen=0;
    u32     length=0;
    Rc_t    Rc=RC_SUCCESS;
    u32     cnt=0;

    if(((LOCAL_FIRMWARE_ADD + offset) % 4096) == 0)
    {
        lark_xflash_erase(LOCAL_FIRMWARE_ADD+offset,4096);
    }

    RmsgLen=lark_xflash_write(LOCAL_FIRMWARE_ADD+offset,value,len);
    if(RmsgLen!=len)
    {
       Rc=RC_FAIL;
    }

    return Rc;
}
#endif
/*--------------------------------------
|   Function Name:
|       OTA_Upgrade
|   Description:application upgrader interface
|   Parameters:
|   Returns:
+-------------------------------------*/
void OTA_Upgrade()
{
    T_UPDATE Fw;
    T_OTA dev;
    dev.type=HOST_DEV_OTA;
    dev.start_addr=LOCAL_FIRMWARE_ADD;
    Fw.type=FW_APP;
    Fw.dev=dev;
    Fw.pvalue=NULL;
    while(!Get_dev_UsbConnect_Status())
    {
        LcdClearAll(LCD_NORMAL);
        LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Please Connect USB\nUpdate...");
        WaitEvents(APP_EVENT_TIMEOUT,1000,NULL);
    }
    FirmwareOTA(Fw);
}
static u32 HttpHeadGetStrKey(pu8 psrc,const char * key,pu8 pValue)
{
    char * pNode=NULL;
    u32 idx=0;
    pNode=Strstr_(psrc,key);
    if(pNode)
    {
        pNode+=Strlen_(key);
        pNode+=2;
    }
    if(Strcmp_(key,"Content-Range")==0)
    {
        while(*pNode!='/')
        {
            pNode++;
        }
        pNode++;
    }
    while(*pNode!='\r')
    {
        pValue[idx++]=*pNode;
        pNode++;
    }
    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"key=%s   value=%s\r\n",key,pValue);
    #endif
    return idx;
}
static u32 VerifyFwData(pu8 pvalue,pu8 pTlv,pu32 TotalLen)
{
    bool res=false;
    pu8 pTemp=NULL;
    u32 len=0;

    pTemp=lark_alloc_mem(32);
    Memset_(pTemp,0x00,32);
    len=HttpHeadGetStrKey(pvalue,"ETag",pTemp);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"%s--ETag(%d)\r\n",__FUNCTION__,len);
    TRACE_ASCII(DBG_TRACE_LVL,pTemp,len);
    #endif

    u8 tem[4]={0};
    T_U8_VIEW Fw_crc={tem,0};
    Fw_crc.len=asc_to_bcd(pTemp,len,Fw_crc.head);
    set_tlv_view(pTlv,TMS_FW_CRC,Fw_crc);

    Memset_(pTemp,0x00,32);
    len=HttpHeadGetStrKey(pvalue,"Content-Range",pTemp);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"%s--Range(%d)\r\n",__FUNCTION__,len);
    TRACE_ASCII(DBG_TRACE_LVL,pTemp,len);
    #endif

    *TotalLen=Atoi_(pTemp);
    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"total_length=%d\r\n",Atoi_(pTemp));
    #endif

    #if defined(CFG_RCD_DOWNLOAD)
    BankupDownRcd(pTlv,0,Atoi_(pTemp));
    #endif

    Memset_(pTemp,0x00,32);
    len=HttpHeadGetStrKey(pvalue,"Content-Length",pTemp);

    #if defined(CFG_OTA_DEBUG)
    TRACE(DBG_TRACE_LVL,"%s--Content-Length(%d)\r\n",__FUNCTION__,len);
    TRACE_ASCII(DBG_TRACE_LVL,pTemp,len);
    #endif

    len=Atoi_(pTemp);

    lark_free_mem(pTemp);
    return len;
}

static u32 FormatReqRange(pu8 pRange,u32 offset)
{
    u32 idx=0;
    Memcpy_(&pRange[idx],"bytes=",Strlen_("bytes="));
    idx+=Strlen_("bytes=");
    idx+=Sprintf_(&pRange[idx],"%d",offset);
    pRange[idx++]='-';
    idx+=Sprintf_(&pRange[idx],"%d",offset+CFG_FW_DOWNLOAD_LENGTH-1);
    return idx;
}

s32 HttpFirmwareRequest(pu8 pTlv,pu8 pRange,pu8 pRequest,pu8 pRecv,u32 offset,pu32 Totallen)
{
    s32     RecvLen=0;
    u32     HeadLen=0;
    u32     RmsgLen=0;
    char   * pUrl=NULL;

    T_U8_VIEW Req_url=get_tlv_view(pTlv,TMS_REQ_URL);
    if(UV_OK(Req_url))
    {
        pUrl=lark_alloc_mem(256);
        Memset_(pUrl,0x00,256);
        Memcpy_(pUrl,Req_url.head,Req_url.len);
    }

    FormatReqRange(pRange,offset);
    RequestPackHttpFrame(NULL,pUrl,"GET",pRequest,pRange,512);
    RecvLen=RequestHttpTransmit(pRequest,Strlen_(pRequest),pUrl,TMS_PORT,pRecv,SERVER_RECV_LENGTH);
    if(RecvLen>0)
    {
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"\r\nHttp_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"\r\nResponse(%d)\r\n",Http_Response.len);
        TRACE_VALUE(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        #endif
        if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS)
        {
            RecvLen=VerifyFwData(pRecv,pTlv,Totallen);
            if(RecvLen==Http_Response.len)
            {
                #if defined(CFG_FIRMWARE_SAVE)
                if(LocalFwSave(Http_Response.head,Http_Response.len,offset)==RC_FAIL)
                {
                    #if defined(CFG_OTA_DEBUG)
                    TRACE(DBG_TRACE_LVL,"flash write error\r\n");
                    #endif
                    RecvLen=-1;
                }
                #endif
            }
            else
            {
                TRACE(DBG_TRACE_LVL,"Recv Wrong Data length\r\n");
                RecvLen=-1;
            }

        }
        else
        {
            #if defined(CFG_OTA_DEBUG)
            TRACE(DBG_TRACE_LVL,"server request fail\r\n");
            #endif
            RecvLen=-2;
        }

    }
    else{}
    lark_free_mem(pUrl);
    return RecvLen;
}
static Rc_t checkRepo(pu8 pValue)
{
    return (Strstr_(pValue,"path")?RC_SUCCESS:RC_FAIL);
}
Rc_t GetTmsToken(pu8 pTlv)
{
    Rc_t Rc=RC_FAIL;
    s32 RecvLen=0;
    u32 HeadLen=0;
    char * pRecv=NULL;
    char * pRequest=NULL;
    char * pUrl=NULL;
    T_U8_VIEW Req_url=get_tlv_view(pTlv,TMS_REQ_URL);
    if(UV_OK(Req_url))
    {
        pUrl=lark_alloc_mem(256);
        Memset_(pUrl,0x00,256);
        Memcpy_(pUrl,Req_url.head,Req_url.len);
    }

    DispHttpRequst();
    pRequest=lark_alloc_mem(512);
    pRecv=lark_alloc_mem(1024);
    RequestPackHttpFrame(NULL,pUrl,"GET",pRequest,NULL,512);
    RecvLen=RequestHttpTransmit(pRequest,Strlen_(pRequest),pUrl,TMS_PORT,pRecv,1024);
    lark_free_mem(pRequest);
    lark_free_mem(pUrl);
    if(RecvLen>0)
    {
        HeadLen=GetHttpHeadFramLen((char*)pRecv);
        T_U8_VIEW Http_head={pRecv,HeadLen};
        T_U8_VIEW Http_Response={pRecv+HeadLen,RecvLen-HeadLen};
        //#if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"Http_head(%d)\r\n",Http_head.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_head.head,Http_head.len);
        TRACE(DBG_TRACE_LVL,"Response(%d)\r\n",Http_Response.len);
        TRACE_ASCII(DBG_TRACE_LVL,Http_Response.head,Http_Response.len);
        //#endif

        if(checkRepo(Http_Response.head)==RC_FAIL)
        {
            Rc=RC_DOWN;
        }
        else
        {
            if(HttpLineStatusCheck(NULL,Http_head.head)==RC_SUCCESS &&
            KeyDataCompare(Http_Response.head,"msg","successfully")==RC_SUCCESS)
            {
                Rc=RC_SUCCESS;
                tlv_delete_(pTlv,TMS_REQ_URL);
                UpdateKeyDataToTLV(Http_Response.head,"path",TMS_FW_PATH,pTlv);
                UpdateKeyDataToTLV(Http_Response.head,"firmware",TMS_FW_MSG,pTlv);
                UpdateKeyDataToTLV(Http_Response.head,"terminal",TMS_DEV,pTlv);
                Rc=TMSConfigUpdate(Http_Response.head,pTlv);
            }
            else
            {
                UpdateKeyDataToTLV(Http_Response.head,"msg",TMS_FW_MSG,pTlv);
            }
        }
    }
    lark_free_mem(pRecv);
    return Rc;
}

static void LocalFirmwareUpdate()
{
    LcdClearAll(LCD_NORMAL);
    LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Firmware verify...");
    if(FirmwareVerify(LOCAL_FIRMWARE_ADD)==RC_SUCCESS)
    {
        LcdClearAll(LCD_NORMAL);
        LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Verify Success\nUpgrader...");
        OTA_Upgrade();
    }
    else
    {
        LocalParameterDelete(TMS_FW_LENGTH);
        LocalParameterDelete(TMS_UPDATE_OFFSET);
        dispStrWait(LCD_CENTERED_ALIGNMENT,"Firmware Verify Fail\nplease download again",3);
    }

}
Rc_t ServerReqFirmware(pu8 pTlv)
{
    char * pRequest=NULL;
    char * pRecv=NULL;
    char *pRange=NULL;

    Rc_t    Rc=RC_FAIL;
    u32     offset=0;
    u32     Filelen=0;
    u32     Rcd_Fw_len=0;
    u32     Total_Cnt=0;
    u32     Count=0;
    u32     Content_len=0;
    u32     progress=0;
    u32     Fwlen=0;
    s32     length=0;
    u32     Event=0;

    #if defined(CFG_RCD_DOWNLOAD)
    BankupDownRcdload(pTlv,&offset,&Rcd_Fw_len);
    #endif
    pRequest=lark_alloc_mem(512);
    pRecv=lark_alloc_mem(SERVER_RECV_LENGTH);
    pRange=lark_alloc_mem(64);

    T_U8_VIEW Req_url=get_tlv_view(pTlv,TMS_REQ_URL);
    if(UV_OK(Req_url))
    {
        Progress_Init("Firmware Download...");
        while(true)
        {
            Memset_(pRange,0x00,64);
            Memset_(pRequest,0x00,512);
            Memset_(pRecv,0x00,SERVER_RECV_LENGTH);
            #if defined(CFG_OTA_DEBUG)
            TRACE(DBG_TRACE_LVL,"offset=%d  Content_len=%d Filelen=%d\r\n",offset,Content_len,Filelen);
            #endif
            if(Rcd_Fw_len)
            {
                Count=offset/CFG_FW_DOWNLOAD_LENGTH+1;
                Content_len=offset;
            }
            length=HttpFirmwareRequest(pTlv,pRange,pRequest,pRecv,offset,&Filelen);
            if(length<0)
            {
                LcdClearAll(LCD_NORMAL);
                #if defined(CFG_OTA_DEBUG)
                TRACE(DBG_TRACE_LVL,"recv Abnormal exit ...\r\n");
                #endif
                #if defined(CFG_RCD_DOWNLOAD)
                BankupDownRcd(pTlv,offset,0);
                #endif
                break;
            }
            else if(length==0)
            {
                if(identify_wifi_dev() == COMMON_WIFI && lark_flag_is_true(OS_FLAG_GROUP_SYS, SYS_EVENT_WIFI_AP_STATUS))
                {
                    disconnect_wifi_des();
                }
                #if defined(CFG_OTA_DEBUG)
                TRACE(DBG_TRACE_LVL,"no data recv,request again\r\n");
                #endif
                continue;
            }

            if(Rcd_Fw_len && Filelen && Rcd_Fw_len!=Filelen)
            {
                LcdClearAll(LCD_NORMAL);
                LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"pls check network\nota again");
                #if defined(CFG_OTA_DEBUG)
                TRACE(DBG_TRACE_LVL,"Firmware differ from Rcd firmware\r\n");
                #endif
                LocalParameterDelete(TMS_FW_LENGTH);
                LocalParameterDelete(TMS_UPDATE_OFFSET);
                break;
            }

            Content_len+=length;
            SetProgress(Count,Content_len,CFG_FW_DOWNLOAD_LENGTH,Filelen);
            #if defined(CFG_OTA_DEBUG)
            TRACE(DBG_TRACE_LVL,"<%s>length=%d content_length=%d FileLen=%d\r\n",__FUNCTION__,length,Content_len,Filelen);
            #endif
            if(Content_len== Filelen)
            {
                #if defined(CFG_OTA_DEBUG)
                TRACE(DBG_TRACE_LVL,"firmware download finish\r\n");
                #endif
                TmsConfigSave(pTlv);
                LocalParameterDelete(TMS_FW_LENGTH);
                LocalParameterDelete(TMS_UPDATE_OFFSET);
                Rc=RC_DOWN;
                break;

            }

            if(Filelen>0 && offset<Filelen)
            {
                offset+=(Filelen-offset>=CFG_FW_DOWNLOAD_LENGTH?CFG_FW_DOWNLOAD_LENGTH:(Filelen-offset));
                Count++;
            }

        }
    }
    else
    {
        #if defined(CFG_OTA_DEBUG)
        TRACE(DBG_TRACE_LVL,"URL not find\r\n");
        #endif
    }

    lark_free_mem(pRange);
    lark_free_mem(pRequest);
    lark_free_mem(pRecv);

    if(Rc==RC_DOWN)
    {
        LcdClearAll(LCD_NORMAL);
        LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Fw Ddownload success\nupdate...");
        Event=WaitEvents(APP_EVENT_USER_CONFIRM|APP_EVENT_USER_CANCEL|APP_EVENT_TIMEOUT,10000,NULL);
        switch(Event)
        {
            case APP_EVENT_USER_CONFIRM:
            case APP_EVENT_TIMEOUT:
                LocalFirmwareUpdate();
                break;
            case APP_EVENT_USER_CANCEL:
                Rc=RC_QUIT;
                break;
        }
    }
    return Rc;
}

//static bool s_isNewVer = 0;
T_BLOCK_HEAD *parseDownload(char *strRespBody, char *arrRev)
{
	char strTmp[50] = {0};
	T_BLOCK_HEAD blk = {0, 0, 0, 0, 0};

	if(!HttpUnpackGetLineStatus(strRespBody, strTmp, sizeof(strTmp)))
	{
		TRACE(TMS_TRACE_LVL, "\r\n<%s>CANNOT parse line status: %s\r\n", __FUNCTION__, strRespBody);
		return NULL;
	}
	blk.wStatus = (u16)str2num(strTmp, Strlen_(strTmp));
	Memset_(strTmp, 0, sizeof(strTmp));
	HttpUnpackGetHeadParam(strRespBody, "Content-Length", strTmp, sizeof(strTmp));
	blk.dwLenBlk = str2num(strTmp, Strlen_(strTmp));
	Memset_(strTmp, 0, sizeof(strTmp));
	HttpUnpackGetHeadParam(strRespBody, "revision", strTmp, sizeof(strTmp));
	if(!strTmp[0] || !Strstr_(arrRev, strTmp))
	{
		TRACE(TMS_TRACE_LVL, "\r\n<%s>Error revision replied: %s -- %s\r\n",
			__FUNCTION__, strTmp?"NULL":strTmp, arrRev);
		return NULL;
	}
	Memset_(strTmp, 0, sizeof(strTmp));
	HttpUnpackGetHeadParam(strRespBody, "ETag", strTmp, sizeof(strTmp));
	trim(strTmp, '"');
	T_U8_VIEW uvETagAsc = {(u8 *)strTmp, Strlen_(strTmp)}, uvETagBin = {(u8 *)strTmp + 40, sizeof(strTmp) - 40};
	uv2bin(uvETagBin.head, uvETagAsc, PAD_0_LEFT);
	blk.dwETag = PTR2DWORD(uvETagBin.head);
	Memset_(strTmp, 0, sizeof(strTmp));
	HttpUnpackGetHeadParam(strRespBody, "Content-Range", strTmp, sizeof(strTmp));
	char *pszCur = Strstr_(strTmp, "/");
	if(!pszCur)
	{
		TRACE(TMS_TRACE_LVL, "<%s>No / found: %s\r\n", __FUNCTION__, strTmp);
		return NULL;
	}
	if(blk.wStatus == 200 && pszCur[-1] == '*')
	{
		blk.dwLenBlk = 0;
		TRACE(TMS_TRACE_LVL, "<%s>END reached, reset blk.dwLenBlk = 0\r\n", __FUNCTION__);
	}
	pszCur += 1;
	blk.dwLenFile = str2num(pszCur, Strlen_(pszCur)); //0-999/474886
	pszCur = Strstr_(strRespBody, "\r\n\r\n");
	if(!pszCur)
	{
		TRACE(TMS_TRACE_LVL, "<%s>HTTP Head has no END\r\n", __FUNCTION__);
		return NULL;
	}
	pszCur += 4;
    if(strRespBody + sizeof(T_BLOCK_HEAD) < pszCur)
    {
		T_BLOCK_HEAD *pBlk = (T_BLOCK_HEAD *)(pszCur - sizeof(T_BLOCK_HEAD));
		*pBlk = blk;
		TRACE(TMS_TRACE_LVL, "BLK info dwLenBlk: %d, dwLenFile: %d, wStatus: %d, etag: %08XH\r\n",
			pBlk->dwLenBlk, pBlk->dwLenFile, pBlk->wStatus, pBlk->dwETag);
		return pBlk;
    }
	TRACE(TMS_TRACE_LVL, "<%s>strRespBody ERROR\r\n", __FUNCTION__);
	return NULL;
}

Rc_t upgrade_fw(void)
{
	TRACE(TMS_TRACE_LVL, "<%s>: KICK OFF.....\r\n", __FUNCTION__);
	//LcdClearAll(LCD_NORMAL);
	//LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Firmware verify...");
	if(FirmwareVerify(LOCAL_FIRMWARE_ADD) == RC_SUCCESS)
	{
		TRACE(TMS_TRACE_LVL, "<%s>: FW Verified OK.....\r\n", __FUNCTION__);
		//LcdClearAll(LCD_NORMAL);
		//LcdPrint(LCD_NORMAL,LCD_LINE_0,LCD_CENTERED_ALIGNMENT, CH_HEIGHT,"Verify Success\nUpgrader...");
		OTA_Upgrade();
	}
	else
	{
		TRACE(TMS_TRACE_LVL, "<%s>: FW Verified BAD.....\r\n", __FUNCTION__);
		LocalParameterDelete(TMS_FW_LENGTH);
		LocalParameterDelete(TMS_UPDATE_OFFSET);
		dispStrWait(LCD_CENTERED_ALIGNMENT,"Firmware Verify Fail\nplease download again",3);
	}
	return RC_FAIL;
}

u8* down_bin_block(u8 *pTlv, bool isCut, T_BLOCK_HEAD *pPatch, u8 *pRecvBuf)
{
	T_U8_VIEW uvPath = get_tlv_view(pTlv, TMS_REQ_URL);
	char *szPath = (char *)uvPath.head;
	T_URL_INFO urlInfo;
	parse_url(&urlInfo, szPath);
	urlInfo.eProtocol = HTTP;

	char strReqBody[256] = "GET /", *strRevision = strReqBody;
	Memcpy_(strReqBody + Strlen_(strReqBody), urlInfo.uvPath.head, urlInfo.uvPath.len);
	if(!isCut)
	{
		char *pCur = Strstr_(strReqBody, "&cut=1");
		if(pCur) pCur[5] = '0';
	}
	
	Sprintf_(strReqBody + Strlen_(strReqBody),
		" HTTP/1.1\r\nAccept: */*\r\nConnection: Keep-Alive\r\nRange: bytes=%d-%d\r\nHost: ",
		pPatch->dwOffset, pPatch->dwOffset + UPG_BLOCK_SIZE(isCut) - 1);
	Memcpy_(strReqBody + Strlen_(strReqBody), urlInfo.uvSite.head, urlInfo.uvSite.len);
	Sprintf_(strReqBody + Strlen_(strReqBody), ":%d\r\n\r\n", urlInfo.wPort);
	u32 dwLen = Strlen_(strReqBody);
	TRACE(TMS_TRACE_LVL, "\r\nTMS Req(%d)....\r\n%s\r\n", dwLen, strReqBody);

	int i = 0, iLenResp;
	do{
		if(i) TRACE(DBG_TRACE_LVL, "WiFiSocket Retrying %d\r\n", i);
		Memset_(pRecvBuf, 0, HTTP_DOWN_LENGTH_MAX);
		iLenResp = WiFiSocket(&urlInfo, (T_U8_VIEW ){(u8 *)strReqBody, dwLen}, (T_U8_VIEW ){pRecvBuf, HTTP_DOWN_LENGTH_MAX});
		i = ((iLenResp == NET_ERROR_WIFI_DES_WRITE_DATA||iLenResp < -100) ? (i + 1) : 0);
		TRACE(DBG_TRACE_LVL, "\r\nPatch Download RESP(%d bytes), i = %d, %s", iLenResp, i, i?"Retry...\r\n":"Pass, ");
	}while(i <= 3 && i > 0);

	int iLenHead = (iLenResp > 0 ? (Strstr_((char *)pRecvBuf, "Keep-Alive") - (char *)pRecvBuf + Strlen_("Keep-Alive")) : 0);
	TRACE_INFO(TMS_TRACE_LVL, pRecvBuf, iLenHead > 0 ? iLenHead : 20, "%c", "HEAD......\r\n", "\r\n\r\n");
	if(iLenResp <= 0 || i >= 3)
	{
		TRACE(DBG_TRACE_LVL, "WiFiSocket Retrying quit(%d)\r\n", i);
		return NULL;
	}

	T_U8_VIEW uvRevision = get_tlv_view(pTlv, TMS_CONFIG_REVISION);
	Memcpy_(strRevision, uvRevision.head, uvRevision.len);
	strRevision[uvRevision.len] = 0;

	T_BLOCK_HEAD *pBlk = parseDownload((char *)pRecvBuf, strRevision);
	if(!pBlk) return NULL;

	pPatch->dwLenBlk = pBlk->dwLenBlk;
	T_U8_VIEW uvRespData = {(u8 *)pBlk + sizeof(T_BLOCK_HEAD), pBlk->dwLenBlk};
	switch(pBlk->wStatus)
	{
		case 206:
		break;
		default: //200, 301, 404, 416, 417
			TRACE_INFO(TMS_TRACE_LVL, uvRespData.head, uvRespData.len, "%c", "Response TXT ---- ");
			TRACE(TMS_TRACE_LVL, "Detected %s, offset = %08x vs lenFile = %08x, saved ETag = %08x\r\n",	pBlk->dwLenBlk?"ERR":"EOF",
				pPatch->dwOffset, pPatch->dwLenFile, pPatch->dwETag);
			return NULL;
	}

	TRACE_INFO(TMS_TRACE_LVL, uvRespData.head, uvRespData.len > 0x10 ? 0x10 : uvRespData.len, "%02X ", "Response Param ---- ");

	if(pPatch->dwLenFile && pPatch->dwLenFile != pBlk->dwLenFile)
	{
		TRACE(TMS_TRACE_LVL, "File length NOT match %d -- %d, reset download......\r\n", pPatch->dwLenFile, pBlk->dwLenFile);
		pBlk->wStatus = 0;
		return NULL;
	}

	if(!pPatch->dwETag) pPatch->dwETag = pBlk->dwETag;
	if(!pBlk->dwOffset) pPatch->dwLenFile = pBlk->dwLenFile;
	if(pBlk->dwETag && pPatch->dwETag != pBlk->dwETag)
	{
		TRACE(TMS_TRACE_LVL, "ETag NOT match %XH -- %XH, reset download......\r\n", pPatch->dwETag, pBlk->dwETag);
		pBlk->wStatus = 0;
		//pPatch->dwOffset = pPatch->dwLenFile = 0;
		//pPatch->dwETag = pBlk->dwETag;
		return NULL;
	}
	return uvRespData.head;
}


static T_BLOCK_HEAD* breakpoint(u8 *pTlv, T_BLOCK_HEAD *pBlkInfo, Rw_t rw)
{
	typedef struct{
		T_BLOCK_HEAD data;
		u32 dwChkSum;
	} T_BKPT;
	
	if(!pTlv && rw == RW_WRITE && !pBlkInfo)
	{
		TRACE(TMS_TRACE_LVL, "<%s>CLEAN download breakpoint...\r\n", __FUNCTION__);
		LocalParameterDelete(TMS_FW_BKPT_DATA);
		return pBlkInfo;
	}
	
	T_U8_VIEW uvTmp = {NULL, 0};
	switch(rw)
	{
		case RW_READ:
			uvTmp = get_tlv_view(pTlv, TMS_FW_BKPT_DATA);
			TRACE_VIEW(TMS_TRACE_LVL, uvTmp);
			if(UV_OK(uvTmp))
			{
				T_BKPT *pBreak = (T_BKPT *)uvTmp.head;
				u32 dwChkSum = crc32bzip2(0, (u8 *)uvTmp.head, sizeof(T_BLOCK_HEAD));
				TRACE(TMS_TRACE_LVL, "<%s>off: %d, len: %d, eTag: %XH, checksum: %XH(%XH)\r\n", __FUNCTION__, 
					pBreak->data.dwOffset, pBreak->data.dwLenFile, pBreak->data.dwETag, pBreak->dwChkSum, dwChkSum);
				if(pBreak->dwChkSum != dwChkSum)
				{
					TRACE(TMS_TRACE_LVL, "<%s>dwChkSum NOT match, reset breakpoint...\r\n", __FUNCTION__);
					LocalParameterDelete(TMS_FW_BKPT_DATA);
					uvTmp = (T_U8_VIEW){NULL, 0};
				}
				else
				{
					TRACE(TMS_TRACE_LVL, "<%s>breakpoint OK\r\n", __FUNCTION__);
					*pBlkInfo = pBreak->data;
				}
			}
		break;
		case RW_WRITE:
			if(pBlkInfo->dwOffset)
			{
				T_BKPT bkpt = {*pBlkInfo, crc32bzip2(0, (u8 *)pBlkInfo, sizeof(T_BLOCK_HEAD))};
				uvTmp = (T_U8_VIEW ){(u8 *)&bkpt, sizeof(T_BKPT)};
				set_tlv_view(pTlv, TMS_FW_BKPT_DATA, uvTmp);
				LocalParametersSave(pTlv, TMS_FW_BKPT_DATA);
			}
			else
				TRACE(TMS_TRACE_LVL, "<%s>ERROR: 0 offset needn't record breakpoint\r\n", __FUNCTION__);
		break;
		default:
		break;
	}
	return (T_BLOCK_HEAD *)uvTmp.head;
}

static u32 breakpoint_crc(u32 dwOffset)
{
	u8 *pRecvBuf = lark_alloc_mem(HTTP_DOWN_LENGTH_MAX);
	
	u32 i = 0, dwCrcFile = 0;
	while(i < dwOffset)
	{
		Memset_(pRecvBuf, 0, HTTP_DOWN_LENGTH_MAX);
		u32 dwLen = ((dwOffset - i) >= HTTP_DOWN_LENGTH_MAX ? HTTP_DOWN_LENGTH_MAX : (dwOffset - i));
		lark_xflash_read(LOCAL_FIRMWARE_ADD + i, pRecvBuf, dwLen);
		dwCrcFile = crc32bzip2(dwCrcFile, pRecvBuf, dwLen);
		i += dwLen;
	}
	lark_free_mem(pRecvBuf);
	return dwCrcFile;
}

Rc_t req_patch_wifi(pu8 pTlv)
{
	u8 arrTime[16] = {0};
	lark_rtc_read(arrTime);
	TRACE(DBG_TRACE_LVL, "%s, Patch download starting...4\r\n", arrTime);
	T_BLOCK_HEAD patch = {0,0,0,0,0};
	u32 dwCrcFile = 0, dwCount = 1;
#if defined(CFG_RCD_DOWNLOAD)
	if(breakpoint(pTlv, &patch, RW_READ))
	{
		if(patch.dwOffset >= patch.dwLenFile)
		{
			TRACE(TMS_TRACE_LVL, "INVALID breakpoint : %u/%u, rest...\r\n", patch.dwOffset, patch.dwLenFile);
			patch = (T_BLOCK_HEAD){0,0,0,0,0};
		}
		
		//re-calc crc for the patial file downloaded
		if(patch.dwOffset)
		{
			dwCrcFile = breakpoint_crc(patch.dwOffset);
			dwCount = patch.dwOffset/patch.dwLenBlk + 1;
		}
		TRACE(TMS_TRACE_LVL, "Load download point: %u/%u, crc %XH, block NO %u", patch.dwOffset, patch.dwLenFile, dwCrcFile, dwCount);
	}
	else
		TRACE(TMS_TRACE_LVL, "Load download point NULL");
#endif
	Rc_t result = RC_FAIL;
	bool isCut = false;
	u8 *pRecvBuf = lark_alloc_mem(HTTP_DOWN_LENGTH_MAX);
	
	Progress_Init("Firmware Download...");
	do{
		u8 *pContent = down_bin_block(pTlv, isCut, &patch, pRecvBuf);
		isCut = true;
		if(!pContent )
		{
			if(!patch.wStatus) breakpoint(NULL, NULL, RW_WRITE);
			break;
		}
		TRACE(TMS_TRACE_LVL, "BLOCK *** %d / %d ***, ", dwCount + 1,
			dwCount + (patch.dwLenFile - patch.dwOffset + patch.dwLenBlk - 1)/patch.dwLenBlk);
		dwCrcFile = crc32bzip2(dwCrcFile, pContent, patch.dwLenBlk);
		if(lark_xflash_write(LOCAL_FIRMWARE_ADD + patch.dwOffset, pContent, patch.dwLenBlk) != patch.dwLenBlk)
		{
			TRACE(TMS_TRACE_LVL, "Flash write ERROR!\r\n");
			break;
		}
		patch.dwOffset += patch.dwLenBlk;
#if defined(CFG_RCD_DOWNLOAD)
		breakpoint(pTlv, &patch, RW_WRITE);		
		TRACE(TMS_TRACE_LVL, "Save download point: (%u/%u, crc %XH)", patch.dwOffset, patch.dwLenFile, patch.dwETag);
#endif
		if(patch.dwOffset == patch.dwLenFile) result = RC_SUCCESS;
		TRACE(TMS_TRACE_LVL, "Flash write %dB, offset %d / %d, crc %08x --> eTag %08x, %s\r\n",
			patch.dwLenBlk, patch.dwOffset, patch.dwLenFile, dwCrcFile, patch.dwETag,
			result == RC_SUCCESS ? "***END***":"TO BE CONTINUED... ");
		SetProgress(dwCount++, patch.dwOffset, patch.dwLenBlk, patch.dwLenFile);
	}while(result != RC_SUCCESS);
	lark_free_mem(pRecvBuf);
	
	if(result == RC_SUCCESS)
	{
		result = (dwCrcFile == patch.dwETag ? RC_SUCCESS : RC_FAIL);
		TRACE(DBG_TRACE_LVL, "%s, Patch download started...\r\n", arrTime);
		lark_rtc_read(arrTime);
		TRACE(DBG_TRACE_LVL, "%s, Patch verified to %s\r\n", arrTime, result == RC_SUCCESS ? "OK":"BAD");
	}
	if(result == RC_SUCCESS)
	{
		TmsConfigSave(pTlv);
		LocalParameterDelete(TMS_FW_LENGTH);
		LocalParameterDelete(TMS_UPDATE_OFFSET);
		LocalParameterDelete(TMS_FW_BKPT_DATA);
		upgrade_fw();
	}

	return result;
}
#endif

void LoadVersion(pu8 pTlv)
{
    if(LocalParametersLoad(pTlv,FOTA_VERSION)!=RC_SUCCESS)
    {
        pu8 pVer=lark_alloc_mem(64);
        u32 len=0;
        Memset_(pVer,0x00,64);
        len=GetCellularVersion(pVer);
        T_U8_VIEW Ver={pVer,len};
        set_tlv_view(pTlv,FOTA_VERSION,Ver);
        LocalParametersSave(pTlv,FOTA_VERSION);
        lark_free_mem(pVer);
    }
}
Rc_t CheckVersion()
{
    Rc_t Rc=RC_FAIL;
    u32 Event=0;

    pu8 pTlvBuffer=lark_alloc_mem(512);
    tlv_new_(pTlvBuffer, PARAM_TLV_HEAD, 0, NULL);

    if(LocalParametersLoad(pTlvBuffer,FOTA_VERSION)==RC_SUCCESS)
    {
        pu8 pVer=lark_alloc_mem(64);
        u32 len=0;
        T_U8_VIEW Ver=get_tlv_view(pTlvBuffer,FOTA_VERSION);
        if(UV_OK(Ver))
        {
            while(1)
            {
                Memset_(pVer,0x00,64);
                len=GetCellularVersion(pVer);
                if(Ver.len==len && Memcmp_(pVer,Ver.head,Ver.len)!=0)
                {
                    #if defined(CFG_OTA_DEBUG)
                    TRACE(DBG_TRACE_LVL,"update Fota ver[%s]-[%s]\r\n",Ver.head,pVer);
                    #endif
                    T_U8_VIEW Up_ver={pVer,len};
                    set_tlv_view(pTlvBuffer,FOTA_VERSION,Up_ver);
                    LocalParametersSave(pTlvBuffer,FOTA_VERSION);
                    Rc=RC_SUCCESS;
                    break;
                }
                Event=WaitEvents(APP_EVENT_TIMEOUT|APP_EVENT_USER_CANCEL,5000,NULL);
                if(Event==APP_EVENT_USER_CANCEL)
                {
                    break;
                }
            }
        }
        lark_free_mem(pVer);

    }
    lark_free_mem(pTlvBuffer);
    return Rc;
}